//
//  Event.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/12/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event : NSObject

@property(nonatomic,retain) NSString *strEventID;
@property(nonatomic,retain) NSString *strEventName;
@property(nonatomic,retain) NSString *strEventDescription;
@property(nonatomic,retain) NSString *strEventDate;
@property(nonatomic,retain) NSString *strEventLocation;
@property(nonatomic,retain) NSString *strEventVenueAddress;
@property(nonatomic,assign) BOOL boolIsEventLive;

@end
