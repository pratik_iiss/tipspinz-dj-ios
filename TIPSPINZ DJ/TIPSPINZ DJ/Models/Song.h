//
//  Song.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 30/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Song : NSObject

@property(nonatomic,retain) NSString *strSongID;
@property(nonatomic,retain) NSString *strSongName;
@property(nonatomic,retain) NSString *strSongArtistName;

@end
