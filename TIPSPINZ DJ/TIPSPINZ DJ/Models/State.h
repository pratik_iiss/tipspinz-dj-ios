//
//  State.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 28/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface State : NSObject

@property(nonatomic,retain) NSString *strStateID;
@property(nonatomic,retain) NSString *strStateName;

@property(nonatomic,retain) NSMutableArray *arrayCity;

@end
