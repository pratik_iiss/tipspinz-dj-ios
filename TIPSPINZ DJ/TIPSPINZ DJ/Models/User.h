//
//  User.h
//  HungerE
//
//  Created by Pratik Gujarati on 23/09/16.
//  Copyright © 2016 accereteinfotech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CommonUtility.h"

@interface User : NSObject

@property(nonatomic,retain) NSString *strValidationMessage;

@property(nonatomic,retain) NSString *strUserID;
@property(nonatomic,retain) NSString *strUserType;//1 = Dj, 0 = Club Owner
@property(nonatomic,retain) NSString *strEmail;
@property(nonatomic,retain) NSString *strPassword;
@property(nonatomic,retain) NSString *strFullname;
@property(nonatomic,retain) NSString *strGender;
@property(nonatomic,retain) NSString *strPhoneNumber;
@property(nonatomic,retain) NSString *strStateId;
@property(nonatomic,retain) NSString *strStateName;
@property(nonatomic,retain) NSString *strCityId;
@property(nonatomic,retain) NSString *strCityName;

@property(nonatomic,assign) BOOL boolIsOnline;
@property(nonatomic,retain) NSString *strOnlineClubId;
@property(nonatomic,retain) NSString *strOnlineClubName;
@property(nonatomic,retain) NSString *strOnlinePlaylistId;
@property(nonatomic,retain) NSString *strOnlinePlaylistName;
@property(nonatomic,assign) BOOL  boolIsOnlineClubSetWithPresetPlaylist;

@property(nonatomic,retain) NSString *strOnlineManagerId;
@property(nonatomic,retain) NSString *strOnlineManagerName;
@property(nonatomic,assign) BOOL  boolIsOnlineManagerSetWithPresetPlaylist;

@property(nonatomic,assign) BOOL boolIsOnlineAtEvent;
@property(nonatomic,retain) NSString *strOnlineEventId;
@property(nonatomic,retain) NSString *strOnlineEventName;
@property(nonatomic,retain) NSString *strOnlineEventPlaylistName;
@property(nonatomic,retain) NSString *strOnlineEventPlaylistId;

@property(nonatomic,retain) NSString *strProfilePictureImageUrl;

@property(nonatomic,retain) NSMutableArray *arrayDjApprovedClubIds;
@property(nonatomic,retain) NSMutableArray *arrayDjApprovedClubs;
@property(nonatomic,retain) NSMutableArray *arrayDjPlaylists;

@property(nonatomic,retain) NSMutableArray *arrayArtistApprovedManagerIds;
@property(nonatomic,retain) NSMutableArray *arrayArtistApprovedManagers;
@property(nonatomic,retain) NSMutableArray *arrayArtistPlaylists;

@property(nonatomic,retain) NSString *strDeviceToken;

@property(nonatomic,retain) NSString *strFacebookId;
@property(nonatomic,retain) NSString *strFacebookAccessToken;

@property(nonatomic,retain) NSString *strFacebookProfileUrl;
@property(nonatomic,retain) NSString *strTwitterProfileUrl;
@property(nonatomic,retain) NSString *strInstagramProfileUrl;
@property(nonatomic,retain) NSString *strGooglePlusProfileUrl;

@property(nonatomic,retain) NSString *strTwitterInstagramMesasage;

@property(nonatomic,retain) NSMutableArray *arrayClubOwnerPlaylists;
@property(nonatomic,retain) NSMutableArray *arrayCompanyPlaylists;

-(BOOL)isValidateUserForLogin;
-(BOOL)isValidateUserForRegistration;

@end
