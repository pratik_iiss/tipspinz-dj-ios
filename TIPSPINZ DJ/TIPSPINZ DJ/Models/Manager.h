//
//  Manager.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 28/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Manager : NSObject

@property(nonatomic,retain) NSString *strManagerID;
@property(nonatomic,retain) NSString *strManagerName;
@property(nonatomic,retain) NSString *strManagerType;
@property(nonatomic,retain) NSString *strManagerAddress;
@property(nonatomic,retain) NSString *strManagerImageUrl;
@property(nonatomic,retain) NSString *strManagerBannerImageUrl;
@property(nonatomic,retain) NSString *strManagerStateID;
@property(nonatomic,retain) NSString *strManagerCityID;

@property(nonatomic,retain) NSString *strManagerPlaylistID;
@property(nonatomic,retain) NSString *strManagerPlaylistName;

@end
