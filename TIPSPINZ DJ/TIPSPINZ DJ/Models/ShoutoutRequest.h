//
//  ShoutoutRequest.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShoutoutRequest : NSObject

@property(nonatomic,retain) NSString *strShoutoutRequestID;
@property(nonatomic,retain) NSString *strShoutoutRequestOccasion;
@property(nonatomic,retain) NSString *strShoutoutRequestForWhom;
@property(nonatomic,retain) NSString *strShoutoutRequestNoteComments;
@property(nonatomic,retain) NSString *strShoutoutRequestTime;
@property(nonatomic,assign) BOOL boolIsShoutoutRequestVIP;
@property(nonatomic,retain) NSString *strShoutoutRequestStatus;
@property(nonatomic,retain) NSString *strShoutoutRequestAmount;
@property(nonatomic,retain) NSString *strShoutoutRequestDjAmount;
@property(nonatomic,assign) BOOL boolIsShoutoutRequestExpired;

@property(nonatomic,retain) NSString *strShoutoutRequestDjName;
@property(nonatomic,retain) NSString *strShoutoutRequestClubName;
@property(nonatomic,retain) NSString *strShoutoutRequestUserName;

@end
