//
//  Dj.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dj : NSObject

@property(nonatomic,retain) NSString *strDjID;
@property(nonatomic,retain) NSString *strDjName;
@property(nonatomic,retain) NSString *strDjClubId;
@property(nonatomic,retain) NSString *strDjClubName;
@property(nonatomic,retain) NSString *strDjProfilePictureImageUrl;

@property(nonatomic,retain) NSString *strDjOnlineDate;
@property(nonatomic,retain) NSString *strDjOfflineDate;

@property(nonatomic,retain) NSString *strDjOnlineTime;
@property(nonatomic,retain) NSString *strDjOfflineTime;

@end
