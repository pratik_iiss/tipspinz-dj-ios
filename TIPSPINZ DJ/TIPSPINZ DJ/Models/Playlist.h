//
//  Playlist.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 10/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Playlist : NSObject

@property(nonatomic,retain) NSString *strPlaylistID;
@property(nonatomic,retain) NSString *strPlaylistName;

@property(nonatomic,retain) NSMutableArray *arrayPlaylistSongs;

@end
