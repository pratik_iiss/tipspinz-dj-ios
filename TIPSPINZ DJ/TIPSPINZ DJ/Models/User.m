//
//  User.m
//  HungerE
//
//  Created by Pratik Gujarati on 23/09/16.
//  Copyright © 2016 accereteinfotech. All rights reserved.
//

#import "User.h"

@implementation User

-(BOOL)isValidateUserForLogin
{
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    //================BLANK FIELD VALIDATION===========//
    if(self.strEmail.length <= 0 ||  self.strPassword.length <=  0 )
    {
        if(self.strEmail.length <= 0)
        {
            self.strValidationMessage = @"Please enter email";
            return false;
        }
        else if(self.strPassword.length <= 0)
        {
            self.strValidationMessage = @"Please enter password";
            return false;
        }
    }
    else if(![objUtility isValidEmailAddress:self.strEmail])
    {
        self.strValidationMessage = @"Invalid email address";
        return false;
    }
    else
    {
        return true;
    }
    
    return true;
}

-(BOOL)isValidateUserForRegistration
{
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    //================BLANK FIELD VALIDATION===========//
    if(self.strEmail.length <= 0 ||  self.strPassword.length <=  0 || self.strFullname.length <= 0 || self.strGender.length <= 0 || self.strPhoneNumber.length <= 0 || self.strStateName.length <= 0 || self.strCityName.length <= 0)
    {
        if(self.strEmail.length <= 0)
        {
            self.strValidationMessage = @"Please enter email";
            return false;
        }
        else if(self.strPassword.length <= 0)
        {
            self.strValidationMessage = @"Please enter password";
            return false;
        }
        else if(self.strFullname.length <= 0)
        {
            self.strValidationMessage = @"Please enter your full name";
            return false;
        }
        else if(self.strGender.length <= 0)
        {
            self.strValidationMessage = @"Please select your gender";
            return false;
        }
        else if(self.strPhoneNumber.length <= 0)
        {
            self.strValidationMessage = @"Please enter your phone number";
            return false;
        }
        else if(self.strStateName.length <= 0)
        {
            self.strValidationMessage = @"Please select a state";
            return false;
        }
        else if(self.strCityName.length <= 0)
        {
            self.strValidationMessage = @"Please select a city";
            return false;
        }
    }
    else if(![objUtility isValidEmailAddress:self.strEmail])
    {
        self.strValidationMessage = @"Please enter a valid email address";
        return false;
    }
//    else if(self.strPhoneNumber.length != 10)
//    {
//        self.strValidationMessage = @"Mobile Number must contain 10 digits.";
//        return false;
//    }
    else
    {
        return true;
    }
    
    return true;
}

@end
