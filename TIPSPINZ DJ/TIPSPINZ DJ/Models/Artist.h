//
//  Artist.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 22/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Artist : NSObject

@property(nonatomic,retain) NSString *strArtistID;
@property(nonatomic,retain) NSString *strArtistName;
@property(nonatomic,retain) NSString *strArtistManagerId;
@property(nonatomic,retain) NSString *strArtistManagerName;
@property(nonatomic,retain) NSString *strArtistProfilePictureImageUrl;

@property(nonatomic,retain) NSString *strArtistOnlineDate;
@property(nonatomic,retain) NSString *strArtistOfflineDate;

@property(nonatomic,retain) NSString *strArtistOnlineTime;
@property(nonatomic,retain) NSString *strArtistOfflineTime;

@end
