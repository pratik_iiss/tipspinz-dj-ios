//
//  SongRequest.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SongRequest : NSObject

@property(nonatomic,retain) NSString *strSongRequestID;
@property(nonatomic,retain) NSString *strSongID;
@property(nonatomic,retain) NSString *strSongName;
@property(nonatomic,retain) NSString *strSongArtistName;
@property(nonatomic,retain) NSString *strSongRequestTime;
@property(nonatomic,assign) BOOL boolIsSongRequestVIP;
@property(nonatomic,retain) NSString *strSongRequestStatus;
@property(nonatomic,retain) NSString *strSongRequestAmount;
@property(nonatomic,retain) NSString *strSongRequestDjAmount;
@property(nonatomic,retain) NSString *strSongRequestAcceptanceType;
@property(nonatomic,assign) BOOL boolIsSongRequestExpired;

@property(nonatomic,retain) NSString *strSongRequestDjName;
@property(nonatomic,retain) NSString *strSongRequestClubName;
@property(nonatomic,retain) NSString *strSongRequestUserName;

@end
