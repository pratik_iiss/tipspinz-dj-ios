//
//  Advertisement.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 07/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Advertisement : NSObject

@property(nonatomic,retain) NSString *strAdvertisementId;
@property(nonatomic,retain) NSString *strAdvertisementImageUrl;
@property(nonatomic,retain) NSString *strAdvertisementType;
@property(nonatomic,retain) NSString *strAdvertisementRedirectionUrl;
@property(nonatomic,retain) NSString *strAdvertisementTitle;
@property(nonatomic,retain) NSString *strAdvertisementTableName;

@end
