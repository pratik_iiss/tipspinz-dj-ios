//
//  Club.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 21/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Club : NSObject

@property(nonatomic,retain) NSString *strClubID;
@property(nonatomic,retain) NSString *strClubName;
@property(nonatomic,retain) NSString *strClubType;
@property(nonatomic,retain) NSString *strClubAddress;
@property(nonatomic,retain) NSString *strClubImageUrl;
@property(nonatomic,retain) NSString *strClubBannerImageUrl;
@property(nonatomic,retain) NSString *strClubStateID;
@property(nonatomic,retain) NSString *strClubCityID;

@property(nonatomic,retain) NSString *strClubPlaylistID;
@property(nonatomic,retain) NSString *strClubPlaylistName;

@end
