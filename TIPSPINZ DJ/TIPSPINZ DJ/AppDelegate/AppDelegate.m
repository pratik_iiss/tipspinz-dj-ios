//
//  AppDelegate.m
//  PROJECT
//
//  Created by Pratik Gujarati on 00/00/00.
//  Copyright © 2017 Innovative Iteration Software Solutions. All rights reserved.
//

#import "AppDelegate.h"
#import "MySingleton.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "MyRequestsViewController.h"
#import "ArtistMyRequestsViewController.h"
@import Firebase;

//@import Stripe;

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize splashVC,navC;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    NSUUID *deviceId;
    #if !TARGET_IPHONE_SIMULATOR
        deviceId = [UIDevice currentDevice].identifierForVendor;
    #else
        deviceId = [[NSUUID alloc] initWithUUIDString:@"A5D59C2F-FE68-4BE7-B318-95029619C759"];
    #endif
    
    NSString *strDeviceId = [deviceId UUIDString];
    
    [prefs setObject:strDeviceId forKey:@"uniqueDeviceId"];
    [prefs synchronize];
    
    //Firebase
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;
    
#if !(TARGET_IPHONE_SIMULATOR)
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0"))
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
         {
             if(!error && granted)
             {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                         [[UIApplication sharedApplication] registerForRemoteNotifications];
                     });
                 NSLog( @"Push registration success." );
             }
             else
             {
                 NSLog( @"Push registration FAILED" );
                 NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                 NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
                 
                 NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                 [prefs setObject:@"" forKey:@"deviceToken"];
                 [prefs synchronize];
             }
         }];
    }
    else
    {
        //-- Set Notification
        if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
        {
            // iOS 8 Notifications
            
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            // iOS < 8 Notifications
            [application registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
        }
    }
#endif
    
    NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if(notification)
    {
        //NSString *strNotificationType = [NSString stringWithFormat:@"%@",[notification valueForKey:@"notification_type"]];
        
        NSString *strNotificationType = [NSString stringWithFormat:@"%@",[notification valueForKey:@"type"]];
        
        if([strNotificationType isEqualToString:@"102"])
        {
            // DJ/ARTIST ONLINE REQUEST APPROVED
            
            if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
            {
                [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
            }
            
            [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = true;
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strUserType = [prefs objectForKey:@"usertype"];
            
            if([strUserType isEqualToString:@"1"])
            {
                //DJ
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"djsOnlineStatusUpdatedEvent" object:nil];
            }
            else if([strUserType isEqualToString:@"3"])
            {
                //ARTIST
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"artistsOnlineStatusUpdatedEvent" object:nil];
            }
        }
        else if([strNotificationType isEqualToString:@"103"])
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strUserType = [prefs objectForKey:@"usertype"];
            
            if([strUserType isEqualToString:@"1"])
            {
                //DJ
                
                // DJ NEW SONG REQUEST RECEIVED
                
                [MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsSongsScreenForDj = true;
            }
            else if([strUserType isEqualToString:@"3"])
            {
                //ARTIST
                
                // ARTIST NEW SONG REQUEST RECEIVED
                
                [MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsSongsScreenForArtist = true;
            }
        }
        else if([strNotificationType isEqualToString:@"104"])
        {
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strUserType = [prefs objectForKey:@"usertype"];
            
            if([strUserType isEqualToString:@"1"])
            {
                //DJ
                
                // DJ NEW SHOUT OUT REQUEST RECEIVED
                
                [MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsShoutoutScreenForDj = true;
            }
            else if([strUserType isEqualToString:@"3"])
            {
                //ARTIST
                
                // ARTIST NEW SONG REQUEST RECEIVED
                
                [MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsShoutoutScreenForArtist = true;
            }
        }
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    self.splashVC = [[SplashViewController alloc] init];
    self.navC = [[UINavigationController alloc]initWithRootViewController:self.splashVC];
    self.window.rootViewController = self.navC;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [self.window endEditing:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"applicationDidEnterBackground" object:nil];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    [FBSDKAppEvents activateApp];
    
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options
{
    return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                          openURL:url
                                                sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                       annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}

#pragma mark - Notification Methods

//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    
    NSString *message = [[[notification.request.content.userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"];
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"Notification";
    alertViewController.message = message;
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action)
                                    {
                                        [alertViewController dismissViewControllerAnimated:YES completion:nil];
                                        
                                        //NSString *strNotificationType = [NSString stringWithFormat:@"%@",[notification.request.content.userInfo valueForKey:@"notification_type"]];
                                        NSString *strNotificationType = [NSString stringWithFormat:@"%@",[notification.request.content.userInfo valueForKey:@"type"]];
        
                                        if([strNotificationType isEqualToString:@"102"])
                                        {
                                            // DJ ONLINE REQUEST APPROVED
                                            
                                            if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
                                            {
                                                [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
                                            }
                                            
                                            [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = true;
                                            
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"djsOnlineStatusUpdatedEvent" object:nil];
                                        }
                                        else if([strNotificationType isEqualToString:@"103"])
                                        {
                                            // NEW SONG REQUEST RECEIVED
                                            
                                            [self navigateToMyRequestsScreenForSongs];
                                        }
                                        else if([strNotificationType isEqualToString:@"104"])
                                        {
                                            // NEW SHOUT OUT REQUEST RECEIVED
                                            
                                            [self navigateToMyRequestsScreenForShoutouts];
                                        }
                                    }]];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
}

//Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler
{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
    
    //NSString *strNotificationType = [NSString stringWithFormat:@"%@",[response.notification.request.content.userInfo valueForKey:@"notification_type"]];
    
    NSString *strNotificationType = [NSString stringWithFormat:@"%@",[response.notification.request.content.userInfo valueForKey:@"type"]];
    
    if([strNotificationType isEqualToString:@"102"])
    {
        // DJ ONLINE REQUEST APPROVED
        
        if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
        {
            [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
        }
        
        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = true;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"djsOnlineStatusUpdatedEvent" object:nil];
    }
    else if([strNotificationType isEqualToString:@"103"])
    {
        // NEW SONG REQUEST RECEIVED
        
        [self navigateToMyRequestsScreenForSongs];
    }
    else if([strNotificationType isEqualToString:@"104"])
    {
        // NEW SHOUT OUT REQUEST RECEIVED
        
        [self navigateToMyRequestsScreenForShoutouts];
    }
}

#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    /*
    NSString *strDeviceToken = [[[deviceToken description]
                                 stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
                                stringByReplacingOccurrencesOfString:@" "
                                withString:@""];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:strDeviceToken forKey:@"deviceToken"];
    [prefs synchronize];
    */
    
    const unsigned *tokenBytes = [deviceToken bytes];
          NSString *strDeviceToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                               ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                               ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                               ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:strDeviceToken forKey:@"deviceToken"];
    [prefs synchronize];
    
    NSLog(@"APNS token: %@", strDeviceToken);
    
    [FIRMessaging messaging].APNSToken = deviceToken;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceTokenGeneratedEvent" object:nil];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError, error  : %@",error);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:@"" forKey:@"deviceToken"];
    [prefs synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"deviceTokenGeneratedEvent" object:nil];
}

// will be called when in foreground
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"didReceiveRemoteNotification called.");
    
    // iOS 10 will handle notifications through other methods
    
    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO( @"10.0" ) )
    {
        NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
        // set a member variable to tell the new delegate that this is background
        return;
    }
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
    // custom code to handle notification content
    
    UIApplicationState state = [application applicationState];
    
    if (state == UIApplicationStateActive)
    {
        //NSString *message = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
        NSString *message = [[[userInfo valueForKey:@"aps"] valueForKey:@"alert"] valueForKey:@"body"];
        
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"Notification";
        alertViewController.message = message;
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = YES;
        alertViewController.swipeDismissalGestureEnabled = YES;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action)
                                        {
                                            [alertViewController dismissViewControllerAnimated:YES completion:nil];
                                            
                                            //NSString *strNotificationType = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"notification_type"]];
                                            NSString *strNotificationType = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"type"]];
            
                                            if([strNotificationType isEqualToString:@"102"])
                                            {
                                                // DJ ONLINE REQUEST APPROVED
                                                
                                                if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
                                                {
                                                    [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
                                                }
                                                
                                                [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = true;
                                                
                                                [[NSNotificationCenter defaultCenter] postNotificationName:@"djsOnlineStatusUpdatedEvent" object:nil];
                                            }
                                            else if([strNotificationType isEqualToString:@"103"])
                                            {
                                                // NEW SONG REQUEST RECEIVED
                                                
                                                [self navigateToMyRequestsScreenForSongs];
                                            }
                                            else if([strNotificationType isEqualToString:@"104"])
                                            {
                                                // NEW SHOUT OUT REQUEST RECEIVED
                                                
                                                [self navigateToMyRequestsScreenForShoutouts];
                                            }
                                        }]];
        
        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
    }
    else if (state == UIApplicationStateInactive)
    {
        //NSString *strNotificationType = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"notification_type"]];
        NSString *strNotificationType = [NSString stringWithFormat:@"%@",[userInfo valueForKey:@"type"]];
        
        if([strNotificationType isEqualToString:@"102"])
        {
            // DJ ONLINE REQUEST APPROVED
            
            if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
            {
                [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
            }
            
            [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = true;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"djsOnlineStatusUpdatedEvent" object:nil];
        }
        else if([strNotificationType isEqualToString:@"103"])
        {
            // NEW SONG REQUEST RECEIVED
            
            [self navigateToMyRequestsScreenForSongs];
        }
        else if([strNotificationType isEqualToString:@"104"])
        {
            // NEW SHOUT OUT REQUEST RECEIVED
            
            [self navigateToMyRequestsScreenForShoutouts];
        }
    }
    else
    {
        //Do stuff that you would do if the application was not active
    }
}

//MARK:- Firebase Methods
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    // Notify about received token.
    NSDictionary *dataDict = [NSDictionary dictionaryWithObject:fcmToken forKey:@"token"];
    [[NSNotificationCenter defaultCenter] postNotificationName:
     @"FCMToken" object:nil userInfo:dataDict];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:fcmToken forKey:@"fcmToken"];
    [prefs synchronize];
    
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}

#pragma mark - Other Methods

-(MBProgressHUD *)showGlobalProgressHUDWithTitle:(NSString *)title
{
    [self dismissGlobalHUD];
    UIWindow *window = [(AppDelegate *)[[UIApplication sharedApplication] delegate] window];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:window animated:YES];
    hud.label.text = title;
    hud.dimBackground = YES;
    return hud;
}

-(void)dismissGlobalHUD
{
//    dispatch_async(dispatch_get_main_queue(), ^{
        UIWindow *window = [(AppDelegate *)[[UIApplication sharedApplication] delegate] window];
        [MBProgressHUD hideHUDForView:window animated:YES];
//    });
}

//=========================FUNCTION TO SHOW THE HHAlertView ========================//

-(void)showErrorAlertViewWithTitle:(NSString *)title withDetails:(NSString *)detail
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = title;
    alertViewController.message = detail;
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action)
    {
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
}

-(BOOL)isClock24Hour
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterNoStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
    BOOL is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    
    return is24h;
}

- (void)navigateToMyRequestsScreenForSongs
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserType = [prefs objectForKey:@"usertype"];
    
    if([strUserType isEqualToString:@"1"])
    {
        //DJ
        
        MyRequestsViewController *viewController = [[MyRequestsViewController alloc] init];
        
        viewController.boolIsLoadedForSongsRequests = true;
        
        SideMenuViewController *sideMenuViewController;
        
        if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight >= 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                               leftViewController:sideMenuViewController
                                                                                              rightViewController:nil];
        
        sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
        sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
        
        sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
        sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
        
        [self.navC pushViewController:sideMenuController animated:YES];
    }
    else if([strUserType isEqualToString:@"3"])
    {
        //ARTIST
        
        ArtistMyRequestsViewController *viewController = [[ArtistMyRequestsViewController alloc] init];
        
        viewController.boolIsLoadedForSongsRequests = true;
        
        SideMenuViewController *sideMenuViewController;
        
        if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight >= 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                               leftViewController:sideMenuViewController
                                                                                              rightViewController:nil];
        
        sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
        sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
        
        sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
        sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
        
        [self.navC pushViewController:sideMenuController animated:YES];
    }
}

- (void)navigateToMyRequestsScreenForShoutouts
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserType = [prefs objectForKey:@"usertype"];
    
    if([strUserType isEqualToString:@"1"])
    {
        //DJ
        
        MyRequestsViewController *viewController = [[MyRequestsViewController alloc] init];
        
        viewController.boolIsLoadedForSongsRequests = false;
        
        SideMenuViewController *sideMenuViewController;
        
        if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight >= 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                               leftViewController:sideMenuViewController
                                                                                              rightViewController:nil];
        
        sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
        sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
        
        sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
        sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
        
        [self.navC pushViewController:sideMenuController animated:YES];
    }
    else if([strUserType isEqualToString:@"3"])
    {
        //ARTIST
        ArtistMyRequestsViewController *viewController = [[ArtistMyRequestsViewController alloc] init];
        
        viewController.boolIsLoadedForSongsRequests = false;
        
        SideMenuViewController *sideMenuViewController;
        
        if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight >= 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                               leftViewController:sideMenuViewController
                                                                                              rightViewController:nil];
        
        sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
        sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
        
        sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
        sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
        
        [self.navC pushViewController:sideMenuController animated:YES];
    }
}

@end
