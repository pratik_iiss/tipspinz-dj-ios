//
//  DataManager.h
//  HungerE
//
//  Created by Pratik Gujarati on 23/09/16.
//  Copyright © 2016 accereteinfotech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "CommonUtility.h"

//IMPORTING MODELS
#import "User.h"
#import "State.h"
#import "City.h"
#import "Club.h"
#import "Song.h"
#import "Playlist.h"
#import "SongRequest.h"
#import "ShoutoutRequest.h"
#import "Dj.h"
#import "Advertisement.h"
#import "Event.h"

#import "Artist.h"
#import "Manager.h"

@interface DataManager : NSObject
{
    // The delegate - Will be notified of various changes of state via the DataManagerDelegate
    
    NSNumber  *isNetworkAvailable;
    AppDelegate *appDelegate;
    
    User *objLoggedInUser;
}

@property(nonatomic,retain) AppDelegate *appDelegate;
@property(nonatomic,retain) NSDictionary *dictSettings;

@property(nonatomic,retain) User *objLoggedInUser;

@property(nonatomic,retain) NSMutableArray *arrayStates;

@property(nonatomic,retain) NSMutableArray *arraySponsorsAdvertisementsImages;

@property(nonatomic,retain) NSString *strSideMenuBackgroundImageUrl;

//DJ

@property(nonatomic,retain) NSMutableArray *arrayAllClubs;
@property(nonatomic,retain) NSMutableArray *arrayClubsByUserStateId;
@property(nonatomic,retain) NSMutableArray *arrayAllApprovedClubsByUserId;

@property(nonatomic,retain) NSMutableArray *arrayDjSongsRequests;
@property(nonatomic,retain) NSMutableArray *arrayDjShoutoutRequests;

@property(nonatomic,retain) NSMutableArray *arrayDjAcceptedSongsRequests;
@property(nonatomic,retain) NSMutableArray *arrayDjAcceptedShoutoutRequests;

@property(nonatomic,retain) NSMutableArray *arrayDjPlayedSongsRequests;
@property(nonatomic,retain) NSMutableArray *arrayDjPlayedShoutoutRequests;

@property(nonatomic,retain) NSMutableArray *arrayDjMyEarningsSongsRequests;
@property(nonatomic,retain) NSMutableArray *arrayDjMyEarningsShoutoutRequests;

@property(nonatomic,retain) NSMutableArray *arrayDjAdvertisements;

@property(nonatomic,retain) NSMutableArray *arrayClubOwnerApprovedDjs;
@property(nonatomic,retain) NSMutableArray *arrayClubOwnerDjRequests;
@property(nonatomic,retain) NSMutableArray *arrayClubOwnerDjOnlineRequests;
@property(nonatomic,retain) NSMutableArray *arrayClubOwnerOnlineDjs;
@property(nonatomic,retain) NSMutableArray *arrayClubOwnerDjLogs;
@property(nonatomic,retain) NSMutableArray *arrayClubOwnerAllClubs;

@property(nonatomic,retain) NSString *strClubOwnerSelectedPlaylistIdForClub;
@property(nonatomic,retain) NSString *strClubOwnerSelectedPlaylistNameForClub;

@property(nonatomic,assign) BOOL boolIsSongsDeletedFromPlaylistSuccessfully;
@property(nonatomic,assign) BOOL boolIsSongsAddedIntoPlaylistSuccessfully;

@property(nonatomic,retain) NSString *strSignedUpDjEmail;
@property(nonatomic,retain) NSString *strSignedUpDjPassword;
@property(nonatomic,retain) NSString *strSignedUpDjStripeConnectUrl;

@property(nonatomic,retain) NSMutableArray *arrayAllEventsByDj;

@property(nonatomic,assign) BOOL boolIsEventStatusChangedSuccessfully;

@property(nonatomic,assign) BOOL boolIsAdvertisementAddedSuccessfully;

@property(nonatomic,retain) NSString *strDjSongRequestAcceptanceTypeMessage;


//Artist

@property(nonatomic,retain) NSMutableArray *arrayAllManagers;
@property(nonatomic,retain) NSMutableArray *arrayManagersByArtistStateId;
@property(nonatomic,retain) NSMutableArray *arrayAllApprovedManagersByArtistId;

@property(nonatomic,retain) NSMutableArray *arrayArtistSongsRequests;
@property(nonatomic,retain) NSMutableArray *arrayArtistShoutoutRequests;

@property(nonatomic,retain) NSMutableArray *arrayArtistAcceptedSongsRequests;
@property(nonatomic,retain) NSMutableArray *arrayArtistAcceptedShoutoutRequests;

@property(nonatomic,retain) NSMutableArray *arrayArtistPlayedSongsRequests;
@property(nonatomic,retain) NSMutableArray *arrayArtistPlayedShoutoutRequests;

@property(nonatomic,retain) NSMutableArray *arrayArtistMyEarningsSongsRequests;
@property(nonatomic,retain) NSMutableArray *arrayArtistMyEarningsShoutoutRequests;

@property(nonatomic,retain) NSMutableArray *arrayArtistAdvertisements;

@property(nonatomic,retain) NSMutableArray *arrayCompanyApprovedArtists;
@property(nonatomic,retain) NSMutableArray *arrayCompanyArtistsRequests;
@property(nonatomic,retain) NSMutableArray *arrayCompanyArtistsOnlineRequests;
@property(nonatomic,retain) NSMutableArray *arrayCompanyOnlineArtists;
@property(nonatomic,retain) NSMutableArray *arrayCompanyArtistsLogs;
@property(nonatomic,retain) NSMutableArray *arrayCompanyAllManagers;

@property(nonatomic,retain) NSString *strCompanySelectedPlaylistIdForManager;
@property(nonatomic,retain) NSString *strCompanySelectedPlaylistNameForManager;

@property(nonatomic,assign) BOOL boolIsArtistSongsDeletedFromPlaylistSuccessfully;
@property(nonatomic,assign) BOOL boolIsArtistSongsAddedIntoPlaylistSuccessfully;

@property(nonatomic,retain) NSString *strSignedUpArtistEmail;
@property(nonatomic,retain) NSString *strSignedUpArtistPassword;
@property(nonatomic,retain) NSString *strSignedUpArtistStripeConnectUrl;

@property(nonatomic,retain) NSMutableArray *arrayAllEventsByArtist;

@property(nonatomic,assign) BOOL boolIsArtistEventStatusChangedSuccessfully;

@property(nonatomic,assign) BOOL boolIsArtistAdvertisementAddedSuccessfully;

@property(nonatomic,retain) NSString *strArtistSongRequestAcceptanceTypeMessage;

@property(nonatomic,retain) NSString *strShareLink;

#pragma mark - Server communication

//FUNCTION TO CHECK IF INTERNET CONNECTION IS AVAILABLE OR NOT
-(BOOL)isNetworkAvailable;

//FUNCTION TO SHOW ERROR ALERT
-(void)showErrorMessage:(NSString *)errorTitle withErrorContent:(NSString *)errorDescription;

//FUNCTION TO SUCCESS 0 CASE ALERT
-(void)showWebserviceFailureCaseMessage:(NSString *)errorTitle withErrorContent:(NSString *)errorDescription;

//FUNCTION TO CHECK FACEBOOK BUTTON IS HIDDED OR NOT
-(void)facebookButtonHiddenStatus;

//========== DJ WEBSERVICES ==========//

//FUNCTION TO CHECK ONLINE STATUS OF DJ BY USERID
-(void)checkOnlineStatusOfDj:(NSString *)strUserId;

//FUNCTION FOR USER LOGIN
-(void)userLogin:(User *)objUser;

//FUNCTION FOR CLUBOWNER LOGIN
-(void)clubOwnerLogin:(User *)objUser;

//FUNCTION FOR USER LOGOUT
-(void)userLogout:(NSString *)strUserId withUserType:(NSString *)strUserType;

//FUNCTION FOR FACEBOOK USERS TO CHECK IF THEY ARE ALREADY REGISTERED
-(void)checkIfTheFacebookUserIsAlreadyRegistered:(User *)objUser;

//FUNCTION TO GET ALL STATES
-(void)getAllStates;

//FUNCTION FOR USER SIGNUP
-(void)userSignUp:(User *)objUser;

//FUNCTION FOR CLUB OWNER SIGNUP
-(void)clubOwnerSignUp:(User *)objUser;

//FUNCTION FOR FORGET PASSWORD
-(void)forgetPassword:(NSString *)strEmail withUserType:(NSString *)strUserType;

//FUNCTION TO GET SPONSORS ADVERTISEMENTS IMAGES BY USER ID
-(void)getSponsorsAdvertisementsImagesByUserId:(NSString *)strUserId;

//FUNCTION TO CHANGE STATUS BY USER ID
-(void)changeStatus:(NSString *)strStatus withDjUserId:(NSString *)strUserId withClubId:(NSString *)strClubId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION TO CHANGE ONLINE PLAYLLIST BY USER ID
-(void)changeOnlinePlaylist:(NSString *)strUserId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION TO GET ALL CLUBS
-(void)getAllClubs:(NSString *)strUserId;

//FUNCTION TO GET ALL APPROVED CLUBS
-(void)getAllApprovedClubs:(NSString *)strUserId;

//FUNCTION TO ADD CLUBS FOR DJ BY USER ID AND CLUBS IDS
-(void)addClubs:(NSDictionary *)dictParameters;

//FUNCTION TO REMOVE APPROVED CLUBS FOR DJ BY USER ID AND CLUBS IDS
-(void)removeApprovedClubs:(NSDictionary *)dictParameters;

//FUNCTION TO GET USER PROFILE DETAILS BY USER ID
-(void)getUserProfileByUserId:(NSString *)strUserId;

//FUNCTION TO UPDATE USER PROFILE DETAILS BY USER ID
-(void)updateUserProfile:(NSDictionary *)dictParameters;

//FUNCTION TO UPLOAD USER PROFILE PICTURE WITH USER ID
-(void)uploadProfilePicture:(NSString *)strProfilePictureBase64Data;

//FUNCTION TO UPLOAD USER PROFILE PICTURE WITH USER ID WITH $_FILE
-(void)uploadProfilePictureWith_File:(NSData *)profilePictureData;

//FUNCTION TO UPDATE USER SOCIAL MEDIA PROFILE URL BY USER ID
-(void)updateUserSocialMediaProfileUrl:(NSDictionary *)dictParameters;

//FUNCTION TO CHANGE PASSWORD
-(void)changePassword:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL PLAYLISTS BY USER ID
-(void)getAllPlaylistsByUserId:(NSString *)strUserId;

//FUNCTION TO DELETE PLAYLIST BY USER ID AND PLAYLIST ID
-(void)deletePlaylistByUserId:(NSString *)strUserId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION TO DELETE SONG BY USER ID AND SONG ID
-(void)deleteSongByUserId:(NSString *)strUserId withSongId:(NSString *)strSongId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION TO ADD SONG
-(void)addSong:(NSDictionary *)dictParameters;

//FUNCTION TO ADD PLAYLIST
-(void)addPlaylist:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL UNACCEPTED REQUESTS LIST BY USER ID
-(void)getAllUnacceptedRequestsByUserId:(NSString *)strUserId;

//FUNCTION TO GET ALL ACCEPTED REQUESTS LIST BY USER ID
-(void)getAllAcceptedRequestsByUserId:(NSString *)strUserId;

//FUNCTION TO GET ALL PLAYED REQUESTS LIST BY USER ID
-(void)getAllPlayedRequestsByUserId:(NSString *)strUserId;

//FUNCTION FOR DJS TO ACCEPT SONG REQUEST
-(void)acceptSongRequest:(NSDictionary *)dictParameters;

//FUNCTION FOR DJS TO DECLINE SONG REQUEST
-(void)declineSongRequest:(NSDictionary *)dictParameters;

//FUNCTION FOR DJS TO ACCEPT SHOUTOUT REQUEST
-(void)acceptShoutoutRequest:(NSDictionary *)dictParameters;

//FUNCTION FOR DJS TO DECLINE SHOUTOUT REQUEST
-(void)declineShoutoutRequest:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL MY EARNINGS REQUESTS LIST BY USER ID
-(void)getAllMyEarningsRequestsByUserId:(NSString *)strUserId;

//FUNCTION FOR DJS TO MARK SONG REQUEST AS PLAYED
-(void)markSongRequestAsPlayed:(NSDictionary *)dictParameters;

//FUNCTION FOR DJS TO MARK SHOUTOUT REQUEST AS PLAYED
-(void)markShoutoutRequestAsPlayed:(NSDictionary *)dictParameters;

//FUNCTION FOR DJS TO SET TWITTER INSTAGRAM ONLINE MESSAGE
-(void)setTwitterInstagramOnlineMessage:(NSString *)strMessage;

//FUNCTION TO GET ALL EVENTS BY DJ ID
-(void)getAllEventsByDjId:(NSDictionary *)dictParameters;

//FUNCTION TO ADD EVENT
-(void)addEvent:(NSDictionary *)dictParameters;

//FUNCTION TO DELETE EVENT
-(void)deleteEvent:(NSDictionary *)dictParameters;

//FUNCTION TO GET STATUS BY EVENT ID AND USER ID
-(void)getUserStatusForEvent:(NSDictionary *)dictParameters;

//FUNCTION TO CHANGE EVENT STATUS
-(void)changeEventStatus:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL ADVERTISEMENTS LIST BY USER ID
-(void)getAllAdvertisementsByUserId:(NSString *)strUserId;

//FUNCTION TO DELETE ADVERTISEMENT BY ADVERTISEMENT ID
-(void)deleteAdvertisementByAdvertisementId:(NSString *)strAdvertisementId;

//FUNCTION TO ADD ADVERTISEMENT
-(void)addAdvertisement:(NSDictionary *)dictParameters with_File:(NSData *)imageData;

//========== CLUB OWNER WEBSERVICES ==========//

//FUNCTION FOR CLUB OWNERS TO GET APPROVED DJS LIST BY USER ID
-(void)getClubOwnerApprovedDjListByUserId:(NSString *)strUserId;

//FUNCTION FOR CLUB OWNERS TO GET DJS REQUEST LIST BY USER ID
-(void)getClubOwnerDjRequestListByUserId:(NSString *)strUserId;

//FUNCTION FOR CLUB OWNERS TO ACCEPT DJ APPROVAL REQUEST BY USER ID AND DJ ID
-(void)acceptDjApprovalRequestByUserId:(NSString *)strUserId withDjId:(NSString *)strDjId withClubId:(NSString *)strClubId;

//FUNCTION FOR CLUB OWNERS TO DECLINE DJ APPROVAL REQUEST BY USER ID AND DJ ID
-(void)declineDjApprovalRequestByUserId:(NSString *)strUserId withDjId:(NSString *)strDjId withClubId:(NSString *)strClubId;

//FUNCTION FOR CLUB OWNERS TO GET DJS ONLINE REQUEST LIST BY USER ID
-(void)getClubOwnerDjOnlineRequestListByUserId:(NSString *)strUserId;

//FUNCTION FOR CLUB OWNERS TO ACCEPT DJ ONLINE REQUEST BY USER ID AND DJ ID
-(void)acceptDjOnlineRequestByUserId:(NSString *)strUserId withDjId:(NSString *)strDjId withClubId:(NSString *)strClubId;

//FUNCTION FOR CLUB OWNERS TO DECLINE DJ ONLINE REQUEST BY USER ID AND DJ ID
-(void)declineDjOnlineRequestByUserId:(NSString *)strUserId withDjId:(NSString *)strDjId withClubId:(NSString *)strClubId;

//FUNCTION FOR CLUB OWNERS TO GET ONLINE DJS LIST BY USER ID
-(void)getClubOwnerOnlineDjListByUserId:(NSString *)strUserId;

//FUNCTION FOR CLUB OWNERS TO GET DJ LOGS BY USER ID
-(void)getClubOwnerDjLogsByUserId:(NSString *)strUserId;

//FUNCTION FOR CLUB OWNERS TO GET ALL CLUBS
-(void)getClubOwnerAllClubs:(NSString *)strUserId;

//FUNCTION FOR CLUB OWNERS TO GET ALL PLAYLISTS BY USER ID
-(void)getAllClubOwnerPlaylistsByUserId:(NSString *)strUserId;

//FUNCTION FOR CLUB OWNERS TO DELETE PLAYLIST BY USER ID AND PLAYLIST ID
-(void)deleteClubOwnerPlaylistByUserId:(NSString *)strUserId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION FOR CLUB OWNERS TO DELETE SONG BY USER ID AND SONG ID
-(void)deleteClubOwnerSongByUserId:(NSString *)strUserId withSongId:(NSString *)strSongId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION FOR CLUB OWNERS TO ADD SONG
-(void)addClubOwnerSong:(NSDictionary *)dictParameters;

//FUNCTION FOR CLUB OWNERS TO ADD PLAYLIST
-(void)addClubOwnerPlaylist:(NSDictionary *)dictParameters;

//FUNCTION FOR CLUB OWNERS TO GET SELECTED PLAYLIST BY CUB ID AND USER ID
-(void)getClubOwnerSelectedPlaylistByUserId:(NSString *)strUserId withClubId:(NSString *)strClubId;

//FUNCTION FOR CLUB OWNERS TO SET PLAYLIST BY CLUB ID, PLAYLIST ID AND USER ID
-(void)setClubOwnerPlaylistByUserId:(NSString *)strUserId withClubId:(NSString *)strClubId withPlaylistId:(NSString *)strPlaylistId;

//========== ARTIST WEBSERVICES ==========//

//FUNCTION TO CHECK ONLINE STATUS OF ARTIST BY USERID
-(void)checkOnlineStatusOfArtist:(NSString *)strArtistId;

//FUNCTION FOR ARTIST LOGIN
-(void)artistLogin:(User *)objUser;

//FUNCTION FOR COMPANY LOGIN
-(void)companyLogin:(User *)objUser;

//FUNCTION FOR FACEBOOK ARTIST TO CHECK IF THEY ARE ALREADY REGISTERED
-(void)checkIfTheFacebookArtistIsAlreadyRegistered:(User *)objUser;

//FUNCTION FOR ARTIST SIGNUP
-(void)artistSignUp:(User *)objUser;

////FUNCTION FOR COMPANY SIGNUP
//-(void)companySignUp:(User *)objUser;

//FUNCTION FOR FORGET PASSWORD
-(void)forgetArtistPassword:(NSString *)strEmail withUserType:(NSString *)strUserType;

//FUNCTION TO CHANGE STATUS BY ARTIST ID
-(void)changeStatus:(NSString *)strStatus withArtistId:(NSString *)strArtistId withManagerId:(NSString *)strManagerId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION TO CHANGE ONLINE PLAYLLIST BY ARTIST ID
-(void)changeArtistOnlinePlaylist:(NSString *)strArtistId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION TO GET ALL MANAGERS
-(void)getAllManagers:(NSString *)strArtistId;

//FUNCTION TO GET ALL APPROVED MANAGERS
-(void)getAllApprovedManagers:(NSString *)strArtistId;

//FUNCTION TO ADD MANAGERS FOR ARTIST BY ARTIST ID AND MANAGERS IDS
-(void)addManagers:(NSDictionary *)dictParameters;

//FUNCTION TO REMOVE APPROVED MANAGERS FOR ARTIST BY ARTIST ID AND MANAGERS IDS
-(void)removeApprovedManagers:(NSDictionary *)dictParameters;

//FUNCTION TO GET ARTIST PROFILE DETAILS BY ARTIST ID
-(void)getArtistProfileByArtistId:(NSString *)strArtistId;

//FUNCTION TO UPDATE ARTIST PROFILE DETAILS BY ARTIST ID
-(void)updateArtistProfile:(NSDictionary *)dictParameters;

//FUNCTION TO UPLOAD ARTIST PROFILE PICTURE WITH ARTIST ID
-(void)uploadArtistProfilePicture:(NSString *)strProfilePictureBase64Data;

//FUNCTION TO UPLOAD ARTIST PROFILE PICTURE WITH ARTIST ID WITH $_FILE
-(void)uploadArtistProfilePictureWith_File:(NSData *)profilePictureData;

//FUNCTION TO UPDATE ARTIST SOCIAL MEDIA PROFILE URL BY ARTIST ID
-(void)updateArtistSocialMediaProfileUrl:(NSDictionary *)dictParameters;

//FUNCTION TO CHANGE ARTIST PASSWORD
-(void)changeArtistPassword:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL PLAYLISTS BY ARTIST ID
-(void)getAllPlaylistsByArtistId:(NSString *)strArtistId;

//FUNCTION TO DELETE PLAYLIST BY ARTIST ID AND PLAYLIST ID
-(void)deletePlaylistByArtistId:(NSString *)strArtistId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION TO DELETE SONG BY ARTIST ID AND SONG ID
-(void)deleteSongByArtistId:(NSString *)strArtistId withSongId:(NSString *)strSongId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION TO ADD ARTIST SONG
-(void)addArtistSong:(NSDictionary *)dictParameters;

//FUNCTION TO ADD ARTIST PLAYLIST
-(void)addArtistPlaylist:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL UNACCEPTED REQUESTS LIST BY ARTIST ID
-(void)getAllUnacceptedRequestsByArtistId:(NSString *)strArtistId;

//FUNCTION TO GET ALL ACCEPTED REQUESTS LIST BY ARTIST ID
-(void)getAllAcceptedRequestsByArtistId:(NSString *)strArtistId;

//FUNCTION TO GET ALL PLAYED REQUESTS LIST BY ARTIST ID
-(void)getAllPlayedRequestsByArtistId:(NSString *)strArtistId;

//FUNCTION FOR ARTISTS TO ACCEPT SONG REQUEST
-(void)acceptArtistSongRequest:(NSDictionary *)dictParameters;

//FUNCTION FOR ARTISTS TO DECLINE SONG REQUEST
-(void)declineArtistongRequest:(NSDictionary *)dictParameters;

//FUNCTION FOR ARTISTS TO ACCEPT SHOUTOUT REQUEST
-(void)acceptArtistShoutoutRequest:(NSDictionary *)dictParameters;

//FUNCTION FOR ARTISTS TO DECLINE SHOUTOUT REQUEST
-(void)declineArtistShoutoutRequest:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL MY EARNINGS REQUESTS LIST BY ARTIST ID
-(void)getAllMyEarningsRequestsByArtistId:(NSString *)strArtistId;

//FUNCTION FOR ARTISTS TO MARK SONG REQUEST AS PLAYED
-(void)markArtistSongRequestAsPlayed:(NSDictionary *)dictParameters;

//FUNCTION FOR ARTISTS TO MARK SHOUTOUT REQUEST AS PLAYED
-(void)markArtistShoutoutRequestAsPlayed:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL EVENTS BY ARTIST ID
-(void)getAllEventsByArtistId:(NSDictionary *)dictParameters;

//FUNCTION TO ADD ARTIST EVENT
-(void)addArtistEvent:(NSDictionary *)dictParameters;

//FUNCTION TO DELETE ARTIST EVENT
-(void)deleteArtistEvent:(NSDictionary *)dictParameters;

//FUNCTION TO GET STATUS BY EVENT ID AND ARTIST ID
-(void)getArtistStatusForEvent:(NSDictionary *)dictParameters;

//FUNCTION TO CHANGE ARTIST EVENT STATUS
-(void)changeArtistEventStatus:(NSDictionary *)dictParameters;

//FUNCTION TO GET ALL ADVERTISEMENTS LIST BY ARTIST ID
-(void)getAllArtistAdvertisementsByArtistId:(NSString *)strArtistId;

//FUNCTION TO DELETE ARTIST ADVERTISEMENT BY ADVERTISEMENT ID
-(void)deleteArtistAdvertisementByAdvertisementId:(NSString *)strAdvertisementId;

//FUNCTION TO ADD ARTIST ADVERTISEMENT
-(void)addArtistAdvertisement:(NSDictionary *)dictParameters with_File:(NSData *)imageData;

//========== COMPANY WEBSERVICES ==========//

//FUNCTION FOR COMPANY TO GET APPROVED ARTISTS LIST BY COMPANY ID
-(void)getCompanyApprovedArtistListByCompanyId:(NSString *)strCompanyId;

//FUNCTION FOR COMPANY TO GET ARTISTS REQUEST LIST BY COMPANY ID
-(void)getCompanyArtistRequestListByCompanyId:(NSString *)strCompanyId;

//FUNCTION FOR COMPANY TO ACCEPT ARTIST APPROVAL REQUEST BY COMPANY ID AND ARTIST ID
-(void)acceptArtistApprovalRequestByCompanyId:(NSString *)strCompanyId withArtistId:(NSString *)strArtistId withManagerId:(NSString *)strManagerId;

//FUNCTION FOR COMPANY TO DECLINE ARTIST APPROVAL REQUEST BY COMPANY ID AND ARTIST ID
-(void)declineArtistApprovalRequestByCompanyId:(NSString *)strCompanyId withArtistId:(NSString *)strArtistId withManagerId:(NSString *)strManagerId;

//FUNCTION FOR COMPANY TO GET ARTISTS ONLINE REQUEST LIST BY COMPANY ID
-(void)getCompanyArtistOnlineRequestListByCompanyId:(NSString *)strCompanyId;

//FUNCTION FOR COMPANY TO ACCEPT ARTIST ONLINE REQUEST BY COMPANY ID AND ARTIST ID
-(void)acceptArtistOnlineRequestByCompanyId:(NSString *)strCompanyId withArtistId:(NSString *)strArtistId withManagerId:(NSString *)strManagerId;

//FUNCTION FOR COMPANY TO DECLINE ARTIST ONLINE REQUEST BY COMPANY ID AND ARTIST ID
-(void)declineArtistOnlineRequestByCompanyId:(NSString *)strCompanyId withArtistId:(NSString *)strArtistId withManagerId:(NSString *)strManagerId;

//FUNCTION FOR COMPANY TO GET ONLINE ARTISTS LIST BY COMPANY ID
-(void)getCompanyOnlineArtistListByCompanyId:(NSString *)strCompanyId;

//FUNCTION FOR COMPANY TO GET ARTIST LOGS BY COMPANY ID
-(void)getCompanyArtistLogsByCompanyId:(NSString *)strCompanyId;

//FUNCTION FOR COMPANY TO GET ALL MANAGERS
-(void)getCompanyAllManagers:(NSString *)strCompanyId;

//FUNCTION FOR COMPANY TO GET ALL PLAYLISTS BY COMPANY ID
-(void)getAllCompanyPlaylistsByCompanyId:(NSString *)strCompanyId;

//FUNCTION FOR COMPANY TO DELETE PLAYLIST BY COMPANY ID AND PLAYLIST ID
-(void)deleteCompanyPlaylistByCompanyId:(NSString *)strCompanyId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION FOR COMPANY TO DELETE SONG BY COMPANY ID AND SONG ID
-(void)deleteCompanySongByCompanyId:(NSString *)strCompanyId withSongId:(NSString *)strSongId withPlaylistId:(NSString *)strPlaylistId;

//FUNCTION FOR COMPANY TO ADD SONG
-(void)addCompanySong:(NSDictionary *)dictParameters;

//FUNCTION FOR COMPANY TO ADD PLAYLIST
-(void)addCompanyPlaylist:(NSDictionary *)dictParameters;

//FUNCTION FOR COMPANY TO GET SELECTED PLAYLIST BY CLUB ID AND COMPANY ID
-(void)getCompanySelectedPlaylistByCompanyId:(NSString *)strCompanyId withManagerId:(NSString *)strManagerId;

//FUNCTION FOR COMPANY TO SET PLAYLIST BY MANAGER ID, PLAYLIST ID AND COMPANY ID
-(void)setCompanyPlaylistByCompanyId:(NSString *)strCompanyId withManagerId:(NSString *)strManagerId withPlaylistId:(NSString *)strPlaylistId;

//========== COMMON WEBSERVICES ==========//

//FUNCTION TO SEND ADVERTISEMENT CLICK DATA TO SERVER
-(void)sendAdvertisementClickDataToServer:(NSDictionary *)dictParameters;

//FUNCTION TO GET SHARE LINK
-(void)getShareLink;

@end
