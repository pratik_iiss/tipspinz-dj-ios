 //
//  DataManager.m
//  HungerE
//
//  Created by Pratik Gujarati on 23/09/16.
//  Copyright © 2016 accereteinfotech. All rights reserved.
//

#import "DataManager.h"
#import "MySingleton.h"

@implementation DataManager

-(id)init
{
    _dictSettings = [NSDictionary dictionaryWithContentsOfFile: [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent: @"WebservicesUrls.plist"]];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    return self;
}

- (BOOL) isNetworkAvailable
{
    Reachability *reach = [Reachability reachabilityWithHostName:[_dictSettings objectForKey:@"AvailabilityHostToCheck"]];
    NetworkStatus status = [reach currentReachabilityStatus];
    isNetworkAvailable = [NSNumber numberWithBool:!(status == NotReachable)];
    reach = nil;
    return [isNetworkAvailable boolValue];
}

-(void)showInternetNotConnectedError
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"No Internet Connection";
    alertViewController.message = @"Please make sure that you are connected to the internet.";
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action)
    {
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        if (UIApplicationOpenSettingsURLString != NULL)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }]];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
}

-(void)showErrorMessage:(NSString *)errorTitle withErrorContent:(NSString *)errorDescription
{
    if([errorTitle isEqualToString:@"Server Error"])
    {
        errorDescription = @"Oops! Something went wrong. Please try again later.";
    }
    else
    {
        errorDescription = errorDescription;
    }
    
    [appDelegate dismissGlobalHUD];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:errorTitle withDetails:errorDescription];
    });
}

-(void)showWebserviceFailureCaseMessage:(NSString *)errorTitle withErrorContent:(NSString *)errorDescription
{
    [appDelegate dismissGlobalHUD];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:errorTitle withDetails:errorDescription];
    });
}

- (void)connectionError
{
    [appDelegate dismissGlobalHUD];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"connectionErrorEvent" object:nil];
    [self showInternetNotConnectedError];
}

//MARK:- Splash Api To Check Facebook Button is hidden or not
-(void)facebookButtonHiddenStatus
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIP"],[_dictSettings objectForKey:@"Splash"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([[jsonResult objectForKey:@"IsFacebookHidden"] integerValue] == 1)
                    {
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"1" forKey:@"IsFacebookHidden"];
                        [prefs synchronize];
                    }
                    else if([[jsonResult objectForKey:@"IsFacebookHidden"] integerValue] == 0) {
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"0" forKey:@"IsFacebookHidden"];
                        [prefs synchronize];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"facebookButtonStatusEvent" object:nil];
                }
                else
                {
                    [appDelegate dismissGlobalHUD];
                    [self showErrorMessage:@"Server Error" withErrorContent:@""];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO CHECK ONLINE STATUS OF DJ BY USERID

-(void)checkOnlineStatusOfDj:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
//        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"CheckOnlineStatusOfDj"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = [[jsonResult objectForKey:@"isOnline"] boolValue];
                    
                    //USER IS A DJ
                    
                    NSArray *arrayDjApprovedClubIds = [jsonResult objectForKey:@"approved_clubs_ids"];
                    self.objLoggedInUser.arrayDjApprovedClubIds = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayDjApprovedClubIds.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayDjApprovedClubIds objectAtIndex:i];
                        
                        NSString *strDjApprovedClubId = [currentDictionary objectForKey:@"club_id"];
                        [self.objLoggedInUser.arrayDjApprovedClubIds addObject:strDjApprovedClubId];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"checkedOnlineStatusOfDjEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR USER LOGIN

-(void)userLogin:(User *)objUser
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"Login"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objUser.strEmail forKey:@"email"];
        [parameters setObject:objUser.strPassword forKey:@"password"];
        [parameters setObject:objUser.strDeviceToken forKey:@"device_id"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
        
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([[jsonResult objectForKey:@"isStripeConnected"] boolValue] == true)
                    {
                        if(self.objLoggedInUser == nil)
                        {
                            self.objLoggedInUser = [[User alloc] init];
                        }
                        
                        self.objLoggedInUser.strUserID = [jsonResult objectForKey:@"user_id"];
                        self.objLoggedInUser.strEmail = objUser.strEmail;
                        self.objLoggedInUser.strFullname = [jsonResult objectForKey:@"fullname"];
                        self.objLoggedInUser.strStateId = [jsonResult objectForKey:@"state_id"];
                        self.objLoggedInUser.strCityId = [jsonResult objectForKey:@"city_id"];
                        self.objLoggedInUser.strUserType = [jsonResult objectForKey:@"user_type"];
                        
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        
                        self.objLoggedInUser.boolIsOnline = [[jsonResult objectForKey:@"isOnline"] boolValue];
                        
                        //USER IS A DJ
                        
                        NSArray *arrayDjApprovedClubIds = [jsonResult objectForKey:@"approved_clubs_ids"];
                        self.objLoggedInUser.arrayDjApprovedClubIds = [[NSMutableArray alloc] init];
                        
                        for(int i = 0 ; i < arrayDjApprovedClubIds.count; i++)
                        {
                            NSDictionary *currentDictionary = [arrayDjApprovedClubIds objectAtIndex:i];
                            
                            NSString *strDjApprovedClubId = [currentDictionary objectForKey:@"club_id"];
                            [self.objLoggedInUser.arrayDjApprovedClubIds addObject:strDjApprovedClubId];
                        }
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"1" forKey:@"autologin"];
                        [prefs setObject:self.objLoggedInUser.strUserID forKey:@"userid"];
                        [prefs setObject:self.objLoggedInUser.strEmail forKey:@"useremail"];
                        [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
                        [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
                        [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
                        [prefs setObject:self.objLoggedInUser.strUserType forKey:@"usertype"];
                        [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedinEvent" object:nil];
                    }
                    else
                    {
                        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                        alertViewController.title = @"Stripe Not Connected";
                        alertViewController.message = @"You have not connected your stripe account yet. Please connect your stripe account to use our application.";
                        alertViewController.view.tintColor = [UIColor whiteColor];
                        alertViewController.backgroundTapDismissalGestureEnabled = YES;
                        alertViewController.swipeDismissalGestureEnabled = YES;
                        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                        
                        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                        
                        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Go to Stripe" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                            
                            [alertViewController dismissViewControllerAnimated:YES completion:nil];
                            
                            /*
                            LIVE: OLD
                            ca_AYa4lXZN4UitCyVGYSwfkyokYa7iUBJ6
                            
                            LIVE: 12 Mar, 21
                            ca_H90W3EE2QB6C59KEepdN6jvHjRCCSSaY
                            
                            TEST: 12 Mar, 21
                            ca_H90WjQuZmQckSBZdpXysG3vtY7i9iI20
                            */
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api/stripe/store_auth_dj.php&client_id=ca_H90WjQuZmQckSBZdpXysG3vtY7i9iI20&state=%@", [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api_test2/stripe/store_auth_dj.php&client_id=%@&state=%@", [MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@store_auth_dj.php&client_id=%@&state=%@",[_dictSettings objectForKey:@"StripeConnectURL"],[MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            
                            NSLog(@"Stripe connet url for dj : %@", webPageUrl);
                            
                            if ([[UIApplication sharedApplication] canOpenURL:webPageUrl])
                            {
                                [[UIApplication sharedApplication] openURL:webPageUrl];
                            }
                            
                        }]];
                        
                        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
                    }
                }
                else
                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"Login Failed" withErrorContent:message];
                    
                    NSString *message = @"Either your email or password is incorrect. Please try again with correct email and password.";
                    [self showWebserviceFailureCaseMessage:@"Login Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUBOWNER LOGIN

-(void)clubOwnerLogin:(User *)objUser
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"ClubOwnerLogin"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objUser.strEmail forKey:@"email"];
        [parameters setObject:objUser.strPassword forKey:@"password"];
        [parameters setObject:objUser.strDeviceToken forKey:@"device_id"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([[jsonResult objectForKey:@"isStripeConnected"] boolValue] == true)
                    {
                        if(self.objLoggedInUser == nil)
                        {
                            self.objLoggedInUser = [[User alloc] init];
                        }
                        
                        self.objLoggedInUser.strUserID = [jsonResult objectForKey:@"user_id"];
                        self.objLoggedInUser.strEmail = objUser.strEmail;
                        self.objLoggedInUser.strFullname = [jsonResult objectForKey:@"fullname"];
                        self.objLoggedInUser.strStateId = [jsonResult objectForKey:@"state_id"];
                        self.objLoggedInUser.strCityId = [jsonResult objectForKey:@"city_id"];
                        self.objLoggedInUser.strUserType = [jsonResult objectForKey:@"user_type"];
                        
                        //USER IS A CLUB OWNER
                        
                        NSArray *arrayClubOwnerApprovedDjs = [jsonResult objectForKey:@"club_owner_approved_djs"];
                        self.arrayClubOwnerApprovedDjs = [[NSMutableArray alloc] init];
                        
                        for(int i = 0 ; i < arrayClubOwnerApprovedDjs.count; i++)
                        {
                            NSDictionary *currentDictionary = [arrayClubOwnerApprovedDjs objectAtIndex:i];
                            
                            Dj *objDj = [[Dj alloc] init];
                            objDj.strDjID = [currentDictionary objectForKey:@"DjId"];
                            objDj.strDjName = [currentDictionary objectForKey:@"DjName"];
                            objDj.strDjClubId = [currentDictionary objectForKey:@"ClubId"];
                            objDj.strDjClubName = [currentDictionary objectForKey:@"DjClubName"];
                            
                            NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                            objDj.strDjProfilePictureImageUrl = [[currentDictionary objectForKey:@"DjProfilePictureImageUrl"] stringByAddingPercentEncodingWithAllowedCharacters:set];
                            
                            [self.arrayClubOwnerApprovedDjs addObject:objDj];
                        }
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"1" forKey:@"autologin"];
                        [prefs setObject:self.objLoggedInUser.strUserID forKey:@"userid"];
                        [prefs setObject:self.objLoggedInUser.strEmail forKey:@"useremail"];
                        [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
                        [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
                        [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
                        [prefs setObject:self.objLoggedInUser.strUserType forKey:@"usertype"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"clubOwnerLoggedinEvent" object:nil];
                    }
                    else
                    {
                        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                        alertViewController.title = @"Stripe Not Connected";
                        alertViewController.message = @"You have not connected your stripe account yet. Please connect your stripe account to use our application.";
                        alertViewController.view.tintColor = [UIColor whiteColor];
                        alertViewController.backgroundTapDismissalGestureEnabled = YES;
                        alertViewController.swipeDismissalGestureEnabled = YES;
                        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                        
                        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                        
                        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Go to Stripe" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                            
                            [alertViewController dismissViewControllerAnimated:YES completion:nil];
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api/stripe/store_auth_club.php&client_id=ca_AYa4lXZN4UitCyVGYSwfkyokYa7iUBJ6&state=%@", [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api_test2/stripe/store_auth_clubowner.php&client_id=%@&state=%@", [MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@store_auth_clubowner.php&client_id=%@&state=%@",[_dictSettings objectForKey:@"StripeConnectURL"],[MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            
                            NSLog(@"Stripe connet url for club : %@", webPageUrl);
                            
                            if ([[UIApplication sharedApplication] canOpenURL:webPageUrl])
                            {
                                [[UIApplication sharedApplication] openURL:webPageUrl];
                            }
                                                            
                        }]];
                        
                        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
                    }
                }
                else
                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"Login Failed" withErrorContent:message];
                    
                    NSString *message = @"Either your email or password is incorrect. Please try again with correct email and password.";
                    [self showWebserviceFailureCaseMessage:@"Login Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR USER LOGOUT

-(void)userLogout:(NSString *)strUserId withUserType:(NSString *)strUserType
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"Logout"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strUserType forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"userLoggedoutEvent" object:nil];
                }
                else
                {
                    [appDelegate dismissGlobalHUD];
                    [self showErrorMessage:@"Server Error" withErrorContent:@""];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR FACEBOOK USERS TO CHECK IF THEY ARE ALREADY REGISTERED

-(void)checkIfTheFacebookUserIsAlreadyRegistered:(User *)objUser
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"CheckIfTheFacebookUserIsAlreadyRegistered"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objUser.strEmail forKey:@"email"];
        [parameters setObject:objUser.strDeviceToken forKey:@"device_id"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([[jsonResult objectForKey:@"isStripeConnected"] boolValue] == true)
                    {
                        if(self.objLoggedInUser == nil)
                        {
                            self.objLoggedInUser = [[User alloc] init];
                        }
                        
                        self.objLoggedInUser.strUserID = [jsonResult objectForKey:@"user_id"];
                        self.objLoggedInUser.strEmail = objUser.strEmail;
                        self.objLoggedInUser.strFullname = [jsonResult objectForKey:@"fullname"];
                        self.objLoggedInUser.strStateId = [jsonResult objectForKey:@"state_id"];
                        self.objLoggedInUser.strCityId = [jsonResult objectForKey:@"city_id"];
                        self.objLoggedInUser.strUserType = @"1";
                        
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        
                        self.objLoggedInUser.boolIsOnline = [[jsonResult objectForKey:@"isOnline"] boolValue];
                        
                        NSArray *arrayDjApprovedClubIds = [jsonResult objectForKey:@"approved_clubs_ids"];
                        self.objLoggedInUser.arrayDjApprovedClubIds = [[NSMutableArray alloc] init];
                        
                        for(int i = 0 ; i < arrayDjApprovedClubIds.count; i++)
                        {
                            NSDictionary *currentDictionary = [arrayDjApprovedClubIds objectAtIndex:i];
                            
                            NSString *strDjApprovedClubId = [currentDictionary objectForKey:@"club_id"];
                            [self.objLoggedInUser.arrayDjApprovedClubIds addObject:strDjApprovedClubId];
                        }
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"1" forKey:@"autologin"];
                        [prefs setObject:self.objLoggedInUser.strUserID forKey:@"userid"];
                        [prefs setObject:self.objLoggedInUser.strEmail forKey:@"useremail"];
                        [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
                        [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
                        [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
                        [prefs setObject:self.objLoggedInUser.strUserType forKey:@"usertype"];
                        [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"checkedIfTheFacebookUserIsAlreadyRegisteredEvent" object:nil];
                    }
                    else
                    {
                        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                        alertViewController.title = @"Stripe Not Connected";
                        alertViewController.message = @"You have not connected your stripe account yet. Please connect your stripe account to use our application.";
                        alertViewController.view.tintColor = [UIColor whiteColor];
                        alertViewController.backgroundTapDismissalGestureEnabled = YES;
                        alertViewController.swipeDismissalGestureEnabled = YES;
                        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                        
                        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                        
                        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Go to Stripe" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                            
                            [alertViewController dismissViewControllerAnimated:YES completion:nil];
                            
                            /*
                            //LIVE: OLD
                            ca_AYa4lXZN4UitCyVGYSwfkyokYa7iUBJ6
                            
                            //LIVE: 12 Mar, 21
                            ca_H90W3EE2QB6C59KEepdN6jvHjRCCSSaY
                            
                            //TEST: 12 Mar, 21
                            ca_H90WjQuZmQckSBZdpXysG3vtY7i9iI20
                            */
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api/stripe/store_auth_dj.php&client_id=ca_H90WjQuZmQckSBZdpXysG3vtY7i9iI20&state=%@", [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api_test2/stripe/store_auth_dj.php&client_id=%@&state=%@", [MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@store_auth_dj.php&client_id=%@&state=%@",[_dictSettings objectForKey:@"StripeConnectURL"],[MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            
                            NSLog(@"Stripe connet url for dj : %@", webPageUrl);
                            
                            if ([[UIApplication sharedApplication] canOpenURL:webPageUrl])
                            {
                                [[UIApplication sharedApplication] openURL:webPageUrl];
                            }
                            
                        }]];
                        
                        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
                    }
                }
                else
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"checkedIfTheFacebookUserIsAlreadyRegisteredEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL STATES

-(void)getAllStates
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllStates"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                NSArray *arrayStateList = [jsonResult objectForKey:@"states"];
                
                self.arrayStates = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayStateList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayStateList objectAtIndex:i];
                    
                    State *objState = [[State alloc] init];
                    objState.strStateID = [currentDictionary objectForKey:@"StateId"];
                    objState.strStateName = [currentDictionary objectForKey:@"StateName"];
                    
                    NSArray *arrayCityList = [currentDictionary objectForKey:@"Cities"];
                    
                    objState.arrayCity = [[NSMutableArray alloc] init];
                    
                    for(int j = 0 ; j < arrayCityList.count; j++)
                    {
                        NSDictionary *currentDictionaryCity = [arrayCityList objectAtIndex:j];
                        
                        City *objCity = [[City alloc] init];
                        objCity.strCityID = [currentDictionaryCity objectForKey:@"CityId"];
                        objCity.strCityName = [currentDictionaryCity objectForKey:@"CityName"];
                        
                        [objState.arrayCity addObject:objCity];
                    }
                    
                    [self.arrayStates addObject:objState];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllStatesEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR USER SIGNUP

-(void)userSignUp:(User *)objUser
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"Signup"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objUser.strEmail forKey:@"email"];
        [parameters setObject:objUser.strPassword forKey:@"password"];
        [parameters setObject:objUser.strFullname forKey:@"fullname"];
        [parameters setObject:[objUser.strGender lowercaseString] forKey:@"gender"];
        [parameters setObject:objUser.strPhoneNumber forKey:@"phone_number"];
        [parameters setObject:objUser.strStateId forKey:@"state_id"];
        [parameters setObject:objUser.strStateName forKey:@"state_name"];
        [parameters setObject:objUser.strCityId forKey:@"city_id"];
        [parameters setObject:objUser.strCityName forKey:@"city_name"];
        [parameters setObject:objUser.strDeviceToken forKey:@"device_id"];
        [parameters setObject:objUser.strUserType forKey:@"user_type"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    self.strSignedUpDjEmail = objUser.strEmail;
                    self.strSignedUpDjPassword = objUser.strPassword;
                    
                    /*
                    self.strSignedUpDjStripeConnectUrl = [NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api/stripe/store_auth_dj.php&client_id=ca_AYa4lXZN4UitCyVGYSwfkyokYa7iUBJ6&state=%@", [jsonResult objectForKey:@"user_id"]];
                    */
                    
                    /*
                    self.strSignedUpDjStripeConnectUrl = [NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api_test2/stripe/store_auth_dj.php&client_id=%@&state=%@",[MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]];
                    */
                    
                    self.strSignedUpDjStripeConnectUrl = [NSString stringWithFormat:@"%@store_auth_dj.php&client_id=%@&state=%@",[_dictSettings objectForKey:@"StripeConnectURL"], [MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]];
                    
                    NSLog(@"Stripe connect url for DJ : %@",self.strSignedUpDjStripeConnectUrl);
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"userSignedUpEvent" object:nil];
                    
//                    if(self.objLoggedInUser == nil)
//                    {
//                        self.objLoggedInUser = [[User alloc] init];
//                    }
//                    
//                    self.objLoggedInUser.strUserID = [jsonResult objectForKey:@"user_id"];
//                    self.objLoggedInUser.strEmail = objUser.strEmail;
//                    self.objLoggedInUser.strFullname = objUser.strFullname;
//                    self.objLoggedInUser.strStateId = objUser.strStateId;
//                    self.objLoggedInUser.strCityId = objUser.strCityId;
//                    self.objLoggedInUser.strUserType = objUser.strUserType;
////                    self.objLoggedInUser.strProfilePictureImageUrl = @"";
//                    self.objLoggedInUser.strProfilePictureImageUrl = [NSString stringWithFormat:@"%@/uploads/dj_images/default_dj.jpg", [_dictSettings objectForKey:@"ServerIPForImages"]];
//                    self.objLoggedInUser.boolIsOnline = false;
//                                        
//                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//                    [prefs setObject:@"1" forKey:@"autologin"];
//                    [prefs setObject:self.objLoggedInUser.strUserID forKey:@"userid"];
//                    [prefs setObject:self.objLoggedInUser.strEmail forKey:@"useremail"];
//                    [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
//                    [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
//                    [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
//                    [prefs setObject:self.objLoggedInUser.strUserType forKey:@"usertype"];
//                    [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
//                    [prefs synchronize];
//                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"userSignedUpEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"Registration Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

-(void)displayErrorForSignup:(NSString *)strError
{
    [appDelegate dismissGlobalHUD];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Registration Failed" withDetails:strError];
    });
}

#pragma mark - FUNCTION FOR CLUB OWNER SIGNUP

-(void)clubOwnerSignUp:(User *)objUser
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"ClubOwnerSignup"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objUser.strEmail forKey:@"email"];
        [parameters setObject:objUser.strPassword forKey:@"password"];
        [parameters setObject:objUser.strFullname forKey:@"fullname"];
        [parameters setObject:[objUser.strGender lowercaseString] forKey:@"gender"];
        [parameters setObject:objUser.strPhoneNumber forKey:@"phone_number"];
        [parameters setObject:objUser.strStateId forKey:@"state_id"];
        [parameters setObject:objUser.strStateName forKey:@"state_name"];
        [parameters setObject:objUser.strCityId forKey:@"city_id"];
        [parameters setObject:objUser.strCityName forKey:@"city_name"];
        [parameters setObject:objUser.strDeviceToken forKey:@"device_id"];
        [parameters setObject:objUser.strUserType forKey:@"user_type"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.strUserID = [jsonResult objectForKey:@"user_id"];
                    self.objLoggedInUser.strEmail = objUser.strEmail;
                    self.objLoggedInUser.strFullname = objUser.strFullname;
                    self.objLoggedInUser.strStateId = objUser.strStateId;
                    self.objLoggedInUser.strCityId = objUser.strCityId;
                    self.objLoggedInUser.strUserType = objUser.strUserType;
                    self.objLoggedInUser.strProfilePictureImageUrl = @"";
                    self.objLoggedInUser.arrayDjApprovedClubIds = [[NSMutableArray alloc] init];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setObject:@"1" forKey:@"autologin"];
                    [prefs setObject:self.objLoggedInUser.strUserID forKey:@"userid"];
                    [prefs setObject:self.objLoggedInUser.strEmail forKey:@"useremail"];
                    [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
                    [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
                    [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
                    [prefs setObject:self.objLoggedInUser.strUserType forKey:@"usertype"];
                    [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
                    [prefs synchronize];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"clubOwnerSignedUpEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"Registration Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR FORGET PASSWORD

-(void)forgetPassword:(NSString *)strEmail withUserType:(NSString *)strUserType
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"ForgetPassword"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strEmail forKey:@"email"];
        [parameters setObject:strUserType forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showErrorMessage:@"" withErrorContent:message];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"forgotPasswordEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET SPONSORS ADVERTISEMENTS IMAGES BY USER ID

-(void)getSponsorsAdvertisementsImagesByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetSponsorsAdvertisementsImages"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"dj_id"];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        [parameters setObject:strUserType forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    NSArray *arraySponsorsAdvertisementsImagesList = [jsonResult objectForKey:@"advertisements"];
                    
                    self.arraySponsorsAdvertisementsImages = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arraySponsorsAdvertisementsImagesList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arraySponsorsAdvertisementsImagesList objectAtIndex:i];
                        
                        Advertisement *objAdvertisement = [[Advertisement alloc] init];
                        objAdvertisement.strAdvertisementId = [currentDictionary objectForKey:@"AdvertisementId"];
                        objAdvertisement.strAdvertisementTitle = [currentDictionary objectForKey:@"AdvertisementTitle"];
                        objAdvertisement.strAdvertisementTableName = [currentDictionary objectForKey:@"AdvertisementTableName"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objAdvertisement.strAdvertisementImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"AdvertisementImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objAdvertisement.strAdvertisementType = [currentDictionary objectForKey:@"AdvertisementType"];
                        objAdvertisement.strAdvertisementRedirectionUrl = [[currentDictionary objectForKey:@"AdvertisementRedirectionUrl"]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        [self.arraySponsorsAdvertisementsImages addObject:objAdvertisement];
                    }
                    
                    for (int i = 0; i < self.arraySponsorsAdvertisementsImages.count; i++)
                    {
                        int randomInt1 = arc4random() % [self.arraySponsorsAdvertisementsImages count];
                        int randomInt2 = arc4random() % [self.arraySponsorsAdvertisementsImages count];
                        [self.arraySponsorsAdvertisementsImages exchangeObjectAtIndex:randomInt1 withObjectAtIndex:randomInt2];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotSponsorsAdvertisementsImagesByUserIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"Server Error" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO CHANGE STATUS BY USER ID

-(void)changeStatus:(NSString *)strStatus withDjUserId:(NSString *)strUserId withClubId:(NSString *)strClubId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"ChangeStatus"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strStatus forKey:@"status"];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strClubId forKey:@"club_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([strStatus isEqualToString:@"offline"])
                    {
                        if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
                        {
                            [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
                        }
                        
                        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = false;
                        
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineClubId = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineClubName = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistId = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistName = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineClubSetWithPresetPlaylist = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineAtEvent = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventName = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventPlaylistName = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventPlaylistId = nil;
                    }
                    else if([strStatus isEqualToString:@"online"])
                    {
                        if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
                        {
                            [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
                        }
                        
                        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = true;
                        
//                        [self showErrorMessage:@"Request Sent" withErrorContent:@"Your request has been sent to club owner successfully. You will see yourself online in the system only when club owner will approve your request"];
                        
                        [self showErrorMessage:@"Status Changed" withErrorContent:@"You are online now."];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedStatusEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO CHANGE ONLINE PLAYLLIST BY USER ID

-(void)changeOnlinePlaylist:(NSString *)strUserId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"ChangeOnlinePlaylist"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    self.objLoggedInUser.strOnlinePlaylistId = strPlaylistId;
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedOnlinePlaylistEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL CLUBS

-(void)getAllClubs:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllClubs"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    NSArray *arrayDjApprovedClubIds = [jsonResult objectForKey:@"approved_clubs_ids"];
                    self.objLoggedInUser.arrayDjApprovedClubIds = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayDjApprovedClubIds.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayDjApprovedClubIds objectAtIndex:i];
                        
                        NSString *strDjApprovedClubId = [currentDictionary objectForKey:@"club_id"];
                        [self.objLoggedInUser.arrayDjApprovedClubIds addObject:strDjApprovedClubId];
                    }
                    
                    //========== FILL CLUBS ARRAY ==========//
                    NSArray *arrayClubList = [jsonResult objectForKey:@"clubs"];
                    
                    self.arrayAllClubs = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayClubList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayClubList objectAtIndex:i];
                        
                        Club *objClub = [[Club alloc] init];
                        objClub.strClubID = [currentDictionary objectForKey:@"ClubId"];
                        objClub.strClubName = [currentDictionary objectForKey:@"ClubName"];
                        objClub.strClubType = [currentDictionary objectForKey:@"ClubType"];
                        objClub.strClubAddress = [currentDictionary objectForKey:@"ClubAddress"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objClub.strClubImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ClubImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objClub.strClubBannerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ClubBannerUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objClub.strClubStateID = [currentDictionary objectForKey:@"ClubStateId"];
                        objClub.strClubCityID = [currentDictionary objectForKey:@"ClubCityId"];
                        
                        NSLog(@"objClub.strClubImageUrl : %@", objClub.strClubImageUrl);
                        NSLog(@"objClub.strClubBannerImageUrl : %@", objClub.strClubBannerImageUrl);
                        
                        [self.arrayAllClubs addObject:objClub];
                    }
                    
                    //========== FILL STATES ARRAY ==========//
                    NSArray *arrayStateList = [jsonResult objectForKey:@"states"];
                    
                    self.arrayStates = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayStateList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayStateList objectAtIndex:i];
                        
                        State *objState = [[State alloc] init];
                        objState.strStateID = [currentDictionary objectForKey:@"StateId"];
                        objState.strStateName = [currentDictionary objectForKey:@"StateName"];
                        
                        NSArray *arrayCityList = [currentDictionary objectForKey:@"Cities"];
                        
                        objState.arrayCity = [[NSMutableArray alloc] init];
                        
                        for(int j = 0 ; j < arrayCityList.count; j++)
                        {
                            NSDictionary *currentDictionaryCity = [arrayCityList objectAtIndex:j];
                            
                            City *objCity = [[City alloc] init];
                            objCity.strCityID = [currentDictionaryCity objectForKey:@"CityId"];
                            objCity.strCityName = [currentDictionaryCity objectForKey:@"CityName"];
                            
                            [objState.arrayCity addObject:objCity];
                        }
                        
                        [self.arrayStates addObject:objState];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllClubsEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL APPROVED CLUBS

-(void)getAllApprovedClubs:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllApprovedClubs"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                    //========== FILL APPROVED CLUBS ARRAY ==========//
                    NSArray *arrayClubList = [jsonResult objectForKey:@"clubs"];
                    
                    self.arrayAllApprovedClubsByUserId = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayClubList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayClubList objectAtIndex:i];
                        
                        Club *objClub = [[Club alloc] init];
                        objClub.strClubID = [currentDictionary objectForKey:@"ClubId"];
                        objClub.strClubName = [currentDictionary objectForKey:@"ClubName"];
                        objClub.strClubType = [currentDictionary objectForKey:@"ClubType"];
                        objClub.strClubAddress = [currentDictionary objectForKey:@"ClubAddress"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objClub.strClubImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ClubImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objClub.strClubBannerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ClubBannerUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objClub.strClubStateID = [currentDictionary objectForKey:@"ClubStateId"];
                        objClub.strClubCityID = [currentDictionary objectForKey:@"ClubCityId"];
                        
                        [self.arrayAllApprovedClubsByUserId addObject:objClub];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllApprovedClubsEvent" object:nil];
//                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO ADD CLUBS FOR DJ BY USER ID AND CLUBS IDS

-(void)addClubs:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AddClubs"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"array_club_ids_for_approval"] forKey:@"array_club_ids_for_approval"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedClubsEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO REMOVE APPROVED CLUBS FOR DJ BY USER ID AND CLUBS IDS

-(void)removeApprovedClubs:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"RemoveApprovedClubs"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"array_club_ids_for_removal"] forKey:@"array_club_ids_for_removal"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL APPROVED CLUBS ARRAY ==========//
                    NSArray *arrayClubList = [jsonResult objectForKey:@"clubs"];
                    
                    self.arrayAllApprovedClubsByUserId = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayClubList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayClubList objectAtIndex:i];
                        
                        Club *objClub = [[Club alloc] init];
                        objClub.strClubID = [currentDictionary objectForKey:@"ClubId"];
                        objClub.strClubName = [currentDictionary objectForKey:@"ClubName"];
                        objClub.strClubType = [currentDictionary objectForKey:@"ClubType"];
                        objClub.strClubAddress = [currentDictionary objectForKey:@"ClubAddress"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objClub.strClubImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ClubImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objClub.strClubBannerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ClubBannerUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objClub.strClubStateID = [currentDictionary objectForKey:@"ClubStateId"];
                        objClub.strClubCityID = [currentDictionary objectForKey:@"ClubCityId"];
                        
                        [self.arrayAllApprovedClubsByUserId addObject:objClub];
                    }
                    
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.arrayDjApprovedClubIds = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < self.arrayAllApprovedClubsByUserId.count; i++)
                    {
                        Club *objClub = [self.arrayAllApprovedClubsByUserId objectAtIndex:i];
                        [self.objLoggedInUser.arrayDjApprovedClubIds addObject:objClub.strClubID];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"removedApprovedClubsEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET USER PROFILE DETAILS BY USER ID

-(void)getUserProfileByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetUserProfileByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    self.strSideMenuBackgroundImageUrl = [[jsonResult objectForKey:@"sidemenubackgroundimageurl"] stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
//                    self.strSideMenuBackgroundImageUrl = [[jsonResult objectForKey:@"sidemenubackgroundimageurl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    self.objLoggedInUser.strFullname = [jsonResult objectForKey:@"fullname"];
                    self.objLoggedInUser.strEmail = [jsonResult objectForKey:@"useremail"];
                    self.objLoggedInUser.strPhoneNumber = [jsonResult objectForKey:@"phone_number"];
                    self.objLoggedInUser.strGender = [jsonResult objectForKey:@"gender"];
                    self.objLoggedInUser.strStateId = [jsonResult objectForKey:@"state_id"];
                    self.objLoggedInUser.strStateName = [jsonResult objectForKey:@"state_name"];
                    self.objLoggedInUser.strCityId = [jsonResult objectForKey:@"city_id"];
                    self.objLoggedInUser.strCityName = [jsonResult objectForKey:@"city_name"];
                    self.objLoggedInUser.strUserType = [jsonResult objectForKey:@"user_type"];
                    
                    self.objLoggedInUser.boolIsOnline = [[jsonResult objectForKey:@"isOnline"] boolValue];
                    self.objLoggedInUser.strOnlineClubId = [jsonResult objectForKey:@"online_club_id"];
                    self.objLoggedInUser.strOnlineClubName = [jsonResult objectForKey:@"online_club_name"];
                    self.objLoggedInUser.strOnlinePlaylistId = [jsonResult objectForKey:@"online_playlist_id"];
                    self.objLoggedInUser.strOnlinePlaylistName = [jsonResult objectForKey:@"online_playlist_name"];
                    self.objLoggedInUser.boolIsOnlineClubSetWithPresetPlaylist = [[jsonResult objectForKey:@"isOnlineClubSetWithPresetPlaylist"] boolValue];
                    self.objLoggedInUser.strOnlineEventId = [jsonResult objectForKey:@"online_event_id"];
                    self.objLoggedInUser.strOnlineEventName = [jsonResult objectForKey:@"online_event_name"];
                    
                    self.objLoggedInUser.strFacebookProfileUrl = [jsonResult objectForKey:@"facebook_profile_url"];
                    self.objLoggedInUser.strTwitterProfileUrl = [jsonResult objectForKey:@"twitter_profile_url"];
                    self.objLoggedInUser.strInstagramProfileUrl = [jsonResult objectForKey:@"instagram_profile_url"];
                    self.objLoggedInUser.strGooglePlusProfileUrl = [jsonResult objectForKey:@"googleplus_profile_url"];
                    
                    self.objLoggedInUser.strTwitterInstagramMesasage = [jsonResult objectForKey:@"twitter_instagram_online_message"];
                    
                    //========== FILL CLUBS ARRAY ==========//
                    NSArray *arrayDjApprovedClubList = [jsonResult objectForKey:@"approved_clubs"];
                    
                    self.objLoggedInUser.arrayDjApprovedClubs = [[NSMutableArray alloc] init];
                    
                    self.objLoggedInUser.arrayDjApprovedClubIds = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayDjApprovedClubList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayDjApprovedClubList objectAtIndex:i];
                        
                        NSString *strDjApprovedClubId = [currentDictionary objectForKey:@"ClubId"];
                        [self.objLoggedInUser.arrayDjApprovedClubIds addObject:strDjApprovedClubId];
                        
                        Club *objClub = [[Club alloc] init];
                        objClub.strClubID = [currentDictionary objectForKey:@"ClubId"];
                        objClub.strClubName = [currentDictionary objectForKey:@"ClubName"];
                        objClub.strClubType = [currentDictionary objectForKey:@"ClubType"];
                        objClub.strClubAddress = [currentDictionary objectForKey:@"ClubAddress"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objClub.strClubImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ClubImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objClub.strClubBannerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ClubBannerUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objClub.strClubStateID = [currentDictionary objectForKey:@"ClubStateId"];
                        objClub.strClubCityID = [currentDictionary objectForKey:@"ClubCityId"];
                        
                        objClub.strClubPlaylistID = [currentDictionary objectForKey:@"ClubPlaylistId"];
                        objClub.strClubPlaylistName = [currentDictionary objectForKey:@"ClubPlaylistName"];
                        
                        [self.objLoggedInUser.arrayDjApprovedClubs addObject:objClub];
                    }
                    
                    //========== FILL PLAYLISTS ARRAY ==========//
                    NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                    
                    self.objLoggedInUser.arrayDjPlaylists = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                        
                        Playlist *objPlaylist = [[Playlist alloc] init];
                        objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                        objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                        
                        objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                        
                        NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                        
                        for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                        {
                            NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                            
                            Song *objSong = [[Song alloc] init];
                            
                            objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                            objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                            objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                            
                            [objPlaylist.arrayPlaylistSongs addObject:objSong];
                        }
                        
                        [self.objLoggedInUser.arrayDjPlaylists addObject:objPlaylist];
                    }
                    
                    //========== FILL STATES ARRAY ==========//
                    NSArray *arrayStateList = [jsonResult objectForKey:@"states"];
                    
                    self.arrayStates = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayStateList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayStateList objectAtIndex:i];
                        
                        State *objState = [[State alloc] init];
                        objState.strStateID = [currentDictionary objectForKey:@"StateId"];
                        objState.strStateName = [currentDictionary objectForKey:@"StateName"];
                        
                        NSArray *arrayCityList = [currentDictionary objectForKey:@"Cities"];
                        
                        objState.arrayCity = [[NSMutableArray alloc] init];
                        
                        for(int j = 0 ; j < arrayCityList.count; j++)
                        {
                            NSDictionary *currentDictionaryCity = [arrayCityList objectAtIndex:j];
                            
                            City *objCity = [[City alloc] init];
                            objCity.strCityID = [currentDictionaryCity objectForKey:@"CityId"];
                            objCity.strCityName = [currentDictionaryCity objectForKey:@"CityName"];
                            
                            [objState.arrayCity addObject:objCity];
                        }
                        
                        [self.arrayStates addObject:objState];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotUserProfileByUserIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO UPDATE USER PROFILE DETAILS BY USER ID

-(void)updateUserProfile:(NSDictionary *)dictParameters;
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"UpdateUserProfile"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"fullname"] forKey:@"fullname"];
        [parameters setObject:[[dictParameters objectForKey:@"gender"] lowercaseString] forKey:@"gender"];
        [parameters setObject:[dictParameters objectForKey:@"phone_number"] forKey:@"phone_number"];
        [parameters setObject:[dictParameters objectForKey:@"state_id"] forKey:@"state_id"];
        [parameters setObject:[dictParameters objectForKey:@"state_name"] forKey:@"state_name"];
        [parameters setObject:[dictParameters objectForKey:@"city_id"] forKey:@"city_id"];
        [parameters setObject:[dictParameters objectForKey:@"city_name"] forKey:@"city_name"];
        [parameters setObject:[dictParameters objectForKey:@"user_type"] forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.strFullname = [dictParameters objectForKey:@"fullname"];
                    self.objLoggedInUser.strPhoneNumber = [dictParameters objectForKey:@"phone_number"];
                    self.objLoggedInUser.strGender = [dictParameters objectForKey:@"gender"];
                    self.objLoggedInUser.strStateId = [dictParameters objectForKey:@"state_id"];
                    self.objLoggedInUser.strStateName = [dictParameters objectForKey:@"state_name"];
                    self.objLoggedInUser.strCityId = [dictParameters objectForKey:@"city_id"];
                    self.objLoggedInUser.strCityName = [dictParameters objectForKey:@"city_name"];
                    self.objLoggedInUser.strUserType = [dictParameters objectForKey:@"user_type"];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
                    [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
                    [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
                    [prefs synchronize];
                    
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedUserProfileEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO UPLOAD USER PROFILE PICTURE WITH USER ID

-(void)uploadProfilePicture:(NSString *)strProfilePictureBase64Data
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strLoggedInUserId = [prefs objectForKey:@"userid"];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"UploadProfileImage"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strLoggedInUserId forKey:@"user_id"];
        [parameters setObject:strProfilePictureBase64Data forKey:@"user_profile_picture_data"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
                    [prefs synchronize];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"uploadedProfilePictureEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO UPLOAD USER PROFILE PICTURE WITH USER ID WITH $_FILE

-(void)uploadProfilePictureWith_File:(NSData *)profilePictureData
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strLoggedInUserId = [prefs objectForKey:@"userid"];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"UploadProfileImage"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strLoggedInUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        manager.requestSerializer.timeoutInterval = 60000;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        
        [manager POST:urlString parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileData:profilePictureData name:@"user_profile_picture_data" fileName:[NSString stringWithFormat:@"%@.png", strLoggedInUserId] mimeType:@"image/png"];
            
        } progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
                    [prefs synchronize];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"uploadedProfilePictureEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO UPDATE USER SOCIAL MEDIA PROFILE URL BY USER ID

-(void)updateUserSocialMediaProfileUrl:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"UpdateUserSocialMediaProfileUrl"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"social_media_profile_url"] forKey:@"social_media_profile_url"];
        [parameters setObject:[dictParameters objectForKey:@"social_media_profile_type"] forKey:@"social_media_profile_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                    
                    if([[dictParameters objectForKey:@"social_media_profile_type"] isEqualToString:@"facebook"])
                    {
                        self.objLoggedInUser.strFacebookProfileUrl = [dictParameters objectForKey:@"social_media_profile_url"];
                    }
                    else if([[dictParameters objectForKey:@"social_media_profile_type"] isEqualToString:@"twitter"])
                    {
                        self.objLoggedInUser.strTwitterProfileUrl = [dictParameters objectForKey:@"social_media_profile_url"];
                    }
                    else if([[dictParameters objectForKey:@"social_media_profile_type"] isEqualToString:@"instagram"])
                    {
                        self.objLoggedInUser.strInstagramProfileUrl = [dictParameters objectForKey:@"social_media_profile_url"];
                    }
                    else if([[dictParameters objectForKey:@"social_media_profile_type"] isEqualToString:@"googleplus"])
                    {
                        self.objLoggedInUser.strGooglePlusProfileUrl = [dictParameters objectForKey:@"social_media_profile_url"];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedUserSocialMediaProfileUrlEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO CHANGE PASSWORD

-(void)changePassword:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"ChangePassword"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"strOldPassword"] forKey:@"oldpassword"];
        [parameters setObject:[dictParameters objectForKey:@"strNewPassword"] forKey:@"newpassword"];
        [parameters setObject:[dictParameters objectForKey:@"strRepeatPassword"] forKey:@"repeatpassword"];
        [parameters setObject:[dictParameters objectForKey:@"strUserType"] forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedPasswordEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL PLAYLISTS BY USER ID

-(void)getAllPlaylistsByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllPlaylistsByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
        
                NSDictionary *jsonResult = responseObject;
                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                    //========== FILL PLAYLISTS ARRAY ==========//
                    NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                    
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.arrayDjPlaylists = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                        
                        Playlist *objPlaylist = [[Playlist alloc] init];
                        objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                        objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                        
                        objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                        
                        NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                        
                        for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                        {
                            NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                            
                            Song *objSong = [[Song alloc] init];
                            
                            objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                            objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                            objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                            
                            [objPlaylist.arrayPlaylistSongs addObject:objSong];
                        }
                        
                        [self.objLoggedInUser.arrayDjPlaylists addObject:objPlaylist];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllPlaylistsByUserIdEvent" object:nil];
//                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO DELETE PLAYLIST BY USER ID AND PLAYLIST ID

-(void)deletePlaylistByUserId:(NSString *)strUserId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"DeletePlaylistByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL PLAYLISTS ARRAY ==========//
                    NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                    
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.arrayDjPlaylists = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                        
                        Playlist *objPlaylist = [[Playlist alloc] init];
                        objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                        objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                        
                        objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                        
                        NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                        
                        for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                        {
                            NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                            
                            Song *objSong = [[Song alloc] init];
                            
                            objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                            objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                            objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                            
                            [objPlaylist.arrayPlaylistSongs addObject:objSong];
                        }
                        
                        [self.objLoggedInUser.arrayDjPlaylists addObject:objPlaylist];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedPlaylistByUserIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO DELETE SONG BY USER ID AND SONG ID

-(void)deleteSongByUserId:(NSString *)strUserId withSongId:(NSString *)strSongId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"DeleteSongByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strSongId forKey:@"song_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedSongByUserIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO ADD SONG

-(void)addSong:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AddSong"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistName"] forKey:@"playlist_name"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistID"] forKey:@"playlist_id"];
        [parameters setObject:[dictParameters objectForKey:@"strSongName"] forKey:@"song_name"];
        [parameters setObject:[dictParameters objectForKey:@"strArtistName"] forKey:@"artist_name"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedSongEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO ADD PLAYLIST

-(void)addPlaylist:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AddPlaylist"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistName"] forKey:@"playlist_name"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedPlaylistEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL UNACCEPTED REQUESTS LIST BY USER ID

-(void)getAllUnacceptedRequestsByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllUnacceptedRequestsByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                    //========== FILL SONG REQUESTS ARRAY ==========//
                    NSArray *arraySongRequestsList = [jsonResult objectForKey:@"song_requests"];
                    
                    self.arrayDjSongsRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arraySongRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arraySongRequestsList objectAtIndex:i];
                        
                        SongRequest *objSongRequest = [[SongRequest alloc] init];
                        objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                        objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                        objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                        objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                        objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                        objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                        objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                        objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestDjAmount"];
                        
                        if(!objSongRequest.boolIsSongRequestExpired)
                        {
                            [self.arrayDjSongsRequests addObject:objSongRequest];
                        }
                    }
                    
                    //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                    NSArray *arrayShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                    
                    self.arrayDjShoutoutRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayShoutoutRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayShoutoutRequestsList objectAtIndex:i];
                        
                        ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                        objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                        objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                        objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                        objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                        objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                        objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                        objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                        objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestDjAmount"];
                        
                        if(!objShoutoutRequest.boolIsShoutoutRequestExpired)
                        {
                            [self.arrayDjShoutoutRequests addObject:objShoutoutRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllUnacceptedRequestsByUserIdEvent" object:nil];
//                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL ACCEPTED REQUESTS LIST BY USER ID

-(void)getAllAcceptedRequestsByUserId:(NSString *)strUserId;
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllAcceptedRequestsByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                    //========== FILL SONG REQUESTS ARRAY ==========//
                    NSArray *arrayDjAcceptedSongRequestsList = [jsonResult objectForKey:@"song_requests"];
                    
                    self.arrayDjAcceptedSongsRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayDjAcceptedSongRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayDjAcceptedSongRequestsList objectAtIndex:i];
                        
                        SongRequest *objSongRequest = [[SongRequest alloc] init];
                        objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                        objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                        objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                        objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                        objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                        objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                        objSongRequest.strSongRequestStatus = [currentDictionary objectForKey:@"SongRequestStatus"];
                        objSongRequest.strSongRequestAcceptanceType = [currentDictionary objectForKey:@"SongRequestAcceptanceType"];
                        objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                        objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestDjAmount"];
                        
                        objSongRequest.strSongRequestDjName = [currentDictionary objectForKey:@"SongRequestDjName"];
                        objSongRequest.strSongRequestClubName = [currentDictionary objectForKey:@"SongRequestClubName"];
                        objSongRequest.strSongRequestUserName = [currentDictionary objectForKey:@"SongRequestUserName"];
                        
                        if(!objSongRequest.boolIsSongRequestExpired)
                        {
                            [self.arrayDjAcceptedSongsRequests addObject:objSongRequest];
                        }
                    }
                    
                    //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                    NSArray *arrayDjAcceptedShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                    
                    self.arrayDjAcceptedShoutoutRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayDjAcceptedShoutoutRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayDjAcceptedShoutoutRequestsList objectAtIndex:i];
                        
                        ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                        objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                        objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                        objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                        objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                        objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                        objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                        objShoutoutRequest.strShoutoutRequestStatus = [currentDictionary objectForKey:@"ShoutoutRequestStatus"];
                        objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                        objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestDjAmount"];
                        
                        objShoutoutRequest.strShoutoutRequestDjName = [currentDictionary objectForKey:@"ShoutoutRequestDjName"];
                        objShoutoutRequest.strShoutoutRequestClubName = [currentDictionary objectForKey:@"ShoutoutRequestClubName"];
                        objShoutoutRequest.strShoutoutRequestUserName = [currentDictionary objectForKey:@"ShoutoutRequestUserName"];
                        
                        if(!objShoutoutRequest.boolIsShoutoutRequestExpired)
                        {
                            [self.arrayDjAcceptedShoutoutRequests addObject:objShoutoutRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllAcceptedRequestsByUserIdEvent" object:nil];
//                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL PLAYED REQUESTS LIST BY USER ID

-(void)getAllPlayedRequestsByUserId:(NSString *)strUserId;
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllPlayedRequestsByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                //========== FILL SONG REQUESTS ARRAY ==========//
                NSArray *arrayDjPlayedSongRequestsList = [jsonResult objectForKey:@"song_requests"];
                
                self.arrayDjPlayedSongsRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayDjPlayedSongRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayDjPlayedSongRequestsList objectAtIndex:i];
                    
                    SongRequest *objSongRequest = [[SongRequest alloc] init];
                    objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                    objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                    objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                    objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                    objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                    objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                    objSongRequest.strSongRequestStatus = [currentDictionary objectForKey:@"SongRequestStatus"];
                    objSongRequest.strSongRequestAcceptanceType = [currentDictionary objectForKey:@"SongRequestAcceptanceType"];
                    objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                    objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestDjAmount"];
                    
                    [self.arrayDjPlayedSongsRequests addObject:objSongRequest];
                }
                
                //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                NSArray *arrayDjPlayedShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                
                self.arrayDjPlayedShoutoutRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayDjPlayedShoutoutRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayDjPlayedShoutoutRequestsList objectAtIndex:i];
                    
                    ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                    objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                    objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                    objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                    objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                    objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                    objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                    objShoutoutRequest.strShoutoutRequestStatus = [currentDictionary objectForKey:@"ShoutoutRequestStatus"];
                    objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                    objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestDjAmount"];
                    
                    [self.arrayDjPlayedShoutoutRequests addObject:objShoutoutRequest];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllPlayedRequestsByUserIdEvent" object:nil];
//                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR DJS TO ACCEPT SONG REQUEST

-(void)acceptSongRequest:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AcceptSongRequest"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"songrequest_id"] forKey:@"songrequest_id"];
        [parameters setObject:[dictParameters objectForKey:@"songrequest_acceptance_type"] forKey:@"songrequest_acceptance_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([[dictParameters objectForKey:@"songrequest_acceptance_type"] isEqualToString:[MySingleton sharedManager].strDjSongRequestAcceptanceTypeWithinNext3Songs])
                    {
                        [MySingleton sharedManager].dataManager.strDjSongRequestAcceptanceTypeMessage = @"You have accepted this song request to be played within next 3 songs";
                    }
                    else if([[dictParameters objectForKey:@"songrequest_acceptance_type"] isEqualToString:[MySingleton sharedManager].strDjSongRequestAcceptanceTypeAfter15Minutes])
                    {
                        [MySingleton sharedManager].dataManager.strDjSongRequestAcceptanceTypeMessage = @"You have accepted this song request to be played after 15 minutes";
                    }
                    
                    //========== FILL SONG REQUESTS ARRAY ==========//
                    NSArray *arraySongRequestsList = [jsonResult objectForKey:@"song_requests"];
                    
                    self.arrayDjSongsRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arraySongRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arraySongRequestsList objectAtIndex:i];
                        
                        SongRequest *objSongRequest = [[SongRequest alloc] init];
                        objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                        objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                        objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                        objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                        objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                        objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                        objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                        objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestDjAmount"];
                        
                        if(!objSongRequest.boolIsSongRequestExpired)
                        {
                            [self.arrayDjSongsRequests addObject:objSongRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptedSongRequestEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR DJS TO DECLINE SONG REQUEST

-(void)declineSongRequest:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"DeclineSongRequest"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"songrequest_id"] forKey:@"songrequest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You have declined this song request"];
                    
                    //========== FILL SONG REQUESTS ARRAY ==========//
                    NSArray *arraySongRequestsList = [jsonResult objectForKey:@"song_requests"];
                    
                    self.arrayDjSongsRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arraySongRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arraySongRequestsList objectAtIndex:i];
                        
                        SongRequest *objSongRequest = [[SongRequest alloc] init];
                        objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                        objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                        objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                        objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                        objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                        objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                        objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                        objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestDjAmount"];
                        
                        if(!objSongRequest.boolIsSongRequestExpired)
                        {
                            [self.arrayDjSongsRequests addObject:objSongRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"declinedSongRequestEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR DJS TO ACCEPT SHOUTOUT REQUEST

-(void)acceptShoutoutRequest:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AcceptShoutoutRequest"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"shoutoutrequest_id"] forKey:@"shoutoutrequest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
//                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You have accepted this shoutout request"];
                    
                    //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                    NSArray *arrayShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                    
                    self.arrayDjShoutoutRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayShoutoutRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayShoutoutRequestsList objectAtIndex:i];
                        
                        ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                        objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                        objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                        objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                        objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                        objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                        objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                        objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                        objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestDjAmount"];
                        
                        if(!objShoutoutRequest.boolIsShoutoutRequestExpired)
                        {
                            [self.arrayDjShoutoutRequests addObject:objShoutoutRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptedShoutoutRequestEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR DJS TO DECLINE SHOUTOUT REQUEST

-(void)declineShoutoutRequest:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"DeclineShoutoutRequest"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"shoutoutrequest_id"] forKey:@"shoutoutrequest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You have declined this shoutout request"];
                    
                    //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                    NSArray *arrayShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                    
                    self.arrayDjShoutoutRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayShoutoutRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayShoutoutRequestsList objectAtIndex:i];
                        
                        ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                        objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                        objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                        objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                        objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                        objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                        objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                        objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                        objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestDjAmount"];
                        
                        if(!objShoutoutRequest.boolIsShoutoutRequestExpired)
                        {
                            [self.arrayDjShoutoutRequests addObject:objShoutoutRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"declinedShoutoutRequestEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL MY EARNINGS REQUESTS LIST BY USER ID

-(void)getAllMyEarningsRequestsByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllMyEarningsRequestsByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
    
                NSDictionary *jsonResult = responseObject;
        
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                //========== FILL SONG REQUESTS ARRAY ==========//
                NSArray *arrayDjMyEarningsSongsRequestsList = [jsonResult objectForKey:@"song_requests"];
                
                self.arrayDjMyEarningsSongsRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayDjMyEarningsSongsRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayDjMyEarningsSongsRequestsList objectAtIndex:i];
                    
                    SongRequest *objSongRequest = [[SongRequest alloc] init];
                    objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                    objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                    objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                    objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                    objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                    objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                    objSongRequest.strSongRequestStatus = [currentDictionary objectForKey:@"SongRequestStatus"];
                    objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestAmount"];
                    objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestDjAmount"];
                    
                    [self.arrayDjMyEarningsSongsRequests addObject:objSongRequest];
                }
                
                //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                NSArray *arrayDjMyEarningsShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                
                self.arrayDjMyEarningsShoutoutRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayDjMyEarningsShoutoutRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayDjMyEarningsShoutoutRequestsList objectAtIndex:i];
                    
                    ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                    objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                    objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                    objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                    objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                    objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                    objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                    objShoutoutRequest.strShoutoutRequestStatus = [currentDictionary objectForKey:@"ShoutoutRequestStatus"];
                    objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestAmount"];
                    objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestDjAmount"];
                    
                    [self.arrayDjMyEarningsShoutoutRequests addObject:objShoutoutRequest];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllMyEarningsRequestsByUserIdEvent" object:nil];
//                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR DJS TO MARK SONG REQUEST AS PLAYED

-(void)markSongRequestAsPlayed:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"MarkSongRequestAsPlayed"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"songrequest_id"] forKey:@"songrequest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You have successfully marked this request as played."];
                    
                    //========== FILL SONG REQUESTS ARRAY ==========//
                    NSArray *arrayDjAcceptedSongRequestsList = [jsonResult objectForKey:@"song_requests"];
                    
                    self.arrayDjAcceptedSongsRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayDjAcceptedSongRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayDjAcceptedSongRequestsList objectAtIndex:i];
                        
                        SongRequest *objSongRequest = [[SongRequest alloc] init];
                        objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                        objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                        objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                        objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                        objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                        objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                        objSongRequest.strSongRequestStatus = [currentDictionary objectForKey:@"SongRequestStatus"];
                        objSongRequest.strSongRequestAcceptanceType = [currentDictionary objectForKey:@"SongRequestAcceptanceType"];
                        objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                        objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestDjAmount"];
                        
                        if(!objSongRequest.boolIsSongRequestExpired)
                        {
                            [self.arrayDjAcceptedSongsRequests addObject:objSongRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"markedSongRequestAsPlayedEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR DJS TO MARK SHOUTOUT REQUEST AS PLAYED

-(void)markShoutoutRequestAsPlayed:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"MarkShoutoutRequestAsPlayed"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"shoutoutrequest_id"] forKey:@"shoutoutrequest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You have successfully marked this request as played."];
                    
                    //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                    NSArray *arrayDjAcceptedShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                    
                    self.arrayDjAcceptedShoutoutRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayDjAcceptedShoutoutRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayDjAcceptedShoutoutRequestsList objectAtIndex:i];
                        
                        ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                        objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                        objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                        objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                        objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                        objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                        objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                        objShoutoutRequest.strShoutoutRequestStatus = [currentDictionary objectForKey:@"ShoutoutRequestStatus"];
                        objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                        objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestDjAmount"];
                        
                        if(!objShoutoutRequest.boolIsShoutoutRequestExpired)
                        {
                            [self.arrayDjAcceptedShoutoutRequests addObject:objShoutoutRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"markedShoutoutRequestAsPlayedEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR DJS TO SET TWITTER INSTAGRAM ONLINE MESSAGE

-(void)setTwitterInstagramOnlineMessage:(NSString *)strMessage
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strLoggedInUserId = [prefs objectForKey:@"userid"];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"SetTwitterInstagramOnlineMessage"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strLoggedInUserId forKey:@"user_id"];
        [parameters setObject:strMessage forKey:@"message"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
//                    if(self.objLoggedInUser == nil)
//                    {
//                        self.objLoggedInUser = [[User alloc] init];
//                    }
//
//                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
//                    self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
//
//                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//                    [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
//                    [prefs synchronize];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"setTwitterInstagramOnlineMessageEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL EVENTS BY DJ ID

-(void)getAllEventsByDjId:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllEventsByDjId"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [parameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getalleventsbydjid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                    //========== FILL EVENTS ARRAY ==========//
                    NSArray *arrayEventList = [jsonResult objectForKey:@"events"];
                    
                    self.arrayAllEventsByDj = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayEventList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayEventList objectAtIndex:i];
                        
                        Event *objEvent = [[Event alloc] init];
                        objEvent.strEventID = [currentDictionary objectForKey:@"EventId"];
                        objEvent.strEventName = [currentDictionary objectForKey:@"EventName"];
                        objEvent.strEventDescription = [currentDictionary objectForKey:@"EventDescription"];
                        objEvent.strEventDate = [currentDictionary objectForKey:@"EventDate"];
                        objEvent.strEventLocation = [currentDictionary objectForKey:@"EventLocation"];
                        objEvent.strEventVenueAddress = [currentDictionary objectForKey:@"EventVenueAddress"];
                        objEvent.boolIsEventLive = [[currentDictionary objectForKey:@"IsEventLive"] boolValue];
                        
                        [self.arrayAllEventsByDj addObject:objEvent];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllEventsByDjIdEvent" object:nil];
//                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showErrorMessage:@"Server Error" withErrorContent:message];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];

        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO ADD EVENT

-(void)addEvent:(NSDictionary *)dictParameters
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AddEvent"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"event_name"] forKey:@"event_name"];
        [parameters setObject:[dictParameters objectForKey:@"event_location"] forKey:@"event_location"];
        [parameters setObject:[dictParameters objectForKey:@"event_date"] forKey:@"event_date"];
        [parameters setObject:[dictParameters objectForKey:@"event_venue_address"] forKey:@"event_venue_address"];
        [parameters setObject:[dictParameters objectForKey:@"event_description"] forKey:@"event_description"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedDjEventEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO DELETE EVENT

-(void)deleteEvent:(NSDictionary *)dictParameters
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"DeleteEvent"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"event_id"] forKey:@"event_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL EVENTS ARRAY ==========//
                    NSArray *arrayEventList = [jsonResult objectForKey:@"events"];
                    
                    self.arrayAllEventsByDj = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayEventList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayEventList objectAtIndex:i];
                        
                        Event *objEvent = [[Event alloc] init];
                        objEvent.strEventID = [currentDictionary objectForKey:@"EventId"];
                        objEvent.strEventName = [currentDictionary objectForKey:@"EventName"];
                        objEvent.strEventDescription = [currentDictionary objectForKey:@"EventDescription"];
                        objEvent.strEventDate = [currentDictionary objectForKey:@"EventDate"];
                        objEvent.strEventLocation = [currentDictionary objectForKey:@"EventLocation"];
                        objEvent.strEventVenueAddress = [currentDictionary objectForKey:@"EventVenueAddress"];
                        objEvent.boolIsEventLive = [[currentDictionary objectForKey:@"IsEventLive"] boolValue];
                        
                        [self.arrayAllEventsByDj addObject:objEvent];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedEventEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET STATUS BY EVENT ID AND USER ID

-(void)getUserStatusForEvent:(NSDictionary *)dictParameters
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetUserStatusForEvent"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"event_id"] forKey:@"event_id"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getuserstatusforevent" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.boolIsOnlineAtEvent = [[jsonResult objectForKey:@"isOnlineAtEvent"] boolValue];
                    self.objLoggedInUser.strOnlineEventId = [jsonResult objectForKey:@"online_event_id"];
                    self.objLoggedInUser.strOnlineEventPlaylistId = [jsonResult objectForKey:@"online_event_playlist_id"];
                    self.objLoggedInUser.strOnlineEventPlaylistName = [jsonResult objectForKey:@"online_event_playlist_name"];
                    
                    //========== FILL PLAYLISTS ARRAY ==========//
                    NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                    
                    self.objLoggedInUser.arrayDjPlaylists = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                        
                        Playlist *objPlaylist = [[Playlist alloc] init];
                        objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                        objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                        
                        objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                        
                        NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                        
                        for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                        {
                            NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                            
                            Song *objSong = [[Song alloc] init];
                            
                            objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                            objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                            objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                            
                            [objPlaylist.arrayPlaylistSongs addObject:objSong];
                        }
                        
                        [self.objLoggedInUser.arrayDjPlaylists addObject:objPlaylist];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotUserStatusForEventEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO CHANGE ONLINE EVENT STATUS

-(void)changeEventStatus:(NSDictionary *)dictParameters
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"ChangeEventStatus"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"event_id"] forKey:@"event_id"];
        [parameters setObject:[dictParameters objectForKey:@"playlist_id"] forKey:@"playlist_id"];
        [parameters setObject:[dictParameters objectForKey:@"is_online"] forKey:@"is_online"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"changeeventstatus" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.boolIsOnlineAtEvent = [[jsonResult objectForKey:@"isOnlineAtEvent"] boolValue];
                    self.objLoggedInUser.strOnlineEventId = [jsonResult objectForKey:@"online_event_id"];
                    self.objLoggedInUser.strOnlineEventPlaylistId = [jsonResult objectForKey:@"online_event_playlist_id"];
                    self.objLoggedInUser.strOnlineEventPlaylistName = [jsonResult objectForKey:@"online_event_playlist_name"];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedEventStatusEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL ADVERTISEMENTS LIST BY USER ID

-(void)getAllAdvertisementsByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllAdvertisementsByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        [parameters setObject:strUserType forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL SONG REQUESTS ARRAY ==========//
                NSArray *arrayAdvertisementsList = [jsonResult objectForKey:@"advertisements"];
                
                self.arrayDjAdvertisements = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAdvertisementsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAdvertisementsList objectAtIndex:i];
                    
                    Advertisement *objAdvertisement = [[Advertisement alloc] init];
                    objAdvertisement.strAdvertisementId = [currentDictionary objectForKey:@"id"];
                    objAdvertisement.strAdvertisementImageUrl = [currentDictionary objectForKey:@"image"];
                    objAdvertisement.strAdvertisementRedirectionUrl = [currentDictionary objectForKey:@"url"];
                    
                    [self.arrayDjAdvertisements addObject:objAdvertisement];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllAdvertisementsByUserIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO DELETE ADVERTISEMENT BY ADVERTISEMENT ID

-(void)deleteAdvertisementByAdvertisementId:(NSString *)strAdvertisementId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"DeleteAdvertisement"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strAdvertisementId forKey:@"ad_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedAdvertisementByAdvertisementIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO ADD ADVERTISEMENT

-(void)addAdvertisement:(NSDictionary *)dictParameters with_File:(NSData *)imageData
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AddAdvertisement"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"url"] forKey:@"url"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        manager.requestSerializer.timeoutInterval = 60000;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        
        [manager POST:urlString parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"1.png" mimeType:@"image/png"];
            
        } progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedAdvertisementEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

//========== CLUB OWNER WEBSERVICES ==========//

#pragma mark - FUNCTION FOR CLUB OWNERS TO GET APPROVED DJS LIST BY USER ID

-(void)getClubOwnerApprovedDjListByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetClubOwnerApprovedDjListByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                    //========== FILL CLUB OWNER APPROVED DJS ARRAY ==========//
                    NSArray *arrayClubOwnerApprovedDjsList = [jsonResult objectForKey:@"club_owner_approved_djs"];
                    
                    self.arrayClubOwnerApprovedDjs = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayClubOwnerApprovedDjsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayClubOwnerApprovedDjsList objectAtIndex:i];
                        
                        Dj *objDj = [[Dj alloc] init];
                        objDj.strDjID = [currentDictionary objectForKey:@"DjId"];
                        objDj.strDjName = [currentDictionary objectForKey:@"DjName"];
                        objDj.strDjClubId = [currentDictionary objectForKey:@"ClubId"];
                        objDj.strDjClubName = [currentDictionary objectForKey:@"DjClubName"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objDj.strDjProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"DjProfilePictureImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        
                        [self.arrayClubOwnerApprovedDjs addObject:objDj];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotClubOwnerApprovedDjListByUserIdEvent" object:nil];
                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
//                }
//            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO GET DJS REQUEST LIST BY USER ID

-(void)getClubOwnerDjRequestListByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetClubOwnerDjRequestListByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                self.strSideMenuBackgroundImageUrl = [[jsonResult objectForKey:@"sidemenubackgroundimageurl"] stringByAddingPercentEncodingWithAllowedCharacters:set];
                
//                self.strSideMenuBackgroundImageUrl = [[jsonResult objectForKey:@"sidemenubackgroundimageurl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                //========== FILL CLUB OWNER DJ REQUESTS ARRAY ==========//
                NSArray *arrayClubOwnerDjRequestsList = [jsonResult objectForKey:@"club_owner_djs_requests"];
                
                self.arrayClubOwnerDjRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayClubOwnerDjRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayClubOwnerDjRequestsList objectAtIndex:i];
                    
                    Dj *objDj = [[Dj alloc] init];
                    objDj.strDjID = [currentDictionary objectForKey:@"DjId"];
                    objDj.strDjName = [currentDictionary objectForKey:@"DjName"];
                    objDj.strDjClubId = [currentDictionary objectForKey:@"ClubId"];
                    objDj.strDjClubName = [currentDictionary objectForKey:@"DjClubName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objDj.strDjProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"DjProfilePictureImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayClubOwnerDjRequests addObject:objDj];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotClubOwnerDjRequestListByUserIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO ACCEPT DJ APPROVAL REQUEST BY USER ID AND DJ ID

-(void)acceptDjApprovalRequestByUserId:(NSString *)strUserId withDjId:(NSString *)strDjId withClubId:(NSString *)strClubId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AcceptDjApprovalRequestByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strDjId forKey:@"dj_id"];
        [parameters setObject:strClubId forKey:@"club_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
    
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                    //========== FILL CLUB OWNER DJ REQUESTS ARRAY ==========//
                    NSArray *arrayClubOwnerDjRequestsList = [jsonResult objectForKey:@"club_owner_djs_requests"];
                    
                    self.arrayClubOwnerDjRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayClubOwnerDjRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayClubOwnerDjRequestsList objectAtIndex:i];
                        
                        Dj *objDj = [[Dj alloc] init];
                        objDj.strDjID = [currentDictionary objectForKey:@"DjId"];
                        objDj.strDjName = [currentDictionary objectForKey:@"DjName"];
                        objDj.strDjClubId = [currentDictionary objectForKey:@"ClubId"];
                        objDj.strDjClubName = [currentDictionary objectForKey:@"DjClubName"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objDj.strDjProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"DjProfilePictureImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        
                        [self.arrayClubOwnerDjRequests addObject:objDj];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptedDjApprovalRequestByUserIdEvent" object:nil];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO DECLINE DJ APPROVAL REQUEST BY USER ID AND DJ ID

-(void)declineDjApprovalRequestByUserId:(NSString *)strUserId withDjId:(NSString *)strDjId withClubId:(NSString *)strClubId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"DeclineDjApprovalRequestByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strDjId forKey:@"dj_id"];
        [parameters setObject:strClubId forKey:@"club_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                    //========== FILL CLUB OWNER DJ REQUESTS ARRAY ==========//
                    NSArray *arrayClubOwnerDjRequestsList = [jsonResult objectForKey:@"club_owner_djs_requests"];
                    
                    self.arrayClubOwnerDjRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayClubOwnerDjRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayClubOwnerDjRequestsList objectAtIndex:i];
                        
                        Dj *objDj = [[Dj alloc] init];
                        objDj.strDjID = [currentDictionary objectForKey:@"DjId"];
                        objDj.strDjName = [currentDictionary objectForKey:@"DjName"];
                        objDj.strDjClubId = [currentDictionary objectForKey:@"ClubId"];
                        objDj.strDjClubName = [currentDictionary objectForKey:@"DjClubName"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objDj.strDjProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"DjProfilePictureImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        
                        [self.arrayClubOwnerDjRequests addObject:objDj];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"declinedDjApprovalRequestByUserIdEvent" object:nil];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO GET DJS ONLINE REQUEST LIST BY USER ID

-(void)getClubOwnerDjOnlineRequestListByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetClubOwnerDjOnlineRequestListByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL CLUB OWNER DJ ONLINE REQUESTS ARRAY ==========//
                NSArray *arrayClubOwnerDjOnlineRequestsList = [jsonResult objectForKey:@"club_owner_djs_online_requests"];
                
                self.arrayClubOwnerDjOnlineRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayClubOwnerDjOnlineRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayClubOwnerDjOnlineRequestsList objectAtIndex:i];
                    
                    Dj *objDj = [[Dj alloc] init];
                    objDj.strDjID = [currentDictionary objectForKey:@"DjId"];
                    objDj.strDjName = [currentDictionary objectForKey:@"DjName"];
                    objDj.strDjClubId = [currentDictionary objectForKey:@"ClubId"];
                    objDj.strDjClubName = [currentDictionary objectForKey:@"DjClubName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objDj.strDjProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"DjProfilePictureImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayClubOwnerDjOnlineRequests addObject:objDj];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotClubOwnerDjOnlineRequestListByUserIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO ACCEPT DJ ONLINE REQUEST BY USER ID AND DJ ID

-(void)acceptDjOnlineRequestByUserId:(NSString *)strUserId withDjId:(NSString *)strDjId withClubId:(NSString *)strClubId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AcceptDjOnlineRequestByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strDjId forKey:@"dj_id"];
        [parameters setObject:strClubId forKey:@"club_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                    //========== FILL CLUB OWNER DJ ONLINE REQUESTS ARRAY ==========//
                    NSArray *arrayClubOwnerDjOnlineRequestsList = [jsonResult objectForKey:@"club_owner_djs_online_requests"];
                    
                    self.arrayClubOwnerDjOnlineRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayClubOwnerDjOnlineRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayClubOwnerDjOnlineRequestsList objectAtIndex:i];
                        
                        Dj *objDj = [[Dj alloc] init];
                        objDj.strDjID = [currentDictionary objectForKey:@"DjId"];
                        objDj.strDjName = [currentDictionary objectForKey:@"DjName"];
                        objDj.strDjClubId = [currentDictionary objectForKey:@"ClubId"];
                        objDj.strDjClubName = [currentDictionary objectForKey:@"DjClubName"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objDj.strDjProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"DjProfilePictureImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        
                        [self.arrayClubOwnerDjOnlineRequests addObject:objDj];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptedDjOnlineRequestByUserIdEvent" object:nil];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO DECLINE DJ ONLINE REQUEST BY USER ID AND DJ ID

-(void)declineDjOnlineRequestByUserId:(NSString *)strUserId withDjId:(NSString *)strDjId withClubId:(NSString *)strClubId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"DeclineDjOnlineRequestByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strDjId forKey:@"dj_id"];
        [parameters setObject:strClubId forKey:@"club_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                    //========== FILL CLUB OWNER DJ ONLINE REQUESTS ARRAY ==========//
                    NSArray *arrayClubOwnerDjOnlineRequestsList = [jsonResult objectForKey:@"club_owner_djs_online_requests"];
                    
                    self.arrayClubOwnerDjOnlineRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayClubOwnerDjOnlineRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayClubOwnerDjOnlineRequestsList objectAtIndex:i];
                        
                        Dj *objDj = [[Dj alloc] init];
                        objDj.strDjID = [currentDictionary objectForKey:@"DjId"];
                        objDj.strDjName = [currentDictionary objectForKey:@"DjName"];
                        objDj.strDjClubId = [currentDictionary objectForKey:@"ClubId"];
                        objDj.strDjClubName = [currentDictionary objectForKey:@"DjClubName"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objDj.strDjProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"DjProfilePictureImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        
                        [self.arrayClubOwnerDjOnlineRequests addObject:objDj];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"declinedDjOnlineRequestByUserIdEvent" object:nil];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO GET ONLINE DJS LIST BY USER ID

-(void)getClubOwnerOnlineDjListByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetClubOwnerOnlineDjListByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL CLUB OWNER ONLINE DJ ARRAY ==========//
                NSArray *arrayClubOwnerDjOnlineRequestsList = [jsonResult objectForKey:@"club_owner_djs_requests"];
                
                self.arrayClubOwnerOnlineDjs = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayClubOwnerDjOnlineRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayClubOwnerDjOnlineRequestsList objectAtIndex:i];
                    
                    Dj *objDj = [[Dj alloc] init];
                    objDj.strDjID = [currentDictionary objectForKey:@"DjId"];
                    objDj.strDjName = [currentDictionary objectForKey:@"DjName"];
                    objDj.strDjClubId = [currentDictionary objectForKey:@"ClubId"];
                    objDj.strDjClubName = [currentDictionary objectForKey:@"DjClubName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objDj.strDjProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"DjProfilePictureImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayClubOwnerOnlineDjs addObject:objDj];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotClubOwnerOnlineDjListByUserIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO GET DJ LOGS BY USER ID

-(void)getClubOwnerDjLogsByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetClubOwnerDjLogsByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL CLUB OWNER ONLINE DJ ARRAY ==========//
                NSArray *arrayClubOwnerDjOnlineRequestsList = [jsonResult objectForKey:@"club_owner_approved_djs"];
                
                self.arrayClubOwnerDjLogs = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayClubOwnerDjOnlineRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayClubOwnerDjOnlineRequestsList objectAtIndex:i];
                    
                    Dj *objDj = [[Dj alloc] init];
                    objDj.strDjID = [currentDictionary objectForKey:@"DjId"];
                    objDj.strDjName = [currentDictionary objectForKey:@"DjName"];
                    objDj.strDjClubId = [currentDictionary objectForKey:@"ClubId"];
                    objDj.strDjClubName = [currentDictionary objectForKey:@"DjClubName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objDj.strDjProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"DjProfilePictureImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    NSString *strDjOnlineDateTime = [currentDictionary objectForKey:@"DjOnlineTime"];
                    NSString *strDjOfflineDateTime = [currentDictionary objectForKey:@"DjOfflineTime"];
                    
                    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
                    [dateFormatter1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                    dateFormatter1.dateStyle = NSDateFormatterMediumStyle;
                    [dateFormatter1 setDateFormat:@"dd MMM, yy hh:mm:ss a"];
                    
                    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc]init];
                    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                    dateFormatter2.dateStyle = NSDateFormatterMediumStyle;
                    [dateFormatter2 setDateFormat:@"dd MMM, yy"];
                    
                    NSDateFormatter *timeFormatter1 = [[NSDateFormatter alloc]init];
                    [timeFormatter1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                    timeFormatter1.dateStyle = NSDateFormatterMediumStyle;
                    [timeFormatter1 setDateFormat:@"hh:mm a"];
                    
                    if(strDjOnlineDateTime.length > 0)
                    {
                        NSDate *onlineDateTime = [dateFormatter1 dateFromString:strDjOnlineDateTime];
                        
                        objDj.strDjOnlineDate = [dateFormatter2 stringFromDate:onlineDateTime];
                        objDj.strDjOnlineTime = [timeFormatter1 stringFromDate:onlineDateTime];
                    }
                    else
                    {
                        objDj.strDjOnlineDate = @"NA";
                        objDj.strDjOnlineTime = @"NA";
                        
                    }
                    
                    if(strDjOfflineDateTime.length > 0)
                    {
                        NSDate *offlineDateTime = [dateFormatter1 dateFromString:strDjOfflineDateTime];
                        
                        objDj.strDjOfflineDate = [dateFormatter2 stringFromDate:offlineDateTime];
                        objDj.strDjOfflineTime = [timeFormatter1 stringFromDate:offlineDateTime];
                    }
                    else
                    {
                        objDj.strDjOfflineDate = @"ONLINE";
                        objDj.strDjOfflineTime = @"ONLINE";
                    }
                    
                    [self.arrayClubOwnerDjLogs addObject:objDj];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotClubOwnerDjLogsByUserIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO GET ALL CLUBS

-(void)getClubOwnerAllClubs:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetClubOwnerAllClubs"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getclubownerallclubs" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL CLUBS ARRAY ==========//
                    NSArray *arrayClubList = [jsonResult objectForKey:@"clubs"];
                    
                    self.arrayClubOwnerAllClubs = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayClubList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayClubList objectAtIndex:i];
                        
                        Club *objClub = [[Club alloc] init];
                        objClub.strClubID = [currentDictionary objectForKey:@"ClubId"];
                        objClub.strClubName = [currentDictionary objectForKey:@"ClubName"];
                        objClub.strClubType = [currentDictionary objectForKey:@"ClubType"];
                        objClub.strClubAddress = [currentDictionary objectForKey:@"ClubAddress"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objClub.strClubImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ClubImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objClub.strClubBannerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ClubBannerUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objClub.strClubStateID = [currentDictionary objectForKey:@"ClubStateId"];
                        objClub.strClubCityID = [currentDictionary objectForKey:@"ClubCityId"];
                        
                        [self.arrayClubOwnerAllClubs addObject:objClub];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotClubOwnerAllClubsEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO GET ALL PLAYLISTS BY USER ID

-(void)getAllClubOwnerPlaylistsByUserId:(NSString *)strUserId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetAllClubOwnerPlaylistsByUserId"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;

//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getallclubownerplaylistsbyuserid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                //========== FILL PLAYLISTS ARRAY ==========//
                NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                
                if(self.objLoggedInUser == nil)
                {
                    self.objLoggedInUser = [[User alloc] init];
                }
                
                self.objLoggedInUser.arrayClubOwnerPlaylists = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                    
                    Playlist *objPlaylist = [[Playlist alloc] init];
                    objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                    objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                    
                    objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                    
                    NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                    
                    for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                    {
                        NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                        
                        Song *objSong = [[Song alloc] init];
                        
                        objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                        objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                        objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                        
                        [objPlaylist.arrayPlaylistSongs addObject:objSong];
                    }
                    
                    [self.objLoggedInUser.arrayClubOwnerPlaylists addObject:objPlaylist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllClubOwnerPlaylistsByUserIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO DELETE PLAYLIST BY USER ID AND PLAYLIST ID

-(void)deleteClubOwnerPlaylistByUserId:(NSString *)strUserId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"DeleteClubOwnerPlaylistByUserId"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"deleteclubownerplaylistbyuseridandplaylistid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL PLAYLISTS ARRAY ==========//
                    NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                    
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.arrayClubOwnerPlaylists = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                        
                        Playlist *objPlaylist = [[Playlist alloc] init];
                        objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                        objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                        
                        objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                        
                        NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                        
                        for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                        {
                            NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                            
                            Song *objSong = [[Song alloc] init];
                            
                            objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                            objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                            objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                            
                            [objPlaylist.arrayPlaylistSongs addObject:objSong];
                        }
                        
                        [self.objLoggedInUser.arrayClubOwnerPlaylists addObject:objPlaylist];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedClubOwnerPlaylistByUserIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO DELETE SONG BY USER ID AND SONG ID

-(void)deleteClubOwnerSongByUserId:(NSString *)strUserId withSongId:(NSString *)strSongId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"DeleteClubOwnerSongByUserId"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strSongId forKey:@"song_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"deleteclubownersongbyuseridandsongid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedClubOwnerSongByUserIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO ADD SONG

-(void)addClubOwnerSong:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AddClubOwnerSong"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistName"] forKey:@"playlist_name"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistID"] forKey:@"playlist_id"];
        [parameters setObject:[dictParameters objectForKey:@"strSongName"] forKey:@"song_name"];
        [parameters setObject:[dictParameters objectForKey:@"strArtistName"] forKey:@"artist_name"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"addclubownersong" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedClubOwnerSongEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO ADD PLAYLIST

-(void)addClubOwnerPlaylist:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"AddClubOwnerPlaylist"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistName"] forKey:@"playlist_name"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;
        
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"addclubownerplaylist" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedClubOwnerPlaylistEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO GET SELECTED PLAYLIST BY CUB ID AND USER ID

-(void)getClubOwnerSelectedPlaylistByUserId:(NSString *)strUserId withClubId:(NSString *)strClubId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetClubOwnerSelectedPlaylistByUserIdAndClubId"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strClubId forKey:@"club_id"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"getclubownerselectedplaylistsbyuseridandclubid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
                self.strClubOwnerSelectedPlaylistIdForClub = [jsonResult objectForKey:@"selected_playlist_id"];
                self.strClubOwnerSelectedPlaylistNameForClub = [jsonResult objectForKey:@"selected_playlist_name"];
    
                //========== FILL PLAYLISTS ARRAY ==========//
                NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                
                if(self.objLoggedInUser == nil)
                {
                    self.objLoggedInUser = [[User alloc] init];
                }
                
                self.objLoggedInUser.arrayClubOwnerPlaylists = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                    
                    Playlist *objPlaylist = [[Playlist alloc] init];
                    objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                    objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                    
                    objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                    
                    NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                    
                    for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                    {
                        NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                        
                        Song *objSong = [[Song alloc] init];
                        
                        objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                        objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                        objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                        
                        [objPlaylist.arrayPlaylistSongs addObject:objSong];
                    }
                    
                    [self.objLoggedInUser.arrayClubOwnerPlaylists addObject:objPlaylist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotClubOwnerSelectedPlaylistByUserIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUB OWNERS TO SET PLAYLIST BY CUB ID AND USER ID

-(void)setClubOwnerPlaylistByUserId:(NSString *)strUserId withClubId:(NSString *)strClubId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];

        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"SetClubOwnerPlaylistByUserIdAndClubId"]];

        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strClubId forKey:@"club_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];

        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];

            if ([responseObject isKindOfClass:[NSArray class]]) {

                NSArray *responseArray = responseObject;

            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {

                NSDictionary *jsonResult = responseObject;
    
//                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"setclubownerselectedplaylistsbyuseridandclubid" ofType:@"json"];
//                NSData *data = [NSData dataWithContentsOfFile:filePath];
//                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"setClubOwnerPlaylistByUserIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"Server Error" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

//========== ARTIST WEBSERVICES ==========//

#pragma mark - FUNCTION TO CHECK ONLINE STATUS OF DJ BY USERID

-(void)checkOnlineStatusOfArtist:(NSString *)strArtistId;
{
    if([self isNetworkAvailable])
    {
//        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"CheckOnlineStatusOfArtist"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = [[jsonResult objectForKey:@"isOnline"] boolValue];
                    
                    //USER IS AN ARTIST
                    
                    NSArray *arrayArtistApprovedManagerIds = [jsonResult objectForKey:@"approved_managers_ids"];
                    self.objLoggedInUser.arrayArtistApprovedManagerIds = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayArtistApprovedManagerIds.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayArtistApprovedManagerIds objectAtIndex:i];
                        
                        NSString *strArtistApprovedManagersId = [currentDictionary objectForKey:@"manager_id"];
                        [self.objLoggedInUser.arrayArtistApprovedManagerIds addObject:strArtistApprovedManagersId];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"checkedOnlineStatusOfArtistEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR ARTIST LOGIN

-(void)artistLogin:(User *)objUser
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"ArtistLogin"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objUser.strEmail forKey:@"email"];
        [parameters setObject:objUser.strPassword forKey:@"password"];
        [parameters setObject:objUser.strDeviceToken forKey:@"device_id"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([[jsonResult objectForKey:@"isStripeConnected"] boolValue] == true)
                    {
                        if(self.objLoggedInUser == nil)
                        {
                            self.objLoggedInUser = [[User alloc] init];
                        }
                        
                        self.objLoggedInUser.strUserID = [jsonResult objectForKey:@"user_id"];
                        self.objLoggedInUser.strEmail = objUser.strEmail;
                        self.objLoggedInUser.strFullname = [jsonResult objectForKey:@"fullname"];
                        self.objLoggedInUser.strStateId = [jsonResult objectForKey:@"state_id"];
                        self.objLoggedInUser.strCityId = [jsonResult objectForKey:@"city_id"];
                        self.objLoggedInUser.strUserType = [jsonResult objectForKey:@"user_type"];
                        
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        
                        self.objLoggedInUser.boolIsOnline = [[jsonResult objectForKey:@"isOnline"] boolValue];
                        
                        //USER IS AN ARTIST
                        
                        NSArray *arrayArtistApprovedManagerIds = [jsonResult objectForKey:@"approved_managers_ids"];
                        self.objLoggedInUser.arrayArtistApprovedManagerIds = [[NSMutableArray alloc] init];
                        
                        for(int i = 0 ; i < arrayArtistApprovedManagerIds.count; i++)
                        {
                            NSDictionary *currentDictionary = [arrayArtistApprovedManagerIds objectAtIndex:i];
                            
                            NSString *strArtistApprovedManagersId = [currentDictionary objectForKey:@"manager_id"];
                            [self.objLoggedInUser.arrayArtistApprovedManagerIds addObject:strArtistApprovedManagersId];
                        }
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"1" forKey:@"autologin"];
                        [prefs setObject:self.objLoggedInUser.strUserID forKey:@"userid"];
                        [prefs setObject:self.objLoggedInUser.strEmail forKey:@"useremail"];
                        [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
                        [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
                        [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
                        [prefs setObject:self.objLoggedInUser.strUserType forKey:@"usertype"];
                        [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"artistLoggedinEvent" object:nil];
                    }
                    else
                    {
                        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                        alertViewController.title = @"Stripe Not Connected";
                        alertViewController.message = @"You have not connected your stripe account yet. Please connect your stripe account to use our application.";
                        alertViewController.view.tintColor = [UIColor whiteColor];
                        alertViewController.backgroundTapDismissalGestureEnabled = YES;
                        alertViewController.swipeDismissalGestureEnabled = YES;
                        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                        
                        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                        
                        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Go to Stripe" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                            
                            [alertViewController dismissViewControllerAnimated:YES completion:nil];
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api/stripe/store_auth_artist.php&client_id=ca_AYa4lXZN4UitCyVGYSwfkyokYa7iUBJ6&state=%@", [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api_test2/stripe/store_auth_artist.php&client_id=%@&state=%@", [MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@store_auth_artist.php&client_id=%@&state=%@",[_dictSettings objectForKey:@"StripeConnectURL"],[MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            
                            NSLog(@"Stripe connet url for artist : %@", webPageUrl);
                            
                            if ([[UIApplication sharedApplication] canOpenURL:webPageUrl])
                            {
                                [[UIApplication sharedApplication] openURL:webPageUrl];
                            }
                            
                        }]];
                        
                        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
                    }
                }
                else
                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"Login Failed" withErrorContent:message];
                    
                    NSString *message = @"Either your email or password is incorrect. Please try again with correct email and password.";
                    [self showWebserviceFailureCaseMessage:@"Login Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR CLUBOWNER LOGIN

-(void)companyLogin:(User *)objUser
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"CompanyLogin"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objUser.strEmail forKey:@"email"];
        [parameters setObject:objUser.strPassword forKey:@"password"];
        [parameters setObject:objUser.strDeviceToken forKey:@"device_id"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([[jsonResult objectForKey:@"isStripeConnected"] boolValue] == true)
                    {
                        if(self.objLoggedInUser == nil)
                        {
                            self.objLoggedInUser = [[User alloc] init];
                        }
                        
                        self.objLoggedInUser.strUserID = [jsonResult objectForKey:@"user_id"];
                        self.objLoggedInUser.strEmail = objUser.strEmail;
                        self.objLoggedInUser.strFullname = [jsonResult objectForKey:@"fullname"];
                        self.objLoggedInUser.strStateId = [jsonResult objectForKey:@"state_id"];
                        self.objLoggedInUser.strCityId = [jsonResult objectForKey:@"city_id"];
                        self.objLoggedInUser.strUserType = [jsonResult objectForKey:@"user_type"];
                        
                        //USER IS A CLUB OWNER
                        
                        NSArray *arrayCompanyApprovedArtists = [jsonResult objectForKey:@"manager_company_approved_artists"];
                        self.arrayCompanyApprovedArtists = [[NSMutableArray alloc] init];
                        
                        for(int i = 0 ; i < arrayCompanyApprovedArtists.count; i++)
                        {
                            NSDictionary *currentDictionary = [arrayCompanyApprovedArtists objectAtIndex:i];
                            
                            Artist *objArtist = [[Artist alloc] init];
                            objArtist.strArtistID = [currentDictionary objectForKey:@"artist_id"];
                            objArtist.strArtistName = [currentDictionary objectForKey:@"artist_name"];
                            objArtist.strArtistManagerId = [currentDictionary objectForKey:@"manager_id"];
                            objArtist.strArtistManagerName = [currentDictionary objectForKey:@"manager_name"];
                            
                            NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                            objArtist.strArtistProfilePictureImageUrl = [[currentDictionary objectForKey:@"artist_image"] stringByAddingPercentEncodingWithAllowedCharacters:set];
                            
                            [self.arrayCompanyApprovedArtists addObject:objArtist];
                        }
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"1" forKey:@"autologin"];
                        [prefs setObject:self.objLoggedInUser.strUserID forKey:@"userid"];
                        [prefs setObject:self.objLoggedInUser.strEmail forKey:@"useremail"];
                        [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
                        [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
                        [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
                        [prefs setObject:self.objLoggedInUser.strUserType forKey:@"usertype"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"companyLoggedinEvent" object:nil];
                    }
                    else
                    {
                        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                        alertViewController.title = @"Stripe Not Connected";
                        alertViewController.message = @"You have not connected your stripe account yet. Please connect your stripe account to use our application.";
                        alertViewController.view.tintColor = [UIColor whiteColor];
                        alertViewController.backgroundTapDismissalGestureEnabled = YES;
                        alertViewController.swipeDismissalGestureEnabled = YES;
                        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                        
                        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                        
                        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Go to Stripe" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                            
                            [alertViewController dismissViewControllerAnimated:YES completion:nil];
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api/stripe/store_auth_company.php&client_id=ca_AYa4lXZN4UitCyVGYSwfkyokYa7iUBJ6&state=%@", [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api_test2/stripe/store_auth_company.php&client_id=%@&state=%@", [MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@store_auth_company.php&client_id=%@&state=%@",[_dictSettings objectForKey:@"StripeConnectURL"],[MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            
                            NSLog(@"Stripe connet url for company : %@", webPageUrl);
                            
                            if ([[UIApplication sharedApplication] canOpenURL:webPageUrl])
                            {
                                [[UIApplication sharedApplication] openURL:webPageUrl];
                            }
                            
                        }]];
                        
                        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
                    }
                }
                else
                {
                    NSString *message = @"Either your email or password is incorrect. Please try again with correct email and password.";
                    [self showWebserviceFailureCaseMessage:@"Login Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR FACEBOOK ARTIST TO CHECK IF THEY ARE ALREADY REGISTERED

-(void)checkIfTheFacebookArtistIsAlreadyRegistered:(User *)objUser
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"CheckIfTheFacebookArtistIsAlreadyRegistered"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objUser.strEmail forKey:@"email"];
        [parameters setObject:objUser.strDeviceToken forKey:@"device_id"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([[jsonResult objectForKey:@"isStripeConnected"] boolValue] == true)
                    {
                        if(self.objLoggedInUser == nil)
                        {
                            self.objLoggedInUser = [[User alloc] init];
                        }
                        
                        self.objLoggedInUser.strUserID = [jsonResult objectForKey:@"user_id"];
                        self.objLoggedInUser.strEmail = objUser.strEmail;
                        self.objLoggedInUser.strFullname = [jsonResult objectForKey:@"fullname"];
                        self.objLoggedInUser.strStateId = [jsonResult objectForKey:@"state_id"];
                        self.objLoggedInUser.strCityId = [jsonResult objectForKey:@"city_id"];
                        self.objLoggedInUser.strUserType = @"3";
                        
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        
                        self.objLoggedInUser.boolIsOnline = [[jsonResult objectForKey:@"isOnline"] boolValue];
                        
                        NSArray *arrayArtistApprovedManagerIds = [jsonResult objectForKey:@"approved_managers_ids"];
                        self.objLoggedInUser.arrayArtistApprovedManagerIds = [[NSMutableArray alloc] init];
                        
                        for(int i = 0 ; i < arrayArtistApprovedManagerIds.count; i++)
                        {
                            NSDictionary *currentDictionary = [arrayArtistApprovedManagerIds objectAtIndex:i];
                            
                            NSString *strArtistApprovedManagersId = [currentDictionary objectForKey:@"manager_id"];
                            [self.objLoggedInUser.arrayArtistApprovedManagerIds addObject:strArtistApprovedManagersId];
                        }
                        
                        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                        [prefs setObject:@"1" forKey:@"autologin"];
                        [prefs setObject:self.objLoggedInUser.strUserID forKey:@"userid"];
                        [prefs setObject:self.objLoggedInUser.strEmail forKey:@"useremail"];
                        [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
                        [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
                        [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
                        [prefs setObject:self.objLoggedInUser.strUserType forKey:@"usertype"];
                        [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
                        [prefs synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"checkedIfTheFacebookArtistIsAlreadyRegisteredEvent" object:nil];
                    }
                    else
                    {
                        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                        alertViewController.title = @"Stripe Not Connected";
                        alertViewController.message = @"You have not connected your stripe account yet. Please connect your stripe account to use our application.";
                        alertViewController.view.tintColor = [UIColor whiteColor];
                        alertViewController.backgroundTapDismissalGestureEnabled = YES;
                        alertViewController.swipeDismissalGestureEnabled = YES;
                        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                        
                        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                        
                        [alertViewController addAction:[NYAlertAction actionWithTitle:@"Go to Stripe" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                            
                            [alertViewController dismissViewControllerAnimated:YES completion:nil];
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api/stripe/store_auth_artist.php&client_id=ca_AYa4lXZN4UitCyVGYSwfkyokYa7iUBJ6&state=%@", [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            /*
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api_test2/stripe/store_auth_artist.php&client_id=%@&state=%@", [MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            */
                            
                            NSURL *webPageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@store_auth_artist.php&client_id=%@&state=%@",[_dictSettings objectForKey:@"StripeConnectURL"],[MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]]];
                            
                            NSLog(@"Stripe connet url for artist : %@", webPageUrl);
                            
                            if ([[UIApplication sharedApplication] canOpenURL:webPageUrl])
                            {
                                [[UIApplication sharedApplication] openURL:webPageUrl];
                            }
                            
                        }]];
                        
                        [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
                    }
                }
                else
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"checkedIfTheFacebookUserIsAlreadyRegisteredEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR ARTIST SIGNUP

-(void)artistSignUp:(User *)objUser
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"ArtistSignup"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:objUser.strEmail forKey:@"email"];
        [parameters setObject:objUser.strPassword forKey:@"password"];
        [parameters setObject:objUser.strFullname forKey:@"fullname"];
        [parameters setObject:[objUser.strGender lowercaseString] forKey:@"gender"];
        [parameters setObject:objUser.strPhoneNumber forKey:@"phone_number"];
        [parameters setObject:objUser.strStateId forKey:@"state_id"];
        [parameters setObject:objUser.strStateName forKey:@"state_name"];
        [parameters setObject:objUser.strCityId forKey:@"city_id"];
        [parameters setObject:objUser.strCityName forKey:@"city_name"];
        [parameters setObject:objUser.strDeviceToken forKey:@"device_id"];
        [parameters setObject:objUser.strUserType forKey:@"user_type"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    self.strSignedUpDjEmail = objUser.strEmail;
                    self.strSignedUpDjPassword = objUser.strPassword;
                    
                    /*
                    self.strSignedUpDjStripeConnectUrl = [NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api/stripe/store_auth_artist.php&client_id=ca_AYa4lXZN4UitCyVGYSwfkyokYa7iUBJ6&state=%@", [jsonResult objectForKey:@"user_id"]];
                    */
                    
                    /*
                    self.strSignedUpDjStripeConnectUrl = [NSString stringWithFormat:@"https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://www.tipspinz.com/app/api_test2/stripe/store_auth_dj.php&client_id=%@&state=%@", [MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]];
                    */
                    
                    self.strSignedUpDjStripeConnectUrl = [NSString stringWithFormat:@"%@store_auth_dj.php&client_id=%@&state=%@", [_dictSettings objectForKey:@"StripeConnectURL"], [MySingleton sharedManager].strStripeConnectKey, [jsonResult objectForKey:@"user_id"]];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"artistSignedUpEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"Registration Failed" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

//#pragma mark - FUNCTION FOR COMPANY SIGNUP
//
//-(void)companySignUp:(User *)objUser
//{
//    if ([self isNetworkAvailable])
//    {
//        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
//        
//        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"ClubOwnerSignup"]];
//        
//        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//        [parameters setObject:objUser.strEmail forKey:@"email"];
//        [parameters setObject:objUser.strPassword forKey:@"password"];
//        [parameters setObject:objUser.strFullname forKey:@"fullname"];
//        [parameters setObject:[objUser.strGender lowercaseString] forKey:@"gender"];
//        [parameters setObject:objUser.strPhoneNumber forKey:@"phone_number"];
//        [parameters setObject:objUser.strStateId forKey:@"state_id"];
//        [parameters setObject:objUser.strStateName forKey:@"state_name"];
//        [parameters setObject:objUser.strCityId forKey:@"city_id"];
//        [parameters setObject:objUser.strCityName forKey:@"city_name"];
//        [parameters setObject:objUser.strDeviceToken forKey:@"device_id"];
//        [parameters setObject:objUser.strUserType forKey:@"user_type"];
//        [parameters setObject:@"1" forKey:@"device_type"];
//        
//        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//        
//        [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//            [appDelegate dismissGlobalHUD];
//            
//            if ([responseObject isKindOfClass:[NSArray class]]) {
//                
//                NSArray *responseArray = responseObject;
//                
//            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
//                
//                NSDictionary *jsonResult = responseObject;
//                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
//                    if(self.objLoggedInUser == nil)
//                    {
//                        self.objLoggedInUser = [[User alloc] init];
//                    }
//                    
//                    self.objLoggedInUser.strUserID = [jsonResult objectForKey:@"user_id"];
//                    self.objLoggedInUser.strEmail = objUser.strEmail;
//                    self.objLoggedInUser.strFullname = objUser.strFullname;
//                    self.objLoggedInUser.strStateId = objUser.strStateId;
//                    self.objLoggedInUser.strCityId = objUser.strCityId;
//                    self.objLoggedInUser.strUserType = objUser.strUserType;
//                    self.objLoggedInUser.strProfilePictureImageUrl = @"";
//                    self.objLoggedInUser.arrayDjApprovedClubIds = [[NSMutableArray alloc] init];
//                    
//                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//                    [prefs setObject:@"1" forKey:@"autologin"];
//                    [prefs setObject:self.objLoggedInUser.strUserID forKey:@"userid"];
//                    [prefs setObject:self.objLoggedInUser.strEmail forKey:@"useremail"];
//                    [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
//                    [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
//                    [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
//                    [prefs setObject:self.objLoggedInUser.strUserType forKey:@"usertype"];
//                    [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
//                    [prefs synchronize];
//                    
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"clubOwnerSignedUpEvent" object:nil];
//                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"Registration Failed" withErrorContent:message];
//                }
//            }
//        } failure:^(NSURLSessionTask *operation, NSError *error) {
//            
//            [appDelegate dismissGlobalHUD];
//            [self showErrorMessage:@"Server Error" withErrorContent:@""];
//            
//        }];
//    }
//    else
//    {
//        [self showInternetNotConnectedError];
//    }
//}

#pragma mark - FUNCTION FOR FORGET PASSWORD

-(void)forgetArtistPassword:(NSString *)strEmail withUserType:(NSString *)strUserType
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"ForgetArtistPassword"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strEmail forKey:@"email"];
        [parameters setObject:strUserType forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"forgotArtistPasswordEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO CHANGE STATUS BY ARTIST ID

-(void)changeStatus:(NSString *)strStatus withArtistId:(NSString *)strArtistId withManagerId:(NSString *)strManagerId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"ChangeArtistStatus"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strStatus forKey:@"status"];
        [parameters setObject:strArtistId forKey:@"user_id"];
        [parameters setObject:strManagerId forKey:@"manager_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([strStatus isEqualToString:@"offline"])
                    {
                        if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
                        {
                            [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
                        }
                        
                        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = false;
                        
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineManagerId = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineManagerName = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistId = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistName = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineManagerSetWithPresetPlaylist = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineAtEvent = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventName = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventPlaylistName = nil;
                        [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventPlaylistId = nil;
                    }
                    else if([strStatus isEqualToString:@"online"])
                    {
                        if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
                        {
                            [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
                        }
                        
                        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = true;
                        
//                        [self showErrorMessage:@"Request Sent" withErrorContent:@"Your request has been sent to club owner successfully. You will see yourself online in the system only when club owner will approve your request"];
                        
                        [self showErrorMessage:@"Status Changed" withErrorContent:@"You are online now."];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedStatusEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO CHANGE ONLINE PLAYLLIST BY ARTIST ID

-(void)changeArtistOnlinePlaylist:(NSString *)strArtistId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"ChangeArtistOnlinePlaylist"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    self.objLoggedInUser.strOnlinePlaylistId = strPlaylistId;
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedArtistOnlinePlaylistEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL MANAGERS

-(void)getAllManagers:(NSString *)strArtistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetAllManagers"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    NSArray *arrayArtistApprovedManagerIds = [jsonResult objectForKey:@"approved_managers_ids"];
                    self.objLoggedInUser.arrayArtistApprovedManagerIds = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayArtistApprovedManagerIds.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayArtistApprovedManagerIds objectAtIndex:i];
                        
                        NSString *strArtistApprovedManagerId = [currentDictionary objectForKey:@"manager_id"];
                        [self.objLoggedInUser.arrayArtistApprovedManagerIds addObject:strArtistApprovedManagerId];
                    }
                    
                    //========== FILL MANAGERS ARRAY ==========//
                    NSArray *arrayManagerList = [jsonResult objectForKey:@"managers"];
                    
                    self.arrayAllManagers = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayManagerList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayManagerList objectAtIndex:i];
                        
                        Manager *objManager = [[Manager alloc] init];
                        objManager.strManagerID = [currentDictionary objectForKey:@"ManagerId"];
                        objManager.strManagerName = [currentDictionary objectForKey:@"ManagerName"];
                        objManager.strManagerType = [currentDictionary objectForKey:@"ManagerType"];
                        objManager.strManagerAddress = [currentDictionary objectForKey:@"ManagerAddress"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objManager.strManagerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ManagerImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objManager.strManagerBannerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ManagerBannerUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objManager.strManagerStateID = [currentDictionary objectForKey:@"ManagerStateId"];
                        objManager.strManagerCityID = [currentDictionary objectForKey:@"ManagerCityId"];
                        
                        NSLog(@"objManager.strManagerImageUrl : %@", objManager.strManagerImageUrl);
                        NSLog(@"objManager.strManagerBannerImageUrl : %@", objManager.strManagerBannerImageUrl);
                        
                        [self.arrayAllManagers addObject:objManager];
                    }
                    
                    //========== FILL STATES ARRAY ==========//
                    NSArray *arrayStateList = [jsonResult objectForKey:@"states"];
                    
                    self.arrayStates = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayStateList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayStateList objectAtIndex:i];
                        
                        State *objState = [[State alloc] init];
                        objState.strStateID = [currentDictionary objectForKey:@"StateId"];
                        objState.strStateName = [currentDictionary objectForKey:@"StateName"];
                        
                        NSArray *arrayCityList = [currentDictionary objectForKey:@"Cities"];
                        
                        objState.arrayCity = [[NSMutableArray alloc] init];
                        
                        for(int j = 0 ; j < arrayCityList.count; j++)
                        {
                            NSDictionary *currentDictionaryCity = [arrayCityList objectAtIndex:j];
                            
                            City *objCity = [[City alloc] init];
                            objCity.strCityID = [currentDictionaryCity objectForKey:@"CityId"];
                            objCity.strCityName = [currentDictionaryCity objectForKey:@"CityName"];
                            
                            [objState.arrayCity addObject:objCity];
                        }
                        
                        [self.arrayStates addObject:objState];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllManagersEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL APPROVED MANAGERS

-(void)getAllApprovedManagers:(NSString *)strArtistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetAllApprovedManagers"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
//                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
//                {
                //========== FILL APPROVED MANAGERS ARRAY ==========//
                NSArray *arrayManagerList = [jsonResult objectForKey:@"managers"];
                
                self.arrayAllApprovedManagersByArtistId = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayManagerList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayManagerList objectAtIndex:i];
                    
                    Manager *objManager = [[Manager alloc] init];
                    objManager.strManagerID = [currentDictionary objectForKey:@"ManagerId"];
                    objManager.strManagerName = [currentDictionary objectForKey:@"ManagerName"];
                    objManager.strManagerType = [currentDictionary objectForKey:@"ManagerType"];
                    objManager.strManagerAddress = [currentDictionary objectForKey:@"ManagerAddress"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objManager.strManagerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ManagerImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    objManager.strManagerBannerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ManagerBannerUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    objManager.strManagerStateID = [currentDictionary objectForKey:@"ManagerStateId"];
                    objManager.strManagerCityID = [currentDictionary objectForKey:@"ManagerCityId"];
                    
                    [self.arrayAllApprovedManagersByArtistId addObject:objManager];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllApprovedManagersEvent" object:nil];
//                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO ADD MANAGERS FOR ARTIST BY ARTIST ID AND MANAGERS IDS

-(void)addManagers:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AddManagers"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"array_manager_ids_for_approval"] forKey:@"array_manager_ids_for_approval"];
        [parameters setObject:@"1" forKey:@"device_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedManagersEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO REMOVE APPROVED MANAGERS FOR ARTIST BY ARTIST ID AND MANAGERS IDS

-(void)removeApprovedManagers:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"RemoveApprovedManagers"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"array_manager_ids_for_removal"] forKey:@"array_manager_ids_for_removal"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL APPROVED MANAGERS ARRAY ==========//
                    NSArray *arrayManagerList = [jsonResult objectForKey:@"managers"];
                    
                    self.arrayAllApprovedManagersByArtistId = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayManagerList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayManagerList objectAtIndex:i];
                        
                        Manager *objManager = [[Manager alloc] init];
                        objManager.strManagerID = [currentDictionary objectForKey:@"ManagerId"];
                        objManager.strManagerName = [currentDictionary objectForKey:@"ManagerName"];
                        objManager.strManagerType = [currentDictionary objectForKey:@"ManagerType"];
                        objManager.strManagerAddress = [currentDictionary objectForKey:@"ManagerAddress"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objManager.strManagerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ManagerImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objManager.strManagerBannerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ManagerBannerUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objManager.strManagerStateID = [currentDictionary objectForKey:@"ManagerStateId"];
                        objManager.strManagerCityID = [currentDictionary objectForKey:@"ManagerCityId"];
                        
                        [self.arrayAllApprovedManagersByArtistId addObject:objManager];
                    }
                    
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.arrayArtistApprovedManagerIds = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < self.arrayAllApprovedManagersByArtistId.count; i++)
                    {
                        Manager *objManager = [self.arrayAllApprovedManagersByArtistId objectAtIndex:i];
                        [self.objLoggedInUser.arrayArtistApprovedManagerIds addObject:objManager.strManagerID];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"removedApprovedManagersEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ARTIST PROFILE DETAILS BY ARTIST ID

-(void)getArtistProfileByArtistId:(NSString *)strArtistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetArtistProfileByArtistId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    self.strSideMenuBackgroundImageUrl = [[jsonResult objectForKey:@"sidemenubackgroundimageurl"] stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
//                    self.strSideMenuBackgroundImageUrl = [[jsonResult objectForKey:@"sidemenubackgroundimageurl"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    self.objLoggedInUser.strFullname = [jsonResult objectForKey:@"fullname"];
                    self.objLoggedInUser.strEmail = [jsonResult objectForKey:@"useremail"];
                    self.objLoggedInUser.strPhoneNumber = [jsonResult objectForKey:@"phone_number"];
                    self.objLoggedInUser.strGender = [jsonResult objectForKey:@"gender"];
                    self.objLoggedInUser.strStateId = [jsonResult objectForKey:@"state_id"];
                    self.objLoggedInUser.strStateName = [jsonResult objectForKey:@"state_name"];
                    self.objLoggedInUser.strCityId = [jsonResult objectForKey:@"city_id"];
                    self.objLoggedInUser.strCityName = [jsonResult objectForKey:@"city_name"];
                    self.objLoggedInUser.strUserType = [jsonResult objectForKey:@"user_type"];
                    
                    self.objLoggedInUser.boolIsOnline = [[jsonResult objectForKey:@"isOnline"] boolValue];
                    self.objLoggedInUser.strOnlineManagerId = [jsonResult objectForKey:@"online_manager_id"];
                    self.objLoggedInUser.strOnlineManagerName = [jsonResult objectForKey:@"online_manager_name"];
                    self.objLoggedInUser.strOnlinePlaylistId = [jsonResult objectForKey:@"online_playlist_id"];
                    self.objLoggedInUser.strOnlinePlaylistName = [jsonResult objectForKey:@"online_playlist_name"];
                    self.objLoggedInUser.boolIsOnlineManagerSetWithPresetPlaylist = [[jsonResult objectForKey:@"isOnlineManagerSetWithPresetPlaylist"] boolValue];
                    self.objLoggedInUser.strOnlineEventId = [jsonResult objectForKey:@"online_event_id"];
                    self.objLoggedInUser.strOnlineEventName = [jsonResult objectForKey:@"online_event_name"];
                    
                    self.objLoggedInUser.strFacebookProfileUrl = [jsonResult objectForKey:@"facebook_profile_url"];
                    self.objLoggedInUser.strTwitterProfileUrl = [jsonResult objectForKey:@"twitter_profile_url"];
                    self.objLoggedInUser.strInstagramProfileUrl = [jsonResult objectForKey:@"instagram_profile_url"];
                    self.objLoggedInUser.strGooglePlusProfileUrl = [jsonResult objectForKey:@"googleplus_profile_url"];
                    
                    self.objLoggedInUser.strTwitterInstagramMesasage = [jsonResult objectForKey:@"twitter_instagram_online_message"];
                    
                    //========== FILL MANAGERS ARRAY ==========//
                    NSArray *arrayArtistApprovedManagerList = [jsonResult objectForKey:@"approved_managers"];
                    
                    self.objLoggedInUser.arrayArtistApprovedManagers = [[NSMutableArray alloc] init];
                    
                    self.objLoggedInUser.arrayArtistApprovedManagerIds = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayArtistApprovedManagerList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayArtistApprovedManagerList objectAtIndex:i];
                        
                        NSString *strArtistApprovedManagerId = [currentDictionary objectForKey:@"ManagerId"];
                        [self.objLoggedInUser.arrayArtistApprovedManagerIds addObject:strArtistApprovedManagerId];
                        
                        Manager *objManager = [[Manager alloc] init];
                        objManager.strManagerID = [currentDictionary objectForKey:@"ManagerId"];
                        objManager.strManagerName = [currentDictionary objectForKey:@"ManagerName"];
                        objManager.strManagerType = [currentDictionary objectForKey:@"ManagerType"];
                        objManager.strManagerAddress = [currentDictionary objectForKey:@"ManagerAddress"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objManager.strManagerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ManagerImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objManager.strManagerBannerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ManagerBannerUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objManager.strManagerStateID = [currentDictionary objectForKey:@"ManagerStateId"];
                        objManager.strManagerCityID = [currentDictionary objectForKey:@"ManagerCityId"];
                        
                        objManager.strManagerPlaylistID = [currentDictionary objectForKey:@"ManagerPlaylistId"];
                        objManager.strManagerPlaylistName = [currentDictionary objectForKey:@"ManagerPlaylistName"];
                        
                        [self.objLoggedInUser.arrayArtistApprovedManagers addObject:objManager];
                    }
                    
                    //========== FILL PLAYLISTS ARRAY ==========//
                    NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                    
                    self.objLoggedInUser.arrayDjPlaylists = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                        
                        Playlist *objPlaylist = [[Playlist alloc] init];
                        objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                        objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                        
                        objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                        
                        NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                        
                        for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                        {
                            NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                            
                            Song *objSong = [[Song alloc] init];
                            
                            objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                            objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                            objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                            
                            [objPlaylist.arrayPlaylistSongs addObject:objSong];
                        }
                        
                        [self.objLoggedInUser.arrayDjPlaylists addObject:objPlaylist];
                    }
                    
                    //========== FILL STATES ARRAY ==========//
                    NSArray *arrayStateList = [jsonResult objectForKey:@"states"];
                    
                    self.arrayStates = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayStateList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayStateList objectAtIndex:i];
                        
                        State *objState = [[State alloc] init];
                        objState.strStateID = [currentDictionary objectForKey:@"StateId"];
                        objState.strStateName = [currentDictionary objectForKey:@"StateName"];
                        
                        NSArray *arrayCityList = [currentDictionary objectForKey:@"Cities"];
                        
                        objState.arrayCity = [[NSMutableArray alloc] init];
                        
                        for(int j = 0 ; j < arrayCityList.count; j++)
                        {
                            NSDictionary *currentDictionaryCity = [arrayCityList objectAtIndex:j];
                            
                            City *objCity = [[City alloc] init];
                            objCity.strCityID = [currentDictionaryCity objectForKey:@"CityId"];
                            objCity.strCityName = [currentDictionaryCity objectForKey:@"CityName"];
                            
                            [objState.arrayCity addObject:objCity];
                        }
                        
                        [self.arrayStates addObject:objState];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotArtistProfileByArtistIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO UPDATE ARTIST PROFILE DETAILS BY ARTIST ID

-(void)updateArtistProfile:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"UpdateArtistProfile"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"fullname"] forKey:@"fullname"];
        [parameters setObject:[[dictParameters objectForKey:@"gender"] lowercaseString] forKey:@"gender"];
        [parameters setObject:[dictParameters objectForKey:@"phone_number"] forKey:@"phone_number"];
        [parameters setObject:[dictParameters objectForKey:@"state_id"] forKey:@"state_id"];
        [parameters setObject:[dictParameters objectForKey:@"state_name"] forKey:@"state_name"];
        [parameters setObject:[dictParameters objectForKey:@"city_id"] forKey:@"city_id"];
        [parameters setObject:[dictParameters objectForKey:@"city_name"] forKey:@"city_name"];
        [parameters setObject:[dictParameters objectForKey:@"user_type"] forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.strFullname = [dictParameters objectForKey:@"fullname"];
                    self.objLoggedInUser.strPhoneNumber = [dictParameters objectForKey:@"phone_number"];
                    self.objLoggedInUser.strGender = [dictParameters objectForKey:@"gender"];
                    self.objLoggedInUser.strStateId = [dictParameters objectForKey:@"state_id"];
                    self.objLoggedInUser.strStateName = [dictParameters objectForKey:@"state_name"];
                    self.objLoggedInUser.strCityId = [dictParameters objectForKey:@"city_id"];
                    self.objLoggedInUser.strCityName = [dictParameters objectForKey:@"city_name"];
                    self.objLoggedInUser.strUserType = [dictParameters objectForKey:@"user_type"];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setObject:self.objLoggedInUser.strFullname forKey:@"userfullname"];
                    [prefs setObject:self.objLoggedInUser.strStateId forKey:@"stateid"];
                    [prefs setObject:self.objLoggedInUser.strCityId forKey:@"cityid"];
                    [prefs synchronize];
                    
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedArtistProfileEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO UPLOAD ARTIST PROFILE PICTURE WITH ARTIST ID

-(void)uploadArtistProfilePicture:(NSString *)strProfilePictureBase64Data
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strLoggedInUserId = [prefs objectForKey:@"userid"];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"UploadArtistProfileImage"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strLoggedInUserId forKey:@"user_id"];
        [parameters setObject:strProfilePictureBase64Data forKey:@"user_profile_picture_data"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
                    [prefs synchronize];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"uploadedArtistProfilePictureEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO UPLOAD ARTIST PROFILE PICTURE WITH ARTIST ID WITH $_FILE

-(void)uploadArtistProfilePictureWith_File:(NSData *)profilePictureData
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strLoggedInUserId = [prefs objectForKey:@"userid"];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"UploadArtistProfileImage"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strLoggedInUserId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        manager.requestSerializer.timeoutInterval = 60000;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        
        [manager POST:urlString parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileData:profilePictureData name:@"user_profile_picture_data" fileName:[NSString stringWithFormat:@"%@.png", strLoggedInUserId] mimeType:@"image/png"];
            
        } progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    self.objLoggedInUser.strProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [jsonResult objectForKey:@"profilepictureimageurl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [prefs setObject:self.objLoggedInUser.strProfilePictureImageUrl forKey:@"profilepictureimageurl"];
                    [prefs synchronize];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"uploadedArtistProfilePictureWith_FileEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO UPDATE ARTIST SOCIAL MEDIA PROFILE URL BY ARTIST ID

-(void)updateArtistSocialMediaProfileUrl:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"UpdateArtistSocialMediaProfileUrl"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"social_media_profile_url"] forKey:@"social_media_profile_url"];
        [parameters setObject:[dictParameters objectForKey:@"social_media_profile_type"] forKey:@"social_media_profile_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //                    NSString *message = [jsonResult objectForKey:@"msg"];
                    //                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                    
                    if([[dictParameters objectForKey:@"social_media_profile_type"] isEqualToString:@"facebook"])
                    {
                        self.objLoggedInUser.strFacebookProfileUrl = [dictParameters objectForKey:@"social_media_profile_url"];
                    }
                    else if([[dictParameters objectForKey:@"social_media_profile_type"] isEqualToString:@"twitter"])
                    {
                        self.objLoggedInUser.strTwitterProfileUrl = [dictParameters objectForKey:@"social_media_profile_url"];
                    }
                    else if([[dictParameters objectForKey:@"social_media_profile_type"] isEqualToString:@"instagram"])
                    {
                        self.objLoggedInUser.strInstagramProfileUrl = [dictParameters objectForKey:@"social_media_profile_url"];
                    }
                    else if([[dictParameters objectForKey:@"social_media_profile_type"] isEqualToString:@"googleplus"])
                    {
                        self.objLoggedInUser.strGooglePlusProfileUrl = [dictParameters objectForKey:@"social_media_profile_url"];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updatedArtistSocialMediaProfileUrlEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO CHANGE ARTIST PASSWORD

-(void)changeArtistPassword:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"ChangeArtistPassword"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"strOldPassword"] forKey:@"oldpassword"];
        [parameters setObject:[dictParameters objectForKey:@"strNewPassword"] forKey:@"newpassword"];
        [parameters setObject:[dictParameters objectForKey:@"strRepeatPassword"] forKey:@"repeatpassword"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedArtistPasswordEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL PLAYLISTS BY ARTIST ID

-(void)getAllPlaylistsByArtistId:(NSString *)strArtistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetAllPlaylistsByArtistId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                //                {
                //========== FILL PLAYLISTS ARRAY ==========//
                NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                
                if(self.objLoggedInUser == nil)
                {
                    self.objLoggedInUser = [[User alloc] init];
                }
                
                self.objLoggedInUser.arrayArtistPlaylists = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                    
                    Playlist *objPlaylist = [[Playlist alloc] init];
                    objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                    objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                    
                    objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                    
                    NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                    
                    for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                    {
                        NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                        
                        Song *objSong = [[Song alloc] init];
                        
                        objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                        objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                        objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                        
                        [objPlaylist.arrayPlaylistSongs addObject:objSong];
                    }
                    
                    [self.objLoggedInUser.arrayArtistPlaylists addObject:objPlaylist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllPlaylistsByArtistIdEvent" object:nil];
//                }
//                else
//                {
//                    NSString *message = [jsonResult objectForKey:@"msg"];
//                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
//                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO DELETE PLAYLIST BY ARTIST ID AND PLAYLIST ID

-(void)deletePlaylistByArtistId:(NSString *)strArtistId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"DeletePlaylistByArtistId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL PLAYLISTS ARRAY ==========//
                    NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                    
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.arrayArtistPlaylists = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                        
                        Playlist *objPlaylist = [[Playlist alloc] init];
                        objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                        objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                        
                        objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                        
                        NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                        
                        for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                        {
                            NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                            
                            Song *objSong = [[Song alloc] init];
                            
                            objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                            objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                            objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                            
                            [objPlaylist.arrayPlaylistSongs addObject:objSong];
                        }
                        
                        [self.objLoggedInUser.arrayArtistPlaylists addObject:objPlaylist];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedPlaylistByArtistIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO DELETE SONG BY ARTIST ID AND SONG ID

-(void)deleteSongByArtistId:(NSString *)strArtistId withSongId:(NSString *)strSongId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"DeleteSongByArtistId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        [parameters setObject:strSongId forKey:@"song_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedSongByArtistIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO ADD ARTIST SONG

-(void)addArtistSong:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AddArtistSong"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistName"] forKey:@"playlist_name"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistID"] forKey:@"playlist_id"];
        [parameters setObject:[dictParameters objectForKey:@"strSongName"] forKey:@"song_name"];
        [parameters setObject:[dictParameters objectForKey:@"strArtistName"] forKey:@"artist_name"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedArtistSongEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO ADD ARTIST PLAYLIST

-(void)addArtistPlaylist:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AddArtistPlaylist"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistName"] forKey:@"playlist_name"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedArtistPlaylistEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL UNACCEPTED REQUESTS LIST BY ARTIST ID

-(void)getAllUnacceptedRequestsByArtistId:(NSString *)strArtistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetAllUnacceptedRequestsByArtistId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                //                {
                //========== FILL SONG REQUESTS ARRAY ==========//
                NSArray *arraySongRequestsList = [jsonResult objectForKey:@"song_requests"];
                
                self.arrayArtistSongsRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arraySongRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arraySongRequestsList objectAtIndex:i];
                    
                    SongRequest *objSongRequest = [[SongRequest alloc] init];
                    objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                    objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                    objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                    objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                    objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                    objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                    objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                    objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestArtistAmount"];
                    
                    if(!objSongRequest.boolIsSongRequestExpired)
                    {
                        [self.arrayArtistSongsRequests addObject:objSongRequest];
                    }
                }
                
                //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                NSArray *arrayShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                
                self.arrayArtistShoutoutRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayShoutoutRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayShoutoutRequestsList objectAtIndex:i];
                    
                    ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                    objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                    objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                    objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                    objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                    objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                    objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                    objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                    objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestArtistAmount"];
                    
                    if(!objShoutoutRequest.boolIsShoutoutRequestExpired)
                    {
                        [self.arrayArtistShoutoutRequests addObject:objShoutoutRequest];
                    }
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllUnacceptedRequestsByArtistIdEvent" object:nil];
                //                }
                //                else
                //                {
                //                    NSString *message = [jsonResult objectForKey:@"msg"];
                //                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                //                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL ACCEPTED REQUESTS LIST BY ARTIST ID

-(void)getAllAcceptedRequestsByArtistId:(NSString *)strArtistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetAllAcceptedRequestsByArtistId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                //                {
                //========== FILL SONG REQUESTS ARRAY ==========//
                NSArray *arrayArtistAcceptedSongRequestsList = [jsonResult objectForKey:@"song_requests"];
                
                self.arrayArtistAcceptedSongsRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayArtistAcceptedSongRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayArtistAcceptedSongRequestsList objectAtIndex:i];
                    
                    SongRequest *objSongRequest = [[SongRequest alloc] init];
                    objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                    objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                    objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                    objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                    objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                    objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                    objSongRequest.strSongRequestStatus = [currentDictionary objectForKey:@"SongRequestStatus"];
                    objSongRequest.strSongRequestAcceptanceType = [currentDictionary objectForKey:@"SongRequestAcceptanceType"];
                    objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                    objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestArtistAmount"];
                    
                    objSongRequest.strSongRequestDjName = [currentDictionary objectForKey:@"SongRequestArtistName"];
                    objSongRequest.strSongRequestClubName = [currentDictionary objectForKey:@"SongRequestManagerName"];
                    objSongRequest.strSongRequestUserName = [currentDictionary objectForKey:@"SongRequestUserName"];
                    
                    if(!objSongRequest.boolIsSongRequestExpired)
                    {
                        [self.arrayArtistAcceptedSongsRequests addObject:objSongRequest];
                    }
                }
                
                //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                NSArray *arrayArtistAcceptedShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                
                self.arrayArtistAcceptedShoutoutRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayArtistAcceptedShoutoutRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayArtistAcceptedShoutoutRequestsList objectAtIndex:i];
                    
                    ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                    objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                    objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                    objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                    objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                    objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                    objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                    objShoutoutRequest.strShoutoutRequestStatus = [currentDictionary objectForKey:@"ShoutoutRequestStatus"];
                    objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                    objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestArtistAmount"];
                    
                    objShoutoutRequest.strShoutoutRequestDjName = [currentDictionary objectForKey:@"ShoutoutRequestArtistName"];
                    objShoutoutRequest.strShoutoutRequestClubName = [currentDictionary objectForKey:@"ShoutoutRequestManagerName"];
                    objShoutoutRequest.strShoutoutRequestUserName = [currentDictionary objectForKey:@"ShoutoutRequestUserName"];
                    
                    if(!objShoutoutRequest.boolIsShoutoutRequestExpired)
                    {
                        [self.arrayArtistAcceptedShoutoutRequests addObject:objShoutoutRequest];
                    }
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllAcceptedRequestsByArtistIdEvent" object:nil];
                //                }
                //                else
                //                {
                //                    NSString *message = [jsonResult objectForKey:@"msg"];
                //                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                //                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL PLAYED REQUESTS LIST BY ARTIST ID

-(void)getAllPlayedRequestsByArtistId:(NSString *)strArtistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetAllPlayedRequestsByArtistId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                //                {
                //========== FILL SONG REQUESTS ARRAY ==========//
                NSArray *arrayArtistPlayedSongRequestsList = [jsonResult objectForKey:@"song_requests"];
                
                self.arrayArtistPlayedSongsRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayArtistPlayedSongRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayArtistPlayedSongRequestsList objectAtIndex:i];
                    
                    SongRequest *objSongRequest = [[SongRequest alloc] init];
                    objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                    objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                    objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                    objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                    objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                    objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                    objSongRequest.strSongRequestStatus = [currentDictionary objectForKey:@"SongRequestStatus"];
                    objSongRequest.strSongRequestAcceptanceType = [currentDictionary objectForKey:@"SongRequestAcceptanceType"];
                    objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                    objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestArtistAmount"];
                    
                    [self.arrayArtistPlayedSongsRequests addObject:objSongRequest];
                }
                
                //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                NSArray *arrayArtistPlayedShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                
                self.arrayArtistPlayedShoutoutRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayArtistPlayedShoutoutRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayArtistPlayedShoutoutRequestsList objectAtIndex:i];
                    
                    ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                    objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                    objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                    objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                    objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                    objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                    objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                    objShoutoutRequest.strShoutoutRequestStatus = [currentDictionary objectForKey:@"ShoutoutRequestStatus"];
                    objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                    objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestArtistAmount"];
                    
                    [self.arrayArtistPlayedShoutoutRequests addObject:objShoutoutRequest];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllPlayedRequestsByArtistIdEvent" object:nil];
                //                }
                //                else
                //                {
                //                    NSString *message = [jsonResult objectForKey:@"msg"];
                //                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                //                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR ARTISTS TO ACCEPT SONG REQUEST

-(void)acceptArtistSongRequest:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AcceptArtistSongRequest"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"songrequest_id"] forKey:@"artist_songrequest_id"];
        [parameters setObject:[dictParameters objectForKey:@"songrequest_acceptance_type"] forKey:@"songrequest_acceptance_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if([[dictParameters objectForKey:@"songrequest_acceptance_type"] isEqualToString:[MySingleton sharedManager].strDjSongRequestAcceptanceTypeWithinNext3Songs])
                    {
                        [MySingleton sharedManager].dataManager.strDjSongRequestAcceptanceTypeMessage = @"You have accepted this song request to be played within next 3 songs";
                    }
                    else if([[dictParameters objectForKey:@"songrequest_acceptance_type"] isEqualToString:[MySingleton sharedManager].strDjSongRequestAcceptanceTypeAfter15Minutes])
                    {
                        [MySingleton sharedManager].dataManager.strDjSongRequestAcceptanceTypeMessage = @"You have accepted this song request to be played after 15 minutes";
                    }
                    
                    //========== FILL SONG REQUESTS ARRAY ==========//
                    NSArray *arraySongRequestsList = [jsonResult objectForKey:@"song_requests"];
                    
                    self.arrayArtistSongsRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arraySongRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arraySongRequestsList objectAtIndex:i];
                        
                        SongRequest *objSongRequest = [[SongRequest alloc] init];
                        objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                        objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                        objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                        objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                        objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                        objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                        objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                        objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestArtistAmount"];
                        
                        if(!objSongRequest.boolIsSongRequestExpired)
                        {
                            [self.arrayArtistSongsRequests addObject:objSongRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptedArtistSongRequestEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR ARTISTS TO DECLINE SONG REQUEST

-(void)declineArtistongRequest:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"DeclineArtistSongRequest"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"songrequest_id"] forKey:@"songrequest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You have declined this song request"];
                    
                    //========== FILL SONG REQUESTS ARRAY ==========//
                    NSArray *arraySongRequestsList = [jsonResult objectForKey:@"song_requests"];
                    
                    self.arrayArtistSongsRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arraySongRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arraySongRequestsList objectAtIndex:i];
                        
                        SongRequest *objSongRequest = [[SongRequest alloc] init];
                        objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                        objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                        objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                        objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                        objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                        objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                        objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                        objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestArtistAmount"];
                        
                        if(!objSongRequest.boolIsSongRequestExpired)
                        {
                            [self.arrayArtistSongsRequests addObject:objSongRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"declinedArtistongRequestEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR ARTISTS TO ACCEPT SHOUTOUT REQUEST

-(void)acceptArtistShoutoutRequest:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AcceptArtistShoutoutRequest"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"shoutoutrequest_id"] forKey:@"artist_shoutoutrequest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You have accepted this shoutout request"];
                    
                    //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                    NSArray *arrayShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                    
                    self.arrayArtistShoutoutRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayShoutoutRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayShoutoutRequestsList objectAtIndex:i];
                        
                        ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                        objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                        objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                        objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                        objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                        objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                        objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                        objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                        objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestArtistAmount"];
                        
                        if(!objShoutoutRequest.boolIsShoutoutRequestExpired)
                        {
                            [self.arrayArtistShoutoutRequests addObject:objShoutoutRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptedArtistShoutoutRequestEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR ARTISTS TO DECLINE SHOUTOUT REQUEST

-(void)declineArtistShoutoutRequest:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"DeclineArtistShoutoutRequest"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"shoutoutrequest_id"] forKey:@"shoutoutrequest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You have declined this shoutout request"];
                    
                    //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                    NSArray *arrayShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                    
                    self.arrayArtistShoutoutRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayShoutoutRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayShoutoutRequestsList objectAtIndex:i];
                        
                        ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                        objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                        objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                        objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                        objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                        objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                        objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                        objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                        objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestArtistAmount"];
                        
                        if(!objShoutoutRequest.boolIsShoutoutRequestExpired)
                        {
                            [self.arrayArtistShoutoutRequests addObject:objShoutoutRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"declinedArtistShoutoutRequestEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL MY EARNINGS REQUESTS LIST BY ARTIST ID

-(void)getAllMyEarningsRequestsByArtistId:(NSString *)strArtistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetAllMyEarningsRequestsByArtistId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                //                {
                //========== FILL SONG REQUESTS ARRAY ==========//
                NSArray *arrayArtistMyEarningsSongsRequestsList = [jsonResult objectForKey:@"song_requests"];
                
                self.arrayArtistMyEarningsSongsRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayArtistMyEarningsSongsRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayArtistMyEarningsSongsRequestsList objectAtIndex:i];
                    
                    SongRequest *objSongRequest = [[SongRequest alloc] init];
                    objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                    objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                    objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                    objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                    objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                    objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                    objSongRequest.strSongRequestStatus = [currentDictionary objectForKey:@"SongRequestStatus"];
                    objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestAmount"];
                    objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestArtistAmount"];
                    
                    [self.arrayArtistMyEarningsSongsRequests addObject:objSongRequest];
                }
                
                //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                NSArray *arrayArtistMyEarningsShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                
                self.arrayArtistMyEarningsShoutoutRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayArtistMyEarningsShoutoutRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayArtistMyEarningsShoutoutRequestsList objectAtIndex:i];
                    
                    ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                    objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                    objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                    objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                    objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                    objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                    objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                    objShoutoutRequest.strShoutoutRequestStatus = [currentDictionary objectForKey:@"ShoutoutRequestStatus"];
                    objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestAmount"];
                    objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                    objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestArtistAmount"];
                    
                    [self.arrayArtistMyEarningsShoutoutRequests addObject:objShoutoutRequest];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllMyEarningsRequestsByArtistIdEvent" object:nil];
                //                }
                //                else
                //                {
                //                    NSString *message = [jsonResult objectForKey:@"msg"];
                //                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                //                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR ARTISTS TO MARK SONG REQUEST AS PLAYED

-(void)markArtistSongRequestAsPlayed:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"MarkArtistSongRequestAsPlayed"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"songrequest_id"] forKey:@"artist_songrequest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You have successfully marked this request as played."];
                    
                    //========== FILL SONG REQUESTS ARRAY ==========//
                    NSArray *arrayArtistAcceptedSongRequestsList = [jsonResult objectForKey:@"song_requests"];
                    
                    self.arrayArtistAcceptedSongsRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayArtistAcceptedSongRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayArtistAcceptedSongRequestsList objectAtIndex:i];
                        
                        SongRequest *objSongRequest = [[SongRequest alloc] init];
                        objSongRequest.strSongRequestID = [currentDictionary objectForKey:@"SongRequestId"];
                        objSongRequest.strSongID = [currentDictionary objectForKey:@"SongId"];
                        objSongRequest.strSongName = [currentDictionary objectForKey:@"SongName"];
                        objSongRequest.strSongArtistName = [currentDictionary objectForKey:@"SongArtistName"];
                        objSongRequest.strSongRequestTime = [currentDictionary objectForKey:@"SongRequestTime"];
                        objSongRequest.boolIsSongRequestVIP = [[currentDictionary objectForKey:@"IsSongRequestVIP"] boolValue];
                        objSongRequest.strSongRequestStatus = [currentDictionary objectForKey:@"SongRequestStatus"];
                        objSongRequest.strSongRequestAcceptanceType = [currentDictionary objectForKey:@"SongRequestAcceptanceType"];
                        objSongRequest.boolIsSongRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objSongRequest.strSongRequestAmount = [currentDictionary objectForKey:@"SongRequestTotalAmount"];
                        objSongRequest.strSongRequestDjAmount = [currentDictionary objectForKey:@"SongRequestArtistAmount"];
                        
                        objSongRequest.strSongRequestDjName = [currentDictionary objectForKey:@"SongRequestArtistName"];
                        objSongRequest.strSongRequestClubName = [currentDictionary objectForKey:@"SongRequestManagerName"];
                        objSongRequest.strSongRequestUserName = [currentDictionary objectForKey:@"SongRequestUserName"];
                        
                        if(!objSongRequest.boolIsSongRequestExpired)
                        {
                            [self.arrayArtistAcceptedSongsRequests addObject:objSongRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"markedArtistSongRequestAsPlayedEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR ARTISTS TO MARK SHOUTOUT REQUEST AS PLAYED

-(void)markArtistShoutoutRequestAsPlayed:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"MarkArtistShoutoutRequestAsPlayed"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"shoutoutrequest_id"] forKey:@"artist_shoutoutrequest_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You have successfully marked this request as played."];
                    
                    //========== FILL SHOUTOUT REQUESTS ARRAY ==========//
                    NSArray *arrayArtistAcceptedShoutoutRequestsList = [jsonResult objectForKey:@"shoutout_requests"];
                    
                    self.arrayArtistAcceptedShoutoutRequests = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayArtistAcceptedShoutoutRequestsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayArtistAcceptedShoutoutRequestsList objectAtIndex:i];
                        
                        ShoutoutRequest *objShoutoutRequest = [[ShoutoutRequest alloc] init];
                        objShoutoutRequest.strShoutoutRequestID = [currentDictionary objectForKey:@"ShoutoutRequestId"];
                        objShoutoutRequest.strShoutoutRequestOccasion = [currentDictionary objectForKey:@"ShoutoutOccasion"];
                        objShoutoutRequest.strShoutoutRequestForWhom = [currentDictionary objectForKey:@"ShoutoutForWhom"];
                        objShoutoutRequest.strShoutoutRequestNoteComments = [currentDictionary objectForKey:@"ShoutoutNoteComments"];
                        objShoutoutRequest.strShoutoutRequestTime = [currentDictionary objectForKey:@"ShoutoutRequestTime"];
                        objShoutoutRequest.boolIsShoutoutRequestVIP = [[currentDictionary objectForKey:@"IsShoutoutRequestVIP"] boolValue];
                        objShoutoutRequest.strShoutoutRequestStatus = [currentDictionary objectForKey:@"ShoutoutRequestStatus"];
                        objShoutoutRequest.boolIsShoutoutRequestExpired = [[currentDictionary objectForKey:@"isExpired"] boolValue];
                        objShoutoutRequest.strShoutoutRequestAmount = [currentDictionary objectForKey:@"ShoutoutRequestTotalAmount"];
                        objShoutoutRequest.strShoutoutRequestDjAmount = [currentDictionary objectForKey:@"ShoutoutRequestArtistAmount"];
                        
                        objShoutoutRequest.strShoutoutRequestDjName = [currentDictionary objectForKey:@"ShoutoutRequestArtistName"];
                        objShoutoutRequest.strShoutoutRequestClubName = [currentDictionary objectForKey:@"ShoutoutRequestManagerName"];
                        objShoutoutRequest.strShoutoutRequestUserName = [currentDictionary objectForKey:@"ShoutoutRequestUserName"];
                        
                        if(!objShoutoutRequest.boolIsShoutoutRequestExpired)
                        {
                            [self.arrayArtistAcceptedShoutoutRequests addObject:objShoutoutRequest];
                        }
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"markedArtistShoutoutRequestAsPlayedEvent" object:nil];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL EVENTS BY ARTIST ID

-(void)getAllEventsByArtistId:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetAllEventsByArtistId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [parameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL EVENTS ARRAY ==========//
                NSArray *arrayEventList = [jsonResult objectForKey:@"events"];
                
                self.arrayAllEventsByArtist = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayEventList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayEventList objectAtIndex:i];
                    
                    Event *objEvent = [[Event alloc] init];
                    objEvent.strEventID = [currentDictionary objectForKey:@"EventId"];
                    objEvent.strEventName = [currentDictionary objectForKey:@"EventName"];
                    objEvent.strEventDescription = [currentDictionary objectForKey:@"EventDescription"];
                    objEvent.strEventDate = [currentDictionary objectForKey:@"EventDate"];
                    objEvent.strEventLocation = [currentDictionary objectForKey:@"EventLocation"];
                    objEvent.strEventVenueAddress = [currentDictionary objectForKey:@"EventVenueAddress"];
                    objEvent.boolIsEventLive = [[currentDictionary objectForKey:@"IsEventLive"] boolValue];
                    
                    [self.arrayAllEventsByArtist addObject:objEvent];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllEventsByArtistIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO ADD ARTIST EVENT

-(void)addArtistEvent:(NSDictionary *)dictParameters
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AddArtistEvent"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"event_name"] forKey:@"event_name"];
        [parameters setObject:[dictParameters objectForKey:@"event_location"] forKey:@"event_location"];
        [parameters setObject:[dictParameters objectForKey:@"event_date"] forKey:@"event_date"];
        [parameters setObject:[dictParameters objectForKey:@"event_venue_address"] forKey:@"event_venue_address"];
        [parameters setObject:[dictParameters objectForKey:@"event_description"] forKey:@"event_description"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedArtistEventEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FFUNCTION TO DELETE ARTIST EVENT

-(void)deleteArtistEvent:(NSDictionary *)dictParameters
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"DeleteArtistEvent"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"event_id"] forKey:@"event_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL EVENTS ARRAY ==========//
                    NSArray *arrayEventList = [jsonResult objectForKey:@"events"];
                    
                    self.arrayAllEventsByArtist = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayEventList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayEventList objectAtIndex:i];
                        
                        Event *objEvent = [[Event alloc] init];
                        objEvent.strEventID = [currentDictionary objectForKey:@"EventId"];
                        objEvent.strEventName = [currentDictionary objectForKey:@"EventName"];
                        objEvent.strEventDescription = [currentDictionary objectForKey:@"EventDescription"];
                        objEvent.strEventDate = [currentDictionary objectForKey:@"EventDate"];
                        objEvent.strEventLocation = [currentDictionary objectForKey:@"EventLocation"];
                        objEvent.strEventVenueAddress = [currentDictionary objectForKey:@"EventVenueAddress"];
                        objEvent.boolIsEventLive = [[currentDictionary objectForKey:@"IsEventLive"] boolValue];
                        
                        [self.arrayAllEventsByArtist addObject:objEvent];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedArtistEventEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET STATUS BY EVENT ID AND ARTIST ID

-(void)getArtistStatusForEvent:(NSDictionary *)dictParameters
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetArtistStatusForEvent"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"event_id"] forKey:@"event_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.boolIsOnlineAtEvent = [[jsonResult objectForKey:@"isOnlineAtEvent"] boolValue];
                    self.objLoggedInUser.strOnlineEventId = [jsonResult objectForKey:@"online_event_id"];
                    self.objLoggedInUser.strOnlineEventPlaylistId = [jsonResult objectForKey:@"online_event_playlist_id"];
                    self.objLoggedInUser.strOnlineEventPlaylistName = [jsonResult objectForKey:@"online_event_playlist_name"];
                    
                    //========== FILL PLAYLISTS ARRAY ==========//
                    NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                    
                    self.objLoggedInUser.arrayArtistPlaylists = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                        
                        Playlist *objPlaylist = [[Playlist alloc] init];
                        objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                        objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                        
                        objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                        
                        NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                        
                        for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                        {
                            NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                            
                            Song *objSong = [[Song alloc] init];
                            
                            objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                            objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                            objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                            
                            [objPlaylist.arrayPlaylistSongs addObject:objSong];
                        }
                        
                        [self.objLoggedInUser.arrayArtistPlaylists addObject:objPlaylist];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotArtistStatusForEventEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO CHANGE ARTIST EVENT STATUS

-(void)changeArtistEventStatus:(NSDictionary *)dictParameters
{
    if ([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"ChangeArtistEventStatus"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"event_id"] forKey:@"event_id"];
        [parameters setObject:[dictParameters objectForKey:@"playlist_id"] forKey:@"playlist_id"];
        [parameters setObject:[dictParameters objectForKey:@"is_online"] forKey:@"is_online"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.boolIsOnlineAtEvent = [[jsonResult objectForKey:@"isOnlineAtEvent"] boolValue];
                    self.objLoggedInUser.strOnlineEventId = [jsonResult objectForKey:@"online_event_id"];
                    self.objLoggedInUser.strOnlineEventPlaylistId = [jsonResult objectForKey:@"online_event_playlist_id"];
                    self.objLoggedInUser.strOnlineEventPlaylistName = [jsonResult objectForKey:@"online_event_playlist_name"];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"changedArtistEventStatusEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET ALL ADVERTISEMENTS LIST BY ARTIST ID

-(void)getAllArtistAdvertisementsByArtistId:(NSString *)strArtistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetAllAdvertisementsByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strArtistId forKey:@"user_id"];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        [parameters setObject:strUserType forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL SONG REQUESTS ARRAY ==========//
                NSArray *arrayAdvertisementsList = [jsonResult objectForKey:@"advertisements"];
                
                self.arrayArtistAdvertisements = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAdvertisementsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAdvertisementsList objectAtIndex:i];
                    
                    Advertisement *objAdvertisement = [[Advertisement alloc] init];
                    objAdvertisement.strAdvertisementId = [currentDictionary objectForKey:@"id"];
                    objAdvertisement.strAdvertisementImageUrl = [currentDictionary objectForKey:@"image"];
                    objAdvertisement.strAdvertisementRedirectionUrl = [currentDictionary objectForKey:@"url"];
                    
                    [self.arrayArtistAdvertisements addObject:objAdvertisement];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllArtistAdvertisementsByArtistIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO DELETE ARTIST ADVERTISEMENT BY ADVERTISEMENT ID

-(void)deleteArtistAdvertisementByAdvertisementId:(NSString *)strAdvertisementId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"DeleteArtistAdvertisement"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strAdvertisementId forKey:@"ad_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedArtistAdvertisementByAdvertisementIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO ADD ARTIST ADVERTISEMENT

-(void)addArtistAdvertisement:(NSDictionary *)dictParameters with_File:(NSData *)imageData
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AddArtistAdvertisement"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"url"] forKey:@"url"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        
        manager.requestSerializer.timeoutInterval = 60000;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        
        [manager POST:urlString parameters:parameters headers:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            [formData appendPartWithFileData:imageData name:@"image" fileName:@"1.png" mimeType:@"image/png"];
            
        } progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedArtistAdvertisementEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

//========== COMPANY WEBSERVICES ==========//

#pragma mark - FUNCTION FOR COMPANY TO GET APPROVED ARTISTS LIST BY COMPANY ID

-(void)getCompanyApprovedArtistListByCompanyId:(NSString *)strCompanyId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetClubOwnerApprovedDjListByUserId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL COMPANY APPROVED ARTISTS ARRAY ==========//
                NSArray *arrayCompanyApprovedArtistsList = [jsonResult objectForKey:@"manager_company_approved_artists"];
                
                self.arrayCompanyApprovedArtists = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayCompanyApprovedArtistsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayCompanyApprovedArtistsList objectAtIndex:i];
                    
                    Artist *objArtist = [[Artist alloc] init];
                    objArtist.strArtistID = [currentDictionary objectForKey:@"ArtistId"];
                    objArtist.strArtistName = [currentDictionary objectForKey:@"ArtistName"];
                    objArtist.strArtistManagerId = [currentDictionary objectForKey:@"ManagerId"];
                    objArtist.strArtistManagerName = [currentDictionary objectForKey:@"ManagerName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objArtist.strArtistProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ArtistImage"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayCompanyApprovedArtists addObject:objArtist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotCompanyApprovedArtistListByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO GET ARTISTS REQUEST LIST BY COMPANY ID

-(void)getCompanyArtistRequestListByCompanyId:(NSString *)strCompanyId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetCompanyArtistRequestListByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                self.strSideMenuBackgroundImageUrl = [[jsonResult objectForKey:@"sidemenubackgroundimageurl"] stringByAddingPercentEncodingWithAllowedCharacters:set];
                
                //========== FILL COMPANY ARTIST REQUESTS ARRAY ==========//
                NSArray *arrayCompanyArtistsRequestsList = [jsonResult objectForKey:@"manager_company_artists_requests"];
                
                self.arrayCompanyArtistsRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayCompanyArtistsRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayCompanyArtistsRequestsList objectAtIndex:i];
                    
                    Artist *objArtist = [[Artist alloc] init];
                    objArtist.strArtistID = [currentDictionary objectForKey:@"ArtistId"];
                    objArtist.strArtistName = [currentDictionary objectForKey:@"ArtistName"];
                    objArtist.strArtistManagerId = [currentDictionary objectForKey:@"ManagerId"];
                    objArtist.strArtistManagerName = [currentDictionary objectForKey:@"ManagerName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objArtist.strArtistProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ArtistImage"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayCompanyArtistsRequests addObject:objArtist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotCompanyArtistRequestListByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO ACCEPT ARTIST APPROVAL REQUEST BY COMPANY ID AND ARTIST ID

-(void)acceptArtistApprovalRequestByCompanyId:(NSString *)strCompanyId withArtistId:(NSString *)strArtistId withManagerId:(NSString *)strManagerId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AcceptArtistApprovalRequestByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        [parameters setObject:strArtistId forKey:@"artist_id"];
        [parameters setObject:strManagerId forKey:@"manager_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL COMPANY ARTIST REQUESTS ARRAY ==========//
                NSArray *arrayCompanyArtistsRequestsList = [jsonResult objectForKey:@"manager_company_artists_requests"];
                
                self.arrayCompanyArtistsRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayCompanyArtistsRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayCompanyArtistsRequestsList objectAtIndex:i];
                    
                    Artist *objArtist = [[Artist alloc] init];
                    objArtist.strArtistID = [currentDictionary objectForKey:@"ArtistId"];
                    objArtist.strArtistName = [currentDictionary objectForKey:@"ArtistName"];
                    objArtist.strArtistManagerId = [currentDictionary objectForKey:@"ManagerId"];
                    objArtist.strArtistManagerName = [currentDictionary objectForKey:@"ManagerName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objArtist.strArtistProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ArtistImage"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayCompanyArtistsRequests addObject:objArtist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptedArtistApprovalRequestByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO DECLINE ARTIST APPROVAL REQUEST BY COMPANY ID AND ARTIST ID

-(void)declineArtistApprovalRequestByCompanyId:(NSString *)strCompanyId withArtistId:(NSString *)strArtistId withManagerId:(NSString *)strManagerId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"DeclineArtistApprovalRequestByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        [parameters setObject:strArtistId forKey:@"artist_id"];
        [parameters setObject:strManagerId forKey:@"manager_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL COMPANY ARTIST REQUESTS ARRAY ==========//
                NSArray *arrayCompanyArtistsRequestsList = [jsonResult objectForKey:@"manager_company_artists_requests"];
                
                self.arrayCompanyArtistsRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayCompanyArtistsRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayCompanyArtistsRequestsList objectAtIndex:i];
                    
                    Artist *objArtist = [[Artist alloc] init];
                    objArtist.strArtistID = [currentDictionary objectForKey:@"ArtistId"];
                    objArtist.strArtistName = [currentDictionary objectForKey:@"ArtistName"];
                    objArtist.strArtistManagerId = [currentDictionary objectForKey:@"ManagerId"];
                    objArtist.strArtistManagerName = [currentDictionary objectForKey:@"ManagerName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objArtist.strArtistProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ArtistImage"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayCompanyArtistsRequests addObject:objArtist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"declinedArtistApprovalRequestByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO GET ARTISTS ONLINE REQUEST LIST BY COMPANY ID

-(void)getCompanyArtistOnlineRequestListByCompanyId:(NSString *)strCompanyId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetCompanyArtistOnlineRequestListByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL COMPANY ARTIST ONLINE REQUESTS ARRAY ==========//
                NSArray *arrayCompanyArtistsOnlineRequestsList = [jsonResult objectForKey:@"manager_company_artists_online_requests"];
                
                self.arrayCompanyArtistsOnlineRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayCompanyArtistsOnlineRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayCompanyArtistsOnlineRequestsList objectAtIndex:i];
                    
                    Artist *objArtist = [[Artist alloc] init];
                    objArtist.strArtistID = [currentDictionary objectForKey:@"ArtistId"];
                    objArtist.strArtistName = [currentDictionary objectForKey:@"ArtistName"];
                    objArtist.strArtistManagerId = [currentDictionary objectForKey:@"ManagerId"];
                    objArtist.strArtistManagerName = [currentDictionary objectForKey:@"ManagerName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objArtist.strArtistProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ArtistImage"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayCompanyArtistsOnlineRequests addObject:objArtist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotCompanyArtistOnlineRequestListByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO ACCEPT ARTIST ONLINE REQUEST BY COMPANY ID AND ARTIST ID

-(void)acceptArtistOnlineRequestByCompanyId:(NSString *)strCompanyId withArtistId:(NSString *)strArtistId withManagerId:(NSString *)strManagerId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AcceptArtistOnlineRequestByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        [parameters setObject:strArtistId forKey:@"artist_id"];
        [parameters setObject:strManagerId forKey:@"manager_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL COMPANY ARTIST ONLINE REQUESTS ARRAY ==========//
                NSArray *arrayCompanyArtistsOnlineRequestsList = [jsonResult objectForKey:@"manager_company_artists_online_requests"];
                
                self.arrayCompanyArtistsOnlineRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayCompanyArtistsOnlineRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayCompanyArtistsOnlineRequestsList objectAtIndex:i];
                    
                    Artist *objArtist = [[Artist alloc] init];
                    objArtist.strArtistID = [currentDictionary objectForKey:@"ArtistId"];
                    objArtist.strArtistName = [currentDictionary objectForKey:@"ArtistName"];
                    objArtist.strArtistManagerId = [currentDictionary objectForKey:@"ManagerId"];
                    objArtist.strArtistManagerName = [currentDictionary objectForKey:@"ManagerName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objArtist.strArtistProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ArtistImage"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayCompanyArtistsOnlineRequests addObject:objArtist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"acceptedArtistOnlineRequestByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO DECLINE ARTIST ONLINE REQUEST BY COMPANY ID AND ARTIST ID

-(void)declineArtistOnlineRequestByCompanyId:(NSString *)strCompanyId withArtistId:(NSString *)strArtistId withManagerId:(NSString *)strManagerId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"DeclineArtistOnlineRequestByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        [parameters setObject:strArtistId forKey:@"artist_id"];
        [parameters setObject:strManagerId forKey:@"manager_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL COMPANY ARTIST ONLINE REQUESTS ARRAY ==========//
                NSArray *arrayCompanyArtistsOnlineRequestsList = [jsonResult objectForKey:@"manager_company_artists_online_requests"];
                
                self.arrayCompanyArtistsOnlineRequests = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayCompanyArtistsOnlineRequestsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayCompanyArtistsOnlineRequestsList objectAtIndex:i];
                    
                    Artist *objArtist = [[Artist alloc] init];
                    objArtist.strArtistID = [currentDictionary objectForKey:@"ArtistId"];
                    objArtist.strArtistName = [currentDictionary objectForKey:@"ArtistName"];
                    objArtist.strArtistManagerId = [currentDictionary objectForKey:@"ManagerId"];
                    objArtist.strArtistManagerName = [currentDictionary objectForKey:@"ManagerName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objArtist.strArtistProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ArtistImage"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayCompanyArtistsOnlineRequests addObject:objArtist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"declinedArtistOnlineRequestByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO GET ONLINE ARTISTS LIST BY COMPANY ID

-(void)getCompanyOnlineArtistListByCompanyId:(NSString *)strCompanyId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetCompanyOnlineArtistListByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL COMPANY ONLINE ARTIST ARRAY ==========//
                NSArray *arrayCompanyOnlineArtistsList = [jsonResult objectForKey:@"manager_company_artists_online_requests"];
                
                self.arrayCompanyOnlineArtists = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayCompanyOnlineArtistsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayCompanyOnlineArtistsList objectAtIndex:i];
                    
                    Artist *objArtist = [[Artist alloc] init];
                    objArtist.strArtistID = [currentDictionary objectForKey:@"ArtistId"];
                    objArtist.strArtistName = [currentDictionary objectForKey:@"ArtistName"];
                    objArtist.strArtistManagerId = [currentDictionary objectForKey:@"ManagerId"];
                    objArtist.strArtistManagerName = [currentDictionary objectForKey:@"ManagerName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objArtist.strArtistProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ArtistImage"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    [self.arrayCompanyOnlineArtists addObject:objArtist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotCompanyOnlineArtistListByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO GET ARTIST LOGS BY COMPANY ID

-(void)getCompanyArtistLogsByCompanyId:(NSString *)strCompanyId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetCompanyArtistLogsByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL CLUB OWNER ONLINE DJ ARRAY ==========//
                NSArray *arrayCompanyArtistsLogsList = [jsonResult objectForKey:@"manager_company_approved_artists"];
                
                self.arrayCompanyArtistsLogs = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayCompanyArtistsLogsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayCompanyArtistsLogsList objectAtIndex:i];
                    
                    Artist *objArtist = [[Artist alloc] init];
                    objArtist.strArtistID = [currentDictionary objectForKey:@"ArtistId"];
                    objArtist.strArtistName = [currentDictionary objectForKey:@"ArtistName"];
                    objArtist.strArtistManagerId = [currentDictionary objectForKey:@"ManagerId"];
                    objArtist.strArtistManagerName = [currentDictionary objectForKey:@"ManagerName"];
                    NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                    objArtist.strArtistProfilePictureImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ArtistImage"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                    
                    NSString *strArtistOnlineDateTime = [currentDictionary objectForKey:@"OnlineTime"];
                    NSString *strArtistOfflineDateTime = [currentDictionary objectForKey:@"OfflineTime"];
                    
                    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
                    [dateFormatter1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                    dateFormatter1.dateStyle = NSDateFormatterMediumStyle;
                    [dateFormatter1 setDateFormat:@"dd MMM, yy hh:mm:ss a"];
                    
                    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc]init];
                    [dateFormatter2 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                    dateFormatter2.dateStyle = NSDateFormatterMediumStyle;
                    [dateFormatter2 setDateFormat:@"dd MMM, yy"];
                    
                    NSDateFormatter *timeFormatter1 = [[NSDateFormatter alloc]init];
                    [timeFormatter1 setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                    timeFormatter1.dateStyle = NSDateFormatterMediumStyle;
                    [timeFormatter1 setDateFormat:@"hh:mm a"];
                    
                    if(strArtistOnlineDateTime.length > 0)
                    {
                        NSDate *onlineDateTime = [dateFormatter1 dateFromString:strArtistOnlineDateTime];
                        
                        objArtist.strArtistOnlineDate = [dateFormatter2 stringFromDate:onlineDateTime];
                        objArtist.strArtistOnlineTime = [timeFormatter1 stringFromDate:onlineDateTime];
                    }
                    else
                    {
                        objArtist.strArtistOnlineDate = @"NA";
                        objArtist.strArtistOnlineTime = @"NA";
                        
                    }
                    
                    if(strArtistOfflineDateTime.length > 0)
                    {
                        NSDate *offlineDateTime = [dateFormatter1 dateFromString:strArtistOfflineDateTime];
                        
                        objArtist.strArtistOfflineDate = [dateFormatter2 stringFromDate:offlineDateTime];
                        objArtist.strArtistOfflineTime = [timeFormatter1 stringFromDate:offlineDateTime];
                    }
                    else
                    {
                        objArtist.strArtistOfflineDate = @"ONLINE";
                        objArtist.strArtistOfflineTime = @"ONLINE";
                    }
                    
                    [self.arrayCompanyArtistsLogs addObject:objArtist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"getCompanyArtistLogsByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO GET ALL MANAGERS

-(void)getCompanyAllManagers:(NSString *)strCompanyId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetCompanyAllManagers"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL CLUBS ARRAY ==========//
                    NSArray *arrayCompanyAllManagersList = [jsonResult objectForKey:@"managers"];
                    
                    self.arrayCompanyAllManagers = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayCompanyAllManagersList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayCompanyAllManagersList objectAtIndex:i];
                        
                        Manager *objManager = [[Manager alloc] init];
                        objManager.strManagerID = [currentDictionary objectForKey:@"ManagerId"];
                        objManager.strManagerName = [currentDictionary objectForKey:@"ManagerName"];
                        objManager.strManagerType = [currentDictionary objectForKey:@"ManagerType"];
                        objManager.strManagerAddress = [currentDictionary objectForKey:@"ManagerAddress"];
                        NSCharacterSet *set = [NSCharacterSet URLFragmentAllowedCharacterSet];
                        objManager.strManagerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ManagerImageUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objManager.strManagerBannerImageUrl = [[NSString stringWithFormat:@"%@%@", [_dictSettings objectForKey:@"ServerIPForImages"], [currentDictionary objectForKey:@"ManagerBannerUrl"]]  stringByAddingPercentEncodingWithAllowedCharacters:set];
                        objManager.strManagerStateID = [currentDictionary objectForKey:@"ManagerStateId"];
                        objManager.strManagerCityID = [currentDictionary objectForKey:@"ManagerCityId"];
                        
                        [self.arrayCompanyAllManagers addObject:objManager];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotCompanyAllManagersEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO GET ALL PLAYLISTS BY COMPANY ID

-(void)getAllCompanyPlaylistsByCompanyId:(NSString *)strCompanyId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetAllCompanyPlaylistsByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //========== FILL PLAYLISTS ARRAY ==========//
                NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                
                if(self.objLoggedInUser == nil)
                {
                    self.objLoggedInUser = [[User alloc] init];
                }
                
                self.objLoggedInUser.arrayCompanyPlaylists = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                    
                    Playlist *objPlaylist = [[Playlist alloc] init];
                    objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                    objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                    
                    objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                    
                    NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                    
                    for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                    {
                        NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                        
                        Song *objSong = [[Song alloc] init];
                        
                        objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                        objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                        objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                        
                        [objPlaylist.arrayPlaylistSongs addObject:objSong];
                    }
                    
                    [self.objLoggedInUser.arrayCompanyPlaylists addObject:objPlaylist];
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotAllCompanyPlaylistsByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO DELETE PLAYLIST BY COMPANY ID AND PLAYLIST ID

-(void)deleteCompanyPlaylistByCompanyId:(NSString *)strCompanyId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"DeleteCompanyPlaylistByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    //========== FILL PLAYLISTS ARRAY ==========//
                    NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                    
                    if(self.objLoggedInUser == nil)
                    {
                        self.objLoggedInUser = [[User alloc] init];
                    }
                    
                    self.objLoggedInUser.arrayCompanyPlaylists = [[NSMutableArray alloc] init];
                    
                    for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                    {
                        NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                        
                        Playlist *objPlaylist = [[Playlist alloc] init];
                        objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                        objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                        
                        objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                        
                        NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                        
                        for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                        {
                            NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                            
                            Song *objSong = [[Song alloc] init];
                            
                            objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                            objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                            objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                            
                            [objPlaylist.arrayPlaylistSongs addObject:objSong];
                        }
                        
                        [self.objLoggedInUser.arrayCompanyPlaylists addObject:objPlaylist];
                    }
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedCompanyPlaylistByCompanyIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO DELETE SONG BY COMPANY ID AND SONG ID

-(void)deleteCompanySongByCompanyId:(NSString *)strCompanyId withSongId:(NSString *)strSongId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"DeleteCompanySongByCompanyId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        [parameters setObject:strSongId forKey:@"song_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"deletedCompanySongByCompanyIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO ADD SONG

-(void)addCompanySong:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AddCompanySong"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistName"] forKey:@"playlist_name"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistID"] forKey:@"playlist_id"];
        [parameters setObject:[dictParameters objectForKey:@"strSongName"] forKey:@"song_name"];
        [parameters setObject:[dictParameters objectForKey:@"strArtistName"] forKey:@"artist_name"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedCompanySongEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO ADD PLAYLIST

-(void)addCompanyPlaylist:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"AddCompanyPlaylist"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"strPlaylistName"] forKey:@"playlist_name"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addedCompanyPlaylistEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showWebserviceFailureCaseMessage:@"" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO GET SELECTED PLAYLIST BY CLUB ID AND COMPANY ID

-(void)getCompanySelectedPlaylistByCompanyId:(NSString *)strCompanyId withManagerId:(NSString *)strManagerId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"GetCompanySelectedPlaylistByCompanyIdAndManagerId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        [parameters setObject:strManagerId forKey:@"manager_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                self.strCompanySelectedPlaylistIdForManager = [jsonResult objectForKey:@"selected_playlist_id"];
                self.strCompanySelectedPlaylistNameForManager = [jsonResult objectForKey:@"selected_playlist_name"];
                
                //========== FILL PLAYLISTS ARRAY ==========//
                NSArray *arrayAllPlaylistsList = [jsonResult objectForKey:@"Playlist"];
                
                if(self.objLoggedInUser == nil)
                {
                    self.objLoggedInUser = [[User alloc] init];
                }
                
                self.objLoggedInUser.arrayCompanyPlaylists = [[NSMutableArray alloc] init];
                
                for(int i = 0 ; i < arrayAllPlaylistsList.count; i++)
                {
                    NSDictionary *currentDictionary = [arrayAllPlaylistsList objectAtIndex:i];
                    
                    Playlist *objPlaylist = [[Playlist alloc] init];
                    objPlaylist.strPlaylistID = [currentDictionary objectForKey:@"PlaylistId"];
                    objPlaylist.strPlaylistName = [currentDictionary objectForKey:@"PlaylistName"];
                    
                    objPlaylist.arrayPlaylistSongs = [[NSMutableArray alloc] init];
                    
                    NSArray *arrayDjsSongsPlaylist = [currentDictionary objectForKey:@"PlaylistSongs"];
                    
                    for(int j = 0 ; j < arrayDjsSongsPlaylist.count; j++)
                    {
                        NSDictionary *currentDictionaryDjsSong = [arrayDjsSongsPlaylist objectAtIndex:j];
                        
                        Song *objSong = [[Song alloc] init];
                        
                        objSong.strSongID = [currentDictionaryDjsSong objectForKey:@"SongId"];
                        objSong.strSongName = [currentDictionaryDjsSong objectForKey:@"SongName"];
                        objSong.strSongArtistName = [currentDictionaryDjsSong objectForKey:@"SongArtistName"];
                        
                        [objPlaylist.arrayPlaylistSongs addObject:objSong];
                    }
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"gotCompanySelectedPlaylistByCompanyIdEvent" object:nil];
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION FOR COMPANY TO SET PLAYLIST BY MANAGER ID, PLAYLIST ID AND COMPANY ID

-(void)setCompanyPlaylistByCompanyId:(NSString *)strCompanyId withManagerId:(NSString *)strManagerId withPlaylistId:(NSString *)strPlaylistId
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPArtist"],[_dictSettings objectForKey:@"SetCompanyPlaylistByUserIdAndClubId"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:strCompanyId forKey:@"user_id"];
        [parameters setObject:strManagerId forKey:@"manager_id"];
        [parameters setObject:strPlaylistId forKey:@"playlist_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"setCompanyPlaylistByCompanyIdEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"Server Error" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

//========== COMMON WEBSERVICES ==========//

#pragma mark - FUNCTION TO SEND ADVERTISEMENT CLICK DATA TO SERVER

-(void)sendAdvertisementClickDataToServer:(NSDictionary *)dictParameters
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"SendAdvertisementClickDataToServer"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        [parameters setObject:[dictParameters objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dictParameters objectForKey:@"advertisement_id"] forKey:@"advertisement_id"];
        [parameters setObject:[dictParameters objectForKey:@"AdvertisementTableName"] forKey:@"AdvertisementTableName"];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        [parameters setObject:strUserType forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                //                NSString *filePath = [[NSBundle mainBundle] pathForResource:@"sendadvertisementclickdatatoserver" ofType:@"json"];
                //                NSData *data = [NSData dataWithContentsOfFile:filePath];
                //                NSDictionary *jsonResult = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"sentAdvertisementClickDataToServerEvent" object:nil];
                }
                else
                {
                    NSString *message = [jsonResult objectForKey:@"msg"];
                    [self showErrorMessage:@"Server Error" withErrorContent:message];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

#pragma mark - FUNCTION TO GET SHARE LINK

-(void)getShareLink
{
    if([self isNetworkAvailable])
    {
        [appDelegate showGlobalProgressHUDWithTitle:@"Loading..."];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@",[_dictSettings objectForKey:@"ServerIPDJ"],[_dictSettings objectForKey:@"GetShareLink"]];
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserId = [prefs objectForKey:@"userid"];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        
        [parameters setObject:strUserId forKey:@"user_id"];
        [parameters setObject:strUserType forKey:@"user_type"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        [manager POST:urlString parameters:parameters headers:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            [appDelegate dismissGlobalHUD];
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSArray *responseArray = responseObject;
                
            } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *jsonResult = responseObject;
                
                if([[jsonResult objectForKey:@"success"] integerValue] == 1)
                {
                    self.strShareLink = [jsonResult objectForKey:@"link"];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"gotShareLinkEvent" object:nil];
                }
                else
                {
                    [appDelegate dismissGlobalHUD];
                    [self showErrorMessage:@"Server Error" withErrorContent:@""];
                }
            }
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            [appDelegate dismissGlobalHUD];
            [self showErrorMessage:@"Server Error" withErrorContent:@""];
            
        }];
    }
    else
    {
        [self showInternetNotConnectedError];
    }
}

@end
