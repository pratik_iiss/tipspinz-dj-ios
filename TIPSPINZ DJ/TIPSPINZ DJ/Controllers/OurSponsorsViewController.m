//
//  OurSponsorsViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 29/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "OurSponsorsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "MyProfileViewController.h"
#import "ArtistMyProfileViewController.h"

@interface OurSponsorsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
    
    NSTimer *advertisementTimer;
    int pageNumber;
    int numberOfPages;
}

@end

@implementation OurSponsorsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;
@synthesize mainContainerView;

@synthesize lblOurSponsors;

@synthesize AdvertisementScrollView;

@synthesize mainPageControl;

@synthesize btnNext;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];
    [[IQKeyboardManager sharedManager] setEnable:false];
    
    numberOfPages = [MySingleton sharedManager].dataManager.arraySponsorsAdvertisementsImages.count;
    AdvertisementScrollView.contentSize = CGSizeMake(AdvertisementScrollView.frame.size.width * numberOfPages, AdvertisementScrollView.frame.size.height);
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
    
    [advertisementTimer invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotSponsorsAdvertisementsImagesByUserIdEvent) name:@"gotSponsorsAdvertisementsImagesByUserIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sentAdvertisementClickDataToServerEvent) name:@"sentAdvertisementClickDataToServerEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotSponsorsAdvertisementsImagesByUserIdEvent
{
    [self setUpSlider];
}

-(void)sentAdvertisementClickDataToServerEvent
{
    [self openWebpageInSafari:self.objTappedAdvertisement.strAdvertisementRedirectionUrl];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    mainScrollView.delegate = self;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIFont *lblOurSponsorsFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblOurSponsorsFont = [MySingleton sharedManager].themeFontTwentySizeBold;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblOurSponsorsFont = [MySingleton sharedManager].themeFontTwentyTwoSizeBold;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        lblOurSponsorsFont = [MySingleton sharedManager].themeFontTwentyFourSizeBold;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    lblOurSponsors.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    lblOurSponsors.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    lblOurSponsors.font = lblOurSponsorsFont;
    
    btnNext.backgroundColor = [MySingleton sharedManager].themeGlobalLightestGreyColor;
    btnNext.titleLabel.font = btnFont;
    [btnNext setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnNext addTarget:self action:@selector(btnNextClicked) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getSponsorsAdvertisementsImagesByUserId:[prefs objectForKey:@"userid"]];
}

#pragma mark - Slider Methods

-(void)advertisementTimerMethod:(NSTimer *)timer
{
    CGFloat pageWidth = AdvertisementScrollView.frame.size.width;
    
    pageNumber++;
    
    if(pageNumber > (numberOfPages - 1))
    {
        pageNumber = 0;
    }
    
    float fractionalPage = pageNumber;
    NSInteger page = lround(fractionalPage);
    mainPageControl.currentPage = page;
    [mainPageControl reloadInputViews];
    
    if(pageNumber == 0)
    {
        [AdvertisementScrollView setContentOffset:CGPointMake((pageWidth * pageNumber), 0) animated:NO];
    }
    else
    {
        [AdvertisementScrollView setContentOffset:CGPointMake((pageWidth * pageNumber), 0) animated:YES];
    }
}

-(void)setUpSlider
{
    numberOfPages = [MySingleton sharedManager].dataManager.arraySponsorsAdvertisementsImages.count;
    pageNumber = 0;
    
    advertisementTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(advertisementTimerMethod:) userInfo:nil repeats:YES];
    
    mainPageControl.userInteractionEnabled = FALSE;
    mainPageControl.numberOfPages = numberOfPages;
    mainPageControl.currentPageIndicatorTintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    [AdvertisementScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    AdvertisementScrollView.contentSize = CGSizeMake(AdvertisementScrollView.frame.size.width * numberOfPages, AdvertisementScrollView.frame.size.height);
    AdvertisementScrollView.delegate = self;
    
    for(int i = 0; i < [MySingleton sharedManager].dataManager.arraySponsorsAdvertisementsImages.count; i++)
    {
        AsyncImageView *advertisementImageView = [[AsyncImageView alloc] initWithFrame:CGRectMake((AdvertisementScrollView.frame.size.width*i), 0, (AdvertisementScrollView.frame.size.width), AdvertisementScrollView.frame.size.height)];
        
        Advertisement *objAdvertisement = [[MySingleton sharedManager].dataManager.arraySponsorsAdvertisementsImages objectAtIndex:i];
        
//        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        advertisementImageView.imageURL = [NSURL URLWithString:objAdvertisement.strAdvertisementImageUrl];
        advertisementImageView.layer.masksToBounds = YES;
        //advertisementImageView.contentMode = UIViewContentModeScaleAspectFit;
        advertisementImageView.contentMode = UIViewContentModeScaleToFill;
        
        [AdvertisementScrollView addSubview:advertisementImageView];
    }
    
    UITapGestureRecognizer *AdvertisementScrollViewTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(AdvertisementScrollViewTapped:)];
    AdvertisementScrollViewTapGestureRecognizer.delegate = self;
    AdvertisementScrollView.userInteractionEnabled = true;
    [AdvertisementScrollView addGestureRecognizer:AdvertisementScrollViewTapGestureRecognizer];
}

- (void)AdvertisementScrollViewTapped:(UITapGestureRecognizer*)sender
{
    NSLog(@"pageNumber : %d", pageNumber);
    
    self.objTappedAdvertisement = [[MySingleton sharedManager].dataManager.arraySponsorsAdvertisementsImages objectAtIndex:pageNumber];
    
    if(self.objTappedAdvertisement.strAdvertisementRedirectionUrl != nil && self.objTappedAdvertisement.strAdvertisementRedirectionUrl.length > 0)
    {
//        [self openWebpageInSafari:objAdvertisement.strAdvertisementRedirectionUrl];
        
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
        [dictParameters setObject:self.objTappedAdvertisement.strAdvertisementId forKey:@"advertisement_id"];
        [dictParameters setObject:self.objTappedAdvertisement.strAdvertisementTableName forKey:@"AdvertisementTableName"];
        
        [[MySingleton sharedManager].dataManager sendAdvertisementClickDataToServer:dictParameters];
    }
}

#pragma mark - UIScrollView Delegate Methods

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == AdvertisementScrollView)
    {
        int scrollEndPoint;
        
        if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
        {
            scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
        }
        else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
        {
            scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
        }
        else if([MySingleton sharedManager].screenHeight >= 736)
        {
            scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == AdvertisementScrollView)
    {
        CGFloat pageWidth = AdvertisementScrollView.frame.size.width;
        
        float fractionalPage = AdvertisementScrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        mainPageControl.currentPage = page;
        pageNumber = page;
        [mainPageControl reloadInputViews];
    }
}

#pragma mark - Other Method

-(void)openWebpageInWebViewController:(NSString *)strWebPageUrl withTitle:(NSString *)strWebPageTitle
{
//    WebViewController *viewController = [[WebViewController alloc] init];
//    viewController.webPageUrl = strWebPageUrl;
//    viewController.webPageTitle = strWebPageTitle;
//    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)openWebpageInSafari:(NSString *)strWebPageUrl
{
    NSURL *webPageUrl = [[NSURL alloc]initWithString:strWebPageUrl];
    if ([[UIApplication sharedApplication] canOpenURL:webPageUrl])
    {
        [[UIApplication sharedApplication] openURL:webPageUrl];
    }
}

- (void)btnNextClicked
{
    [self.view endEditing:YES];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserType = [prefs objectForKey:@"usertype"];
    
    if([strUserType isEqualToString:@"1"])
    {
        //DJ
        
        MyProfileViewController *viewController = [[MyProfileViewController alloc] init];
        
        viewController.boolIsLoadedFromSideMenuToGoOnline = false;
        viewController.boolIsLoadedFromSideMenuToGoOffline = false;
        
        SideMenuViewController *sideMenuViewController;
        
        if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight >= 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                               leftViewController:sideMenuViewController
                                                                                              rightViewController:nil];
        
        sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
        sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
        
        sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
        sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
        
        [self.navigationController pushViewController:sideMenuController animated:YES];
    }
    else if([strUserType isEqualToString:@"3"])
    {
        //ARTIST
        
        ArtistMyProfileViewController *viewController = [[ArtistMyProfileViewController alloc] init];
        
        viewController.boolIsLoadedFromSideMenuToGoOnline = false;
        viewController.boolIsLoadedFromSideMenuToGoOffline = false;
        
        SideMenuViewController *sideMenuViewController;
        
        if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
        }
        else if([MySingleton sharedManager].screenHeight >= 736)
        {
            sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
        }
        
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        
        LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                               leftViewController:sideMenuViewController
                                                                                              rightViewController:nil];
        
        sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
        sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
        
        sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
        sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
        
        [self.navigationController pushViewController:sideMenuController animated:YES];
    }
}

@end
