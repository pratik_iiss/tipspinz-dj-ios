//
//  CommonWebViewController.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 29/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface CommonWebViewController : UIViewController<UIScrollViewDelegate, WKNavigationDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet WKWebView *mainWebView;
@property (nonatomic,retain) IBOutlet UIActivityIndicatorView *activityIndicatorView;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSString *strTitle;
@property (nonatomic,retain) NSString *strUrl;

@end
