//
//  IntroductionViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 22/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "IntroductionViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "LoginOptionsViewController.h"

@interface IntroductionViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
    
    int pageNumber;
    int numberOfPages;
}

@end

@implementation IntroductionViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;
@synthesize mainContainerView;

@synthesize imageViewMainLogo;

@synthesize AdvertisementScrollView;

@synthesize imageViewAdvertisement1;
@synthesize lblAdvertisementTitle1;
@synthesize lblAdvertisementDetails1;

@synthesize imageViewAdvertisement2;
@synthesize lblAdvertisementTitle2;
@synthesize lblAdvertisementDetails2;

@synthesize imageViewAdvertisement3;
@synthesize lblAdvertisementTitle3;
@synthesize lblAdvertisementDetails3;

@synthesize mainPageControl;

@synthesize btnSkip;
@synthesize btnJoinNow;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];
    [[IQKeyboardManager sharedManager] setEnable:false];
    
    AdvertisementScrollView.contentSize = CGSizeMake(AdvertisementScrollView.frame.size.width * 3, AdvertisementScrollView.frame.size.height);
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    mainScrollView.delegate = self;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIFont *lblInspiringFeatureFont, *lblDetailsFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblInspiringFeatureFont = [MySingleton sharedManager].themeFontEighteenSizeBold;
        lblDetailsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblInspiringFeatureFont = [MySingleton sharedManager].themeFontNineteenSizeBold;
        lblDetailsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontNineteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        lblInspiringFeatureFont = [MySingleton sharedManager].themeFontTwentySizeBold;
        lblDetailsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontTwentySizeRegular;
    }
    
    lblAdvertisementTitle1.font = lblInspiringFeatureFont;
    lblAdvertisementTitle1.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    lblAdvertisementTitle1.text = @"Request your favorite song or request a shout out";
    
    lblAdvertisementDetails1.font = lblDetailsFont;
    lblAdvertisementDetails1.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    lblAdvertisementDetails1.text = @"- Choose your club and select the online DJ\n\n- Clubs advertise for free\n\n- Clubs can choose their own custom logo for users to select\n\n- Request your favorite song or request a shout out";
    
    lblAdvertisementTitle2.font = lblInspiringFeatureFont;
    lblAdvertisementTitle2.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    lblAdvertisementTitle2.text = @"BECOME VIP";
    
    lblAdvertisementDetails2.font = lblDetailsFont;
    lblAdvertisementDetails2.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    lblAdvertisementDetails2.text = @"Be first in line! Tip the DJ with a VIP Tip and enjoy priority benefits and much more.";
    
    lblAdvertisementTitle3.font = lblInspiringFeatureFont;
    lblAdvertisementTitle3.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    lblAdvertisementTitle3.text = @"ADVERTISE";
    
    lblAdvertisementDetails3.font = lblDetailsFont;
    lblAdvertisementDetails3.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    lblAdvertisementDetails3.text = @"Advertise with TIP SPINZ and get maximum exposure for your brand.";
    
    btnSkip.titleLabel.font = btnFont;
    [btnSkip setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSkip addTarget:self action:@selector(btnSkipClicked) forControlEvents:UIControlEventTouchUpInside];
    
    btnJoinNow.titleLabel.font = btnFont;
    [btnJoinNow setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnJoinNow addTarget:self action:@selector(btnJoinNowClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self setUpSlider];
}

-(void)setUpSlider
{
    numberOfPages = 3;
    pageNumber = 0;
    
    mainPageControl.userInteractionEnabled = FALSE;
    mainPageControl.numberOfPages = numberOfPages;
    mainPageControl.currentPageIndicatorTintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    [AdvertisementScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
//    AdvertisementScrollView.contentSize = CGSizeMake(AdvertisementScrollView.frame.size.width * 3, AdvertisementScrollView.frame.size.height);
    AdvertisementScrollView.delegate = self;
}

#pragma mark - UIScrollView Delegate Methods

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    if(scrollView == AdvertisementScrollView)
    {
        int scrollEndPoint;
        
        if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
        {
            scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
        }
        else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
        {
            scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
        }
        else if([MySingleton sharedManager].screenHeight >= 736)
        {
            scrollEndPoint = [MySingleton sharedManager].screenWidth * (numberOfPages - 1);
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == AdvertisementScrollView)
    {
        CGFloat pageWidth = AdvertisementScrollView.frame.size.width;
        
        float fractionalPage = AdvertisementScrollView.contentOffset.x / pageWidth;
        NSInteger page = lround(fractionalPage);
        mainPageControl.currentPage = page;
        pageNumber = page;
        [mainPageControl reloadInputViews];
    }
}

#pragma mark - Other Method

- (void)btnSkipClicked
{
    [self.view endEditing:YES];
    
    LoginOptionsViewController *viewController = [[LoginOptionsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)btnJoinNowClicked
{
    [self.view endEditing:YES];
    
    LoginOptionsViewController *viewController = [[LoginOptionsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
