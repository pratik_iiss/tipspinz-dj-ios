//
//  SplashViewController.m
//  HungerE
//
//  Created by Pratik Gujarati on 23/09/16.
//  Copyright © 2016 accereteinfotech. All rights reserved.
//

#import "SplashViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "OurSponsorsViewController.h"
#import "IntroductionViewController.h"
#import "LoginOptionsViewController.h"

#import "HomeViewController.h"
#import "ClubOwnerDjApprovalRequestsListViewController.h"
#import "MyRequestsViewController.h"

#import "ArtistHomeViewController.h"
#import "CompanyArtistApprovalRequestsListViewController.h"
#import "ArtistMyRequestsViewController.h"

@interface SplashViewController ()
{
    AppDelegate *appDelegate;
    BOOL boolIsRedirected;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation SplashViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize mainContainerView;

@synthesize mainSplashBackgroundImageView;
@synthesize mainLogoImageView;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];
    [[IQKeyboardManager sharedManager] setEnable:false];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(facebookButtonStatusEvent) name:@"facebookButtonStatusEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkedOnlineStatusOfDjEvent) name:@"checkedOnlineStatusOfDjEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkedOnlineStatusOfArtistEvent) name:@"checkedOnlineStatusOfArtistEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)facebookButtonStatusEvent
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if(![[prefs objectForKey:@"isAppLoadedForFirstTime"] isEqualToString:@"1"])
    {
        //========== APPLICATION IS OPENED FOR THE FIRST TIME ==========//
        [prefs setObject:@"1" forKey:@"isAppLoadedForFirstTime"];
        [prefs synchronize];
    
        boolIsRedirected = TRUE;
        
        [self performSelector:@selector(navigateToIntroductionScreen) withObject:self afterDelay:1.0];
    }
    else
    {
        NSString *strUserId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"userid"]];
        NSString *strAutoLogin = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"autologin"]];
        
        if((strUserId != nil && strUserId.length > 0) && ([strAutoLogin isEqualToString:@"1"]))
        {
            boolIsRedirected = TRUE;
            
            NSString *strUserType = [prefs objectForKey:@"usertype"];
            
            if([strUserType isEqualToString:@"0"])
            {
                //CLUB OWNER
                
                [self performSelector:@selector(navigateToClubOwnerDjApprovalRequestsListViewController) withObject:self afterDelay:1.0];
            }
            else if([strUserType isEqualToString:@"1"])
            {
                //DJ
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [[MySingleton sharedManager].dataManager checkOnlineStatusOfDj:[prefs objectForKey:@"userid"]];
            }
            else if([strUserType isEqualToString:@"2"])
            {
                //COMPANY
                
                [self performSelector:@selector(navigateToCompanyArtistApprovalRequestsListViewController) withObject:self afterDelay:1.0];
            }
            else if([strUserType isEqualToString:@"3"])
            {
                //ARTIST
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [[MySingleton sharedManager].dataManager checkOnlineStatusOfArtist:[prefs objectForKey:@"userid"]];
            }
        }
        else
        {
            boolIsRedirected = TRUE;
            
            [self performSelector:@selector(navigateToLoginOptionsScreen) withObject:self afterDelay:1.0];
        }
    }
}

-(void)checkedOnlineStatusOfDjEvent
{
    if([MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsSongsScreenForDj)
    {
        [MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsSongsScreenForDj = FALSE;
        
        [self performSelector:@selector(navigateToMyRequestsScreenForSongs) withObject:self afterDelay:1.0];
    }
    else if([MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsShoutoutScreenForDj)
    {
        [MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsShoutoutScreenForDj = FALSE;
        
        [self performSelector:@selector(navigateToMyRequestsScreenForShoutouts) withObject:self afterDelay:1.0];
    }
    else
    {
        [self performSelector:@selector(navigateToHomeViewController) withObject:self afterDelay:1.0];
    }
}

-(void)checkedOnlineStatusOfArtistEvent
{
    if([MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsSongsScreenForArtist)
    {
        [MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsSongsScreenForArtist = FALSE;
        
        [self performSelector:@selector(navigateToArtistMyRequestsScreenForSongs) withObject:self afterDelay:1.0];
    }
    else if([MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsShoutoutScreenForArtist)
    {
        [MySingleton sharedManager].isAppOpenedFromNotificationForMyRequestsShoutoutScreenForArtist = FALSE;
        
        [self performSelector:@selector(navigateToArtistMyRequestsScreenForShoutouts) withObject:self afterDelay:1.0];
    }
    else
    {
        [self performSelector:@selector(navigateToArtistHomeViewController) withObject:self afterDelay:1.0];
    }
}

- (void)appWillEnterForeground:(NSNotification *)notification {
    NSLog(@"will enter foreground notification");
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    self.automaticallyAdjustsScrollViewInsets = false;
    
    mainSplashBackgroundImageView.layer.masksToBounds = true;
    mainLogoImageView.layer.masksToBounds = true;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // To hide facebook button
    
    [prefs setObject:@"0" forKey:@"IsFacebookHidden"];
    [prefs synchronize];
    
    [[MySingleton sharedManager].dataManager facebookButtonHiddenStatus];
    
    /*
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if(![[prefs objectForKey:@"isAppLoadedForFirstTime"] isEqualToString:@"1"])
    {
        //========== APPLICATION IS OPENED FOR THE FIRST TIME ==========//
        [prefs setObject:@"1" forKey:@"isAppLoadedForFirstTime"];
        [prefs synchronize];
    
        boolIsRedirected = TRUE;
        
        [self performSelector:@selector(navigateToIntroductionScreen) withObject:self afterDelay:1.0];
    }
    else
    {
        NSString *strUserId = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"userid"]];
        NSString *strAutoLogin = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"autologin"]];
        
        if((strUserId != nil && strUserId.length > 0) && ([strAutoLogin isEqualToString:@"1"]))
        {
            boolIsRedirected = TRUE;
            
            NSString *strUserType = [prefs objectForKey:@"usertype"];
            
            if([strUserType isEqualToString:@"0"])
            {
                //CLUB OWNER
                
                [self performSelector:@selector(navigateToClubOwnerDjApprovalRequestsListViewController) withObject:self afterDelay:1.0];
            }
            else if([strUserType isEqualToString:@"1"])
            {
                //DJ
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [[MySingleton sharedManager].dataManager checkOnlineStatusOfDj:[prefs objectForKey:@"userid"]];
            }
            else if([strUserType isEqualToString:@"2"])
            {
                //COMPANY
                
                [self performSelector:@selector(navigateToCompanyArtistApprovalRequestsListViewController) withObject:self afterDelay:1.0];
            }
            else if([strUserType isEqualToString:@"3"])
            {
                //ARTIST
                
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [[MySingleton sharedManager].dataManager checkOnlineStatusOfArtist:[prefs objectForKey:@"userid"]];
            }
        }
        else
        {
            boolIsRedirected = TRUE;
            
            [self performSelector:@selector(navigateToLoginOptionsScreen) withObject:self afterDelay:1.0];
        }
    }
    */
}

#pragma mark - Other Method

- (void)navigateToIntroductionScreen
{
    [self.view endEditing:true];
    
    IntroductionViewController *viewController = [[IntroductionViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)navigateToLoginOptionsScreen
{
    [self.view endEditing:true];
    
    LoginOptionsViewController *viewController = [[LoginOptionsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)navigateToHomeViewController
{
    [self.view endEditing:true];
    
    OurSponsorsViewController *viewController = [[OurSponsorsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)navigateToMyRequestsScreenForSongs
{
    [self.view endEditing:true];
    
    MyRequestsViewController *viewController = [[MyRequestsViewController alloc] init];
    
    viewController.boolIsLoadedForSongsRequests = true;
    
    SideMenuViewController *sideMenuViewController;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:true];
}

- (void)navigateToMyRequestsScreenForShoutouts
{
    [self.view endEditing:true];
    
    MyRequestsViewController *viewController = [[MyRequestsViewController alloc] init];
    
    viewController.boolIsLoadedForSongsRequests = false;
    
    SideMenuViewController *sideMenuViewController;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:true];
}

- (void)navigateToClubOwnerDjApprovalRequestsListViewController
{
    [self.view endEditing:true];
    
    ClubOwnerDjApprovalRequestsListViewController *viewController = [[ClubOwnerDjApprovalRequestsListViewController alloc] init];
    
    SideMenuViewController *sideMenuViewController;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:true];
}

- (void)navigateToArtistHomeViewController
{
    [self.view endEditing:true];
    
    OurSponsorsViewController *viewController = [[OurSponsorsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)navigateToArtistMyRequestsScreenForSongs
{
    [self.view endEditing:true];
    
    ArtistMyRequestsViewController *viewController = [[ArtistMyRequestsViewController alloc] init];
    
    viewController.boolIsLoadedForSongsRequests = true;
    
    SideMenuViewController *sideMenuViewController;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:true];
}

- (void)navigateToArtistMyRequestsScreenForShoutouts
{
    [self.view endEditing:true];
    
    ArtistMyRequestsViewController *viewController = [[ArtistMyRequestsViewController alloc] init];
    
    viewController.boolIsLoadedForSongsRequests = false;
    
    SideMenuViewController *sideMenuViewController;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:true];
}

- (void)navigateToCompanyArtistApprovalRequestsListViewController
{
    [self.view endEditing:true];
    
    CompanyArtistApprovalRequestsListViewController *viewController = [[CompanyArtistApprovalRequestsListViewController alloc] init];
    
    SideMenuViewController *sideMenuViewController;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:true];
}

@end
