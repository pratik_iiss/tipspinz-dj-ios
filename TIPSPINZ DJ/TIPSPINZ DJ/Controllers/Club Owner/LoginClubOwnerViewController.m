//
//  LoginClubOwnerViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 21/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "LoginClubOwnerViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ClubOwnerDjApprovalRequestsListViewController.h"

#import "CommonWebViewController.h"
#import "ForgetPasswordViewController.h"

#import <objc/runtime.h>

@interface LoginClubOwnerViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation LoginClubOwnerViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize mainContainerView;

@synthesize imageViewBack;
@synthesize btnBack;

@synthesize imageViewMainLogo;

@synthesize imageViewEmail;
@synthesize txtEmail;
@synthesize txtEmailBottomSeparatorView;

@synthesize imageViewPassword;
@synthesize txtPassword;
@synthesize txtPasswordBottomSeparatorView;

@synthesize btnForgetPassword;

@synthesize imageViewTermsAndConditionsCheckbox;
@synthesize lblByLoggingIn;

@synthesize btnLogin;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clubOwnerLoggedinEvent) name:@"clubOwnerLoggedinEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)clubOwnerLoggedinEvent
{
    [self navigateToClubOwnerDjApprovalRequestsListViewController];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *txtFieldFont, *btnForgetPasswordFont, *lblByLoggingInFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblByLoggingInFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        lblByLoggingInFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        lblByLoggingInFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    imageViewBack.layer.masksToBounds = true;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    txtEmail.font = txtFieldFont;
    txtEmail.delegate = self;
    
//    [txtEmail setValue:[MySingleton sharedManager].textfieldPlaceholderColor
//            forKeyPath:@"placeholderLabel.textColor"];
    
    Ivar ivar =  class_getInstanceVariable([UITextField class], "placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(txtEmail, ivar);
    placeholderLabel.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    
    txtEmail.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtEmail.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtEmail setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    
    txtEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    txtPassword.font = txtFieldFont;
    txtPassword.delegate = self;
    
//    [txtPassword setValue:[MySingleton sharedManager].textfieldPlaceholderColor
//               forKeyPath:@"placeholderLabel.textColor"];
    
    UILabel *placeholderLabel1 = object_getIvar(txtPassword, ivar);
    placeholderLabel1.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    
    txtPassword.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPassword.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtPassword.secureTextEntry = true;
    
    txtPasswordBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    btnForgetPassword.titleLabel.font = btnForgetPasswordFont;
    [btnForgetPassword setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnForgetPassword addTarget:self action:@selector(btnForgetPasswordClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.boolIsTermsAndConditionsChecked = false;
    
    imageViewTermsAndConditionsCheckbox.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewTermsAndConditionsCheckboxTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewTermsAndConditionsCheckboxTapped:)];
    imageViewTermsAndConditionsCheckboxTapGestureRecognizer.delegate = self;
    [imageViewTermsAndConditionsCheckbox addGestureRecognizer:imageViewTermsAndConditionsCheckboxTapGestureRecognizer];
    
    lblByLoggingIn.font = lblByLoggingInFont;
    lblByLoggingIn.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblByLoggingIn.userInteractionEnabled = true;
    UITapGestureRecognizer *lblByLoggingInTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblByLoggingInTapped:)];
    lblByLoggingInTapGestureRecognizer.delegate = self;
    [lblByLoggingIn addGestureRecognizer:lblByLoggingInTapGestureRecognizer];
    
    btnLogin.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnLogin.layer.masksToBounds = true;
    btnLogin.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnLogin.titleLabel.font = btnFont;
    [btnLogin setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnLogin addTarget:self action:@selector(btnLoginClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    //========== PRATIK GUJARATI TEMP DATA ==========//
//    txtEmail.text = @"sd@gmail.com";
//    txtPassword.text = @"123456";
    //========== PRATIK GUJARATI TEMP DATA ==========//
}

- (void)lblByLoggingInTapped: (UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    CommonWebViewController *viewController = [[CommonWebViewController alloc] init];
    viewController.strTitle = @"Terms & Conditions";
    viewController.strUrl = @"https://www.tipspinz.com/app/termsandconditions.html";
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)imageViewTermsAndConditionsCheckboxTapped: (UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    if(self.boolIsTermsAndConditionsChecked)
    {
        self.boolIsTermsAndConditionsChecked = false;
        imageViewTermsAndConditionsCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked_privacy_policy.png"];
    }
    else
    {
        self.boolIsTermsAndConditionsChecked = true;
        imageViewTermsAndConditionsCheckbox.image = [UIImage imageNamed:@"checkbox_checked_privacy_policy.png"];
    }
    
//    CommonWebViewController *viewController = [[CommonWebViewController alloc] init];
//    viewController.strTitle = @"Terms & Conditions";
//    viewController.strUrl = @"https://www.tipspinz.com/app/termsandconditions.html";
//    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtEmail)
    {
        imageViewEmail.image = [UIImage imageNamed:@"textfield_email_selected"];
        txtEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
    else if(textField == txtPassword)
    {
        imageViewPassword.image = [UIImage imageNamed:@"textfield_password_selected"];
        txtPasswordBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == txtEmail)
    {
        imageViewEmail.image = [UIImage imageNamed:@"textfield_email_normal"];
        txtEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
    else if(textField == txtPassword)
    {
        imageViewPassword.image = [UIImage imageNamed:@"textfield_password_normal"];
        txtPasswordBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnBackClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnForgetPasswordClicked:(id)sender
{
    [self.view endEditing:true];
    
    ForgetPasswordViewController *viewController = [[ForgetPasswordViewController alloc] init];
    viewController.strUserType = @"1";
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)bindDataToObject
{
    if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
    {
        [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc]init];
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInUser.strEmail = txtEmail.text;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strPassword = txtPassword.text;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //[MySingleton sharedManager].dataManager.objLoggedInUser.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"deviceToken"]];
    [MySingleton sharedManager].dataManager.objLoggedInUser.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"fcmToken"]];
}

- (IBAction)btnLoginClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.boolIsTermsAndConditionsChecked)
    {
        [self bindDataToObject];
        
        if([[MySingleton sharedManager].dataManager.objLoggedInUser isValidateUserForLogin])
        {
            [[MySingleton sharedManager].dataManager clubOwnerLogin:[MySingleton sharedManager].dataManager.objLoggedInUser];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:[MySingleton sharedManager].dataManager.objLoggedInUser.strValidationMessage];
            });
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"You must agree to the terms and conditions to login to our application."];
        });
    }
}

- (void)navigateToClubOwnerDjApprovalRequestsListViewController
{
    [self.view endEditing:true];
    
    ClubOwnerDjApprovalRequestsListViewController *viewController = [[ClubOwnerDjApprovalRequestsListViewController alloc] init];
    
    SideMenuViewController *sideMenuViewController;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:true];
}

@end
