//
//  OurSponsorsViewController.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 29/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#import "Advertisement.h"

@interface OurSponsorsViewController : UIViewController<UIScrollViewDelegate, UIGestureRecognizerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblOurSponsors;

@property (nonatomic,retain) IBOutlet UIScrollView *AdvertisementScrollView;

@property (nonatomic,retain) IBOutlet UIPageControl *mainPageControl;

@property (nonatomic,retain) IBOutlet UIButton *btnNext;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) Advertisement *objTappedAdvertisement;

@end
