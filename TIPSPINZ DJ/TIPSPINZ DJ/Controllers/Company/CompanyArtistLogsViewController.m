//
//  CompanyArtistLogsViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 30/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import "CompanyArtistLogsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "Playlist.h"

#import "ClubOwnerDjLogTableViewCell.h"

@interface CompanyArtistLogsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
    
    UIRefreshControl *refreshControl;
}

@end

@implementation CompanyArtistLogsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewSearch;
@synthesize btnSearch;

@synthesize searchTextContainerView;
@synthesize txtSearch;
@synthesize imageViewCloseSearchTextContainerView;
@synthesize btnCloseSearchTextContainerView;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;
@synthesize mainTableView;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotCompanyArtistLogsByCompanyIdEvent) name:@"gotCompanyArtistLogsByCompanyIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotCompanyArtistLogsByCompanyIdEvent
{
    [refreshControl endRefreshing];
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayCompanyArtistsLogs;
    
    [mainTableView reloadData];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Artist Logs"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    
    imageViewSearch.layer.masksToBounds = YES;
    [btnSearch addTarget:self action:@selector(btnSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *txtFieldFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
    }
    
    searchTextContainerView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    txtSearch.font = txtFieldFont;
    txtSearch.delegate = self;
    [txtSearch setValue:[MySingleton sharedManager].themeGlobalWhiteColor
             forKeyPath:@"placeholderLabel.textColor"];
    txtSearch.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.tintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.returnKeyType = UIReturnKeySearch;
    [txtSearch setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    imageViewCloseSearchTextContainerView.layer.masksToBounds = YES;
    [btnCloseSearchTextContainerView addTarget:self action:@selector(btnCloseSearchTextContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnSearchClicked:(id)sender
{
    [self.view endEditing:YES];
    
    searchTextContainerView.hidden = false;
    [txtSearch becomeFirstResponder];
}

-(void)btnCloseSearchTextContainerViewClicked
{
    [self.view endEditing:YES];
    
    txtSearch.text = @"";
    
    searchTextContainerView.hidden = true;
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayCompanyArtistsLogs;
    
    [mainTableView reloadData];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull down to Refresh"];
    [refreshControl addTarget:self action:@selector(refreshTableView:) forControlEvents:UIControlEventValueChanged];
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor clearColor];
    mainTableView.allowsMultipleSelectionDuringEditing = NO;
    [mainTableView registerNib:[UINib nibWithNibName:@"ClubOwnerDjLogTableViewCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ClubOwnerDjLogTableViewCell_iPhone6" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView registerNib:[UINib nibWithNibName:@"ClubOwnerDjLogTableViewCell_iPhone6Plus" bundle:nil] forCellReuseIdentifier:@"Cell"];
    [mainTableView addSubview:refreshControl];
    
    UIFont *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getCompanyArtistLogsByCompanyId:[prefs objectForKey:@"userid"]];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            return self.dataRows.count;
        }
        else
        {
            return 1;
        }
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            return 120;
        }
        else
        {
            return 44;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            ClubOwnerDjLogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
            
            if(cell != nil)
            {
                NSArray *nib;
                
                if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ClubOwnerDjLogTableViewCell" owner:self options:nil];
                }
                else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ClubOwnerDjLogTableViewCell_iPhone6" owner:self options:nil];
                }
                else if([MySingleton sharedManager].screenHeight >= 736)
                {
                    nib = [[NSBundle mainBundle]loadNibNamed:@"ClubOwnerDjLogTableViewCell_iPhone6Plus" owner:self options:nil];
                }
                
                cell = [nib objectAtIndex:0];
            }
            
            Artist *objArtist = [self.dataRows objectAtIndex:indexPath.row];
            
            cell.mainContainer.backgroundColor =  [UIColor clearColor];
            
            cell.imageViewDjProfilePicture.imageURL = [NSURL URLWithString:objArtist.strArtistProfilePictureImageUrl];
            cell.imageViewDjProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
            cell.imageViewDjProfilePicture.layer.masksToBounds = YES;
            cell.imageViewDjProfilePicture.layer.cornerRadius = (cell.imageViewDjProfilePicture.frame.size.height / 2);
            
            cell.lblDjName.text = objArtist.strArtistName;
            cell.lblDjName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
            cell.lblDjName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
            cell.lblDjName.textAlignment = NSTextAlignmentLeft;
            cell.lblDjName.layer.masksToBounds = YES;
            
            cell.lblClubName.text = objArtist.strArtistManagerName;
            cell.lblClubName.font = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            cell.lblClubName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblClubName.textAlignment = NSTextAlignmentLeft;
            cell.lblClubName.layer.masksToBounds = YES;
            
            cell.lblOnlineTitle.font = [MySingleton sharedManager].themeFontFourteenSizeMedium;
            cell.lblOnlineTitle.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
            cell.lblOnlineTitle.textAlignment = NSTextAlignmentLeft;
            cell.lblOnlineTitle.layer.masksToBounds = YES;
            
            cell.imageViewOnlineDate.layer.masksToBounds = YES;
            
            cell.lblOnlineDate.text = objArtist.strArtistOnlineDate;
            cell.lblOnlineDate.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblOnlineDate.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblOnlineDate.textAlignment = NSTextAlignmentLeft;
            cell.lblOnlineDate.layer.masksToBounds = YES;
            
            cell.imageViewOnlineTime.layer.masksToBounds = YES;
            
            cell.lblOnlineTime.text = objArtist.strArtistOnlineTime;
            cell.lblOnlineTime.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblOnlineTime.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblOnlineTime.textAlignment = NSTextAlignmentLeft;
            cell.lblOnlineTime.layer.masksToBounds = YES;
            
            cell.lblOfflineTitle.font = [MySingleton sharedManager].themeFontFourteenSizeMedium;
            cell.lblOfflineTitle.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
            cell.lblOfflineTitle.textAlignment = NSTextAlignmentLeft;
            cell.lblOfflineTitle.layer.masksToBounds = YES;
            
            cell.imageViewOfflineDate.layer.masksToBounds = YES;
            
            cell.lblOfflineDate.text = objArtist.strArtistOfflineDate;
            cell.lblOfflineDate.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblOfflineDate.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblOfflineDate.textAlignment = NSTextAlignmentLeft;
            cell.lblOfflineDate.layer.masksToBounds = YES;
            if([objArtist.strArtistOfflineDate isEqualToString:@"ONLINE"])
            {
                cell.lblOfflineDate.font = [MySingleton sharedManager].themeFontTenSizeBold;
                cell.lblOfflineDate.textColor = [MySingleton sharedManager].themeGlobalOnlineGreenColor;
                
                cell.imageViewOfflineTime.hidden = true;
                cell.lblOfflineTime.hidden = true;
            }
            
            cell.imageViewOfflineTime.layer.masksToBounds = YES;
            
            cell.lblOfflineTime.text = objArtist.strArtistOfflineTime;
            cell.lblOfflineTime.font = [MySingleton sharedManager].themeFontTenSizeRegular;
            cell.lblOfflineTime.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
            cell.lblOfflineTime.textAlignment = NSTextAlignmentLeft;
            cell.lblOfflineTime.layer.masksToBounds = YES;
            
            cell.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
            
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        }
        else
        {
            UIFont *lblNoDataFont;
            
            if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
            {
                lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            }
            else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
            {
                lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            }
            else
            {
                lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            }
            
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:MyIdentifier];
            
            UILabel *lblNoData = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mainTableView.frame.size.width, cell.frame.size.height)];
            lblNoData.textAlignment = NSTextAlignmentCenter;
            lblNoData.font = lblNoDataFont;
            lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
            lblNoData.text = @"No Requests found.";
            
            [cell.contentView addSubview:lblNoData];
            
            return cell;
        }
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            Artist *objArtist = [self.dataRows objectAtIndex:indexPath.row];
        }
        else
        {
        }
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if(textField == txtSearch)
    {
//        NSLog(@"Search button clicked for txtSearch.");
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == txtSearch)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayCompanyArtistsLogs;
        
        [mainTableView reloadData];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    
    [self searchDjsWithSubstring:substring];
    
    return YES;
}

#pragma mark - Other Methods

- (void)refreshTableView:(UIRefreshControl *)refreshControl
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getCompanyArtistLogsByCompanyId:[prefs objectForKey:@"userid"]];
}

- (void)searchDjsWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        for(Artist *objArtist in [MySingleton sharedManager].dataManager.arrayCompanyArtistsLogs)
        {
            if (([[objArtist.strArtistName lowercaseString] containsString:[substring lowercaseString]]))
            {
                [self.dataRows addObject:objArtist];
            }
        }
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayCompanyArtistsLogs;
    }
    
    [mainTableView reloadData];
}

@end
