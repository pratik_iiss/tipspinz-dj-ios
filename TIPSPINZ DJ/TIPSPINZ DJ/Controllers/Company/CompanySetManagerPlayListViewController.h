//
//  CompanySetManagerPlayListViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 30/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Manager.h"
#import "Playlist.h"

@interface CompanySetManagerPlayListViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblClubNameTitle;
@property (nonatomic,retain) IBOutlet UILabel *lblClubNameValue;

@property (nonatomic,retain) IBOutlet UILabel *lblSelectPlaylist;
@property (nonatomic,retain) IBOutlet UIView *txtPlaylistContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtPlaylist;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewPlaylistDownArrow;

@property (nonatomic,retain) IBOutlet UIButton *btnSubmit;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) UIPickerView *playlistPickerView;

@property (nonatomic,retain) Manager *objSelectedManager;
@property (nonatomic,retain) Playlist *objSelectedPlaylist;

@end
