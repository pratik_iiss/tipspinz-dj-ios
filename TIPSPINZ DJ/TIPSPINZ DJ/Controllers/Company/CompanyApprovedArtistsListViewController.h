//
//  CompanyApprovedArtistsListViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 30/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface CompanyApprovedArtistsListViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewSearch;
@property (nonatomic,retain) IBOutlet UIButton *btnSearch;

@property (nonatomic,retain) IBOutlet UIView *searchTextContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtSearch;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseSearchTextContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseSearchTextContainerView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;
@property (nonatomic,retain) IBOutlet UITableView *mainTableView;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *dataRows;

@end
