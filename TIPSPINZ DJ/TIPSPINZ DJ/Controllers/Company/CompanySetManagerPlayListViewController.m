//
//  CompanySetManagerPlayListViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 30/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import "CompanySetManagerPlayListViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

@interface CompanySetManagerPlayListViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation CompanySetManagerPlayListViewController

//========== IBOUTLETS ==========//

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainScrollView;

@synthesize mainContainerView;

@synthesize lblClubNameTitle;
@synthesize lblClubNameValue;

@synthesize lblSelectPlaylist;
@synthesize txtPlaylistContainerView;
@synthesize txtPlaylist;
@synthesize imageViewPlaylistDownArrow;

@synthesize btnSubmit;

//========== OTHER VARIABLES ==========//

@synthesize playlistPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotCompanySelectedPlaylistByCompanyIdEvent) name:@"gotCompanySelectedPlaylistByCompanyIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCompanyPlaylistByCompanyIdEvent) name:@"setCompanyPlaylistByCompanyIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotCompanySelectedPlaylistByCompanyIdEvent
{
    if([MySingleton sharedManager].dataManager.objLoggedInUser.arrayCompanyPlaylists.count <= 0)
    {
        txtPlaylist.userInteractionEnabled = false;
    }
    else
    {
        txtPlaylist.userInteractionEnabled = true;
        
        NSMutableArray *arrayClubOwnerPlaylistIds = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < [MySingleton sharedManager].dataManager.objLoggedInUser.arrayCompanyPlaylists.count; i++)
        {
            Playlist *objPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayCompanyPlaylists objectAtIndex:i];
            [arrayClubOwnerPlaylistIds addObject:objPlaylist.strPlaylistID];
        }
        
        NSInteger indexOfPlaylistId = [arrayClubOwnerPlaylistIds indexOfObject:[MySingleton sharedManager].dataManager.strCompanySelectedPlaylistIdForManager];
        
        if(NSNotFound != indexOfPlaylistId)
        {
            self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayCompanyPlaylists objectAtIndex:indexOfPlaylistId];
            txtPlaylist.text = [MySingleton sharedManager].dataManager.strCompanySelectedPlaylistNameForManager;
            
            [playlistPickerView selectRow:indexOfPlaylistId inComponent:0 animated:NO];
            [playlistPickerView reloadAllComponents];
        }
    }
}

-(void)setCompanyPlaylistByCompanyIdEvent
{
    txtPlaylist.text = @"";
    self.objSelectedPlaylist = nil;
    
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //        [appDelegate showErrorAlertViewWithTitle:@"Playlist Set" withDetails:@"Your playlist for this club has been set successfully."];
    //    });
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"Playlist Set";
    alertViewController.message = @"Your playlist for this manager has been set successfully.";
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Set Playlist"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblFont, *lblClubNameTitleFont, *lblClubNameValueFont, *txtFieldFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        lblClubNameTitleFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        lblClubNameValueFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        lblClubNameTitleFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        lblClubNameValueFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        lblClubNameTitleFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        lblClubNameValueFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    lblClubNameTitle.font = lblClubNameTitleFont;
    lblClubNameTitle.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblClubNameTitle.text = [NSString stringWithFormat:@"MANAGER :"];
    
    lblClubNameValue.font = lblClubNameValueFont;
    lblClubNameValue.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblClubNameValue.textAlignment = NSTextAlignmentLeft;
    lblClubNameValue.text = [NSString stringWithFormat:@"%@", self.objSelectedManager.strManagerName];
    
    lblSelectPlaylist.font = lblFont;
    lblSelectPlaylist.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    txtPlaylistContainerView.layer.masksToBounds = YES;
    txtPlaylistContainerView.layer.cornerRadius = 5.0f;
    txtPlaylistContainerView.layer.borderWidth = 1.0f;
    txtPlaylistContainerView.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    
    playlistPickerView = [[UIPickerView alloc] init];
    playlistPickerView.delegate = self;
    playlistPickerView.dataSource = self;
    playlistPickerView.showsSelectionIndicator = YES;
    playlistPickerView.tag = 1;
    playlistPickerView.backgroundColor = [UIColor whiteColor];
    
    txtPlaylist.font = txtFieldFont;
    txtPlaylist.delegate = self;
    [txtPlaylist setValue:[MySingleton sharedManager].textfieldPlaceholderColor
               forKeyPath:@"placeholderLabel.textColor"];
    txtPlaylist.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPlaylist.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPlaylist setInputView:playlistPickerView];
    [txtPlaylist setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtPlaylist.userInteractionEnabled = false;
    
    imageViewPlaylistDownArrow.layer.masksToBounds = YES;
    imageViewPlaylistDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewPlaylistDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewPlaylistDownArrowTapped:)];
    imageViewPlaylistDownArrowTapGesture.delegate = self;
    [imageViewPlaylistDownArrow addGestureRecognizer:imageViewPlaylistDownArrowTapGesture];
    
    btnSubmit.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnSubmit.layer.masksToBounds = true;
    btnSubmit.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSubmit.titleLabel.font = btnFont;
    [btnSubmit setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSubmit addTarget:self action:@selector(btnSubmitClicked) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getCompanySelectedPlaylistByCompanyId:[prefs objectForKey:@"userid"] withManagerId:self.objSelectedManager.strManagerID];
}

- (void)imageViewPlaylistDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtPlaylist becomeFirstResponder];
}


#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtPlaylist)
    {
        if(txtPlaylist.text.length == 0 )
        {
            self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayCompanyPlaylists objectAtIndex:0];
            txtPlaylist.text = self.objSelectedPlaylist.strPlaylistName;
            [playlistPickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayCompanyPlaylists count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == playlistPickerView)
    {
        Playlist *objPlaylist = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayCompanyPlaylists[row];
        lblMain.text = objPlaylist.strPlaylistName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayCompanyPlaylists objectAtIndex:row];
        txtPlaylist.text = self.objSelectedPlaylist.strPlaylistName;
    }
}

#pragma mark - Other Methods

- (void)btnSubmitClicked
{
    [self.view endEditing:YES];
    
    if(txtPlaylist.text.length > 0)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        [[MySingleton sharedManager].dataManager setCompanyPlaylistByCompanyId:[prefs objectForKey:@"userid"] withManagerId:self.objSelectedManager.strManagerID withPlaylistId:self.objSelectedPlaylist.strPlaylistID];
    }
    else
    {
        if(txtPlaylist.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please select a playlist"];
            });
        }
    }
}

@end


