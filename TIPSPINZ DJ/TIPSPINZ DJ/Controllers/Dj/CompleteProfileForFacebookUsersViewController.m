//
//  CompleteProfileForFacebookUsersViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 15/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "CompleteProfileForFacebookUsersViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "OurSponsorsViewController.h"

#import "CommonWebViewController.h"

@interface CompleteProfileForFacebookUsersViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation CompleteProfileForFacebookUsersViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize mainContainerView;

@synthesize imageViewMainLogo;

@synthesize imageViewPassword;
@synthesize txtPassword;
@synthesize txtPasswordBottomSeparatorView;

@synthesize imageViewPhoneNumber;
@synthesize txtPhoneNumber;
@synthesize txtPhoneNumberBottomSeparatorView;

@synthesize imageViewState;
@synthesize txtState;
@synthesize imageViewStateDownArrow;
@synthesize txtStateBottomSeparatorView;

@synthesize imageViewCity;
@synthesize txtCity;
@synthesize imageViewCityDownArrow;
@synthesize txtCityBottomSeparatorView;

@synthesize imageViewTermsAndConditionsCheckbox;
@synthesize lblByCreatingAnAccount;

@synthesize btnSignUp;

@synthesize btnAlreadyAMember;

//========== OTHER VARIABLES ==========//

@synthesize statePickerView;

@synthesize cityPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.autoresizesSubviews = false;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainContainerView.frame.origin.y + mainContainerView.frame.size.height);
    
    mainScrollView.autoresizesSubviews = true;
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllStatesEvent) name:@"gotAllStatesEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userSignedUpEvent) name:@"userSignedUpEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllStatesEvent
{
    if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
    {
        txtState.userInteractionEnabled = true;
    }
    else
    {
        txtState.userInteractionEnabled = false;
    }
}

-(void)userSignedUpEvent
{
//    [self navigateToHomeViewController];
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"Stripe Connection";
    alertViewController.message = @"You have successfully registered on our application. Before you start using our application, please connect your stripe account to use our application to its full extent.";
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"Go to Stripe" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        LoginViewController *viewController = [[LoginViewController alloc] init];;
        [self.navigationController pushViewController:viewController animated:YES];
        
        
        NSURL *webPageUrl = [NSURL URLWithString:[MySingleton sharedManager].dataManager.strSignedUpDjStripeConnectUrl];
        
        if ([[UIApplication sharedApplication] canOpenURL:webPageUrl])
        {
            [[UIApplication sharedApplication] openURL:webPageUrl];
        }
        
    }]];
    
    [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:alertViewController animated:YES completion:nil];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *txtFieldFont, *btnForgetPasswordFont, *lblByLoggingInFont, *btnFont, *btnAlreadyAMemberFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblByLoggingInFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        btnAlreadyAMemberFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        lblByLoggingInFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        btnAlreadyAMemberFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        lblByLoggingInFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        btnAlreadyAMemberFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    
    txtPassword.font = txtFieldFont;
    txtPassword.delegate = self;
    [txtPassword setValue:[MySingleton sharedManager].textfieldPlaceholderColor
               forKeyPath:@"placeholderLabel.textColor"];
    txtPassword.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPassword.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtPassword.secureTextEntry = true;
    
    txtPasswordBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    txtPhoneNumber.font = txtFieldFont;
    txtPhoneNumber.delegate = self;
    [txtPhoneNumber setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                  forKeyPath:@"placeholderLabel.textColor"];
    txtPhoneNumber.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPhoneNumber.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPhoneNumber setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtPhoneNumber setKeyboardType:UIKeyboardTypePhonePad];
    
    txtPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    statePickerView = [[UIPickerView alloc] init];
    statePickerView.delegate = self;
    statePickerView.dataSource = self;
    statePickerView.showsSelectionIndicator = YES;
    statePickerView.tag = 1;
    statePickerView.backgroundColor = [UIColor whiteColor];
    
    txtState.font = txtFieldFont;
    txtState.delegate = self;
    [txtState setValue:[MySingleton sharedManager].textfieldPlaceholderColor
            forKeyPath:@"placeholderLabel.textColor"];
    txtState.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtState.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtState setInputView:statePickerView];
    [txtState setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    imageViewStateDownArrow.layer.masksToBounds = YES;
    imageViewStateDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewStateDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewStateDownArrowTapped:)];
    imageViewStateDownArrowTapGesture.delegate = self;
    [imageViewStateDownArrow addGestureRecognizer:imageViewStateDownArrowTapGesture];
    
    txtStateBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    cityPickerView = [[UIPickerView alloc] init];
    cityPickerView.delegate = self;
    cityPickerView.dataSource = self;
    cityPickerView.showsSelectionIndicator = YES;
    cityPickerView.tag = 2;
    cityPickerView.backgroundColor = [UIColor whiteColor];
    
    txtCity.font = txtFieldFont;
    txtCity.delegate = self;
    [txtCity setValue:[MySingleton sharedManager].textfieldPlaceholderColor
           forKeyPath:@"placeholderLabel.textColor"];
    txtCity.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtCity.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtCity setInputView:cityPickerView];
    [txtCity setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtCity.userInteractionEnabled = false;
    
    imageViewCityDownArrow.layer.masksToBounds = YES;
    imageViewCityDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewCityDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewCityDownArrowTapped:)];
    imageViewCityDownArrowTapGesture.delegate = self;
    [imageViewCityDownArrow addGestureRecognizer:imageViewCityDownArrowTapGesture];
    
    txtCityBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    self.boolIsTermsAndConditionsChecked = false;
    
    imageViewTermsAndConditionsCheckbox.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewTermsAndConditionsCheckboxTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewTermsAndConditionsCheckboxTapped:)];
    imageViewTermsAndConditionsCheckboxTapGestureRecognizer.delegate = self;
    [imageViewTermsAndConditionsCheckbox addGestureRecognizer:imageViewTermsAndConditionsCheckboxTapGestureRecognizer];
    
    lblByCreatingAnAccount.font = lblByLoggingInFont;
    lblByCreatingAnAccount.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblByCreatingAnAccount.userInteractionEnabled = true;
    UITapGestureRecognizer *lblByCreatingAnAccountTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblByCreatingAnAccountTapped:)];
    lblByCreatingAnAccountTapGestureRecognizer.delegate = self;
    [lblByCreatingAnAccount addGestureRecognizer:lblByCreatingAnAccountTapGestureRecognizer];
    
    btnSignUp.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnSignUp.layer.masksToBounds = true;
    btnSignUp.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSignUp.titleLabel.font = btnFont;
    [btnSignUp setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSignUp addTarget:self action:@selector(btnSignupClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *strBtnAlreadyAMemberTitle = [NSString stringWithFormat:@"Already a member? Login."];
    NSMutableAttributedString *strAttrBtnAlreadyAMemberTitle = [[NSMutableAttributedString alloc] initWithString:strBtnAlreadyAMemberTitle attributes: nil];
    NSRange rangeOfLogin = [strBtnAlreadyAMemberTitle rangeOfString:@"Login."];
    [strAttrBtnAlreadyAMemberTitle addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:rangeOfLogin];
    [strAttrBtnAlreadyAMemberTitle addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalDarkGreyColor range:NSMakeRange(0, strBtnAlreadyAMemberTitle.length)];
    [btnAlreadyAMember setAttributedTitle:strAttrBtnAlreadyAMemberTitle forState:UIControlStateNormal];
    btnAlreadyAMember.titleLabel.font = btnAlreadyAMemberFont;
    [btnAlreadyAMember addTarget:self action:@selector(btnAlreadyAMemberClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [[MySingleton sharedManager].dataManager getAllStates];
}

- (void)imageViewStateDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtState becomeFirstResponder];
}

- (void)imageViewCityDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtCity becomeFirstResponder];
}

- (void)lblByCreatingAnAccountTapped: (UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    CommonWebViewController *viewController = [[CommonWebViewController alloc] init];
    viewController.strTitle = @"Terms & Conditions";
    viewController.strUrl = @"https://www.tipspinz.com/app/termsandconditions.html";
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)imageViewTermsAndConditionsCheckboxTapped: (UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    if(self.boolIsTermsAndConditionsChecked)
    {
        self.boolIsTermsAndConditionsChecked = false;
        imageViewTermsAndConditionsCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked_privacy_policy.png"];
    }
    else
    {
        self.boolIsTermsAndConditionsChecked = true;
        imageViewTermsAndConditionsCheckbox.image = [UIImage imageNamed:@"checkbox_checked_privacy_policy.png"];
    }
    
//    CommonWebViewController *viewController = [[CommonWebViewController alloc] init];
//    viewController.strTitle = @"Terms & Conditions";
//    viewController.strUrl = @"https://www.tipspinz.com/app/termsandconditions.html";
//    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtPassword)
    {
        imageViewPassword.image = [UIImage imageNamed:@"textfield_password_selected"];
        txtPasswordBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
    else if(textField == txtPhoneNumber)
    {
        imageViewPhoneNumber.image = [UIImage imageNamed:@"textfield_phonenumber_selected"];
        txtPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
    else if(textField == txtState)
    {
        if(txtState.text.length == 0 )
        {
            self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:0];
            txtState.text = self.objSelectedState.strStateName;
            txtCity.userInteractionEnabled = true;
            [statePickerView selectRow:0 inComponent:0 animated:YES];
        }
        
        imageViewState.image = [UIImage imageNamed:@"textfield_state_selected"];
        txtStateBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
    else if(textField == txtCity)
    {
        if(txtCity.text.length == 0 )
        {
            self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:0];
            txtCity.text = self.objSelectedCity.strCityName;
            [cityPickerView selectRow:0 inComponent:0 animated:YES];
        }
        
        imageViewCity.image = [UIImage imageNamed:@"textfield_city_selected"];
        txtCityBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == txtPassword)
    {
        imageViewPassword.image = [UIImage imageNamed:@"textfield_password_normal"];
        txtPasswordBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
    else if(textField == txtPhoneNumber)
    {
        imageViewPhoneNumber.image = [UIImage imageNamed:@"textfield_phonenumber_normal"];
        txtPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
    else if(textField == txtState)
    {
        imageViewState.image = [UIImage imageNamed:@"textfield_state_normal"];
        txtStateBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
    else if(textField == txtCity)
    {
        imageViewCity.image = [UIImage imageNamed:@"textfield_city_normal"];
        txtCityBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.arrayStates count];
    }
    else if(pickerView.tag == 2)
    {
        rowsInComponent = [self.objSelectedState.arrayCity count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == statePickerView)
    {
        State *objState = [MySingleton sharedManager].dataManager.arrayStates[row];
        lblMain.text = objState.strStateName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == cityPickerView)
    {
        City *objCity = self.objSelectedState.arrayCity[row];
        lblMain.text = objCity.strCityName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:row];
        txtState.text = self.objSelectedState.strStateName;
        
        [cityPickerView selectRow:0 inComponent:0 animated:NO];
        txtCity.text = @"";
        txtCity.userInteractionEnabled = true;
    }
    else if(pickerView.tag == 2)
    {
        self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:row];
        txtCity.text = self.objSelectedCity.strCityName;
    }
}

#pragma mark - Other Methods

-(void)bindDataToObject
{
    if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
    {
        [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc]init];
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInUser.strEmail = self.strFacebookUserEmail;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strPassword = txtPassword.text;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strFullname = self.strFacebookUserFullName;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strGender = self.strFacebookUserGender;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strPhoneNumber = txtPhoneNumber.text;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strStateId = self.objSelectedState.strStateID;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strStateName = txtState.text;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strCityId = self.objSelectedCity.strCityID;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strCityName = txtCity.text;
    [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = false;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strUserType = @"1";
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //[MySingleton sharedManager].dataManager.objLoggedInUser.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"deviceToken"]];
    [MySingleton sharedManager].dataManager.objLoggedInUser.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"fcmToken"]];
    [prefs synchronize];
}

- (IBAction)btnSignupClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.boolIsTermsAndConditionsChecked)
    {
        [self bindDataToObject];
        
        if([[MySingleton sharedManager].dataManager.objLoggedInUser isValidateUserForRegistration])
        {
            [[MySingleton sharedManager].dataManager userSignUp:[MySingleton sharedManager].dataManager.objLoggedInUser];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:[MySingleton sharedManager].dataManager.objLoggedInUser.strValidationMessage];
            });
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"You must agree to the terms and conditions to register to our application."];
        });
    }
}

- (IBAction)btnAlreadyAMemberClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)navigateToHomeViewController
{
    [self.view endEditing:true];
    
    OurSponsorsViewController *viewController = [[OurSponsorsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
