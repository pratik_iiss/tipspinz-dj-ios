//
//  AddSongsViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 08/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "AddSongsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

@interface AddSongsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation AddSongsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;

@synthesize lblSelectPlaylist;
@synthesize txtPlaylistContainerView;
@synthesize txtPlaylist;
@synthesize imageViewPlaylistDownArrow;

@synthesize lblSongName;
@synthesize txtSongName;

@synthesize lblArtistName;
@synthesize txtArtistName;

@synthesize lblSongType;
@synthesize txtSongType;

@synthesize btnAddSong;

//========== OTHER VARIABLES ==========//

@synthesize playlistPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllPlaylistsByUserIdEvent) name:@"gotAllPlaylistsByUserIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addedSongEvent) name:@"addedSongEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllPlaylistsByUserIdEvent
{
    if([MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists.count <= 0)
    {
        txtPlaylist.userInteractionEnabled = false;
    }
    else
    {
        txtPlaylist.userInteractionEnabled = true;
    }
}

-(void)addedSongEvent
{
    txtPlaylist.text = @"";
    self.objSelectedPlaylist = nil;
    
    txtSongName.text = @"";
    txtArtistName.text = @"";
    txtSongType.text = @"";
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [MySingleton sharedManager].dataManager.boolIsSongsAddedIntoPlaylistSuccessfully = true;
        
        [appDelegate showErrorAlertViewWithTitle:@"Song Added" withDetails:@"Your song has been added successfully."];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Add Song"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
        
    mainInnerScrollView.delegate = self;
    mainInnerScrollView.contentSize = CGSizeMake(mainInnerScrollView.frame.size.width, mainInnerScrollView.frame.size.height);
    
    UIFont *lblFont, *txtFieldFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    lblSelectPlaylist.font = lblFont;
    lblSelectPlaylist.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    txtPlaylistContainerView.layer.masksToBounds = YES;
    txtPlaylistContainerView.layer.cornerRadius = 5.0f;
    txtPlaylistContainerView.layer.borderWidth = 1.0f;
    txtPlaylistContainerView.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    
    playlistPickerView = [[UIPickerView alloc] init];
    playlistPickerView.delegate = self;
    playlistPickerView.dataSource = self;
    playlistPickerView.showsSelectionIndicator = YES;
    playlistPickerView.tag = 1;
    playlistPickerView.backgroundColor = [UIColor whiteColor];
    
    txtPlaylist.font = txtFieldFont;
    txtPlaylist.delegate = self;
    [txtPlaylist setValue:[MySingleton sharedManager].textfieldPlaceholderColor
               forKeyPath:@"placeholderLabel.textColor"];
    txtPlaylist.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPlaylist.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPlaylist setInputView:playlistPickerView];
    [txtPlaylist setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtPlaylist.userInteractionEnabled = false;
    
    imageViewPlaylistDownArrow.layer.masksToBounds = YES;
    imageViewPlaylistDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewPlaylistDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewPlaylistDownArrowTapped:)];
    imageViewPlaylistDownArrowTapGesture.delegate = self;
    [imageViewPlaylistDownArrow addGestureRecognizer:imageViewPlaylistDownArrowTapGesture];
    
    lblSongName.font = lblFont;
    lblSongName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    UIView *txtSongNamePaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtSongName.frame.size.height)];
    txtSongName.leftView = txtSongNamePaddingView;
    txtSongName.leftViewMode = UITextFieldViewModeAlways;
    txtSongName.layer.masksToBounds = YES;
    txtSongName.layer.cornerRadius = 5.0f;
    txtSongName.layer.borderWidth = 1.0f;
    txtSongName.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtSongName.font = txtFieldFont;
    txtSongName.delegate = self;
    [txtSongName setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                forKeyPath:@"placeholderLabel.textColor"];
    txtSongName.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtSongName.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtSongName setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    
    lblArtistName.font = lblFont;
    lblArtistName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    UIView *txtArtistNamePaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtArtistName.frame.size.height)];
    txtArtistName.leftView = txtArtistNamePaddingView;
    txtArtistName.leftViewMode = UITextFieldViewModeAlways;
    txtArtistName.layer.masksToBounds = YES;
    txtArtistName.layer.cornerRadius = 5.0f;
    txtArtistName.layer.borderWidth = 1.0f;
    txtArtistName.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtArtistName.font = txtFieldFont;
    txtArtistName.delegate = self;
    [txtArtistName setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                 forKeyPath:@"placeholderLabel.textColor"];
    txtArtistName.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtArtistName.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtArtistName setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    
    lblSongType.font = lblFont;
    lblSongType.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    UIView *txtSongTypePaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtSongType.frame.size.height)];
    txtSongType.leftView = txtSongTypePaddingView;
    txtSongType.leftViewMode = UITextFieldViewModeAlways;
    txtSongType.layer.masksToBounds = YES;
    txtSongType.layer.cornerRadius = 5.0f;
    txtSongType.layer.borderWidth = 1.0f;
    txtSongType.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtSongType.font = txtFieldFont;
    txtSongType.delegate = self;
    [txtSongType setValue:[MySingleton sharedManager].textfieldPlaceholderColor
               forKeyPath:@"placeholderLabel.textColor"];
    txtSongType.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtSongType.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtSongType setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    
    btnAddSong.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnAddSong.layer.masksToBounds = true;
    btnAddSong.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnAddSong.titleLabel.font = btnFont;
    [btnAddSong setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnAddSong addTarget:self action:@selector(btnAddSongClicked) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getAllPlaylistsByUserId:[prefs objectForKey:@"userid"]];
}

- (void)imageViewPlaylistDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtPlaylist becomeFirstResponder];
}


#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtPlaylist)
    {
        if(txtPlaylist.text.length == 0 )
        {
            self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists objectAtIndex:0];
            txtPlaylist.text = self.objSelectedPlaylist.strPlaylistName;
            [playlistPickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == playlistPickerView)
    {
        Playlist *objPlaylist = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists[row];
        lblMain.text = objPlaylist.strPlaylistName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists objectAtIndex:row];
        txtPlaylist.text = self.objSelectedPlaylist.strPlaylistName;
    }
}

#pragma mark - Other Methods

- (void)btnAddSongClicked
{
    [self.view endEditing:YES];
    
//    if(txtPlaylist.text.length > 0 && txtSongName.text.length > 0 && txtArtistName.text.length > 0 && txtSongType.text.length > 0)
    if(txtPlaylist.text.length > 0 && txtSongName.text.length > 0 && txtArtistName.text.length > 0)
    {
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
        [dictParameters setObject:txtPlaylist.text forKey:@"strPlaylistName"];
        [dictParameters setObject:self.objSelectedPlaylist.strPlaylistID forKey:@"strPlaylistID"];
        [dictParameters setObject:txtSongName.text forKey:@"strSongName"];
        [dictParameters setObject:txtArtistName.text forKey:@"strArtistName"];
//        [dictParameters setObject:txtSongType.text forKey:@"strSongType"];
        
        [[MySingleton sharedManager].dataManager addSong:dictParameters];
    }
    else
    {
        if(txtPlaylist.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please select a playlist"];
            });
        }
        else if(txtSongName.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter name of the song"];
            });
        }
        else if(txtArtistName.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter name of the artist"];
            });
        }
//        else if(txtSongType.text.length <= 0)
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter type of the song"];
//            });
//        }
    }
}

@end
