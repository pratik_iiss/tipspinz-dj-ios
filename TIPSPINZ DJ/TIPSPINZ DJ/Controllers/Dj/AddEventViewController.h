//
//  AddEventViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 13/12/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#import "AsyncImageView.h"

@interface AddEventViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;

@property (nonatomic,retain) IBOutlet UITextField *txtEventName;
@property (nonatomic,retain) IBOutlet UITextField *txtEventLocation;
@property (nonatomic,retain) IBOutlet UITextField *txtEventDate;

@property (nonatomic,retain) IBOutlet UIView *txtViewEventVenueAddressContainerView;
@property (nonatomic,retain) IBOutlet UITextView *txtViewEventVenueAddress;

@property (nonatomic,retain) IBOutlet UIView *txtViewEventDescriptionContainerView;
@property (nonatomic,retain) IBOutlet UITextView *txtViewEventDescription;

@property (nonatomic,retain) IBOutlet UIButton *btnAdd;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) UIDatePicker *eventDatePicker;

@end
