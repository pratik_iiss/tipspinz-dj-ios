//
//  MyEventsViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 13/12/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "MyEventsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "EventListTableViewCell.h"

#import "Event.h"

#import "EventDetailsViewController.h"

@interface MyEventsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
    
    UIRefreshControl *refreshControl;
}

@end

@implementation MyEventsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewNavigationBarBackground;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewSearch;
@synthesize btnSearch;

@synthesize searchTextContainerView;
@synthesize txtSearch;
@synthesize imageViewCloseSearchTextContainerView;
@synthesize btnCloseSearchTextContainerView;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;
@synthesize mainTableView;
@synthesize lblNoData;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    if ([MySingleton sharedManager].dataManager.boolIsEventStatusChangedSuccessfully)
    {
        [MySingleton sharedManager].dataManager.boolIsEventStatusChangedSuccessfully = false;
        
        // DJ ONLINE
        
        if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
        {
            [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
        }
        
        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = true;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"djsOnlineStatusUpdatedEvent" object:nil];
        
        
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        
        [[MySingleton sharedManager].dataManager getAllEventsByDjId:dictParameters];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllEventsByDjIdEvent) name:@"gotAllEventsByDjIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletedEventEvent) name:@"deletedEventEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllEventsByDjIdEvent
{
    [refreshControl endRefreshing];
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayAllEventsByDj;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

-(void)deletedEventEvent
{
    [refreshControl endRefreshing];
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayAllEventsByDj;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Events"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    
    imageViewSearch.layer.masksToBounds = YES;
    [btnSearch addTarget:self action:@selector(btnSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *txtFieldFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
    }
    
    searchTextContainerView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    txtSearch.font = txtFieldFont;
    txtSearch.delegate = self;
    [txtSearch setValue:[MySingleton sharedManager].themeGlobalWhiteColor
             forKeyPath:@"placeholderLabel.textColor"];
    txtSearch.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.tintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.returnKeyType = UIReturnKeySearch;
    [txtSearch setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    imageViewCloseSearchTextContainerView.layer.masksToBounds = YES;
    [btnCloseSearchTextContainerView addTarget:self action:@selector(btnCloseSearchTextContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnSearchClicked:(id)sender
{
    [self.view endEditing:YES];
    
    searchTextContainerView.hidden = false;
    [txtSearch becomeFirstResponder];
}

-(void)btnCloseSearchTextContainerViewClicked
{
    [self.view endEditing:YES];
    
    txtSearch.text = @"";
    
    searchTextContainerView.hidden = true;
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayAllEventsByDj;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull down to Refresh"];
    [refreshControl addTarget:self action:@selector(refreshTableView:) forControlEvents:UIControlEventValueChanged];
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor clearColor];
    
    UIFont *lblNoDataFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
    }
    else
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblNoData.font = lblNoDataFont;
    lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
    
    [[MySingleton sharedManager].dataManager getAllEventsByDjId:dictParameters];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        //        return 90;
        
        Event *objEvent = [self.dataRows objectAtIndex:indexPath.row];
        
        UILabel *lblEventDescription = [[UILabel alloc] initWithFrame:CGRectMake(10, 60, ([MySingleton sharedManager].screenWidth - 40), 20)];
        lblEventDescription.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblEventDescription.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        lblEventDescription.textAlignment = NSTextAlignmentLeft;
        lblEventDescription.numberOfLines = 0;
        lblEventDescription.layer.masksToBounds = YES;
        lblEventDescription.text = objEvent.strEventDescription;
        [lblEventDescription sizeToFit];
        
        CGRect lblEventDescriptionRect = [lblEventDescription.text boundingRectWithSize:lblEventDescription.frame.size
                                                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                                             attributes:@{NSFontAttributeName:lblEventDescription.font}
                                                                                context:nil];
        
        CGSize lblEventDescriptionSize = lblEventDescriptionRect.size;
        
        CGFloat lblEventDescriptionHeight;
        
        if(objEvent.strEventDescription.length > 0)
        {
            lblEventDescriptionHeight = lblEventDescriptionSize.height;
        }
        else
        {
            lblEventDescriptionHeight = 0;
        }
        
        CGFloat floatCellHeight = lblEventDescription.frame.origin.y + lblEventDescriptionHeight + 10;
        
        if(floatCellHeight > 90)
        {
            return floatCellHeight;
        }
        else
        {
            return 90;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        EventListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[EventListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        Event *objEvent = [self.dataRows objectAtIndex:indexPath.row];
        
        cell.lblEventDate.text = objEvent.strEventDate;
        cell.lblEventName.text = objEvent.strEventName;
        
        cell.lblEventDescription.text = objEvent.strEventDescription;
        [cell.lblEventDescription sizeToFit];
        
        if(objEvent.boolIsEventLive)
        {
            cell.imageViewLive.image = [UIImage imageNamed:@"event_online.png"];
        }
        else
        {
            cell.imageViewLive.image = [UIImage imageNamed:@"event_offline.png"];
        }
        
        cell.btnDelete.tag = indexPath.row;
        [cell.btnDelete addTarget:self action:@selector(btnDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        
        CGFloat lblEventDescriptionHeight;
        
        if(objEvent.strEventDescription.length > 0)
        {
            CGRect lblEventDescriptionRect = [cell.lblEventDescription.text boundingRectWithSize:cell.lblEventDescription.frame.size
                                                                                         options:NSStringDrawingUsesLineFragmentOrigin
                                                                                      attributes:@{NSFontAttributeName:cell.lblEventDescription.font}
                                                                                         context:nil];
            
            CGSize lblEventDescriptionSize = lblEventDescriptionRect.size;
            
            lblEventDescriptionHeight = lblEventDescriptionSize.height;
        }
        else
        {
            lblEventDescriptionHeight = 0;
        }
        
        CGRect lblEventDescriptionFrame = cell.lblEventDescription.frame;
        lblEventDescriptionFrame.size.height = lblEventDescriptionHeight;
        cell.lblEventDescription.frame = lblEventDescriptionFrame;
        
        CGFloat floatCellHeight = cell.lblEventDescription.frame.origin.y + lblEventDescriptionHeight + 10;
        
        CGRect mainContainerFrame = cell.mainContainer.frame;
        
        if(floatCellHeight > 90)
        {
            mainContainerFrame.size.height = floatCellHeight;
        }
        else
        {
            mainContainerFrame.size.height = 90;
        }
        
        cell.mainContainer.frame = mainContainerFrame;
        
        CGRect imageViewRightArrowFrame = cell.imageViewRightArrow.frame;
        imageViewRightArrowFrame.origin.y = (cell.mainContainer.frame.size.height - cell.imageViewRightArrow.frame.size.height)/2;
        cell.imageViewRightArrow.frame = imageViewRightArrowFrame;
        
        CGRect separatorViewFrame = cell.separatorView.frame;
        separatorViewFrame.origin.y = cell.mainContainer.frame.size.height - 1;
        cell.separatorView.frame = separatorViewFrame;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.contentView.userInteractionEnabled = false;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            Event *objEvent = [self.dataRows objectAtIndex:indexPath.row];
            
//            if (objEvent.boolIsEventLive)
//            {
                EventDetailsViewController *viewController = [[EventDetailsViewController alloc] init];
                viewController.objSelectedEvent = objEvent;
                [self.navigationController pushViewController:viewController animated:YES];
//            }
        }
        else
        {
            
        }
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if(textField == txtSearch)
    {
        //        NSLog(@"Search button clicked for txtSearch.");
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == txtSearch)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayAllEventsByDj;
        
        if(self.dataRows.count <= 0)
        {
            mainTableView.hidden = true;
            lblNoData.hidden = false;
        }
        else
        {
            mainTableView.hidden = false;
            lblNoData.hidden = true;
            [mainTableView reloadData];
        }
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    [self searchTableViewDataWithSubstring:substring];
    return YES;
}

#pragma mark - Other Methods

- (void)refreshTableView:(UIRefreshControl *)refreshControl
{
    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
    
    [[MySingleton sharedManager].dataManager getAllEventsByDjId:dictParameters];
}

- (void)searchTableViewDataWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        for(Event *objEvent in [MySingleton sharedManager].dataManager.arrayAllEventsByDj)
        {
            if ([[objEvent.strEventName lowercaseString] containsString:[substring lowercaseString]] || [[objEvent.strEventDescription lowercaseString] containsString:[substring lowercaseString]] || [[objEvent.strEventDate lowercaseString] containsString:[substring lowercaseString]])
            {
                [self.dataRows addObject:objEvent];
            }
        }
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayAllEventsByDj;
    }
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

- (IBAction)btnMainClicked:(id)sender
{
    UIButton *btnSender = (UIButton *)sender;
}

- (IBAction)btnDeleteClicked:(id)sender
{
    [self.view endEditing:YES];
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Are you sure you want to delete?";
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        UIButton *objButton = (UIButton *)sender;
        
        Event *objEvent = [self.dataRows objectAtIndex:objButton.tag];
        
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
        
        [dictParameters setObject:objEvent.strEventID forKey:@"event_id"];
        
        [[MySingleton sharedManager].dataManager deleteEvent:dictParameters];
    }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

@end



