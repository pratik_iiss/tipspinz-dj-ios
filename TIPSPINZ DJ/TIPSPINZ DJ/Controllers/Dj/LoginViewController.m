//
//  LoginViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 22/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "LoginViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#import "SignUpViewController.h"
#import "HomeViewController.h"
#import "OurSponsorsViewController.h"

#import "CommonWebViewController.h"
#import "ForgetPasswordViewController.h"

#import "CompleteProfileForFacebookUsersViewController.h"

@interface LoginViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation LoginViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize mainContainerView;

@synthesize imageViewBack;
@synthesize btnBack;

@synthesize imageViewMainLogo;

@synthesize imageViewEmail;
@synthesize txtEmail;
@synthesize txtEmailBottomSeparatorView;

@synthesize imageViewPassword;
@synthesize txtPassword;
@synthesize txtPasswordBottomSeparatorView;

@synthesize btnForgetPassword;

@synthesize imageViewTermsAndConditionsCheckbox;
@synthesize lblByLoggingIn;

@synthesize btnLogin;

@synthesize btnSignInWithFacebookContainerView;
@synthesize imageViewSignInWithFacebook;
@synthesize lblSignInWithFacebook;
@synthesize btnSignInWithFacebook;

@synthesize btnSignUpWithFacebookContainerView;
@synthesize imageViewSignUpWithFacebook;
@synthesize lblSignUpWithFacebook;
@synthesize btnSignUpWithFacebook;

@synthesize btnJoinNow;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    if ([FBSDKAccessToken currentAccessToken])
    {
        // User is logged in, do work such as go to next view controller.
        NSLog(@"Facebook User is logged in");
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedinEvent) name:@"userLoggedinEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkedIfTheFacebookUserIsAlreadyRegisteredEvent) name:@"checkedIfTheFacebookUserIsAlreadyRegisteredEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)userLoggedinEvent
{
    [self navigateToHomeViewController];
}

-(void)checkedIfTheFacebookUserIsAlreadyRegisteredEvent
{
    [self.view endEditing:true];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    if([prefs objectForKey:@"autologin"] != nil && [[prefs objectForKey:@"autologin"] isEqualToString:@"1"])
    {
        [self navigateToHomeViewController];
    }
    else
    {
        CompleteProfileForFacebookUsersViewController *viewController = [[CompleteProfileForFacebookUsersViewController alloc] init];
        viewController.strFacebookUserFullName = [MySingleton sharedManager].dataManager.objLoggedInUser.strFullname;
        viewController.strFacebookUserEmail = [MySingleton sharedManager].dataManager.objLoggedInUser.strEmail;
        viewController.strFacebookUserGender = [MySingleton sharedManager].dataManager.objLoggedInUser.strGender;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *txtFieldFont, *btnForgetPasswordFont, *lblByLoggingInFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblByLoggingInFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        lblByLoggingInFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnForgetPasswordFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        lblByLoggingInFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    imageViewBack.layer.masksToBounds = true;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    txtEmail.font = txtFieldFont;
    txtEmail.delegate = self;
    [txtEmail setValue:[MySingleton sharedManager].textfieldPlaceholderColor
            forKeyPath:@"placeholderLabel.textColor"];
    txtEmail.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtEmail.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtEmail setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    
    txtEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    txtPassword.font = txtFieldFont;
    txtPassword.delegate = self;
    [txtPassword setValue:[MySingleton sharedManager].textfieldPlaceholderColor
               forKeyPath:@"placeholderLabel.textColor"];
    txtPassword.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPassword.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtPassword.secureTextEntry = true;
    
    txtPasswordBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    btnForgetPassword.titleLabel.font = btnForgetPasswordFont;
    [btnForgetPassword setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnForgetPassword addTarget:self action:@selector(btnForgetPasswordClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.boolIsTermsAndConditionsChecked = false;
    
    imageViewTermsAndConditionsCheckbox.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewTermsAndConditionsCheckboxTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewTermsAndConditionsCheckboxTapped:)];
    imageViewTermsAndConditionsCheckboxTapGestureRecognizer.delegate = self;
    [imageViewTermsAndConditionsCheckbox addGestureRecognizer:imageViewTermsAndConditionsCheckboxTapGestureRecognizer];
    
    lblByLoggingIn.font = lblByLoggingInFont;
    lblByLoggingIn.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblByLoggingIn.userInteractionEnabled = true;
    UITapGestureRecognizer *lblByLoggingInTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblByLoggingInTapped:)];
    lblByLoggingInTapGestureRecognizer.delegate = self;
    [lblByLoggingIn addGestureRecognizer:lblByLoggingInTapGestureRecognizer];
    
    btnLogin.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnLogin.layer.masksToBounds = true;
    btnLogin.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnLogin.titleLabel.font = btnFont;
    [btnLogin setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnLogin addTarget:self action:@selector(btnLoginClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    btnSignInWithFacebookContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalFacebookBlueColor;
    btnSignInWithFacebookContainerView.layer.masksToBounds = true;
    btnSignInWithFacebookContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    
    lblSignInWithFacebook.font = btnFont;
    lblSignInWithFacebook.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    [btnSignInWithFacebook addTarget:self action:@selector(btnSignInWithFacebookClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    btnSignUpWithFacebookContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalFacebookBlueColor;
    btnSignUpWithFacebookContainerView.layer.masksToBounds = true;
    btnSignUpWithFacebookContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    
    lblSignUpWithFacebook.font = btnFont;
    lblSignUpWithFacebook.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    [btnSignUpWithFacebook addTarget:self action:@selector(btnSignUpWithFacebookClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    btnJoinNow.backgroundColor = [MySingleton sharedManager].themeGlobalLightestGreyColor;
    btnJoinNow.titleLabel.font = btnFont;
    [btnJoinNow setTitleColor:[MySingleton sharedManager].themeGlobalDarkGreyColor forState:UIControlStateNormal];
    [btnJoinNow addTarget:self action:@selector(btnJoinNowClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if(([MySingleton sharedManager].dataManager.strSignedUpDjEmail != nil && [MySingleton sharedManager].dataManager.strSignedUpDjEmail.length > 0) && ([MySingleton sharedManager].dataManager.strSignedUpDjPassword != nil && [MySingleton sharedManager].dataManager.strSignedUpDjPassword.length > 0))
    {
        txtEmail.text = [MySingleton sharedManager].dataManager.strSignedUpDjEmail;
        txtPassword.text = [MySingleton sharedManager].dataManager.strSignedUpDjPassword;
    }
    
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  NSString *isFacebookHidden = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"IsFacebookHidden"]];
  
  if([isFacebookHidden isEqualToString:@"1"])
  {
      [btnSignInWithFacebookContainerView setHidden:true];
      [btnSignUpWithFacebookContainerView setHidden:true];
  }
  else {
      [btnSignInWithFacebookContainerView setHidden:false];
      [btnSignUpWithFacebookContainerView setHidden:false];
  }
  
    //========== PRATIK GUJARATI TEMP DATA ==========//
//    txtEmail.text = @"sdivetiya@gmail.com";
//    txtPassword.text = @"123456";
    //========== PRATIK GUJARATI TEMP DATA ==========//
}

- (void)lblByLoggingInTapped: (UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    CommonWebViewController *viewController = [[CommonWebViewController alloc] init];
    viewController.strTitle = @"Terms & Conditions";
    viewController.strUrl = @"https://www.tipspinz.com/app/termsandconditions.html";
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)imageViewTermsAndConditionsCheckboxTapped: (UITapGestureRecognizer *)recognizer
{
    [self.view endEditing:YES];
    
    if(self.boolIsTermsAndConditionsChecked)
    {
        self.boolIsTermsAndConditionsChecked = false;
        imageViewTermsAndConditionsCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked_privacy_policy.png"];
    }
    else
    {
        self.boolIsTermsAndConditionsChecked = true;
        imageViewTermsAndConditionsCheckbox.image = [UIImage imageNamed:@"checkbox_checked_privacy_policy.png"];
    }
    
//    CommonWebViewController *viewController = [[CommonWebViewController alloc] init];
//    viewController.strTitle = @"Terms & Conditions";
//    viewController.strUrl = @"https://www.tipspinz.com/app/termsandconditions.html";
//    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtEmail)
    {
        imageViewEmail.image = [UIImage imageNamed:@"textfield_email_selected"];
        txtEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
    else if(textField == txtPassword)
    {
        imageViewPassword.image = [UIImage imageNamed:@"textfield_password_selected"];
        txtPasswordBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == txtEmail)
    {
        imageViewEmail.image = [UIImage imageNamed:@"textfield_email_normal"];
        txtEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
    else if(textField == txtPassword)
    {
        imageViewPassword.image = [UIImage imageNamed:@"textfield_password_normal"];
        txtPasswordBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnBackClicked:(id)sender
{
    [self.view endEditing:true];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnForgetPasswordClicked:(id)sender
{
    [self.view endEditing:true];
    
    ForgetPasswordViewController *viewController = [[ForgetPasswordViewController alloc] init];
    viewController.strUserType = @"0";
    [self.navigationController pushViewController:viewController animated:YES];
}

-(void)bindDataToObject
{
    if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
    {
        [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc]init];
    }
    
    [MySingleton sharedManager].dataManager.objLoggedInUser.strEmail = txtEmail.text;
    [MySingleton sharedManager].dataManager.objLoggedInUser.strPassword = txtPassword.text;
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    //[MySingleton sharedManager].dataManager.objLoggedInUser.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"deviceToken"]];
    [MySingleton sharedManager].dataManager.objLoggedInUser.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"fcmToken"]];
}

- (IBAction)btnLoginClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.boolIsTermsAndConditionsChecked)
    {
        [self bindDataToObject];
        
        if([[MySingleton sharedManager].dataManager.objLoggedInUser isValidateUserForLogin])
        {
            [[MySingleton sharedManager].dataManager userLogin:[MySingleton sharedManager].dataManager.objLoggedInUser];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:[MySingleton sharedManager].dataManager.objLoggedInUser.strValidationMessage];
            });
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"You must agree to the terms and conditions to login to our application."];
        });
    }
}

- (IBAction)btnSignInWithFacebookClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.boolIsTermsAndConditionsChecked)
    {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result,NSError *error) {
//    [login logInWithReadPermissions:@[@"email",@"public_profile",@"user_location",@"user_birthday",@"user_hometown"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result,NSError *error) {
            if (error)
            {
                // Process error
                NSLog(@"error %@",error);
            }
            else if (result.isCancelled)
            {
                // Handle cancellations
                NSLog(@"Cancelled");
                
                NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                alertViewController.title = @"Login Cancelled";
                alertViewController.message = @"We were unable to connect to Facebook as you cancelled the request.";
                
                alertViewController.view.tintColor = [UIColor whiteColor];
                alertViewController.backgroundTapDismissalGestureEnabled = YES;
                alertViewController.swipeDismissalGestureEnabled = YES;
                alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                
                alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                    [alertViewController dismissViewControllerAnimated:YES completion:nil];
                }]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:alertViewController animated:YES completion:nil];
                });
            }
            else
            {
                if ([result.grantedPermissions containsObject:@"email"])
                {
                    // Do work
                    NSLog(@"Logged in");
                    [self gotFacebookData];
                }
            }
        }];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"You must agree to the terms and conditions to login to our application."];
        });
    }
}

- (IBAction)btnSignUpWithFacebookClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.boolIsTermsAndConditionsChecked)
    {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:@[@"email",@"public_profile"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result,NSError *error) {
//    [login logInWithReadPermissions:@[@"email",@"public_profile",@"user_location",@"user_birthday",@"user_hometown"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result,NSError *error) {
            if (error)
            {
                // Process error
                NSLog(@"error %@",error);
            }
            else if (result.isCancelled)
            {
                // Handle cancellations
                NSLog(@"Cancelled");
                
                NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                alertViewController.title = @"Login Cancelled";
                alertViewController.message = @"We were unable to connect to Facebook as you cancelled the request.";
                
                alertViewController.view.tintColor = [UIColor whiteColor];
                alertViewController.backgroundTapDismissalGestureEnabled = YES;
                alertViewController.swipeDismissalGestureEnabled = YES;
                alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                
                alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                    [alertViewController dismissViewControllerAnimated:YES completion:nil];
                }]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:alertViewController animated:YES completion:nil];
                });
            }
            else
            {
                if ([result.grantedPermissions containsObject:@"email"])
                {
                    // Do work
                    NSLog(@"Logged in");
                    [self gotFacebookData];
                }
            }
        }];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"You must agree to the terms and conditions to register to our application."];
        });
    }
}

- (void)gotFacebookData
{
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             if (!error)
             {
                 NSLog(@"fetched user:%@", result);
                 
                 if (([[result allKeys] containsObject:@"id"]) && ([[result allKeys] containsObject:@"email"]))
                 {
                     [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
                     
                     [MySingleton sharedManager].dataManager.objLoggedInUser.strFullname = [NSString stringWithFormat:@"%@ %@",[result objectForKey:@"first_name"],[result objectForKey:@"last_name"]];
                     [MySingleton sharedManager].dataManager.objLoggedInUser.strEmail = [result objectForKey:@"email"];
                     
                     if ([result objectForKey:@"gender"] == nil) {
                         [MySingleton sharedManager].dataManager.objLoggedInUser.strGender = @"Male";
                     } else {
                         [MySingleton sharedManager].dataManager.objLoggedInUser.strGender = [result objectForKey:@"gender"];
                     } //n
                     
                     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                     //[MySingleton sharedManager].dataManager.objLoggedInUser.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"deviceToken"]];
                    [MySingleton sharedManager].dataManager.objLoggedInUser.strDeviceToken = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"fcmToken"]];
                     
                     [[MySingleton sharedManager].dataManager checkIfTheFacebookUserIsAlreadyRegistered:[MySingleton sharedManager].dataManager.objLoggedInUser];
                 }
                 else
                 {
                     NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                     alertViewController.title = @"Facebook Error";
                     alertViewController.message = @"Please make profile information public to login with facebook.";
                     
                     alertViewController.view.tintColor = [UIColor whiteColor];
                     alertViewController.backgroundTapDismissalGestureEnabled = YES;
                     alertViewController.swipeDismissalGestureEnabled = YES;
                     alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                     
                     alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                     alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                     alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                     alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                     
                     [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                         [alertViewController dismissViewControllerAnimated:YES completion:nil];
                     }]];
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [self presentViewController:alertViewController animated:YES completion:nil];
                     });
                 }
             }
         }];
    }
}

- (IBAction)btnJoinNowClicked:(id)sender
{
    [self.view endEditing:true];
    
    SignUpViewController *viewController = [[SignUpViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)navigateToHomeViewController
{
    [self.view endEditing:true];
    
    OurSponsorsViewController *viewController = [[OurSponsorsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
