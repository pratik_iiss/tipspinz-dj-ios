//
//  MyAdvertisementsViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 22/01/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface MyAdvertisementsViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewAdd;
@property (nonatomic,retain) IBOutlet UIButton *btnAdd;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;
@property (nonatomic,retain) IBOutlet UITableView *mainTableView;
@property (nonatomic,retain) IBOutlet UILabel *lblNoData;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *dataRows;

@end
