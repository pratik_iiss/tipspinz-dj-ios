//
//  HomeViewController.m
//  HungerE
//
//  Created by Pratik Gujarati on 23/09/16.
//  Copyright © 2016 accereteinfotech. All rights reserved.
//

#import "HomeViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "Club.h"

#import "ClubListTableViewCell.h"
#import "StatePickerTableViewCell.h"

@interface HomeViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation HomeViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewStateFilter;
@synthesize btnStateFilter;
@synthesize imageViewSearch;
@synthesize btnSearch;

@synthesize searchTextContainerView;
@synthesize txtSearch;
@synthesize imageViewCloseSearchTextContainerView;
@synthesize btnCloseSearchTextContainerView;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;
@synthesize mainTableView;
@synthesize lblNoData;
@synthesize btnAddClubs;

@synthesize statePickerContainerView;
@synthesize statePickerBlackTransparentView;
@synthesize statePickerInnerContainerView;
@synthesize lblStatePickerTitle;
@synthesize imageViewCloseStatePickerContainerView;
@synthesize btnCloseStatePickerContainerView;
@synthesize statePickerTableView;
@synthesize btnGoInStatePickerContainerView;
@synthesize btnSelectAllStatesInPickerContainerView;
@synthesize btnDeselectAllStatesInPickerContainerView;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllClubsEvent) name:@"gotAllClubsEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addedClubsEvent) name:@"addedClubsEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllClubsByStateIdEvent) name:@"gotAllClubsByStateIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllClubsEvent
{
    NSLog(@" [MySingleton sharedManager].dataManager.arrayAllClubs.count : %d",  [MySingleton sharedManager].dataManager.arrayAllClubs.count);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [MySingleton sharedManager].dataManager.arrayClubsByUserStateId = [[NSMutableArray alloc] init];
    
    for(Club *objClub in [MySingleton sharedManager].dataManager.arrayAllClubs)
    {
        if ([objClub.strClubStateID isEqualToString:[prefs objectForKey:@"stateid"]])
        {
            [[MySingleton sharedManager].dataManager.arrayClubsByUserStateId addObject:objClub];
        }
    }
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayClubsByUserStateId;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
    
    self.arraySelectedStatesIds = [[NSMutableArray alloc] init];
    [self.arraySelectedStatesIds addObject:[prefs objectForKey:@"stateid"]];
}

-(void)gotAllClubsByStateIdEvent
{
    self.dataRows = [MySingleton sharedManager].dataManager.arrayAllClubs;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
    
    self.arraySelectedStatesIds = [[NSMutableArray alloc] init];
    
//    for (int i = 0; i < [MySingleton sharedManager].dataManager.arrayStates.count; i++)
//    {
//        State *objState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:i];
//        
//        [self.arraySelectedStatesIds addObject:objState.strStateID];
//    }
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [self.arraySelectedStatesIds addObject:[prefs objectForKey:@"stateid"]];
}

-(void)addedClubsEvent
{
    [appDelegate showErrorAlertViewWithTitle:@"Request Sent" withDetails:@"Your request has been sent to club owner(s) successfully. We will add selected club(s) to your list when club owner(s) will approve your request"];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Add Clubs"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    
    imageViewStateFilter.layer.masksToBounds = YES;
    [btnStateFilter addTarget:self action:@selector(btnStateFilterClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    imageViewSearch.layer.masksToBounds = YES;
    [btnSearch addTarget:self action:@selector(btnSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *txtFieldFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
    }
    
    searchTextContainerView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    txtSearch.font = txtFieldFont;
    txtSearch.delegate = self;
    [txtSearch setValue:[MySingleton sharedManager].themeGlobalWhiteColor
             forKeyPath:@"placeholderLabel.textColor"];
    txtSearch.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.tintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.returnKeyType = UIReturnKeySearch;
    [txtSearch setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    imageViewCloseSearchTextContainerView.layer.masksToBounds = YES;
    [btnCloseSearchTextContainerView addTarget:self action:@selector(btnCloseSearchTextContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnStateFilterClicked:(id)sender
{
    [self.view endEditing:YES];
    
    statePickerContainerView.hidden = false;
    
    [MySingleton sharedManager].floatStatePickerTableViewWidth = statePickerTableView.frame.size.width;
    [statePickerTableView reloadData];
}

-(IBAction)btnSearchClicked:(id)sender
{
    [self.view endEditing:YES];
    
    searchTextContainerView.hidden = false;
    [txtSearch becomeFirstResponder];
}

-(void)btnCloseSearchTextContainerViewClicked
{
    [self.view endEditing:YES];
    
    txtSearch.text = @"";
    
    searchTextContainerView.hidden = true;
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayClubsByUserStateId;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor clearColor];
    mainTableView.hidden = true;
    
    UIFont *lblNoDataFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    lblNoData.font = lblNoDataFont;
    lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    btnAddClubs.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnAddClubs.layer.masksToBounds = true;
    btnAddClubs.titleLabel.font = btnFont;
    [btnAddClubs setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnAddClubs addTarget:self action:@selector(btnAddClubsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setUpStatePicker];
    
    self.arraySelectedClubIds = [[NSMutableArray alloc] init];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getAllClubs:[prefs objectForKey:@"userid"]];
}

#pragma mark - StatePicker Setup Method

-(void)setUpStatePicker
{
    UIFont *lblStatePickerTitleFont, *btnFont, *btnSelectAllFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblStatePickerTitleFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        btnSelectAllFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblStatePickerTitleFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        btnSelectAllFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
    }
    else
    {
        lblStatePickerTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        btnSelectAllFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    
    statePickerInnerContainerView.layer.masksToBounds = true;
    statePickerInnerContainerView.layer.cornerRadius = 10.0f;
    
    lblStatePickerTitle.font = lblStatePickerTitleFont;
    lblStatePickerTitle.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    imageViewCloseStatePickerContainerView.hidden = true;
    btnCloseStatePickerContainerView.hidden = true;
    imageViewCloseStatePickerContainerView.layer.masksToBounds = YES;
    [btnCloseStatePickerContainerView addTarget:self action:@selector(btnCloseStatePickerContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    statePickerTableView.delegate = self;
    statePickerTableView.dataSource = self;
    statePickerTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    statePickerTableView.backgroundColor = [UIColor clearColor];
    
    btnGoInStatePickerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnGoInStatePickerContainerView.layer.masksToBounds = true;
    btnGoInStatePickerContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnGoInStatePickerContainerView.titleLabel.font = btnFont;
    [btnGoInStatePickerContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnGoInStatePickerContainerView addTarget:self action:@selector(btnGoInStatePickerContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [btnSelectAllStatesInPickerContainerView setTitleColor:[MySingleton sharedManager].themeGlobalBlackColor forState:UIControlStateNormal];
    btnSelectAllStatesInPickerContainerView.titleLabel.font = btnSelectAllFont;
    [btnSelectAllStatesInPickerContainerView addTarget:self action:@selector(btnSelectAllStatesInPickerContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [btnDeselectAllStatesInPickerContainerView setTitleColor:[MySingleton sharedManager].themeGlobalBlackColor forState:UIControlStateNormal];
    btnDeselectAllStatesInPickerContainerView.titleLabel.font = btnSelectAllFont;
    [btnDeselectAllStatesInPickerContainerView addTarget:self action:@selector(btnDeselectAllStatesInPickerContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(void)btnCloseStatePickerContainerViewClicked
{
    [self.view endEditing:YES];
    
    statePickerContainerView.hidden = true;
}

-(void)btnGoInStatePickerContainerViewClicked
{
    [self.view endEditing:YES];
    
    statePickerContainerView.hidden = true;
    
//    if(self.arraySelectedStatesIds.count > 0)
//    {
        [MySingleton sharedManager].dataManager.arrayClubsByUserStateId = [[NSMutableArray alloc] init];
        
        for(Club *objClub in [MySingleton sharedManager].dataManager.arrayAllClubs)
        {
            if ([self.arraySelectedStatesIds containsObject:objClub.strClubStateID])
            {
                [[MySingleton sharedManager].dataManager.arrayClubsByUserStateId addObject:objClub];
            }
        }
    
        self.dataRows = [MySingleton sharedManager].dataManager.arrayClubsByUserStateId;
        
        if(self.dataRows.count <= 0)
        {
            mainTableView.hidden = true;
            lblNoData.hidden = false;
        }
        else
        {
            mainTableView.hidden = false;
            lblNoData.hidden = true;
            [mainTableView reloadData];
        }
//    }
//    else
//    {
//        [appDelegate showErrorAlertViewWithTitle:@"Select State" withDetails:@"Please select atleast one state."];
//    }
}

-(void)btnSelectAllStatesInPickerContainerViewClicked
{
    [self.view endEditing:YES];
    
    self.arraySelectedStatesIds = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [MySingleton sharedManager].dataManager.arrayStates.count; i++)
    {
        State *objState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:i];
        
        [self.arraySelectedStatesIds addObject:objState.strStateID];
    }
    
    [statePickerTableView reloadData];
}

-(void)btnDeselectAllStatesInPickerContainerViewClicked
{
    [self.view endEditing:YES];
    
    self.arraySelectedStatesIds = [[NSMutableArray alloc] init];
    [statePickerTableView reloadData];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    else if(tableView == statePickerTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    else if(tableView == statePickerTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    else if(tableView == statePickerTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    else if(tableView == statePickerTableView)
    {
        return [MySingleton sharedManager].dataManager.arrayStates.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 150;
    }
    else if(tableView == statePickerTableView)
    {
        return 60;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        ClubListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[ClubListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        Club *objClub = [self.dataRows objectAtIndex:indexPath.row];
        
        cell.lblClubName.text = objClub.strClubName;
        
        cell.lblClubType.text = objClub.strClubType;
        
        cell.lblClubAddress.text = objClub.strClubAddress;
        
//        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        cell.imageViewClub.imageURL = [NSURL URLWithString:objClub.strClubImageUrl];
        
        if([[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjApprovedClubIds containsObject:objClub.strClubID])
        {
            cell.imageViewCheckboxClubAdded.image = [UIImage imageNamed:@"checkbox_checked.png"];
        }
        else if ([self.arraySelectedClubIds containsObject:objClub.strClubID])
        {
            cell.imageViewCheckboxClubAdded.image = [UIImage imageNamed:@"checkbox_checked_club_added.png"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if(tableView == statePickerTableView)
    {
        StatePickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[StatePickerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        State *objState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:indexPath.row];
        
        cell.lblStateName.text = objState.strStateName;
        
        if([self.arraySelectedStatesIds containsObject:objState.strStateID])
        {
            cell.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_checked.png"];
        }
        else
        {
            cell.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        Club *objClub = [self.dataRows objectAtIndex:indexPath.row];
        
        if([[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjApprovedClubIds containsObject:objClub.strClubID])
        {
            
        }
        else if([self.arraySelectedClubIds containsObject:objClub.strClubID])
        {
            NSInteger indexOfObject = [self.arraySelectedClubIds indexOfObject:objClub.strClubID];
            
            if(indexOfObject != NSNotFound)
            {
                [self.arraySelectedClubIds removeObjectAtIndex:indexOfObject];
                [mainTableView reloadData];
            }
        }
        else
        {
            [self.arraySelectedClubIds addObject:objClub.strClubID];
            [mainTableView reloadData];
        }
    }
    else if(tableView == statePickerTableView)
    {
        State *objState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:indexPath.row];
        
        if([self.arraySelectedStatesIds containsObject:objState.strStateID])
        {
            NSInteger indexOfObject = [self.arraySelectedStatesIds indexOfObject:objState.strStateID];
            
            if(indexOfObject != NSNotFound)
            {
                [self.arraySelectedStatesIds removeObjectAtIndex:indexOfObject];
                [statePickerTableView reloadData];
            }
        }
        else
        {
            [self.arraySelectedStatesIds addObject:objState.strStateID];
            [statePickerTableView reloadData];
        }
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if(textField == txtSearch)
    {
//        NSLog(@"Search button clicked for txtSearch.");
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == txtSearch)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayClubsByUserStateId;
        
        if(self.dataRows.count <= 0)
        {
            mainTableView.hidden = true;
            lblNoData.hidden = false;
        }
        else
        {
            mainTableView.hidden = false;
            lblNoData.hidden = true;
            [mainTableView reloadData];
        }
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    [self searchClubsWithSubstring:substring];
    return YES;
}

#pragma mark - Other Methods

- (void)searchClubsWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        for(Club *objClub in [MySingleton sharedManager].dataManager.arrayClubsByUserStateId)
        {
            if (([[objClub.strClubName lowercaseString] containsString:[substring lowercaseString]]) || ([[objClub.strClubType lowercaseString] containsString:[substring lowercaseString]]) || ([[objClub.strClubAddress lowercaseString] containsString:[substring lowercaseString]]))
            {
                [self.dataRows addObject:objClub];
            }
        }
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayClubsByUserStateId;
    }
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

- (IBAction)btnAddClubsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
    
    NSString *strarraySelectedClubIds = [self.arraySelectedClubIds componentsJoinedByString:@","];
    [dictParameters setObject:strarraySelectedClubIds forKey:@"array_club_ids_for_approval"];
    
    [[MySingleton sharedManager].dataManager addClubs:dictParameters];
}

@end
