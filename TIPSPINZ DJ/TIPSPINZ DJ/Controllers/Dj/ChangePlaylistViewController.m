//
//  ChangePlaylistViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 20/01/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import "ChangePlaylistViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

@interface ChangePlaylistViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ChangePlaylistViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;

@synthesize lblPlaylistName;
@synthesize txtPlaylistName;
@synthesize imageViewPlaylistDownArrow;

@synthesize btnChangePlaylist;

//========== OTHER VARIABLES ==========//

@synthesize playlistPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotUserProfileByUserIdEvent) name:@"gotUserProfileByUserIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changedOnlinePlaylistEvent) name:@"changedOnlinePlaylistEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotUserProfileByUserIdEvent
{
    txtPlaylistName.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistName;
    
    NSInteger indexOfClubId = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjApprovedClubIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineClubId];
    
    if(NSNotFound != indexOfClubId)
    {
        self.objSelectedClub = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjApprovedClubs objectAtIndex:indexOfClubId];
    }
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineClubSetWithPresetPlaylist)
    {
        txtPlaylistName.userInteractionEnabled = false;
        txtPlaylistName.text = self.objSelectedClub.strClubPlaylistName;
        
        Playlist *objPlaylist = [[Playlist alloc] init];
        objPlaylist.strPlaylistID = [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistId;
        objPlaylist.strPlaylistName = [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistName;
        
        self.objSelectedPlaylist = objPlaylist;
    }
    else
    {
        NSMutableArray *arrayPlaylistIds = [[NSMutableArray alloc] init];
        
        for(int i = 0 ; i < [MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists.count; i++)
        {
            Playlist *objPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists objectAtIndex:i];
            
            [arrayPlaylistIds addObject:objPlaylist.strPlaylistID];
        }
        
        NSInteger indexOfPlaylistId = [arrayPlaylistIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistId];
        
        if(NSNotFound != indexOfPlaylistId)
        {
            self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists objectAtIndex:indexOfPlaylistId];
            [playlistPickerView selectRow:indexOfPlaylistId inComponent:0 animated:NO];
            [playlistPickerView reloadAllComponents];
        }
    }
}

-(void)changedOnlinePlaylistEvent
{
    NSMutableArray *arrayPlaylistIds = [[NSMutableArray alloc] init];
    
    for(int i = 0 ; i < [MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists.count; i++)
    {
        Playlist *objPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists objectAtIndex:i];
        
        [arrayPlaylistIds addObject:objPlaylist.strPlaylistID];
    }
    
    NSInteger indexOfPlaylistId = [arrayPlaylistIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistId];
    
    if(NSNotFound != indexOfPlaylistId)
    {
        self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists objectAtIndex:indexOfPlaylistId];
        [playlistPickerView selectRow:indexOfPlaylistId inComponent:0 animated:NO];
        [playlistPickerView reloadAllComponents];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Playlist Changed" withDetails:@"Your playlist has been changed successfully."];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Change Playlist"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 24)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    mainInnerScrollView.delegate = self;
    mainInnerScrollView.contentSize = CGSizeMake(mainInnerScrollView.frame.size.width, mainInnerScrollView.frame.size.height);
    
    UIFont *lblFont, *txtFieldFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    lblPlaylistName.font = lblFont;
    lblPlaylistName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    playlistPickerView = [[UIPickerView alloc] init];
    playlistPickerView.delegate = self;
    playlistPickerView.dataSource = self;
    playlistPickerView.showsSelectionIndicator = YES;
    playlistPickerView.tag = 1;
    playlistPickerView.backgroundColor = [UIColor whiteColor];
    
    UIView *txtPlaylistNamePaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtPlaylistName.frame.size.height)];
    txtPlaylistName.leftView = txtPlaylistNamePaddingView;
    txtPlaylistName.leftViewMode = UITextFieldViewModeAlways;
    txtPlaylistName.layer.masksToBounds = YES;
    txtPlaylistName.layer.cornerRadius = 5.0f;
    txtPlaylistName.layer.borderWidth = 1.0f;
    txtPlaylistName.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtPlaylistName.font = txtFieldFont;
    txtPlaylistName.delegate = self;
    [txtPlaylistName setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                   forKeyPath:@"placeholderLabel.textColor"];
    txtPlaylistName.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPlaylistName.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPlaylistName setInputView:playlistPickerView];
    [txtPlaylistName setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    imageViewPlaylistDownArrow.layer.masksToBounds = YES;
    imageViewPlaylistDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewPlaylistDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewPlaylistDownArrowTapped:)];
    imageViewPlaylistDownArrowTapGesture.delegate = self;
    [imageViewPlaylistDownArrow addGestureRecognizer:imageViewPlaylistDownArrowTapGesture];
    
    btnChangePlaylist.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnChangePlaylist.layer.masksToBounds = true;
    btnChangePlaylist.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnChangePlaylist.titleLabel.font = btnFont;
    [btnChangePlaylist setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnChangePlaylist addTarget:self action:@selector(btnChangePlaylistClicked) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getUserProfileByUserId:[prefs objectForKey:@"userid"]];
}

- (void)imageViewPlaylistDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtPlaylistName becomeFirstResponder];
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(txtPlaylistName.text.length == 0 )
    {
        self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists objectAtIndex:0];
        txtPlaylistName.text = self.objSelectedPlaylist.strPlaylistName;
        [playlistPickerView selectRow:0 inComponent:0 animated:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == playlistPickerView)
    {
        Playlist *objPlaylist = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists[row];
        lblMain.text = objPlaylist.strPlaylistName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists objectAtIndex:row];
        txtPlaylistName.text = self.objSelectedPlaylist.strPlaylistName;
    }
}

#pragma mark - Other Methods

- (void)btnChangePlaylistClicked
{
    [self.view endEditing:YES];

    if(self.objSelectedPlaylist != nil)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [[MySingleton sharedManager].dataManager changeOnlinePlaylist:[prefs objectForKey:@"userid"] withPlaylistId:self.objSelectedPlaylist.strPlaylistID];
    }
    else
    {
        if(self.objSelectedPlaylist == nil)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You must select a playlist"];
            });
        }
    }
}

@end

