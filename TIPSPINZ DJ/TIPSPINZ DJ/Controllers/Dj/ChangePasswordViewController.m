//
//  ChangePasswordViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 07/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

@interface ChangePasswordViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ChangePasswordViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;

@synthesize txtOldPassword;
@synthesize txtNewPassword;
@synthesize txtRepeatPassword;

@synthesize btnChangePassword;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changedPasswordEvent) name:@"changedPasswordEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)changedPasswordEvent
{
//    [appDelegate showErrorAlertViewWithTitle:@"Password Changed" withDetails:@"Your password has been changed successfully."];
    
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Your password has been changed successfully.";
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Change Password"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
        
    mainInnerScrollView.delegate = self;
    mainInnerScrollView.contentSize = CGSizeMake(mainInnerScrollView.frame.size.width, mainInnerScrollView.frame.size.height);
    
    UIFont *txtFieldFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    UIView *txtOldPasswordPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtOldPassword.frame.size.height)];
    txtOldPassword.leftView = txtOldPasswordPaddingView;
    txtOldPassword.leftViewMode = UITextFieldViewModeAlways;
    txtOldPassword.layer.masksToBounds = YES;
    txtOldPassword.layer.cornerRadius = 5.0f;
    txtOldPassword.layer.borderWidth = 1.0f;
    txtOldPassword.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtOldPassword.font = txtFieldFont;
    txtOldPassword.delegate = self;
    [txtOldPassword setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                forKeyPath:@"placeholderLabel.textColor"];
    txtOldPassword.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtOldPassword.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtOldPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtOldPassword.secureTextEntry = true;
    
    UIView *txtNewPasswordPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtNewPassword.frame.size.height)];
    txtNewPassword.leftView = txtNewPasswordPaddingView;
    txtNewPassword.leftViewMode = UITextFieldViewModeAlways;
    txtNewPassword.layer.masksToBounds = YES;
    txtNewPassword.layer.cornerRadius = 5.0f;
    txtNewPassword.layer.borderWidth = 1.0f;
    txtNewPassword.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtNewPassword.font = txtFieldFont;
    txtNewPassword.delegate = self;
    [txtNewPassword setValue:[MySingleton sharedManager].textfieldPlaceholderColor
              forKeyPath:@"placeholderLabel.textColor"];
    txtNewPassword.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtNewPassword.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtNewPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtNewPassword.secureTextEntry = true;
    
    UIView *txtRepeatPasswordPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtRepeatPassword.frame.size.height)];
    txtRepeatPassword.leftView = txtRepeatPasswordPaddingView;
    txtRepeatPassword.leftViewMode = UITextFieldViewModeAlways;
    txtRepeatPassword.layer.masksToBounds = YES;
    txtRepeatPassword.layer.cornerRadius = 5.0f;
    txtRepeatPassword.layer.borderWidth = 1.0f;
    txtRepeatPassword.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtRepeatPassword.font = txtFieldFont;
    txtRepeatPassword.delegate = self;
    [txtRepeatPassword setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                     forKeyPath:@"placeholderLabel.textColor"];
    txtRepeatPassword.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtRepeatPassword.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtRepeatPassword setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtRepeatPassword.secureTextEntry = true;
    
    btnChangePassword.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnChangePassword.layer.masksToBounds = true;
    btnChangePassword.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnChangePassword.titleLabel.font = btnFont;
    [btnChangePassword setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnChangePassword addTarget:self action:@selector(btnChangePasswordClicked) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - Other Methods

- (void)btnChangePasswordClicked
{
    [self.view endEditing:YES];
    
    if(txtOldPassword.text.length > 0 && txtNewPassword.text.length > 0 && txtRepeatPassword.text.length > 0 && [txtNewPassword.text isEqualToString:txtRepeatPassword.text])
    {
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
        [dictParameters setObject:txtOldPassword.text forKey:@"strOldPassword"];
        [dictParameters setObject:txtNewPassword.text forKey:@"strNewPassword"];
        [dictParameters setObject:txtRepeatPassword.text forKey:@"strRepeatPassword"];
        
        [[MySingleton sharedManager].dataManager changePassword:dictParameters];
    }
    else
    {
        if(txtOldPassword.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter old password"];
            });
        }
        else if(txtNewPassword.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter new password"];
            });
        }
        else if(txtRepeatPassword.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter repeat password"];
            });
        }
        else if(![txtNewPassword.text isEqualToString:txtRepeatPassword.text])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Password and repeat password must match."];
            });
        }
    }
}

@end
