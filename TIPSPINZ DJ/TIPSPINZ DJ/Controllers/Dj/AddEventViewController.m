//
//  AddEventViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 13/12/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "AddEventViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import <objc/runtime.h>

@interface AddEventViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation AddEventViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;

@synthesize txtEventName;
@synthesize txtEventLocation;
@synthesize txtEventDate;

@synthesize txtViewEventVenueAddressContainerView;
@synthesize txtViewEventVenueAddress;

@synthesize txtViewEventDescriptionContainerView;
@synthesize txtViewEventDescription;

@synthesize btnAdd;

//========== OTHER VARIABLES ==========//

@synthesize eventDatePicker;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addedDjEventEvent) name:@"addedDjEventEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)addedDjEventEvent
{
    txtEventName.text = @"";
    txtEventLocation.text = @"";
    txtEventDate.text = @"";
    
    txtViewEventVenueAddress.text = @"Event Venue Address";
    txtViewEventVenueAddress.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    
    txtViewEventDescription.text = @"Event Description";
    txtViewEventDescription.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Event Added" withDetails:@"Your event has been added successfully!!"];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Add Event"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    mainInnerScrollView.delegate = self;
    mainInnerScrollView.contentSize = CGSizeMake(mainInnerScrollView.frame.size.width, mainInnerScrollView.frame.size.height);
    
    UIFont *txtFieldFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    UIView *txtEventNamePaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtEventName.frame.size.height)];
    txtEventName.leftView = txtEventNamePaddingView;
    txtEventName.leftViewMode = UITextFieldViewModeAlways;
    txtEventName.layer.masksToBounds = YES;
    txtEventName.layer.cornerRadius = 5.0f;
    txtEventName.layer.borderWidth = 1.0f;
    txtEventName.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtEventName.font = txtFieldFont;
    txtEventName.delegate = self;
    
//    [txtEventName setValue:[MySingleton sharedManager].textfieldPlaceholderColor
//                forKeyPath:@"placeholderLabel.textColor"];
    
    Ivar ivar =  class_getInstanceVariable([UITextField class], "placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(txtEventName, ivar);
    placeholderLabel.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    
    txtEventName.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtEventName.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtEventName setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    UIView *txtEventLocationPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtEventLocation.frame.size.height)];
    txtEventLocation.leftView = txtEventLocationPaddingView;
    txtEventLocation.leftViewMode = UITextFieldViewModeAlways;
    txtEventLocation.layer.masksToBounds = YES;
    txtEventLocation.layer.cornerRadius = 5.0f;
    txtEventLocation.layer.borderWidth = 1.0f;
    txtEventLocation.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtEventLocation.font = txtFieldFont;
    txtEventLocation.delegate = self;
    
//    [txtEventLocation setValue:[MySingleton sharedManager].textfieldPlaceholderColor
//                    forKeyPath:@"placeholderLabel.textColor"];
    
    Ivar ivar1 =  class_getInstanceVariable([UITextField class], "placeholderLabel");
    UILabel *placeholderLabel1 = object_getIvar(txtEventLocation, ivar1);
    placeholderLabel1.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    
    txtEventLocation.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtEventLocation.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtEventLocation setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    eventDatePicker = [[UIDatePicker alloc] init];
    eventDatePicker.datePickerMode = UIDatePickerModeDate;
    [eventDatePicker setMinimumDate: [NSDate date]];
    eventDatePicker.backgroundColor = [UIColor whiteColor];
    [eventDatePicker addTarget:self action:@selector(eventDateSelected:) forControlEvents:UIControlEventValueChanged];
    
    UIView *txtEventDatePaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtEventDate.frame.size.height)];
    txtEventDate.leftView = txtEventDatePaddingView;
    txtEventDate.leftViewMode = UITextFieldViewModeAlways;
    txtEventDate.layer.masksToBounds = YES;
    txtEventDate.layer.cornerRadius = 5.0f;
    txtEventDate.layer.borderWidth = 1.0f;
    txtEventDate.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtEventDate.font = txtFieldFont;
    txtEventDate.delegate = self;
    
//    [txtEventDate setValue:[MySingleton sharedManager].textfieldPlaceholderColor
//                forKeyPath:@"placeholderLabel.textColor"];
    
    Ivar ivar2 =  class_getInstanceVariable([UITextField class], "placeholderLabel");
    UILabel *placeholderLabel2 = object_getIvar(txtEventDate, ivar2);
    placeholderLabel2.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    
    txtEventDate.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtEventDate.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtEventDate setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtEventDate setInputView:eventDatePicker];
    
    txtViewEventVenueAddressContainerView.layer.masksToBounds = YES;
    txtViewEventVenueAddressContainerView.layer.cornerRadius = 5.0f;
    txtViewEventVenueAddressContainerView.layer.borderWidth = 1.0f;
    txtViewEventVenueAddressContainerView.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    
    txtViewEventVenueAddress.text = @"Event Venue Address";
    txtViewEventVenueAddress.delegate = self;
    txtViewEventVenueAddress.font = txtFieldFont;
    txtViewEventVenueAddress.autocorrectionType = UITextAutocorrectionTypeNo;
    txtViewEventVenueAddress.backgroundColor = [UIColor clearColor];
    txtViewEventVenueAddress.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewEventVenueAddress.tintColor = [MySingleton sharedManager].textfieldTextColor;
    
    txtViewEventDescriptionContainerView.layer.masksToBounds = YES;
    txtViewEventDescriptionContainerView.layer.cornerRadius = 5.0f;
    txtViewEventDescriptionContainerView.layer.borderWidth = 1.0f;
    txtViewEventDescriptionContainerView.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    
    txtViewEventDescription.text = @"Event Description";
    txtViewEventDescription.delegate = self;
    txtViewEventDescription.font = txtFieldFont;
    txtViewEventDescription.autocorrectionType = UITextAutocorrectionTypeNo;
    txtViewEventDescription.backgroundColor = [UIColor clearColor];
    txtViewEventDescription.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtViewEventDescription.tintColor = [MySingleton sharedManager].textfieldTextColor;
    
    btnAdd.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnAdd.layer.masksToBounds = true;
    btnAdd.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnAdd.titleLabel.font = btnFont;
    [btnAdd setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnAdd addTarget:self action:@selector(btnAddClicked:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == txtEventDate)
    {
        if (txtEventDate.text.length <= 0)
        {
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
            [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
            dateFormat.dateStyle = NSDateFormatterMediumStyle;
            [dateFormat setDateFormat:@"dd-MM-yyyy"];
            NSString *strSelectedDate = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:eventDatePicker.date]];
            txtEventDate.text = strSelectedDate;
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == txtViewEventVenueAddress)
    {
        if ([textView.text isEqualToString:@"Event Venue Address"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
    else if(textView == txtViewEventDescription)
    {
        if ([textView.text isEqualToString:@"Event Description"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == txtViewEventVenueAddress)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Event Venue Address";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
    else if(textView == txtViewEventDescription)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Event Description";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
}

#pragma mark - Other Methods

-(void)eventDateSelected:(id)sender
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    dateFormat.dateStyle = NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    NSString *strSelectedDate = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:eventDatePicker.date]];
    txtEventDate.text = strSelectedDate;
}

-(IBAction)btnAddClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if(txtEventName.text.length > 0 && txtEventLocation.text.length > 0  && txtEventDate.text.length > 0 && txtViewEventVenueAddress.text.length > 0 && ![txtViewEventVenueAddress.text isEqualToString:@"Event Venue Address"] && txtViewEventDescription.text.length > 0 && ![txtViewEventDescription.text isEqualToString:@"Event Description"])
    {
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
       
        [dictParameters setObject:txtEventName.text forKey:@"event_name"];
        [dictParameters setObject:txtEventLocation.text forKey:@"event_location"];
        [dictParameters setObject:txtEventDate.text forKey:@"event_date"];
       
        [dictParameters setObject:txtViewEventVenueAddress.text forKey:@"event_venue_address"];
        [dictParameters setObject:txtViewEventDescription.text forKey:@"event_description"];
        
        [[MySingleton sharedManager].dataManager addEvent:dictParameters];
    }
    else
    {
        if(txtEventName.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter event name"];
            });
        }
        else if(txtEventLocation.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter event location"];
            });
        }
        else if(txtEventDate.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please select event date"];
            });
        }
        else if(txtViewEventVenueAddress.text.length <= 0 || [txtViewEventVenueAddress.text isEqualToString:@"Event Venue Address"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter event venue address"];
            });
        }
        else if(txtViewEventDescription.text.length <= 0 || [txtViewEventVenueAddress.text isEqualToString:@"Event Description"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter event description"];
            });
        }
    }
}

@end
