//
//  MyAcceptedRequestsViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "MyAcceptedRequestsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "AcceptedSongRequestTableViewCell.h"
#import "AcceptedShoutoutRequestTableViewCell.h"

@interface MyAcceptedRequestsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
    
    UIRefreshControl *refreshControl;
}

@end

@implementation MyAcceptedRequestsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewSearch;
@synthesize btnSearch;

@synthesize searchTextContainerView;
@synthesize txtSearch;
@synthesize imageViewCloseSearchTextContainerView;
@synthesize btnCloseSearchTextContainerView;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;
@synthesize mainTableView;

@synthesize songsRequestBottomContainerView;
@synthesize imageViewSongsRequest;
@synthesize lblSongsRequest;
@synthesize btnSongsRequest;

@synthesize shoutoutsRequestBottomContainerView;
@synthesize imageViewShoutoutsRequest;
@synthesize lblShoutoutsRequest;
@synthesize btnShoutoutsRequest;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllAcceptedRequestsByUserIdEvent) name:@"gotAllAcceptedRequestsByUserIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(markedSongRequestAsPlayedEvent) name:@"markedSongRequestAsPlayedEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(markedShoutoutRequestAsPlayedEvent) name:@"markedShoutoutRequestAsPlayedEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllAcceptedRequestsByUserIdEvent
{
    [refreshControl endRefreshing];
    
    if(self.boolIsLoadedForSongsRequests)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedSongsRequests;
        
        songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
        shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedShoutoutRequests;
        
        songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
        shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    }
    
    [mainTableView reloadData];
}

-(void)markedSongRequestAsPlayedEvent
{
    if(self.boolIsLoadedForSongsRequests)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedSongsRequests;
        
        songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
        shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedShoutoutRequests;
        
        songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
        shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    }
    
    [mainTableView reloadData];
}

-(void)markedShoutoutRequestAsPlayedEvent
{
    if(self.boolIsLoadedForSongsRequests)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedSongsRequests;
        
        songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
        shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedShoutoutRequests;
        
        songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
        shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    }
    
    [mainTableView reloadData];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Accepted Requests"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    
    imageViewSearch.layer.masksToBounds = YES;
    [btnSearch addTarget:self action:@selector(btnSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *txtFieldFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
    }
    
    searchTextContainerView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    txtSearch.font = txtFieldFont;
    txtSearch.delegate = self;
    [txtSearch setValue:[MySingleton sharedManager].themeGlobalWhiteColor
             forKeyPath:@"placeholderLabel.textColor"];
    txtSearch.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.tintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.returnKeyType = UIReturnKeySearch;
    [txtSearch setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    imageViewCloseSearchTextContainerView.layer.masksToBounds = YES;
    [btnCloseSearchTextContainerView addTarget:self action:@selector(btnCloseSearchTextContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnSearchClicked:(id)sender
{
    [self.view endEditing:YES];
    
    searchTextContainerView.hidden = false;
    [txtSearch becomeFirstResponder];
}

-(void)btnCloseSearchTextContainerViewClicked
{
    [self.view endEditing:YES];
    
    txtSearch.text = @"";
    
    searchTextContainerView.hidden = true;
    
    if(self.boolIsLoadedForSongsRequests)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedSongsRequests;
        
        songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
        shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedShoutoutRequests;
        
        songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
        shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    }
    
    [mainTableView reloadData];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull down to Refresh"];
    [refreshControl addTarget:self action:@selector(refreshTableView:) forControlEvents:UIControlEventValueChanged];
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor clearColor];
    mainTableView.allowsMultipleSelectionDuringEditing = NO;
    [mainTableView addSubview:refreshControl];
    
    UIFont *btnEditFont, *btnFont, *lblBottomTitleFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        lblBottomTitleFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        lblBottomTitleFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
    }
    else
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        lblBottomTitleFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
    }
    
//    if (self.boolIsLoadedForSongsRequests == nil)
//    {
//        self.boolIsLoadedForSongsRequests = true;
//    }
    
    songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    imageViewSongsRequest.layer.masksToBounds = YES;
    lblSongsRequest.font = lblBottomTitleFont;
    lblSongsRequest.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    [btnSongsRequest addTarget:self action:@selector(btnSongsRequestClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    imageViewShoutoutsRequest.layer.masksToBounds = YES;
    lblShoutoutsRequest.font = lblBottomTitleFont;
    lblShoutoutsRequest.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    [btnShoutoutsRequest addTarget:self action:@selector(btnShoutoutsRequestClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getAllAcceptedRequestsByUserId:[prefs objectForKey:@"userid"]];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            return self.dataRows.count;
        }
        else
        {
            return 1;
        }
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            if(self.boolIsLoadedForSongsRequests)
            {
                return 150;
            }
            else
            {
                ShoutoutRequest *objShoutoutRequest = [self.dataRows objectAtIndex:indexPath.row];
                
                if(objShoutoutRequest.strShoutoutRequestNoteComments.length > 0)
                {
                    UILabel *lblShoutoutNoteComments = [[UILabel alloc] initWithFrame:CGRectMake(75, 52, ([MySingleton sharedManager].screenWidth - 75), 24)];
                    lblShoutoutNoteComments.font = [MySingleton sharedManager].themeFontTenSizeRegular;
                    lblShoutoutNoteComments.numberOfLines = 0;
                    lblShoutoutNoteComments.text = objShoutoutRequest.strShoutoutRequestNoteComments;
                    [lblShoutoutNoteComments sizeToFit];
                    
                    CGRect lblShoutoutNoteCommentsTextRect = [lblShoutoutNoteComments.text boundingRectWithSize:lblShoutoutNoteComments.frame.size
                                                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                     attributes:@{NSFontAttributeName:lblShoutoutNoteComments.font}
                                                                                                        context:nil];
                    
                    CGSize lblShoutoutNoteCommentsSize = lblShoutoutNoteCommentsTextRect.size;
                    
                    CGFloat lblShoutoutNoteCommentsHeight = lblShoutoutNoteCommentsSize.height;
                    
                    return (lblShoutoutNoteComments.frame.origin.y + lblShoutoutNoteCommentsHeight + 70);
                }
                else
                {
                    UILabel *lblShoutoutNoteComments = [[UILabel alloc] initWithFrame:CGRectMake(75, 52, ([MySingleton sharedManager].screenWidth - 85), 24)];
                    
                    CGFloat lblShoutoutNoteCommentsHeight = 0;
                    
                    return (lblShoutoutNoteComments.frame.origin.y + lblShoutoutNoteCommentsHeight + 70);
                }
                
            }
        }
        else
        {
            return 44;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            UIFont *lblShareOnSocialMediaFont;
            
            if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
            {
                lblShareOnSocialMediaFont = [MySingleton sharedManager].themeFontSevenSizeBold;
            }
            else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
            {
                lblShareOnSocialMediaFont = [MySingleton sharedManager].themeFontNineSizeBold;
            }
            else
            {
                lblShareOnSocialMediaFont = [MySingleton sharedManager].themeFontTenSizeBold;
            }
            
            if(self.boolIsLoadedForSongsRequests)
            {
                AcceptedSongRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                
                SongRequest *objSongRequest = [self.dataRows objectAtIndex:indexPath.row];
                
                cell = [[AcceptedSongRequestTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                
                cell.lblSongName.text = objSongRequest.strSongName;
                cell.lblSongArtistName.text = objSongRequest.strSongArtistName;
                cell.lblSongRequestTime.text = objSongRequest.strSongRequestTime;
                cell.lblSongRequestStatus.text = [objSongRequest.strSongRequestStatus uppercaseString];
                
                if([objSongRequest.strSongRequestAcceptanceType isEqualToString:[MySingleton sharedManager].strDjSongRequestAcceptanceTypeWithinNext3Songs])
                {
                    cell.lblSongRequestAcceptanceType.text = @"THIS REQUEST IS TO BE PLAYED WITHIN NEXT 3 SONGS";
                }
                else if([objSongRequest.strSongRequestAcceptanceType isEqualToString:[MySingleton sharedManager].strDjSongRequestAcceptanceTypeAfter15Minutes])
                {
                    cell.lblSongRequestAcceptanceType.text = @"THIS REQUEST IS TO BE PLAYED AFTER 15 MINUTES";
                }
                
                if(objSongRequest.boolIsSongRequestVIP)
                {
                    cell.lblSongName.textColor = [MySingleton sharedManager].themeGlobalVIPRequestRedColor;
                }
                
                if([[objSongRequest.strSongRequestStatus lowercaseString] isEqualToString:@"paid"])
                {
                    cell.lblSongRequestStatus.textColor = [MySingleton sharedManager].themeGlobalRequestAcceptedGreenColor;
                }
                
                cell.lblShareOnSocialMedia.font = lblShareOnSocialMediaFont;
                cell.btnShareOnSocialMedia.tag = indexPath.row;
                [cell.btnShareOnSocialMedia addTarget:self action:@selector(btnShareOnSocialMediaClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.lblPlayed.font = lblShareOnSocialMediaFont;
                cell.btnPlayed.tag = indexPath.row;
                [cell.btnPlayed addTarget:self action:@selector(btnPlayedClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.contentView.userInteractionEnabled = false;
                
                return cell;
            }
            else
            {
                AcceptedShoutoutRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                
                ShoutoutRequest *objShoutoutRequest = [self.dataRows objectAtIndex:indexPath.row];
                
                cell = [[AcceptedShoutoutRequestTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                
                cell.lblShoutoutOccasion.text = objShoutoutRequest.strShoutoutRequestOccasion;
                cell.lblShoutoutForWhom.text = objShoutoutRequest.strShoutoutRequestForWhom;
                
                cell.lblShoutoutNoteComments.text = objShoutoutRequest.strShoutoutRequestNoteComments;
                [cell.lblShoutoutNoteComments sizeToFit];
                
                cell.lblShoutoutRequestTime.text = objShoutoutRequest.strShoutoutRequestTime;
                cell.lblShoutoutRequestStatus.text = [objShoutoutRequest.strShoutoutRequestStatus uppercaseString];
                
                if(objShoutoutRequest.boolIsShoutoutRequestVIP)
                {
                    cell.lblShoutoutOccasion.textColor = [MySingleton sharedManager].themeGlobalVIPRequestRedColor;
                }
                
                if([[objShoutoutRequest.strShoutoutRequestStatus lowercaseString] isEqualToString:@"paid"])
                {
                    cell.lblShoutoutRequestStatus.textColor = [MySingleton sharedManager].themeGlobalRequestAcceptedGreenColor;
                }
                
                cell.lblShareOnSocialMedia.font = lblShareOnSocialMediaFont;
                cell.btnShareOnSocialMedia.tag = indexPath.row;
                [cell.btnShareOnSocialMedia addTarget:self action:@selector(btnShareOnSocialMediaClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.lblPlayed.font = lblShareOnSocialMediaFont;
                cell.btnPlayed.tag = indexPath.row;
                [cell.btnPlayed addTarget:self action:@selector(btnPlayedClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                CGFloat lblShoutoutNoteCommentsHeight;
                
                if(objShoutoutRequest.strShoutoutRequestNoteComments.length > 0)
                {
                    CGRect lblShoutoutNoteCommentsTextRect = [cell.lblShoutoutNoteComments.text boundingRectWithSize:cell.lblShoutoutNoteComments.frame.size
                                                                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                          attributes:@{NSFontAttributeName:cell.lblShoutoutNoteComments.font}
                                                                                                             context:nil];
                    
                    CGSize lblShoutoutNoteCommentsSize = lblShoutoutNoteCommentsTextRect.size;
                    
                    lblShoutoutNoteCommentsHeight = lblShoutoutNoteCommentsSize.height;
                }
                else
                {
                    lblShoutoutNoteCommentsHeight = 0;
                }
                
                CGRect mainContainerFrame = cell.mainContainer.frame;
                mainContainerFrame.size.height = (cell.lblShoutoutNoteComments.frame.origin.y + lblShoutoutNoteCommentsHeight + 70);
                cell.mainContainer.frame = mainContainerFrame;
                
//                CGRect imageViewShoutoutIconFrame = cell.imageViewShoutoutIcon.frame;
//                imageViewShoutoutIconFrame.origin.y = (cell.mainContainer.frame.size.height - 50)/2;
//                cell.imageViewShoutoutIcon.frame = imageViewShoutoutIconFrame;
                
                CGRect lblShoutoutNoteCommentsFrame = cell.lblShoutoutNoteComments.frame;
                lblShoutoutNoteCommentsFrame.size.height = lblShoutoutNoteCommentsHeight;
                cell.lblShoutoutNoteComments.frame = lblShoutoutNoteCommentsFrame;
                
                CGRect lblShoutoutRequestTimeFrame = cell.lblShoutoutRequestTime.frame;
                if(objShoutoutRequest.strShoutoutRequestNoteComments.length > 0)
                {
                    lblShoutoutRequestTimeFrame.origin.y = cell.lblShoutoutNoteComments.frame.origin.y + lblShoutoutNoteCommentsHeight + 5;
                }
                else
                {
                    lblShoutoutRequestTimeFrame.origin.y = cell.lblShoutoutForWhom.frame.origin.y + cell.lblShoutoutForWhom.frame.size.height + 5;
                }
                cell.lblShoutoutRequestTime.frame = lblShoutoutRequestTimeFrame;
                
                CGRect lblShoutoutRequestStatusFrame = cell.lblShoutoutRequestStatus.frame;
                lblShoutoutRequestStatusFrame.origin.y = cell.lblShoutoutRequestTime.frame.origin.y;
                cell.lblShoutoutRequestStatus.frame = lblShoutoutRequestStatusFrame;
                
                CGRect bottomContainerFrame = cell.bottomContainer.frame;
                bottomContainerFrame.origin.y = cell.lblShoutoutRequestStatus.frame.origin.y + cell.lblShoutoutRequestStatus.frame.size.height + 10;
                cell.bottomContainer.frame = bottomContainerFrame;
                
                CGRect separatorViewFrame = cell.separatorView.frame;
                separatorViewFrame.origin.y = cell.mainContainer.frame.size.height - 1;
                cell.separatorView.frame = separatorViewFrame;
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.contentView.userInteractionEnabled = false;
                
                return cell;
            }
        }
        else
        {
            UIFont *lblNoDataFont;
            
            if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
            {
                lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            }
            else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
            {
                lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            }
            else
            {
                lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            }
            
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:MyIdentifier];
            
            UILabel *lblNoData = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mainTableView.frame.size.width, cell.frame.size.height)];
            lblNoData.textAlignment = NSTextAlignmentCenter;
            lblNoData.font = lblNoDataFont;
            lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
            lblNoData.text = @"No Requests found.";
            
            [cell.contentView addSubview:lblNoData];
            
            return cell;
        }
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            if(self.boolIsLoadedForSongsRequests)
            {
                SongRequest *objSongRequest = [self.dataRows objectAtIndex:indexPath.row];
            }
            else
            {
                ShoutoutRequest *objShoutoutRequest = [self.dataRows objectAtIndex:indexPath.row];
            }
        }
        else
        {
            
        }
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if(textField == txtSearch)
    {
//        NSLog(@"Search button clicked for txtSearch.");
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == txtSearch)
    {
        if(self.boolIsLoadedForSongsRequests)
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedSongsRequests;
            
            songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
            shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
        }
        else
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedShoutoutRequests;
            
            songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
            shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
        }
        
        [mainTableView reloadData];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    
    if(self.boolIsLoadedForSongsRequests)
    {
        [self searchSongRequestsWithSubstring:substring];
    }
    else
    {
        [self searchShoutoutRequestsWithSubstring:substring];
    }
    
    return YES;
}

#pragma mark - FBSDKShareKit Delegate Methods

- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results
{
    NSLog(@"completed share:%@", results);
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error
{
    NSLog(@"sharing error:%@", error);
    NSString *message = error.userInfo[FBSDKErrorLocalizedDescriptionKey] ?:
    @"There was a problem sharing, please try again later.";
    NSString *title = error.userInfo[FBSDKErrorLocalizedTitleKey] ?: @"Oops!";
    
    [[[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer
{
    NSLog(@"share cancelled");
}

- (FBSDKShareLinkContent *)getShareLinkContentWithContentURL:(NSURL *)objectURL
{
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = objectURL;
    return content;
}

- (FBSDKShareDialog *)getShareDialogWithContentURL:(NSURL *)objectURL
{
    FBSDKShareDialog *shareDialog = [[FBSDKShareDialog alloc] init];
    shareDialog.shareContent = [self getShareLinkContentWithContentURL:objectURL];
    return shareDialog;
}

- (FBSDKMessageDialog *)getMessageDialogWithContentURL:(NSURL *)objectURL
{
    FBSDKMessageDialog *shareDialog = [[FBSDKMessageDialog alloc] init];
    shareDialog.shareContent = [self getShareLinkContentWithContentURL:objectURL];
    return shareDialog;
}

#pragma mark - Share on Facebook (ShareKit) Method

-(void)shareSongRequestOnFacebook:(SongRequest *)objSongRequest
{
    [self.view endEditing:YES];
    
//    NSString *strWebpageLink = [jsonResult objectForKey:@"fbshare"];
//    strWebpageLink = [strWebpageLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [FBSDKShareDialog showFromViewController:self.parentViewController
                                 withContent:[self getShareLinkContentWithContentURL:[NSURL URLWithString:@"http://shareitexampleapp.parseapp.com/goofy/"]]
                                    delegate:self];
}

-(void)shareShoutoutRequestOnFacebook:(ShoutoutRequest *)objShoutoutRequest
{
    [self.view endEditing:YES];
    
//    NSString *strWebpageLink = [jsonResult objectForKey:@"fbshare"];
//    strWebpageLink = [strWebpageLink stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [FBSDKShareDialog showFromViewController:self.parentViewController
                                 withContent:[self getShareLinkContentWithContentURL:[NSURL URLWithString:@"http://shareitexampleapp.parseapp.com/goofy/"]]
                                    delegate:self];
}

#pragma mark - Share on Social Media (UIActivityViewController) Method

-(void)shareSongRequestOnSocialMediaWithActivityViewController:(SongRequest *)objSongRequest
{
    [self.view endEditing:YES];
    
    NSString *strShareMessage = [NSString stringWithFormat:@"I just played %@ for %@ at %@ using TIPSPINZ, join now and earn money.\n\nClick on the following link to download TIPSPINZ DJ app for iOS : %@\n\nClick on the following link to download TIPSPINZ for Android : %@", objSongRequest.strSongName, objSongRequest.strSongRequestUserName, objSongRequest.strSongRequestClubName, [MySingleton sharedManager].striOSAppUrlForDjsAndClubOwners, [MySingleton sharedManager].strAndroidAppUrlForDjsAndClubOwners];
    
//    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:[MySingleton sharedManager].strSocialMediaShareContent, nil] applicationActivities:nil];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:strShareMessage, nil] applicationActivities:nil];
    [activityVC setValue:[MySingleton sharedManager].strSocialMediaShareSubject forKey:@"subject"];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    [activityVC setCompletionHandler:^(NSString *act, BOOL done)
     {
         NSLog(@"act type %@",act);
         
         NSString *ServiceMsg = nil;
         if ( [act isEqualToString:UIActivityTypeMail] )
             ServiceMsg = @"Email Sent";
         if ( [act isEqualToString:UIActivityTypePostToTwitter] )
             ServiceMsg = @"Shared on Twitter!";
         if ( [act isEqualToString:UIActivityTypePostToFacebook] )
             ServiceMsg = @"Shared on Facebook!";
         
         if(done)
         {
             NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
             alertViewController.title = @"";
             alertViewController.message = @"You have shared this content on social media successfully.";
             alertViewController.view.tintColor = [UIColor whiteColor];
             alertViewController.backgroundTapDismissalGestureEnabled = YES;
             alertViewController.swipeDismissalGestureEnabled = YES;
             alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
             
             alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
             alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
             alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
             alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
             
             [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                 
                 [alertViewController dismissViewControllerAnimated:YES completion:nil];
                 
                 [self dismissViewControllerAnimated:YES completion:nil];
             }]];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self presentViewController:alertViewController animated:YES completion:nil];
             });
         }
         else
         {
             // didn't succeed.
         }
     }];
    [self presentViewController:activityVC animated:YES completion:nil];
}

-(void)shareShoutoutRequestOnSocialMediaWithActivityViewController:(ShoutoutRequest *)objShoutoutRequest
{
    [self.view endEditing:YES];
    
    NSString *strShareMessage = [NSString stringWithFormat:@"I just shouted for %@ at %@ using TIPSPINZ, join now and earn money.\n\nClick on the following link to download TIPSPINZ DJ app for iOS : %@\n\nClick on the following link to download TIPSPINZ for Android : %@", objShoutoutRequest.strShoutoutRequestUserName, objShoutoutRequest.strShoutoutRequestClubName, [MySingleton sharedManager].striOSAppUrlForDjsAndClubOwners, [MySingleton sharedManager].strAndroidAppUrlForDjsAndClubOwners];
    
//    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:[MySingleton sharedManager].strSocialMediaShareContent, nil] applicationActivities:nil];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:strShareMessage, nil] applicationActivities:nil];
    [activityVC setValue:[MySingleton sharedManager].strSocialMediaShareSubject forKey:@"subject"];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    [activityVC setCompletionHandler:^(NSString *act, BOOL done)
     {
         NSLog(@"act type %@",act);
         
         NSString *ServiceMsg = nil;
         if ( [act isEqualToString:UIActivityTypeMail] )
             ServiceMsg = @"Email Sent";
         if ( [act isEqualToString:UIActivityTypePostToTwitter] )
             ServiceMsg = @"Shared on Twitter!";
         if ( [act isEqualToString:UIActivityTypePostToFacebook] )
             ServiceMsg = @"Shared on Facebook!";
         
         if(done)
         {
             NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
             alertViewController.title = @"";
             alertViewController.message = @"You have shared this content on social media successfully.";
             alertViewController.view.tintColor = [UIColor whiteColor];
             alertViewController.backgroundTapDismissalGestureEnabled = YES;
             alertViewController.swipeDismissalGestureEnabled = YES;
             alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
             
             alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
             alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
             alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
             alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
             
             [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                 
                 [alertViewController dismissViewControllerAnimated:YES completion:nil];
                 
                 [self dismissViewControllerAnimated:YES completion:nil];
             }]];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self presentViewController:alertViewController animated:YES completion:nil];
             });
         }
         else
         {
             // didn't succeed.
         }
     }];
    [self presentViewController:activityVC animated:YES completion:nil];
}

#pragma mark - Other Methods

- (void)refreshTableView:(UIRefreshControl *)refreshControl
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getAllAcceptedRequestsByUserId:[prefs objectForKey:@"userid"]];
}

- (void)searchSongRequestsWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        for(SongRequest *objSongRequest in [MySingleton sharedManager].dataManager.arrayDjAcceptedSongsRequests)
        {
            if (([[objSongRequest.strSongName lowercaseString] containsString:[substring lowercaseString]]) || ([[objSongRequest.strSongArtistName lowercaseString] containsString:[substring lowercaseString]]))
            {
                [self.dataRows addObject:objSongRequest];
            }
        }
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedSongsRequests;
    }
    
    [mainTableView reloadData];
}

- (void)searchShoutoutRequestsWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        for(ShoutoutRequest *objShoutoutRequest in [MySingleton sharedManager].dataManager.arrayDjAcceptedShoutoutRequests)
        {
            if (([[objShoutoutRequest.strShoutoutRequestOccasion lowercaseString] containsString:[substring lowercaseString]]) || ([[objShoutoutRequest.strShoutoutRequestForWhom lowercaseString] containsString:[substring lowercaseString]]) || ([[objShoutoutRequest.strShoutoutRequestNoteComments lowercaseString] containsString:[substring lowercaseString]]))
            {
                [self.dataRows addObject:objShoutoutRequest];
            }
        }
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedShoutoutRequests;
    }
    
    [mainTableView reloadData];
}

- (IBAction)btnSongsRequestClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsLoadedForSongsRequests = true;
    
    songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedSongsRequests;
    
    [mainTableView reloadData];
}

- (IBAction)btnShoutoutsRequestClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsLoadedForSongsRequests = false;
    
    songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayDjAcceptedShoutoutRequests;
    
    [mainTableView reloadData];
}

- (IBAction)btnShareOnSocialMediaClicked:(id)sender
{
    UIButton *btnSender = (UIButton *)sender;
    
    if(self.boolIsLoadedForSongsRequests)
    {
        SongRequest *objSongRequest = [self.dataRows objectAtIndex:btnSender.tag];
        
//        [self shareSongRequestOnFacebook:objSongRequest];
        [self shareSongRequestOnSocialMediaWithActivityViewController:objSongRequest];
    }
    else
    {
        ShoutoutRequest *objShoutoutRequest = [self.dataRows objectAtIndex:btnSender.tag];
        
//        [self shareShoutoutRequestOnFacebook:objShoutoutRequest];
        [self shareShoutoutRequestOnSocialMediaWithActivityViewController:objShoutoutRequest];
    }
}

- (IBAction)btnPlayedClicked:(id)sender
{
    UIButton *btnSender = (UIButton *)sender;
    
    if(self.boolIsLoadedForSongsRequests)
    {
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"";
        alertViewController.message = @"Are you sure you want to mark this request as played?";
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = YES;
        alertViewController.swipeDismissalGestureEnabled = YES;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
            SongRequest *objSongRequest = [self.dataRows objectAtIndex:btnSender.tag];
            
            NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
            [dictParameters setObject:objSongRequest.strSongRequestID forKey:@"songrequest_id"];
            
            [[MySingleton sharedManager].dataManager markSongRequestAsPlayed:dictParameters];
            
        }]];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertViewController animated:YES completion:nil];
        });
    }
    else
    {
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"";
        alertViewController.message = @"Are you sure you want to mark this request as played?";
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = YES;
        alertViewController.swipeDismissalGestureEnabled = YES;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
            ShoutoutRequest *objShoutoutRequest = [self.dataRows objectAtIndex:btnSender.tag];
            
            NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
            [dictParameters setObject:objShoutoutRequest.strShoutoutRequestID forKey:@"shoutoutrequest_id"];
            
            [[MySingleton sharedManager].dataManager markShoutoutRequestAsPlayed:dictParameters];
            
        }]];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertViewController animated:YES completion:nil];
        });
    }
}

@end
