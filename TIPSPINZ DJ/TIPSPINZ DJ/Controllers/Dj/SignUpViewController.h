//
//  SignUpViewController.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 22/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#import "State.h"
#import "City.h"

@interface SignUpViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewMainLogo;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewEmail;
@property (nonatomic,retain) IBOutlet UITextField *txtEmail;
@property (nonatomic,retain) IBOutlet UIView *txtEmailBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewPassword;
@property (nonatomic,retain) IBOutlet UITextField *txtPassword;
@property (nonatomic,retain) IBOutlet UIView *txtPasswordBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewFullName;
@property (nonatomic,retain) IBOutlet UITextField *txtFullName;
@property (nonatomic,retain) IBOutlet UIView *txtFullNameBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewGender;
@property (nonatomic,retain) IBOutlet UITextField *txtGender;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewGenderDownArrow;
@property (nonatomic,retain) IBOutlet UIView *txtGenderBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewPhoneNumber;
@property (nonatomic,retain) IBOutlet UITextField *txtPhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *txtPhoneNumberBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewState;
@property (nonatomic,retain) IBOutlet UITextField *txtState;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewStateDownArrow;
@property (nonatomic,retain) IBOutlet UIView *txtStateBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewCity;
@property (nonatomic,retain) IBOutlet UITextField *txtCity;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCityDownArrow;
@property (nonatomic,retain) IBOutlet UIView *txtCityBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewTermsAndConditionsCheckbox;
@property (nonatomic,retain) IBOutlet UILabel *lblByCreatingAnAccount;

@property (nonatomic,retain) IBOutlet UIButton *btnSignUp;

@property (nonatomic,retain) IBOutlet UIButton *btnAlreadyAMember;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSString *strFacebookUserFullName;
@property (nonatomic,retain) NSString *strFacebookUserEmail;
@property (nonatomic,retain) NSString *strFacebookUserGender;

@property (nonatomic,retain) NSMutableArray *arrayGender;
@property (nonatomic,retain) UIPickerView *genderPickerView;

@property (nonatomic,retain) UIPickerView *statePickerView;

@property (nonatomic,retain) State *objSelectedState;

@property (nonatomic,retain) UIPickerView *cityPickerView;

@property (nonatomic,retain) City *objSelectedCity;

@property (nonatomic,assign) BOOL boolIsTermsAndConditionsChecked;

@end
