//
//  PlaylistSongsViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 10/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "PlaylistSongsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "Playlist.h"

#import "PlaylistSongTableViewCell.h"

#import "AddSongsViewController.h"

@interface PlaylistSongsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation PlaylistSongsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;
@synthesize imageViewSearch;
@synthesize btnSearch;

@synthesize searchTextContainerView;
@synthesize txtSearch;
@synthesize imageViewCloseSearchTextContainerView;
@synthesize btnCloseSearchTextContainerView;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;
@synthesize btnEdit;
@synthesize mainTableView;
@synthesize lblNoData;
@synthesize btnAddSongs;
@synthesize btnDelete;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    if ([MySingleton sharedManager].dataManager.boolIsSongsAddedIntoPlaylistSuccessfully)
    {
        [MySingleton sharedManager].dataManager.boolIsSongsAddedIntoPlaylistSuccessfully = false;
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [[MySingleton sharedManager].dataManager getAllPlaylistsByUserId:[prefs objectForKey:@"userid"]];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllPlaylistsByUserIdEvent) name:@"gotAllPlaylistsByUserIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletedSongByUserIdEvent) name:@"deletedSongByUserIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllPlaylistsByUserIdEvent
{
    for (int i = 0; i < [MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists.count; i++)
    {
        Playlist *objPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists objectAtIndex:i];
        
        if ([objPlaylist.strPlaylistID isEqualToString:self.objSelectedPlaylist.strPlaylistID])
        {
            self.dataRows = objPlaylist.arrayPlaylistSongs;
            
            if(self.dataRows.count <= 0)
            {
                btnEdit.hidden = true;
                mainTableView.hidden = true;
                lblNoData.hidden = false;
            }
            else
            {
                btnEdit.hidden = false;
                mainTableView.hidden = false;
                lblNoData.hidden = true;
                [mainTableView reloadData];
            }
        }
    }
}

-(void)deletedSongByUserIdEvent
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Song(s) has been deleted successfully.";
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        [MySingleton sharedManager].dataManager.boolIsSongsDeletedFromPlaylistSuccessfully = true;
        
        [self.navigationController popViewControllerAnimated:YES];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Songs"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    
    imageViewSearch.layer.masksToBounds = YES;
    [btnSearch addTarget:self action:@selector(btnSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *txtFieldFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
    }
    
    searchTextContainerView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    txtSearch.font = txtFieldFont;
    txtSearch.delegate = self;
    [txtSearch setValue:[MySingleton sharedManager].themeGlobalWhiteColor
             forKeyPath:@"placeholderLabel.textColor"];
    txtSearch.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.tintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.returnKeyType = UIReturnKeySearch;
    [txtSearch setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    imageViewCloseSearchTextContainerView.layer.masksToBounds = YES;
    [btnCloseSearchTextContainerView addTarget:self action:@selector(btnCloseSearchTextContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)btnSearchClicked:(id)sender
{
    [self.view endEditing:YES];
    
    searchTextContainerView.hidden = false;
    [txtSearch becomeFirstResponder];
}

-(void)btnCloseSearchTextContainerViewClicked
{
    [self.view endEditing:YES];
    
    txtSearch.text = @"";
    
    searchTextContainerView.hidden = true;
    
    self.dataRows = self.objSelectedPlaylist.arrayPlaylistSongs;
    
    if(self.dataRows.count <= 0)
    {
        btnEdit.hidden = true;
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        btnEdit.hidden = false;
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];
    [[IQKeyboardManager sharedManager] setEnable:false];
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor clearColor];
    mainTableView.allowsMultipleSelectionDuringEditing = NO;
    
    UIFont *btnEditFont, *lblNoDataFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    NSString *strBtnEditTitle = [NSString stringWithFormat:@"Edit Songslist"];
    NSMutableAttributedString *strAttrBtnEditTitle = [[NSMutableAttributedString alloc] initWithString:strBtnEditTitle attributes: nil];
    NSRange rangeOfEditSongslist = [strBtnEditTitle rangeOfString:@"Edit Songslist"];
    [strAttrBtnEditTitle addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:rangeOfEditSongslist];
    [strAttrBtnEditTitle addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:NSMakeRange(0, strBtnEditTitle.length)];
    [btnEdit setAttributedTitle:strAttrBtnEditTitle forState:UIControlStateNormal];
    btnEdit.titleLabel.font = btnEditFont;
    [btnEdit addTarget:self action:@selector(btnEditClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNoData.font = lblNoDataFont;
    lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    [btnAddSongs addTarget:self action:@selector(btnAddSongsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnDelete.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnDelete.layer.masksToBounds = true;
    btnDelete.titleLabel.font = btnFont;
    [btnDelete setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnDelete addTarget:self action:@selector(btnDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dataRows = self.objSelectedPlaylist.arrayPlaylistSongs;
    self.boolIsEditingModeOn = false;
    btnDelete.hidden = true;
    self.arraySelectedSongsIds = [[NSMutableArray alloc] init];
    
    if(self.dataRows.count <= 0)
    {
        btnEdit.hidden = true;
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        btnEdit.hidden = false;
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 80;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        PlaylistSongTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        Song *objSong = [self.dataRows objectAtIndex:indexPath.row];
        
        if(self.boolIsEditingModeOn)
        {
            cell = [[PlaylistSongTableViewCell alloc] initWithEditingStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
            
            if([self.arraySelectedSongsIds containsObject:objSong.strSongID])
            {
                cell.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_checked.png"];
            }
            else
            {
                cell.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
            }
        }
        else
        {
            cell = [[PlaylistSongTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        }
        
        cell.lblSongName.text = objSong.strSongName;
        cell.lblSongArtistName.text = objSong.strSongArtistName;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        Song *objSong = [self.dataRows objectAtIndex:indexPath.row];
        
        if(self.boolIsEditingModeOn)
        {
            if([self.arraySelectedSongsIds containsObject:objSong.strSongID])
            {
                NSInteger indexOfObject = [self.arraySelectedSongsIds indexOfObject:objSong.strSongID];
                
                if(indexOfObject != NSNotFound)
                {
                    [self.arraySelectedSongsIds removeObjectAtIndex:indexOfObject];
                    [mainTableView reloadData];
                }
            }
            else
            {
                [self.arraySelectedSongsIds addObject:objSong.strSongID];
                [mainTableView reloadData];
            }
        }
        else
        {
            
        }
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if(textField == txtSearch)
    {
//        NSLog(@"Search button clicked for txtSearch.");
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == txtSearch)
    {
        self.dataRows = self.objSelectedPlaylist.arrayPlaylistSongs;
        
        if(self.dataRows.count <= 0)
        {
            btnEdit.hidden = true;
            mainTableView.hidden = true;
            lblNoData.hidden = false;
        }
        else
        {
            btnEdit.hidden = false;
            mainTableView.hidden = false;
            lblNoData.hidden = true;
            [mainTableView reloadData];
        }
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    [self searchPlaylistSongsWithSubstring:substring];
    return YES;
}

#pragma mark - Other Methods

-(IBAction)btnEditClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if(self.boolIsEditingModeOn)
    {
        self.arraySelectedSongsIds = [[NSMutableArray alloc] init];
        self.boolIsEditingModeOn = false;
        btnDelete.hidden = true;
        
        CGRect mainTableViewFrame = mainTableView.frame;
        mainTableViewFrame.size.height = mainInnerScrollView.frame.size.height - mainTableView.frame.origin.y;
        mainTableView.frame = mainTableViewFrame;
    }
    else
    {
        self.boolIsEditingModeOn = true;
        btnDelete.hidden = false;
        
        CGRect mainTableViewFrame = mainTableView.frame;
        mainTableViewFrame.size.height = btnDelete.frame.origin.y - mainTableView.frame.origin.y;
        mainTableView.frame = mainTableViewFrame;
    }
    
    [mainTableView reloadData];
}

- (void)searchPlaylistSongsWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        for(Song *objSong in self.objSelectedPlaylist.arrayPlaylistSongs)
        {
            if (([[objSong.strSongName lowercaseString] containsString:[substring lowercaseString]]) || ([[objSong.strSongArtistName lowercaseString] containsString:[substring lowercaseString]]))
            {
                [self.dataRows addObject:objSong];
            }
        }
    }
    else
    {
        self.dataRows = self.objSelectedPlaylist.arrayPlaylistSongs;
    }
    
    if(self.dataRows.count <= 0)
    {
        btnEdit.hidden = true;
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        btnEdit.hidden = false;
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

- (IBAction)btnAddSongsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    AddSongsViewController *viewController = [[AddSongsViewController alloc] init];
    
    SideMenuViewController *sideMenuViewController;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

- (IBAction)btnDeleteClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if(self.arraySelectedSongsIds.count > 0)
    {
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"";
        alertViewController.message = @"Are you sure you want to delete?";
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = YES;
        alertViewController.swipeDismissalGestureEnabled = YES;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
            NSString *strArraySelectedSongsIds = [self.arraySelectedSongsIds componentsJoinedByString:@","];
            NSLog(@"strArraySelectedSongsIds : %@", strArraySelectedSongsIds);
            
            self.arraySelectedSongsIds = [[NSMutableArray alloc] init];
            self.boolIsEditingModeOn = false;
            btnDelete.hidden = true;
            
            CGRect mainTableViewFrame = mainTableView.frame;
            mainTableViewFrame.size.height = mainInnerScrollView.frame.size.height - mainTableView.frame.origin.y;
            mainTableView.frame = mainTableViewFrame;
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [[MySingleton sharedManager].dataManager deleteSongByUserId:[prefs objectForKey:@"userid"] withSongId:strArraySelectedSongsIds withPlaylistId:self.objSelectedPlaylist.strPlaylistID];
            
        }]];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertViewController animated:YES completion:nil];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
             [appDelegate showErrorAlertViewWithTitle:@"Select Songs" withDetails:@"Please select atleast one song to delete"];
        });
    }
}

@end
