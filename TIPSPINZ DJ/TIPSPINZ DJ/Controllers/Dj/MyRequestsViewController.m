//
//  MyRequestsViewController.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 11/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "MyRequestsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "UnacceptedSongRequestTableViewCell.h"
#import "UnacceptedShoutoutRequestTableViewCell.h"

#import "MyAcceptedRequestsViewController.h"

@interface MyRequestsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
    
    UIRefreshControl *refreshControl;
}

@end

@implementation MyRequestsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewSearch;
@synthesize btnSearch;

@synthesize searchTextContainerView;
@synthesize txtSearch;
@synthesize imageViewCloseSearchTextContainerView;
@synthesize btnCloseSearchTextContainerView;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;
@synthesize mainTableView;

@synthesize songsRequestBottomContainerView;
@synthesize imageViewSongsRequest;
@synthesize lblSongsRequest;
@synthesize btnSongsRequest;

@synthesize shoutoutsRequestBottomContainerView;
@synthesize imageViewShoutoutsRequest;
@synthesize lblShoutoutsRequest;
@synthesize btnShoutoutsRequest;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllUnacceptedRequestsByUserIdEvent) name:@"gotAllUnacceptedRequestsByUserIdEvent" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedSongRequestEvent) name:@"acceptedSongRequestEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(declinedSongRequestEvent) name:@"declinedSongRequestEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(acceptedShoutoutRequestEvent) name:@"acceptedShoutoutRequestEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(declinedShoutoutRequestEvent) name:@"declinedShoutoutRequestEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllUnacceptedRequestsByUserIdEvent
{
    [refreshControl endRefreshing];
    
    if(self.boolIsLoadedForSongsRequests)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjSongsRequests;
        
        songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
        shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjShoutoutRequests;
        
        songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
        shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    }
    
    [mainTableView reloadData];
}

-(void)acceptedSongRequestEvent
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = [MySingleton sharedManager].dataManager.strDjSongRequestAcceptanceTypeMessage;
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        if(self.boolIsLoadedForSongsRequests)
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayDjSongsRequests;
        }
        else
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayDjShoutoutRequests;
        }
        
        [mainTableView reloadData];
        
        if(self.boolIsLoadedForSongsRequests)
        {
            [self navigateToMyAcceptedRequestsScreenForSongs];
        }
        else
        {
            [self navigateToMyAcceptedRequestsScreenForShoutouts];
        }
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

-(void)declinedSongRequestEvent
{
    if(self.boolIsLoadedForSongsRequests)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjSongsRequests;
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjShoutoutRequests;
    }
    
    [mainTableView reloadData];
}

-(void)acceptedShoutoutRequestEvent
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"You have accepted this shoutout request";
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        if(self.boolIsLoadedForSongsRequests)
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayDjSongsRequests;
        }
        else
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayDjShoutoutRequests;
        }
        
        [mainTableView reloadData];
        
        if(self.boolIsLoadedForSongsRequests)
        {
            [self navigateToMyAcceptedRequestsScreenForSongs];
        }
        else
        {
            [self navigateToMyAcceptedRequestsScreenForShoutouts];
        }
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

-(void)declinedShoutoutRequestEvent
{
    if(self.boolIsLoadedForSongsRequests)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjSongsRequests;
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjShoutoutRequests;
    }
    
    [mainTableView reloadData];
}


#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"My Requests"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    
    imageViewSearch.layer.masksToBounds = YES;
    [btnSearch addTarget:self action:@selector(btnSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *txtFieldFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
    }
    
    searchTextContainerView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    txtSearch.font = txtFieldFont;
    txtSearch.delegate = self;
    [txtSearch setValue:[MySingleton sharedManager].themeGlobalWhiteColor
             forKeyPath:@"placeholderLabel.textColor"];
    txtSearch.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.tintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.returnKeyType = UIReturnKeySearch;
    [txtSearch setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    imageViewCloseSearchTextContainerView.layer.masksToBounds = YES;
    [btnCloseSearchTextContainerView addTarget:self action:@selector(btnCloseSearchTextContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnSearchClicked:(id)sender
{
    [self.view endEditing:YES];
    
    searchTextContainerView.hidden = false;
    [txtSearch becomeFirstResponder];
}

-(void)btnCloseSearchTextContainerViewClicked
{
    [self.view endEditing:YES];
    
    txtSearch.text = @"";
    
    searchTextContainerView.hidden = true;
    
    if(self.boolIsLoadedForSongsRequests)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjSongsRequests;
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjShoutoutRequests;
    }
    
    [mainTableView reloadData];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull down to Refresh"];
    [refreshControl addTarget:self action:@selector(refreshTableView:) forControlEvents:UIControlEventValueChanged];
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor clearColor];
    mainTableView.allowsMultipleSelectionDuringEditing = NO;
    [mainTableView addSubview:refreshControl];
    
    UIFont *btnEditFont, *btnFont, *lblBottomTitleFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        lblBottomTitleFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        lblBottomTitleFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
    }
    else
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        lblBottomTitleFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
    }
    
//    if (self.boolIsLoadedForSongsRequests == nil)
//    {
//        self.boolIsLoadedForSongsRequests = true;
//    }
    
    songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    imageViewSongsRequest.layer.masksToBounds = YES;
    lblSongsRequest.font = lblBottomTitleFont;
    lblSongsRequest.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    [btnSongsRequest addTarget:self action:@selector(btnSongsRequestClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    imageViewShoutoutsRequest.layer.masksToBounds = YES;
    lblShoutoutsRequest.font = lblBottomTitleFont;
    lblShoutoutsRequest.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    [btnShoutoutsRequest addTarget:self action:@selector(btnShoutoutsRequestClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getAllUnacceptedRequestsByUserId:[prefs objectForKey:@"userid"]];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            return self.dataRows.count;
        }
        else
        {
            return 1;
        }
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            if(self.boolIsLoadedForSongsRequests)
            {
                return 130;
            }
            else
            {
                ShoutoutRequest *objShoutoutRequest = [self.dataRows objectAtIndex:indexPath.row];
                
                if(objShoutoutRequest.strShoutoutRequestNoteComments.length > 0)
                {
                    UILabel *lblShoutoutNoteComments = [[UILabel alloc] initWithFrame:CGRectMake(75, 52, ([MySingleton sharedManager].screenWidth - 90), 24)];
                    lblShoutoutNoteComments.font = [MySingleton sharedManager].themeFontTenSizeRegular;
                    lblShoutoutNoteComments.numberOfLines = 0;
                    lblShoutoutNoteComments.text = objShoutoutRequest.strShoutoutRequestNoteComments;
                    [lblShoutoutNoteComments sizeToFit];
                    
                    CGRect lblShoutoutNoteCommentsTextRect = [lblShoutoutNoteComments.text boundingRectWithSize:lblShoutoutNoteComments.frame.size
                                                                                                        options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                     attributes:@{NSFontAttributeName:lblShoutoutNoteComments.font}
                                                                                                        context:nil];
                    
                    CGSize lblShoutoutNoteCommentsSize = lblShoutoutNoteCommentsTextRect.size;
                    
                    CGFloat lblShoutoutNoteCommentsHeight = lblShoutoutNoteCommentsSize.height;
                    
                    return (lblShoutoutNoteComments.frame.origin.y + lblShoutoutNoteCommentsHeight + 100);
                }
                else
                {
                    UILabel *lblShoutoutNoteComments = [[UILabel alloc] initWithFrame:CGRectMake(75, 52, ([MySingleton sharedManager].screenWidth - 90), 24)];
                    
                    CGFloat lblShoutoutNoteCommentsHeight = 0;
                    
                    return (lblShoutoutNoteComments.frame.origin.y + lblShoutoutNoteCommentsHeight + 100);
                }
                
            }
        }
        else
        {
            return 44;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            if(self.boolIsLoadedForSongsRequests)
            {
                UnacceptedSongRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                
                SongRequest *objSongRequest = [self.dataRows objectAtIndex:indexPath.row];
                
                cell = [[UnacceptedSongRequestTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                
                cell.lblSongName.text = objSongRequest.strSongName;
                cell.lblSongArtistName.text = objSongRequest.strSongArtistName;
                cell.lblSongRequestTime.text = objSongRequest.strSongRequestTime;
                
                cell.lblSongRequestTotalAmount.text = [NSString stringWithFormat:@"Total Amount : $%@", objSongRequest.strSongRequestAmount];
                cell.lblSongRequestDjAmount.text = [NSString stringWithFormat:@"DJ Amount : $%@", objSongRequest.strSongRequestDjAmount];
                
                cell.btnAccept.tag = indexPath.row;
                [cell.btnAccept addTarget:self action:@selector(btnAcceptClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.btnDecline.tag = indexPath.row;
                [cell.btnDecline addTarget:self action:@selector(btnDeclineClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                if(objSongRequest.boolIsSongRequestVIP)
                {
                    cell.lblSongName.textColor = [MySingleton sharedManager].themeGlobalVIPRequestRedColor;
                }
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.contentView.userInteractionEnabled = false;
                
                return cell;
            }
            else
            {
                UnacceptedShoutoutRequestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
                
                ShoutoutRequest *objShoutoutRequest = [self.dataRows objectAtIndex:indexPath.row];
                
                cell = [[UnacceptedShoutoutRequestTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
                
                cell.lblShoutoutOccasion.text = objShoutoutRequest.strShoutoutRequestOccasion;
                cell.lblShoutoutForWhom.text = objShoutoutRequest.strShoutoutRequestForWhom;
                
                cell.lblShoutoutNoteComments.text = objShoutoutRequest.strShoutoutRequestNoteComments;
                [cell.lblShoutoutNoteComments sizeToFit];
                
                cell.lblShoutoutRequestTime.text = objShoutoutRequest.strShoutoutRequestTime;
                
                cell.lblShoutoutRequestTotalAmount.text = [NSString stringWithFormat:@"Total Amount : $%@", objShoutoutRequest.strShoutoutRequestAmount];
                cell.lblShoutoutRequestDjAmount.text = [NSString stringWithFormat:@"DJ Amount : $%@", objShoutoutRequest.strShoutoutRequestDjAmount];
                
                cell.btnAccept.tag = indexPath.row;
                [cell.btnAccept addTarget:self action:@selector(btnAcceptClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.btnDecline.tag = indexPath.row;
                [cell.btnDecline addTarget:self action:@selector(btnDeclineClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                if(objShoutoutRequest.boolIsShoutoutRequestVIP)
                {
                    cell.lblShoutoutOccasion.textColor = [MySingleton sharedManager].themeGlobalVIPRequestRedColor;
                }
                
                CGFloat lblShoutoutNoteCommentsHeight;
                
                if(objShoutoutRequest.strShoutoutRequestNoteComments.length > 0)
                {
                    CGRect lblShoutoutNoteCommentsTextRect = [cell.lblShoutoutNoteComments.text boundingRectWithSize:cell.lblShoutoutNoteComments.frame.size
                                                                                                             options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                          attributes:@{NSFontAttributeName:cell.lblShoutoutNoteComments.font}
                                                                                                             context:nil];
                    
                    CGSize lblShoutoutNoteCommentsSize = lblShoutoutNoteCommentsTextRect.size;
                    
                    lblShoutoutNoteCommentsHeight = lblShoutoutNoteCommentsSize.height;
                }
                else
                {
                    lblShoutoutNoteCommentsHeight = 0;
                }
                
                CGRect mainContainerFrame = cell.mainContainer.frame;
                mainContainerFrame.size.height = (cell.lblShoutoutNoteComments.frame.origin.y + lblShoutoutNoteCommentsHeight + 100);
                cell.mainContainer.frame = mainContainerFrame;
                
                //            CGRect imageViewShoutoutIconFrame = cell.imageViewShoutoutIcon.frame;
                //            imageViewShoutoutIconFrame.origin.y = (cell.mainContainer.frame.size.height - 50)/2;
                //            cell.imageViewShoutoutIcon.frame = imageViewShoutoutIconFrame;
                
                CGRect lblShoutoutNoteCommentsFrame = cell.lblShoutoutNoteComments.frame;
                lblShoutoutNoteCommentsFrame.size.height = lblShoutoutNoteCommentsHeight;
                cell.lblShoutoutNoteComments.frame = lblShoutoutNoteCommentsFrame;
                
                CGRect lblShoutoutRequestTimeFrame = cell.lblShoutoutRequestTime.frame;
                if(objShoutoutRequest.strShoutoutRequestNoteComments.length > 0)
                {
                    lblShoutoutRequestTimeFrame.origin.y = cell.lblShoutoutNoteComments.frame.origin.y + lblShoutoutNoteCommentsHeight + 5;
                }
                else
                {
                    lblShoutoutRequestTimeFrame.origin.y = cell.lblShoutoutForWhom.frame.origin.y + cell.lblShoutoutForWhom.frame.size.height + 5;
                }
                cell.lblShoutoutRequestTime.frame = lblShoutoutRequestTimeFrame;
                
                
                CGRect lblShoutoutRequestTotalAmountFrame = cell.lblShoutoutRequestTotalAmount.frame;
                lblShoutoutRequestTotalAmountFrame.origin.y = cell.lblShoutoutRequestTime.frame.origin.y + cell.lblShoutoutRequestTime.frame.size.height + 5;
                cell.lblShoutoutRequestTotalAmount.frame = lblShoutoutRequestTotalAmountFrame;
                
                CGRect lblShoutoutRequestDjAmountFrame = cell.lblShoutoutRequestDjAmount.frame;
                lblShoutoutRequestDjAmountFrame.origin.y = cell.lblShoutoutRequestTime.frame.origin.y + cell.lblShoutoutRequestTime.frame.size.height + 5;
                cell.lblShoutoutRequestDjAmount.frame = lblShoutoutRequestDjAmountFrame;
                
                CGRect btnAcceptContainerViewFrame = cell.btnAcceptContainerView.frame;
                btnAcceptContainerViewFrame.origin.y = cell.lblShoutoutRequestDjAmount.frame.origin.y + cell.lblShoutoutRequestDjAmount.frame.size.height + 10;
                cell.btnAcceptContainerView.frame = btnAcceptContainerViewFrame;
                
                CGRect btnDeclineContainerViewFrame = cell.btnDeclineContainerView.frame;
                btnDeclineContainerViewFrame.origin.y = cell.lblShoutoutRequestDjAmount.frame.origin.y + cell.lblShoutoutRequestDjAmount.frame.size.height + 10;
                cell.btnDeclineContainerView.frame = btnDeclineContainerViewFrame;
                
                CGRect separatorViewFrame = cell.separatorView.frame;
                separatorViewFrame.origin.y = cell.mainContainer.frame.size.height - 1;
                cell.separatorView.frame = separatorViewFrame;
                
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                cell.contentView.userInteractionEnabled = false;
                
                return cell;
            }
        }
        else
        {
            UIFont *lblNoDataFont;
            
            if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
            {
                lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
            }
            else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
            {
                lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
            }
            else
            {
                lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
            }
            
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:MyIdentifier];
            
            UILabel *lblNoData = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mainTableView.frame.size.width, cell.frame.size.height)];
            lblNoData.textAlignment = NSTextAlignmentCenter;
            lblNoData.font = lblNoDataFont;
            lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
            lblNoData.text = @"No Requests found.";
            
            [cell.contentView addSubview:lblNoData];
            
            cell.contentView.userInteractionEnabled = false;
            
            return cell;
        }
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        if(self.dataRows.count > 0)
        {
            if(self.boolIsLoadedForSongsRequests)
            {
                SongRequest *objSongRequest = [self.dataRows objectAtIndex:indexPath.row];
            }
            else
            {
                ShoutoutRequest *objShoutoutRequest = [self.dataRows objectAtIndex:indexPath.row];
            }
        }
        else
        {
            
        }
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if(textField == txtSearch)
    {
//        NSLog(@"Search button clicked for txtSearch.");
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == txtSearch)
    {
        if(self.boolIsLoadedForSongsRequests)
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayDjSongsRequests;
        }
        else
        {
            self.dataRows = [MySingleton sharedManager].dataManager.arrayDjShoutoutRequests;
        }
        
        [mainTableView reloadData];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    
    if(self.boolIsLoadedForSongsRequests)
    {
        [self searchSongRequestsWithSubstring:substring];
    }
    else
    {
        [self searchShoutoutRequestsWithSubstring:substring];
    }
    
    return YES;
}

#pragma mark - Other Methods

- (void)refreshTableView:(UIRefreshControl *)refreshControl
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getAllUnacceptedRequestsByUserId:[prefs objectForKey:@"userid"]];
}

- (void)searchSongRequestsWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        for(SongRequest *objSongRequest in [MySingleton sharedManager].dataManager.arrayDjSongsRequests)
        {
            if (([[objSongRequest.strSongName lowercaseString] containsString:[substring lowercaseString]]) || ([[objSongRequest.strSongArtistName lowercaseString] containsString:[substring lowercaseString]]))
            {
                [self.dataRows addObject:objSongRequest];
            }
        }
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjSongsRequests;
    }
    
    [mainTableView reloadData];
}

- (void)searchShoutoutRequestsWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        for(ShoutoutRequest *objShoutoutRequest in [MySingleton sharedManager].dataManager.arrayDjShoutoutRequests)
        {
            if (([[objShoutoutRequest.strShoutoutRequestOccasion lowercaseString] containsString:[substring lowercaseString]]) || ([[objShoutoutRequest.strShoutoutRequestForWhom lowercaseString] containsString:[substring lowercaseString]]) || ([[objShoutoutRequest.strShoutoutRequestNoteComments lowercaseString] containsString:[substring lowercaseString]]))
            {
                [self.dataRows addObject:objShoutoutRequest];
            }
        }
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayDjShoutoutRequests;
    }
    
    [mainTableView reloadData];
}

- (IBAction)btnSongsRequestClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsLoadedForSongsRequests = true;
    
    songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayDjSongsRequests;
    
    [mainTableView reloadData];
}

- (IBAction)btnShoutoutsRequestClicked:(id)sender
{
    [self.view endEditing:YES];
    
    self.boolIsLoadedForSongsRequests = false;
    
    songsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
    shoutoutsRequestBottomContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayDjShoutoutRequests;
    
    [mainTableView reloadData];
}

- (IBAction)btnAcceptClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIButton *objButton = (UIButton *)sender;
    NSLog(@"objButton.tag : %d", objButton.tag);
    
//    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline)
//    {
        if(self.boolIsLoadedForSongsRequests)
        {
            SongRequest *objSongRequest = [self.dataRows objectAtIndex:objButton.tag];
            
            if(objSongRequest.boolIsSongRequestExpired)
            {
                [appDelegate showErrorAlertViewWithTitle:@"Request Expired" withDetails:@"You can not accept this request as this song request has been made in one of your previous session and is already expired."];
            }
            else
            {
                UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"When will you play?" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
                
                [actionSheet addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                    
                    // Dismiss button tappped.
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                }]];
                
                [actionSheet addAction:[UIAlertAction actionWithTitle:@"Within Next 3 Songs" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
                    [dictParameters setObject:objSongRequest.strSongRequestID forKey:@"songrequest_id"];
                    [dictParameters setObject:[MySingleton sharedManager].strDjSongRequestAcceptanceTypeWithinNext3Songs forKey:@"songrequest_acceptance_type"];
                    
                    [[MySingleton sharedManager].dataManager acceptSongRequest:dictParameters];
                    
                }]];
                
                [actionSheet addAction:[UIAlertAction actionWithTitle:@"After 15 minutes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                    
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
                    [dictParameters setObject:objSongRequest.strSongRequestID forKey:@"songrequest_id"];
                    [dictParameters setObject:[MySingleton sharedManager].strDjSongRequestAcceptanceTypeAfter15Minutes forKey:@"songrequest_acceptance_type"];
                    
                    [[MySingleton sharedManager].dataManager acceptSongRequest:dictParameters];
                    
                }]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:actionSheet animated:YES completion:nil];
                });
            }
        }
        else
        {
            ShoutoutRequest *objShoutoutRequest = [self.dataRows objectAtIndex:objButton.tag];
            
            if(objShoutoutRequest.boolIsShoutoutRequestExpired)
            {
                [appDelegate showErrorAlertViewWithTitle:@"Request Expired" withDetails:@"You can not accept this request as this shoutout request has been made in one of your previous session and is already expired."];
            }
            else
            {
                NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
                [dictParameters setObject:objShoutoutRequest.strShoutoutRequestID forKey:@"shoutoutrequest_id"];
                
                [[MySingleton sharedManager].dataManager acceptShoutoutRequest:dictParameters];
            }
        }
//    }
//    else
//    {
//        [appDelegate showErrorAlertViewWithTitle:@"Status Offline" withDetails:@"You can not accept this request as you are not online right now."];
//    }
}

- (IBAction)btnDeclineClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIButton *objButton = (UIButton *)sender;
    NSLog(@"objButton.tag : %d", objButton.tag);
    
//    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline)
//    {
        if(self.boolIsLoadedForSongsRequests)
        {
            SongRequest *objSongRequest = [self.dataRows objectAtIndex:objButton.tag];
            
            if(objSongRequest.boolIsSongRequestExpired)
            {
                [appDelegate showErrorAlertViewWithTitle:@"Request Expired" withDetails:@"You can not decline this request as this song request has been made in one of your previous session and is already expired."];
            }
            else
            {
                NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                alertViewController.title = @"";
                alertViewController.message = @"Are you sure you want to decline?";
                
                alertViewController.view.tintColor = [UIColor whiteColor];
                alertViewController.backgroundTapDismissalGestureEnabled = YES;
                alertViewController.swipeDismissalGestureEnabled = YES;
                alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                
                alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                    
                    [alertViewController dismissViewControllerAnimated:YES completion:nil];
                    
                    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
                    [dictParameters setObject:objSongRequest.strSongRequestID forKey:@"songrequest_id"];
                    
                    [[MySingleton sharedManager].dataManager declineSongRequest:dictParameters];
                    
                }]];
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
                    
                    [alertViewController dismissViewControllerAnimated:YES completion:nil];
                    
                }]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:alertViewController animated:YES completion:nil];
                });
            }
        }
        else
        {
            ShoutoutRequest *objShoutoutRequest = [self.dataRows objectAtIndex:objButton.tag];
            
            if(objShoutoutRequest.boolIsShoutoutRequestExpired)
            {
                [appDelegate showErrorAlertViewWithTitle:@"Request Expired" withDetails:@"You can not decline this request as this shoutout request has been made in one of your previous session and is already expired."];
            }
            else
            {
                NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                alertViewController.title = @"";
                alertViewController.message = @"Are you sure you want to decline?";
                
                alertViewController.view.tintColor = [UIColor whiteColor];
                alertViewController.backgroundTapDismissalGestureEnabled = YES;
                alertViewController.swipeDismissalGestureEnabled = YES;
                alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                
                alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                    
                    [alertViewController dismissViewControllerAnimated:YES completion:nil];
                    
                    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
                    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                    [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
                    [dictParameters setObject:objShoutoutRequest.strShoutoutRequestID forKey:@"shoutoutrequest_id"];
                    
                    [[MySingleton sharedManager].dataManager declineShoutoutRequest:dictParameters];
                    
                }]];
                
                [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
                    
                    [alertViewController dismissViewControllerAnimated:YES completion:nil];
                    
                }]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:alertViewController animated:YES completion:nil];
                });
            }
        }
//    }
//    else
//    {
//        [appDelegate showErrorAlertViewWithTitle:@"Status Offline" withDetails:@"You can not decline this request as you are not online right now."];
//    }
}

- (void)navigateToMyAcceptedRequestsScreenForSongs
{
    [self.view endEditing:true];
    
    MyAcceptedRequestsViewController *viewController = [[MyAcceptedRequestsViewController alloc] init];
    
    viewController.boolIsLoadedForSongsRequests = true;
    
    SideMenuViewController *sideMenuViewController;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

- (void)navigateToMyAcceptedRequestsScreenForShoutouts
{
    [self.view endEditing:true];
    
    MyAcceptedRequestsViewController *viewController = [[MyAcceptedRequestsViewController alloc] init];
    
    viewController.boolIsLoadedForSongsRequests = false;
    
    SideMenuViewController *sideMenuViewController;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone4" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6" bundle:nil];
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        sideMenuViewController = [[SideMenuViewController alloc] initWithNibName:@"SideMenuViewController_iPhone6Plus" bundle:nil];
    }
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    
    LGSideMenuController *sideMenuController = [LGSideMenuController sideMenuControllerWithRootViewController:navigationController
                                                                                           leftViewController:sideMenuViewController
                                                                                          rightViewController:nil];
    
    sideMenuController.leftViewWidth = [MySingleton sharedManager].floatLeftSideMenuWidth;
    sideMenuController.leftViewPresentationStyle = [MySingleton sharedManager].leftViewPresentationStyle;
    
    sideMenuController.rightViewWidth = [MySingleton sharedManager].floatRightSideMenuWidth;
    sideMenuController.rightViewPresentationStyle = [MySingleton sharedManager].rightViewPresentationStyle;
    
    [self.navigationController pushViewController:sideMenuController animated:YES];
}

@end
