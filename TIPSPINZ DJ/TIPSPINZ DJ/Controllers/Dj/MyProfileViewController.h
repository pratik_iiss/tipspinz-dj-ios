//
//  MyProfileViewController.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 04/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#import "State.h"
#import "City.h"

#import "Club.h"
#import "Playlist.h"

@interface MyProfileViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UITextViewDelegate, UIGestureRecognizerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet AsyncImageView *imageViewNavigationBarBackground;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewOptions;
@property (nonatomic,retain) IBOutlet UIButton *btnOptions;
@property (nonatomic,retain) IBOutlet UIView *profilePictureContainerView;
@property (nonatomic,retain) IBOutlet AsyncImageView *imageViewProfilePicture;
@property (nonatomic,retain) IBOutlet AsyncImageView *imageViewEditProfilePicture;
@property (nonatomic,retain) IBOutlet UIButton *btnEditProfilePicture;
@property (nonatomic,retain) IBOutlet UILabel *lblUserFullNameInNavigationBar;
@property (nonatomic,retain) IBOutlet UILabel *lblChangeStatusToInNavigationBar;
@property (nonatomic,retain) IBOutlet UIButton *btnOnlineInNavigationBar;
@property (nonatomic,retain) IBOutlet UIButton *btnOfflineInNavigationBar;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;

@property (nonatomic,retain) IBOutlet UILabel *lblEmail;
@property (nonatomic,retain) IBOutlet UITextField *txtEmail;
@property (nonatomic,retain) IBOutlet UIView *txtEmailBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblFullName;
@property (nonatomic,retain) IBOutlet UITextField *txtFullName;
@property (nonatomic,retain) IBOutlet UIView *txtFullNameBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblGender;
@property (nonatomic,retain) IBOutlet UITextField *txtGender;
@property (nonatomic,retain) IBOutlet UIView *txtGenderBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewGenderDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblPhoneNumber;
@property (nonatomic,retain) IBOutlet UITextField *txtPhoneNumber;
@property (nonatomic,retain) IBOutlet UIView *txtPhoneNumberBottomSeparatorView;

@property (nonatomic,retain) IBOutlet UILabel *lblState;
@property (nonatomic,retain) IBOutlet UITextField *txtState;
@property (nonatomic,retain) IBOutlet UIView *txtStateBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewStateDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblCity;
@property (nonatomic,retain) IBOutlet UITextField *txtCity;
@property (nonatomic,retain) IBOutlet UIView *txtCityBottomSeparatorView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCityDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblSocialMedia;
@property (nonatomic,retain) IBOutlet UIButton *btnFacebook;
@property (nonatomic,retain) IBOutlet UIButton *btnTwitter;
@property (nonatomic,retain) IBOutlet UIButton *btnInstagram;
@property (nonatomic,retain) IBOutlet UIButton *btnGooglePlus;

@property (nonatomic,retain) IBOutlet UIButton *btnUpdateProfile;
@property (nonatomic,retain) IBOutlet UIButton *btnConnectToStripe;


@property (nonatomic,retain) IBOutlet UIView *statusPickerContainerView;
@property (nonatomic,retain) IBOutlet UIView *statusPickerBlackTransparentView;
@property (nonatomic,retain) IBOutlet UIView *statusPickerInnerContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblStatusPickerTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseStatusPickerContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseStatusPickerContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblClub;
@property (nonatomic,retain) IBOutlet UIView *txtClubContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtClub;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewClubDownArrow;
@property (nonatomic,retain) IBOutlet UILabel *lblPlaylist;
@property (nonatomic,retain) IBOutlet UIView *txtPlaylistContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtPlaylist;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewPlaylistDownArrow;
@property (nonatomic,retain) IBOutlet UIButton *btnChangeInStatusPickerContainerView;

@property (nonatomic,retain) IBOutlet UIView *socialMediaPopupContainerView;
@property (nonatomic,retain) IBOutlet UIView *socialMediaPopupBlackTransparentView;
@property (nonatomic,retain) IBOutlet UIView *socialMediaPopupInnerContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblSocialMediaPopupContainerViewTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseSocialMediaPopupContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseSocialMediaPopupContainerView;
@property (nonatomic,retain) IBOutlet UITextView *txtSocialMediaPopup;
@property (nonatomic,retain) IBOutlet UILabel *lblSocialMediaPopupSuccessFailureMessage;
@property (nonatomic,retain) IBOutlet UIButton *btnSaveInSocialMediaPopupContainerView;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *arrayGender;
@property (nonatomic,retain) UIPickerView *genderPickerView;

@property (nonatomic,retain) NSMutableArray *arrayState;
@property (nonatomic,retain) UIPickerView *statePickerView;

@property (nonatomic,retain) State *objSelectedState;

@property (nonatomic,retain) NSMutableArray *arrayCity;
@property (nonatomic,retain) UIPickerView *cityPickerView;

@property (nonatomic,retain) City *objSelectedCity;

@property (nonatomic,retain) UIImage *imageSelectedProfilePicture;
@property (nonatomic,retain) NSData *imageSelectedProfilePictureData;
@property (nonatomic,retain) NSString *strImageSelectedProfilePictureBase64Data;

@property (nonatomic,retain) UIPickerView *clubPickerView;
@property (nonatomic,retain) Club *objSelectedClub;

@property (nonatomic,retain) UIPickerView *playlistPickerView;
@property (nonatomic,retain) Playlist *objSelectedPlaylist;

@property (nonatomic,retain) NSString *strSocialMediaPopupOpenedFor;

@property (nonatomic,assign) BOOL boolIsLoadedFromSideMenuToGoOnline;
@property (nonatomic,assign) BOOL boolIsLoadedFromSideMenuToGoOffline;

@property (nonatomic,assign) BOOL boolIsStatusPickerOpenedForChangePlaylist;

@end
