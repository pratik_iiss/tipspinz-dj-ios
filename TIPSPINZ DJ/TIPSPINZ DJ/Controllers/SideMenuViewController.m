//
//  SideMenuViewController.m
//  SetMyPace
//
//  Created by Pratik Gujarati on 28/01/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "SideMenuViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "SideBarTableViewHeader.h"
#import "SideBarTableViewCell.h"

//COMMON
#import "TermsAndConditionsViewController.h"
#import "LoginOptionsViewController.h"
#import "OurSponsorsForSideMenuViewController.h"

//CLUB OWNER
#import "ClubOwnerApprovedDjsListViewController.h"
#import "ClubOwnerOnlineDjListViewController.h"
#import "ClubOwnerDjApprovalRequestsListViewController.h"
#import "ClubOwnerDjOnlineRequestsListViewController.h"
#import "ClubOwnerDjLogsViewController.h"
#import "ClubOwnerAllClubsListViewController.h"
#import "ClubOwnerMyPlaylistViewController.h"
#import "ClubOwnerAddPlaylistViewController.h"
#import "ClubOwnerAddSongsViewController.h"

//DJ
#import "HomeViewController.h"
#import "MyProfileViewController.h"
#import "MyPlaylistViewController.h"
#import "AddPlaylistViewController.h"
#import "AddSongsViewController.h"
#import "MyRequestsViewController.h"
#import "MyAcceptedRequestsViewController.h"
#import "RemoveApprovedClubsViewController.h"
#import "MyEarningsViewController.h"
#import "SetTwitterInstagramOnlineMessageViewController.h"
#import "MyEventsViewController.h"
#import "AddEventViewController.h"
#import "ChangePlaylistViewController.h"
#import "MyAdvertisementsViewController.h"

//COMPANY
#import "CompanyApprovedArtistsListViewController.h"
#import "CompanyOnlineArtistListViewController.h"
#import "CompanyArtistApprovalRequestsListViewController.h"
#import "CompanyArtistOnlineRequestsListViewController.h"
#import "CompanyArtistLogsViewController.h"
#import "CompanyAllManagersListViewController.h"
#import "CompanyMyPlaylistViewController.h"
#import "CompanyAddPlaylistViewController.h"
#import "CompanyAddSongsViewController.h"

//ARTIST
#import "ArtistHomeViewController.h"
#import "ArtistMyProfileViewController.h"
#import "ArtistMyPlaylistViewController.h"
#import "AddArtistPlaylistViewController.h"
#import "AddArtistSongsViewController.h"
#import "ArtistMyRequestsViewController.h"
#import "ArtistMyAcceptedRequestsViewController.h"
#import "RemoveApprovedManagersViewController.h"
#import "ArtistMyEarningsViewController.h"
#import "ArtistMyEventsViewController.h"
#import "AddArtistEventViewController.h"
#import "ChangeArtistPlaylistViewController.h"
#import "ArtistMyAdvertisementsViewController.h"

//COMMON
#import "CommonShareLinkViewController.h"

@interface SideMenuViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation SideMenuViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;
@synthesize mainContainerView;

@synthesize mainImageViewBackground;
@synthesize blackTransparentView;
@synthesize mainTableViewContainerScrollView;
@synthesize mainTableView;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    CGSize mainTableViewViewContentSize = mainTableView.contentSize;
    
    if(mainTableViewViewContentSize.height > ([MySingleton sharedManager].screenHeight - 20))
    {
        NSLog(@"side menu table view is bigger");
        
        mainTableViewContainerScrollView.contentSize = CGSizeMake(mainTableViewContainerScrollView.frame.size.width, mainTableViewViewContentSize.height);
        
        CGRect mainTableViewFrame = mainTableView.frame;
        mainTableViewFrame.size.height = mainTableViewViewContentSize.height;
        mainTableView.frame = mainTableViewFrame;
    }
    
    if([MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl != nil && [MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl.length > 0)
    {
        NSURL *sideMenuBackgroundImageUrl;
        if ([[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl.lowercaseString hasPrefix:@"https://"]) {
            sideMenuBackgroundImageUrl = [NSURL URLWithString:[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl];
        } else {
            sideMenuBackgroundImageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl]];
        }
        
//        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        mainImageViewBackground.imageURL = sideMenuBackgroundImageUrl;
    }
    else
    {
//        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        mainImageViewBackground.image = [UIImage imageNamed:@"side_menu_background.png"];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        //DJ
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotUserProfileByUserIdEvent) name:@"gotUserProfileByUserIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotClubOwnerDjRequestListByUserIdEvent) name:@"gotClubOwnerDjRequestListByUserIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changedStatusEvent) name:@"changedStatusEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedUserProfileEvent) name:@"updatedUserProfileEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadedProfilePictureEvent) name:@"uploadedProfilePictureEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userLoggedoutEvent) name:@"userLoggedoutEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(djsOnlineStatusUpdatedEvent) name:@"djsOnlineStatusUpdatedEvent" object:nil];
        
        //ARTIST
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotUserProfileByUserIdEvent) name:@"gotArtistProfileByArtistIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotCompanyArtistRequestListByUserIdEvent) name:@"gotCompanyArtistRequestListByUserIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changedArtistStatusEvent) name:@"changedArtistStatusEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedArtistProfileEvent) name:@"updatedArtistProfileEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadedArtistProfilePictureEvent) name:@"uploadedArtistProfilePictureEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(artistsOnlineStatusUpdatedEvent) name:@"artistsOnlineStatusUpdatedEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotUserProfileByUserIdEvent
{
    if([MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl != nil && [MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl.length > 0)
    {
        NSURL *sideMenuBackgroundImageUrl;
        if ([[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl.lowercaseString hasPrefix:@"https://"]) {
            sideMenuBackgroundImageUrl = [NSURL URLWithString:[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl];
        } else {
            sideMenuBackgroundImageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl]];
        }
        
        mainImageViewBackground.imageURL = sideMenuBackgroundImageUrl;
    }
    else
    {
        mainImageViewBackground.image = [UIImage imageNamed:@"side_menu_background.png"];
    }
}

-(void)gotClubOwnerDjRequestListByUserIdEvent
{
    if([MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl != nil && [MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl.length > 0)
    {
        NSURL *sideMenuBackgroundImageUrl;
        if ([[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl.lowercaseString hasPrefix:@"https://"]) {
            sideMenuBackgroundImageUrl = [NSURL URLWithString:[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl];
        } else {
            sideMenuBackgroundImageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl]];
        }
        
//        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        mainImageViewBackground.imageURL = sideMenuBackgroundImageUrl;
    }
    else
    {
//        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        mainImageViewBackground.image = [UIImage imageNamed:@"side_menu_background.png"];
    }
}

-(void)changedStatusEvent
{
    [mainTableView reloadData];
}

-(void)updatedUserProfileEvent
{
    [mainTableView reloadData];
}

-(void)uploadedProfilePictureEvent
{
    [mainTableView reloadData];
}

-(void)userLoggedoutEvent
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [prefs removeObjectForKey:@"autologin"];
    [prefs removeObjectForKey:@"userid"];
    [prefs removeObjectForKey:@"useremail"];
    [prefs removeObjectForKey:@"userfullname"];
    [prefs removeObjectForKey:@"stateid"];
    [prefs removeObjectForKey:@"cityid"];
    [prefs removeObjectForKey:@"usertype"];
    [prefs synchronize];
    
    LoginOptionsViewController *centerViewController = [[LoginOptionsViewController alloc] init];
    [self.navigationController pushViewController:centerViewController animated:YES];
}

-(void)djsOnlineStatusUpdatedEvent
{
    [mainTableView reloadData];
}

-(void)gotCompanyArtistRequestListByUserIdEvent
{
    if([MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl != nil && [MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl.length > 0)
    {
        NSURL *sideMenuBackgroundImageUrl;
        if ([[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl.lowercaseString hasPrefix:@"https://"]) {
            sideMenuBackgroundImageUrl = [NSURL URLWithString:[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl];
        } else {
            sideMenuBackgroundImageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",[MySingleton sharedManager].dataManager.strSideMenuBackgroundImageUrl]];
        }
        
        //        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        mainImageViewBackground.imageURL = sideMenuBackgroundImageUrl;
    }
    else
    {
        //        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        mainImageViewBackground.image = [UIImage imageNamed:@"side_menu_background.png"];
    }
}

-(void)changedArtistStatusEvent
{
    [mainTableView reloadData];
}

-(void)updatedArtistProfileEvent
{
    [mainTableView reloadData];
}

-(void)uploadedArtistProfilePictureEvent
{
    [mainTableView reloadData];
}

-(void)artistsOnlineStatusUpdatedEvent
{
    [mainTableView reloadData];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    mainScrollView.delegate = self;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainImageViewBackground.layer.masksToBounds = true;
    mainImageViewBackground.contentMode = UIViewContentModeScaleAspectFill;
    
    blackTransparentView.alpha = 0.7f;
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor clearColor];
    mainTableView.scrollEnabled = false;
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        
        if([strUserType isEqualToString:@"0"])
        {
            //CLUB OWNER
            
            return 110;
        }
        else if([strUserType isEqualToString:@"1"])
        {
            //DJ
            
            return 130;
        }
        else if([strUserType isEqualToString:@"2"])
        {
            //COMPANY
            
            return 110;
        }
        else if([strUserType isEqualToString:@"3"])
        {
            //ARTIST
            
            return 130;
        }
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        
        SideBarTableViewHeader *sideBarTableViewHeader;
        
        if([strUserType isEqualToString:@"0"])
        {
            //CLUB OWNER
            
            sideBarTableViewHeader  = [[SideBarTableViewHeader alloc] initWithClubOwnerFrame:CGRectMake(0, 0, tableView.frame.size.width, 110)];
        }
        else if([strUserType isEqualToString:@"1"])
        {
            //DJ
            
            sideBarTableViewHeader  = [[SideBarTableViewHeader alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 130)];
        }
        else if([strUserType isEqualToString:@"2"])
        {
            //COMPANY
            
            sideBarTableViewHeader  = [[SideBarTableViewHeader alloc] initWithClubOwnerFrame:CGRectMake(0, 0, tableView.frame.size.width, 110)];
        }
        else if([strUserType isEqualToString:@"3"])
        {
            //ARTIST
            
            sideBarTableViewHeader  = [[SideBarTableViewHeader alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 130)];
        }
        
        NSString *strProfilePictureImageUrl = [prefs objectForKey:@"profilepictureimageurl"];
        
        if(strProfilePictureImageUrl.length > 0)
        {
            [[AsyncImageLoader sharedLoader].cache removeAllObjects];
            sideBarTableViewHeader.imageViewMain.imageURL = [NSURL URLWithString:strProfilePictureImageUrl];
        }
        else
        {
            [[AsyncImageLoader sharedLoader].cache removeAllObjects];
            sideBarTableViewHeader.imageViewMain.image = [UIImage imageNamed:@"user_profile_picture_avatar"];
        }
        
        sideBarTableViewHeader.lblMain.text = [NSString stringWithFormat:@"%@",[prefs objectForKey:@"userfullname"]];
        
        if([strUserType isEqualToString:@"1"])
        {
            //DJ
            
            if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline)
            {
                if ([MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineClubId != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineClubId.length > 0)
                {
                    sideBarTableViewHeader.lblChangeStatusTo.text = @"Club Status :";
                }
                else if ([MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId.length > 0)
                {
                    sideBarTableViewHeader.lblChangeStatusTo.text = @"Event Status :";
                }
                
                
                [sideBarTableViewHeader.btnOnline setSelected:true];
                [sideBarTableViewHeader.btnOffline setSelected:false];
            }
            else
            {
                [sideBarTableViewHeader.btnOnline setSelected:false];
                [sideBarTableViewHeader.btnOffline setSelected:true];
            }
            
            [sideBarTableViewHeader.btnOnline addTarget:self action:@selector(btnOnlineClicked:) forControlEvents:UIControlEventTouchUpInside];
            [sideBarTableViewHeader.btnOffline addTarget:self action:@selector(btnOfflineClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
        else if([strUserType isEqualToString:@"3"])
        {
            //ARTIST
            
            if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline)
            {
                if ([MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineManagerId != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineManagerId.length > 0)
                {
                    sideBarTableViewHeader.lblChangeStatusTo.text = @"Manager Status :";
                }
                else if ([MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId.length > 0)
                {
                    sideBarTableViewHeader.lblChangeStatusTo.text = @"Event Status :";
                }
                
                
                [sideBarTableViewHeader.btnOnline setSelected:true];
                [sideBarTableViewHeader.btnOffline setSelected:false];
            }
            else
            {
                [sideBarTableViewHeader.btnOnline setSelected:false];
                [sideBarTableViewHeader.btnOffline setSelected:true];
            }
            
            [sideBarTableViewHeader.btnOnline addTarget:self action:@selector(btnOnlineClicked:) forControlEvents:UIControlEventTouchUpInside];
            [sideBarTableViewHeader.btnOffline addTarget:self action:@selector(btnOfflineClicked:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        return sideBarTableViewHeader;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        
        if([strUserType isEqualToString:@"0"])
        {
            //CLUB OWNER
            
            return 9;
        }
        else if([strUserType isEqualToString:@"1"])
        {
            //DJ
            
             return 16;
        }
        else if([strUserType isEqualToString:@"2"])
        {
            //COMPANY
            
            return 9;
        }
        else if([strUserType isEqualToString:@"3"])
        {
            //ARTIST
            
            return 16;
        }
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 50;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        SideBarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[SideBarTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        
        if([strUserType isEqualToString:@"0"])
        {
            //CLUB OWNER
            
            if(indexPath.row == 0)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"dj_approval_requests.png"];
                cell.lblMain.text = @"Dj Approval Requests";
            }
            else if(indexPath.row == 1)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"approved_djs.png"];
                cell.lblMain.text = @"Approved Djs";
            }
            else if(indexPath.row == 2)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"online_dj.png"];
                cell.lblMain.text = @"Online Djs";
            }
            else if(indexPath.row == 3)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"dj_logs.png"];
                cell.lblMain.text = @"Dj Logs";
            }
            else if(indexPath.row == 4)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_clubs.png"];
                cell.lblMain.text = @"My Clubs";
            }
            else if(indexPath.row == 5)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_playlist.png"];
                cell.lblMain.text = @"My Playlist";
            }
            else if(indexPath.row == 6)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_playlist.png"];
                cell.lblMain.text = @"Add Playlist";
            }
            else if(indexPath.row == 7)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_songs.png"];
                cell.lblMain.text = @"Add Song";
            }
            else if(indexPath.row == 8)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"logout.png"];
                cell.lblMain.text = @"Logout";
            }
        }
        else if([strUserType isEqualToString:@"1"])
        {
            //DJ
            
            if(indexPath.row == 0)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_profile.png"];
                cell.lblMain.text = @"My Profile";
            }
            else if(indexPath.row == 1)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_requests.png"];
                cell.lblMain.text = @"My Requests";
            }
            else if(indexPath.row == 2)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"accepted_requests.png"];
                cell.lblMain.text = @"Accepted Requests";
            }
            else if(indexPath.row == 3)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_events.png"];
                cell.lblMain.text = @"My Events";
            }
            else if(indexPath.row == 4)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_event.png"];
                cell.lblMain.text = @"Add Event";
            }
            else if(indexPath.row == 5)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_club.png"];
                cell.lblMain.text = @"Add Clubs";
            }
            else if(indexPath.row == 6)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"remove_club.png"];
                cell.lblMain.text = @"Remove Clubs";
            }
            else if(indexPath.row == 7)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"sponsors_and_events.png"];
                cell.lblMain.text = @"Sponsors and Events";
            }
            else if(indexPath.row == 8)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_playlist.png"];
                cell.lblMain.text = @"My Playlist";
            }
            else if(indexPath.row == 9)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"change_playlist.png"];
                cell.lblMain.text = @"Change Playlist";
            }
            else if(indexPath.row == 10)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_playlist.png"];
                cell.lblMain.text = @"Add Playlist";
            }
            else if(indexPath.row == 11)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_songs.png"];
                cell.lblMain.text = @"Add Song";
            }
            else if(indexPath.row == 12)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_earnings.png"];
                cell.lblMain.text = @"Direct Donation Link";
            }
            else if(indexPath.row == 13)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_earnings.png"];
                cell.lblMain.text = @"My Earnings";
            }
            else if(indexPath.row == 14)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_advertisements.png"];
                cell.lblMain.text = @"My Advertisements";
            }
            else if(indexPath.row == 15)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"logout.png"];
                cell.lblMain.text = @"Logout";
            }
        }
        else if([strUserType isEqualToString:@"2"])
        {
            //COMPANY
            
            if(indexPath.row == 0)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"dj_approval_requests.png"];
                cell.lblMain.text = @"Artist Approval Requests";
            }
            else if(indexPath.row == 1)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"approved_djs.png"];
                cell.lblMain.text = @"Approved Artists";
            }
            else if(indexPath.row == 2)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"online_dj.png"];
                cell.lblMain.text = @"Online Artists";
            }
            else if(indexPath.row == 3)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"dj_logs.png"];
                cell.lblMain.text = @"Artist Logs";
            }
            else if(indexPath.row == 4)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_clubs.png"];
                cell.lblMain.text = @"My Managers";
            }
            else if(indexPath.row == 5)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_playlist.png"];
                cell.lblMain.text = @"My Playlist";
            }
            else if(indexPath.row == 6)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_playlist.png"];
                cell.lblMain.text = @"Add Playlist";
            }
            else if(indexPath.row == 7)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_songs.png"];
                cell.lblMain.text = @"Add Song";
            }
            else if(indexPath.row == 8)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"logout.png"];
                cell.lblMain.text = @"Logout";
            }
        }
        else if([strUserType isEqualToString:@"3"])
        {
            //ARTIST
            
            if(indexPath.row == 0)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_profile.png"];
                cell.lblMain.text = @"My Profile";
            }
            else if(indexPath.row == 1)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_requests.png"];
                cell.lblMain.text = @"My Requests";
            }
            else if(indexPath.row == 2)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"accepted_requests.png"];
                cell.lblMain.text = @"Accepted Requests";
            }
            else if(indexPath.row == 3)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_events.png"];
                cell.lblMain.text = @"My Events";
            }
            else if(indexPath.row == 4)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_event.png"];
                cell.lblMain.text = @"Add Event";
            }
            else if(indexPath.row == 5)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_club.png"];
                cell.lblMain.text = @"Add Managers";
            }
            else if(indexPath.row == 6)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"remove_club.png"];
                cell.lblMain.text = @"Remove Managers";
            }
            else if(indexPath.row == 7)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"sponsors_and_events.png"];
                cell.lblMain.text = @"Sponsors and Events";
            }
            else if(indexPath.row == 8)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_playlist.png"];
                cell.lblMain.text = @"My Playlist";
            }
            else if(indexPath.row == 9)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"change_playlist.png"];
                cell.lblMain.text = @"Change Playlist";
            }
            else if(indexPath.row == 10)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_playlist.png"];
                cell.lblMain.text = @"Add Playlist";
            }
            else if(indexPath.row == 11)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"add_songs.png"];
                cell.lblMain.text = @"Add Song";
            }
            else if(indexPath.row == 12)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_earnings.png"];
                cell.lblMain.text = @"Direct Donation Link";
            }
            else if(indexPath.row == 13)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_earnings.png"];
                cell.lblMain.text = @"My Earnings";
            }
            else if(indexPath.row == 14)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"my_advertisements.png"];
                cell.lblMain.text = @"My Advertisements";
            }
            else if(indexPath.row == 15)
            {
                cell.imageViewMain.image = [UIImage imageNamed:@"logout.png"];
                cell.lblMain.text = @"Logout";
            }
        }
                
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:true];
    
    if(tableView == mainTableView)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        
        if([strUserType isEqualToString:@"0"])
        {
            //CLUB OWNER
            
            if(indexPath.row == 0)
            {
                ClubOwnerDjApprovalRequestsListViewController *centerViewController = [[ClubOwnerDjApprovalRequestsListViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 1)
            {
                ClubOwnerApprovedDjsListViewController *centerViewController = [[ClubOwnerApprovedDjsListViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 2)
            {
                ClubOwnerOnlineDjListViewController *centerViewController = [[ClubOwnerOnlineDjListViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 3)
            {
                ClubOwnerDjLogsViewController *centerViewController = [[ClubOwnerDjLogsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 4)
            {
                ClubOwnerAllClubsListViewController *centerViewController = [[ClubOwnerAllClubsListViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 5)
            {
                ClubOwnerMyPlaylistViewController *centerViewController = [[ClubOwnerMyPlaylistViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 6)
            {
                ClubOwnerAddPlaylistViewController *centerViewController = [[ClubOwnerAddPlaylistViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 7)
            {
                ClubOwnerAddSongsViewController *centerViewController = [[ClubOwnerAddSongsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 8)
            {
                [self logout];
            }
        }
        else if([strUserType isEqualToString:@"1"])
        {
            //DJ
            
            if(indexPath.row == 0)
            {
                MyProfileViewController *centerViewController = [[MyProfileViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 1)
            {
                MyRequestsViewController *centerViewController = [[MyRequestsViewController alloc] init];
                centerViewController.boolIsLoadedForSongsRequests = true;
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 2)
            {
                MyAcceptedRequestsViewController *centerViewController = [[MyAcceptedRequestsViewController alloc] init];
                centerViewController.boolIsLoadedForSongsRequests = true;
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 3)
            {
                MyEventsViewController *centerViewController = [[MyEventsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 4)
            {
                AddEventViewController *centerViewController = [[AddEventViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 5)
            {
                HomeViewController *centerViewController = [[HomeViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 6)
            {
                RemoveApprovedClubsViewController *centerViewController = [[RemoveApprovedClubsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 7)
            {
                OurSponsorsForSideMenuViewController *centerViewController = [[OurSponsorsForSideMenuViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 8)
            {
                MyPlaylistViewController *centerViewController = [[MyPlaylistViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 9)
            {
                if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline == true)
                {
                    ChangePlaylistViewController *centerViewController = [[ChangePlaylistViewController alloc] init];
                    self.sideMenuController.rootViewController = centerViewController;
                    [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"Status Offline" withDetails:@"You are not online right now."];
                    });
                }
            }
            else if(indexPath.row == 10)
            {
                AddPlaylistViewController *centerViewController = [[AddPlaylistViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 11)
            {
                AddSongsViewController *centerViewController = [[AddSongsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 12)
            {
                CommonShareLinkViewController *centerViewController = [[CommonShareLinkViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 13)
            {
                MyEarningsViewController *centerViewController = [[MyEarningsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 14)
            {
                MyAdvertisementsViewController *centerViewController = [[MyAdvertisementsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 15)
            {
                [self logout];
            }
        }
        else if([strUserType isEqualToString:@"2"])
        {
            //COMPANY
            
            if(indexPath.row == 0)
            {
                CompanyArtistApprovalRequestsListViewController *centerViewController = [[CompanyArtistApprovalRequestsListViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 1)
            {
                CompanyApprovedArtistsListViewController *centerViewController = [[CompanyApprovedArtistsListViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 2)
            {
                CompanyOnlineArtistListViewController *centerViewController = [[CompanyOnlineArtistListViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 3)
            {
                CompanyArtistLogsViewController *centerViewController = [[CompanyArtistLogsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 4)
            {
                CompanyAllManagersListViewController *centerViewController = [[CompanyAllManagersListViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 5)
            {
                CompanyMyPlaylistViewController *centerViewController = [[CompanyMyPlaylistViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 6)
            {
                CompanyAddPlaylistViewController *centerViewController = [[CompanyAddPlaylistViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 7)
            {
                CompanyAddSongsViewController *centerViewController = [[CompanyAddSongsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 8)
            {
                [self logout];
            }
        }
        else if([strUserType isEqualToString:@"3"])
        {
            //ARTIST
            
            if(indexPath.row == 0)
            {
                ArtistMyProfileViewController *centerViewController = [[ArtistMyProfileViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 1)
            {
                ArtistMyRequestsViewController *centerViewController = [[ArtistMyRequestsViewController alloc] init];
                centerViewController.boolIsLoadedForSongsRequests = true;
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 2)
            {
                ArtistMyAcceptedRequestsViewController *centerViewController = [[ArtistMyAcceptedRequestsViewController alloc] init];
                centerViewController.boolIsLoadedForSongsRequests = true;
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 3)
            {
                ArtistMyEventsViewController *centerViewController = [[ArtistMyEventsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 4)
            {
                AddArtistEventViewController *centerViewController = [[AddArtistEventViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 5)
            {
                ArtistHomeViewController *centerViewController = [[ArtistHomeViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 6)
            {
                RemoveApprovedManagersViewController *centerViewController = [[RemoveApprovedManagersViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 7)
            {
                OurSponsorsForSideMenuViewController *centerViewController = [[OurSponsorsForSideMenuViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 8)
            {
                ArtistMyPlaylistViewController *centerViewController = [[ArtistMyPlaylistViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 9)
            {
                if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline == true)
                {
                    ChangeArtistPlaylistViewController *centerViewController = [[ChangeArtistPlaylistViewController alloc] init];
                    self.sideMenuController.rootViewController = centerViewController;
                    [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [appDelegate showErrorAlertViewWithTitle:@"Status Offline" withDetails:@"You are not online right now."];
                    });
                }
            }
            else if(indexPath.row == 10)
            {
                AddArtistPlaylistViewController *centerViewController = [[AddArtistPlaylistViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 11)
            {
                AddArtistSongsViewController *centerViewController = [[AddArtistSongsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 12)
            {
                CommonShareLinkViewController *centerViewController = [[CommonShareLinkViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 13)
            {
                ArtistMyEarningsViewController *centerViewController = [[ArtistMyEarningsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 14)
            {
                ArtistMyAdvertisementsViewController *centerViewController = [[ArtistMyAdvertisementsViewController alloc] init];
                self.sideMenuController.rootViewController = centerViewController;
                [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
            }
            else if(indexPath.row == 15)
            {
                [self logout];
            }
        }
    }
}

#pragma mark - Other Method

-(IBAction)btnOnlineClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if(![MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        
        if([strUserType isEqualToString:@"1"])
        {
            //DJ
            
            MyProfileViewController *centerViewController = [[MyProfileViewController alloc] init];
            centerViewController.boolIsLoadedFromSideMenuToGoOnline = true;
            centerViewController.boolIsLoadedFromSideMenuToGoOffline = false;
            self.sideMenuController.rootViewController = centerViewController;
            [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
        }
        else if([strUserType isEqualToString:@"3"])
        {
            //ARTIST
            
            ArtistMyProfileViewController *centerViewController = [[ArtistMyProfileViewController alloc] init];
            centerViewController.boolIsLoadedFromSideMenuToGoOnline = true;
            centerViewController.boolIsLoadedFromSideMenuToGoOffline = false;
            self.sideMenuController.rootViewController = centerViewController;
            [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
        }
    }
}

-(IBAction)btnOfflineClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline)
    {
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        
        if([strUserType isEqualToString:@"1"])
        {
            //DJ
            
            MyProfileViewController *centerViewController = [[MyProfileViewController alloc] init];
            centerViewController.boolIsLoadedFromSideMenuToGoOnline = false;
            centerViewController.boolIsLoadedFromSideMenuToGoOffline = true;
            self.sideMenuController.rootViewController = centerViewController;
            [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
        }
        else if([strUserType isEqualToString:@"3"])
        {
            //ARTIST
            
            ArtistMyProfileViewController *centerViewController = [[ArtistMyProfileViewController alloc] init];
            centerViewController.boolIsLoadedFromSideMenuToGoOnline = false;
            centerViewController.boolIsLoadedFromSideMenuToGoOffline = true;
            self.sideMenuController.rootViewController = centerViewController;
            [self.sideMenuController hideLeftViewAnimated:YES delay:0.0 completionHandler:nil];
        }
    }
}

- (void)logout
{
    NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
    alertViewController.title = @"";
    alertViewController.message = @"Are you sure you want to logout?";
    
    alertViewController.view.tintColor = [UIColor whiteColor];
    alertViewController.backgroundTapDismissalGestureEnabled = YES;
    alertViewController.swipeDismissalGestureEnabled = YES;
    alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
    
    alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
    alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
    alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
    alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserType = [prefs objectForKey:@"usertype"];
        
        if([strUserType isEqualToString:@"0"])
        {
            //CLUB OWNER
            
            [[MySingleton sharedManager].dataManager userLogout:[prefs objectForKey:@"userid"] withUserType:@"club_owner"];
        }
        else if([strUserType isEqualToString:@"1"])
        {
            //DJ
            
            [[MySingleton sharedManager].dataManager userLogout:[prefs objectForKey:@"userid"] withUserType:@"dj"];
        }
        else if([strUserType isEqualToString:@"2"])
        {
            //COMPANY
            
            [[MySingleton sharedManager].dataManager userLogout:[prefs objectForKey:@"userid"] withUserType:@"company"];
        }
        else if([strUserType isEqualToString:@"3"])
        {
            //ARTIST
            
            [[MySingleton sharedManager].dataManager userLogout:[prefs objectForKey:@"userid"] withUserType:@"artist"];
        }
    }]];
    
    [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
        
        [alertViewController dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertViewController animated:YES completion:nil];
    });
}

@end
