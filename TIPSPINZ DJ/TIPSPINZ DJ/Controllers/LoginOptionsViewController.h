//
//  LoginOptionsViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 21/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginOptionsViewController : UIViewController<UIScrollViewDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIImageView *mainBackgroundImageView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewMainLogo;

@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;

@property (nonatomic,retain) IBOutlet UIButton *btnClubOwner;
@property (nonatomic,retain) IBOutlet UIButton *btnDj;

@property (nonatomic,retain) IBOutlet UIButton *btnCompany;
@property (nonatomic,retain) IBOutlet UIButton *btnArtist;

//========== OTHER VARIABLES ==========//

@end
