//
//  SetTwitterInstagramOnlineMessageViewController.m
//  TIPSPINZ DJ
//
//  Created by INNOVATIVE ITERATION 4 on 06/10/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "SetTwitterInstagramOnlineMessageViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface SetTwitterInstagramOnlineMessageViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}
@end

@implementation SetTwitterInstagramOnlineMessageViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize lblInstructions;

@synthesize textViewMessageContainerView;
@synthesize textViewMessage;

@synthesize btnSetMessage;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.autoresizesSubviews = false;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainContainerView.frame.origin.y + mainContainerView.frame.size.height);
    
    mainScrollView.autoresizesSubviews = true;
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotUserProfileByUserIdEvent) name:@"gotUserProfileByUserIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setTwitterInstagramOnlineMessageEvent) name:@"setTwitterInstagramOnlineMessageEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotUserProfileByUserIdEvent
{
    if([MySingleton sharedManager].dataManager.objLoggedInUser.strTwitterInstagramMesasage.length > 0)
    {
        textViewMessage.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strTwitterInstagramMesasage;
        textViewMessage.textColor = [MySingleton sharedManager].textfieldTextColor;
    }
}

-(void)setTwitterInstagramOnlineMessageEvent
{
    [appDelegate showErrorAlertViewWithTitle:@"Message Set" withDetails:@"Your social media message is set successfully."];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Message for Social Media"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInstructionsFont, *textViewFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        textViewFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        textViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        textViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    lblInstructions.font = lblInstructionsFont;
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblInstructions.textAlignment = NSTextAlignmentCenter;
    
    textViewMessageContainerView.layer.masksToBounds = true;
    textViewMessageContainerView.layer.cornerRadius = 5;
    textViewMessageContainerView.layer.borderColor = [MySingleton sharedManager].themeGlobalLightGreyColor.CGColor;
    textViewMessageContainerView.layer.borderWidth = 1;
    
    textViewMessage.delegate = self;
//    textViewMessage.backgroundColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    textViewMessage.backgroundColor = [UIColor clearColor];
    textViewMessage.font = textViewFont;
    textViewMessage.text = @"Enter your message here";
    textViewMessage.textColor = [UIColor lightGrayColor];
    
    btnSetMessage.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnSetMessage.layer.masksToBounds = true;
    btnSetMessage.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSetMessage.titleLabel.font = btnFont;
    [btnSetMessage setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSetMessage addTarget:self action:@selector(btnSetMessageClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getUserProfileByUserId:[prefs objectForKey:@"userid"]];
}

#pragma mark - TEXT VIEW DELEGATE METHODS

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == textViewMessage)
    {
        if ([textView.text isEqualToString:@"Enter your message here"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].textfieldTextColor;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == textViewMessage)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Enter your message here";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
}

#pragma mark - Other Methods

- (IBAction)btnSetMessageClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if (textViewMessage.text.length > 0 || ![textViewMessage.text isEqualToString:@"Enter your message here"])
    {
        [[MySingleton sharedManager].dataManager setTwitterInstagramOnlineMessage:textViewMessage.text];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"Please enter message"];
        });
    }
    
    
////    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
////    {
//        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
//        [tweetSheet setInitialText:textViewMessage.text];
//        [tweetSheet addImage:[UIImage imageNamed:@"played.png"]];
//        [self presentViewController:tweetSheet animated:YES completion:nil];
////    }
    
//    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
//
//    [controller setInitialText:@"First post from my iPhone app"];
//    [self presentViewController:controller animated:YES completion:Nil];
//    [self shareOnTwitterWithMessage:textViewMessage.text];
}

- (void) shareOnTwitterWithMessage:(NSString *)message {
    
    ACAccountStore *twitterAccountStore = [[ACAccountStore alloc]init];
    ACAccountType *TWaccountType= [twitterAccountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];

    [twitterAccountStore requestAccessToAccountsWithType:TWaccountType options:nil completion:^(BOOL granted, NSError *e)
    {
         if (granted)
         {
             NSArray *accounts = [twitterAccountStore accountsWithAccountType:TWaccountType];
             
             ACAccount *twitterAccounts;
             twitterAccounts = [accounts lastObject];
             
             NSDictionary *dataDict = @{@"status": message};
             
             [self performSelectorInBackground:@selector(postToTwitter:) withObject:dataDict];
         }
         else
         {
             return ;
         }
     }];
}

- (void)postToTwitter:(NSDictionary *)dataDict{
    
    NSURL *requestURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update_with_media.json"];
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:requestURL parameters:dataDict];
    
//    NSData *imageData = UIImagePNGRepresentation([UIImage imageNamed:@"icon@2x.png"]);
//
//    [request addMultipartData:imageData
//                     withName:@"media[]"
//                         type:@"image/jpeg"
//                     filename:@"image.jpg"];
    
    ACAccount *twitterAccounts = [[ACAccount alloc]init];
    
    request.account = twitterAccounts;
    
    [request performRequestWithHandler:^(NSData *data, NSHTTPURLResponse *response, NSError *error) {
        
        if(!error){
            
            NSDictionary *list =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            if(![list objectForKey:@"errors"]){
                
                if([list objectForKey:@"error"]!=nil){
                    
                    //Delegate For Fail
                    return ;
                }
            }
        }
    }];
}

@end
