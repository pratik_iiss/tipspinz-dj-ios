//
//  OurSponsorsForSideMenuViewController.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 20/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#import "Advertisement.h"

@interface OurSponsorsForSideMenuViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UIScrollView *AdvertisementScrollView;

@property (nonatomic,retain) IBOutlet UIPageControl *mainPageControl;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) Advertisement *objTappedAdvertisement;

@end
