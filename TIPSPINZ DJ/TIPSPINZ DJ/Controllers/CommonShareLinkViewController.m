//
//  CommonShareLinkViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 05/06/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import "CommonShareLinkViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "MyRequestsViewController.h"
#import "ArtistMyRequestsViewController.h"

@interface CommonShareLinkViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation CommonShareLinkViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;

@synthesize lblInstructions;

@synthesize lblShareUrl;
@synthesize lblTapToCopy;

@synthesize btnShareLink;

//========== OTHER VARIABLES ==========//

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotShareLinkEvent) name:@"gotShareLinkEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotShareLinkEvent
{
    lblShareUrl.text = [MySingleton sharedManager].dataManager.strShareLink;
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Share"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInstructionsFont, *lblShareUrlFont, *lblTapToCopyFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblShareUrlFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblTapToCopyFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        lblShareUrlFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        lblTapToCopyFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        lblShareUrlFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        lblTapToCopyFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    lblInstructions.font = lblInstructionsFont;
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblInstructions.text = [NSString stringWithFormat:@"Now you can ask your fans for a tip as a DJ, an Artist, and even a Model.\nUse the link below to post below your work on any social media platform, website or blog.\nYou can share the link below by copying it or by clicking the Share Link button to share directly on social media."];
    
    lblShareUrl.font = lblShareUrlFont;
    lblShareUrl.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    lblShareUrl.layer.masksToBounds = true;
    lblShareUrl.layer.borderWidth = 1.0f;
    lblShareUrl.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    lblShareUrl.userInteractionEnabled = true;
    UITapGestureRecognizer *lblShareUrlTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lblShareUrlTapped:)];
    lblShareUrlTapRecognizer.delegate = self;
    [lblShareUrl addGestureRecognizer:lblShareUrlTapRecognizer];
    
    lblTapToCopy.font = lblTapToCopyFont;
    lblTapToCopy.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    btnShareLink.layer.masksToBounds = true;
    btnShareLink.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnShareLink.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnShareLink.titleLabel.font = btnFont;
    [btnShareLink setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnShareLink addTarget:self action:@selector(btnShareLinkClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [[MySingleton sharedManager].dataManager getShareLink];
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (void)lblShareUrlTapped:(UITapGestureRecognizer*)sender
{
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString:[MySingleton sharedManager].dataManager.strShareLink];
    
    NSString *strToastMessage = [NSString stringWithFormat:@"Copied to clipboard"];
    [self showToastMessage:strToastMessage];
}

- (IBAction)btnShareLinkClicked:(id)sender
{
    [self.view endEditing:true];
    
    NSString *textToShare = [NSString stringWithFormat:@"%@", [MySingleton sharedManager].dataManager.strShareLink];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:textToShare, nil] applicationActivities:nil];
    [activityVC setValue:[NSString stringWithFormat:@"Share URL"] forKey:@"subject"];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    [self presentViewController:activityVC animated:YES completion:nil];
}

-(void)showToastMessage:(NSString *)strMessage
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabelText = strMessage;
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    hud.userInteractionEnabled = NO;
    
    [hud hide:YES afterDelay:2];
}

@end
