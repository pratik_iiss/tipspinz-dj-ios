//
//  SetTwitterInstagramOnlineMessageViewController.h
//  TIPSPINZ DJ
//
//  Created by INNOVATIVE ITERATION 4 on 06/10/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuViewController.h"

@interface SetTwitterInstagramOnlineMessageViewController : UIViewController<UIScrollViewDelegate, UITextViewDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;

@property (nonatomic,retain) IBOutlet UIView *textViewMessageContainerView;
@property (nonatomic,retain) IBOutlet UITextView *textViewMessage;

@property (nonatomic,retain) IBOutlet UIButton *btnSetMessage;

//========== OTHER VARIABLES ==========//

@end
