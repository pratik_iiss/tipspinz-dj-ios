//
//  ArtistMyProfileViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 01/06/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import "ArtistMyProfileViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "ChangeArtistPasswordViewController.h"

#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface ArtistMyProfileViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ArtistMyProfileViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewNavigationBarBackground;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewOptions;
@synthesize btnOptions;
@synthesize profilePictureContainerView;
@synthesize imageViewProfilePicture;
@synthesize imageViewEditProfilePicture;
@synthesize btnEditProfilePicture;
@synthesize lblUserFullNameInNavigationBar;
@synthesize lblChangeStatusToInNavigationBar;
@synthesize btnOnlineInNavigationBar;
@synthesize btnOfflineInNavigationBar;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;

@synthesize lblEmail;
@synthesize txtEmail;
@synthesize txtEmailBottomSeparatorView;

@synthesize lblFullName;
@synthesize txtFullName;
@synthesize txtFullNameBottomSeparatorView;

@synthesize lblGender;
@synthesize txtGender;
@synthesize txtGenderBottomSeparatorView;
@synthesize imageViewGenderDownArrow;

@synthesize lblPhoneNumber;
@synthesize txtPhoneNumber;
@synthesize txtPhoneNumberBottomSeparatorView;

@synthesize lblState;
@synthesize txtState;
@synthesize txtStateBottomSeparatorView;
@synthesize imageViewStateDownArrow;

@synthesize lblCity;
@synthesize txtCity;
@synthesize txtCityBottomSeparatorView;
@synthesize imageViewCityDownArrow;

@synthesize lblSocialMedia;
@synthesize btnFacebook;
@synthesize btnTwitter;
@synthesize btnInstagram;
@synthesize btnGooglePlus;

@synthesize btnUpdateProfile;


@synthesize statusPickerContainerView;
@synthesize statusPickerBlackTransparentView;
@synthesize statusPickerInnerContainerView;
@synthesize lblStatusPickerTitle;
@synthesize imageViewCloseStatusPickerContainerView;
@synthesize btnCloseStatusPickerContainerView;
@synthesize lblClub;
@synthesize txtClubContainerView;
@synthesize txtClub;
@synthesize imageViewClubDownArrow;
@synthesize lblPlaylist;
@synthesize txtPlaylistContainerView;
@synthesize txtPlaylist;
@synthesize imageViewPlaylistDownArrow;
@synthesize btnChangeInStatusPickerContainerView;

@synthesize socialMediaPopupContainerView;
@synthesize socialMediaPopupBlackTransparentView;
@synthesize socialMediaPopupInnerContainerView;
@synthesize lblSocialMediaPopupContainerViewTitle;
@synthesize imageViewCloseSocialMediaPopupContainerView;
@synthesize btnCloseSocialMediaPopupContainerView;
@synthesize txtSocialMediaPopup;
@synthesize lblSocialMediaPopupSuccessFailureMessage;
@synthesize btnSaveInSocialMediaPopupContainerView;

//========== OTHER VARIABLES ==========//

@synthesize genderPickerView;

@synthesize statePickerView;

@synthesize cityPickerView;

@synthesize clubPickerView;

@synthesize playlistPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    if(self.boolIsLoadedFromSideMenuToGoOnline)
    {
        self.boolIsLoadedFromSideMenuToGoOnline = false;
        
        [self btnOnlineInNavigationBarClicked:self];
    }
    else if(self.boolIsLoadedFromSideMenuToGoOffline)
    {
        self.boolIsLoadedFromSideMenuToGoOffline = false;
        
        [self btnOfflineInNavigationBarClicked:self];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
        
    imageViewProfilePicture.layer.masksToBounds = YES;
    imageViewProfilePicture.layer.cornerRadius = imageViewProfilePicture.frame.size.width / 2;
    
    mainScrollView.autoresizesSubviews = false;
    
    NSLog(@"mainScrollView.frame.size.width : %f", mainScrollView.frame.size.width);
    NSLog(@"mainContainerView.frame.origin.y + mainContainerView.frame.size.height : %f", mainContainerView.frame.origin.y + mainContainerView.frame.size.height);
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainContainerView.frame.origin.y + mainContainerView.frame.size.height);
    
    mainScrollView.autoresizesSubviews = true;
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [txtSocialMediaPopup addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotArtistProfileByArtistIdEvent) name:@"gotArtistProfileByArtistIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedArtistProfileEvent) name:@"updatedArtistProfileEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadedArtistProfilePictureEvent) name:@"uploadedArtistProfilePictureEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changedStatusEvent) name:@"changedStatusEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(artistsOnlineStatusUpdatedEvent) name:@"artistsOnlineStatusUpdatedEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatedArtistSocialMediaProfileUrlEvent) name:@"updatedArtistSocialMediaProfileUrlEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changedArtistOnlinePlaylistEvent) name:@"changedArtistOnlinePlaylistEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [txtSocialMediaPopup removeObserver:self forKeyPath:@"contentSize"];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentSize"])
    {
        UITextView *tv = object;
        CGFloat deadSpace = ([tv bounds].size.height - [tv contentSize].height);
        CGFloat inset = MAX(0, deadSpace/2.0);
        tv.contentInset = UIEdgeInsetsMake(inset, tv.contentInset.left, inset, tv.contentInset.right);
    }
}

-(void)gotArtistProfileByArtistIdEvent
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"artistsOnlineStatusUpdatedEvent" object:nil];
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.strProfilePictureImageUrl.length > 0)
    {
        NSLog(@"[MySingleton sharedManager].dataManager.objLoggedInUser.strProfilePictureImageUrl : %@", [MySingleton sharedManager].dataManager.objLoggedInUser.strProfilePictureImageUrl);
        
        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        imageViewProfilePicture.imageURL = [NSURL URLWithString:[MySingleton sharedManager].dataManager.objLoggedInUser.strProfilePictureImageUrl];
    }
    else
    {
        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        imageViewProfilePicture.image = [UIImage imageNamed:@"user_profile_picture_avatar"];
    }
    
    lblUserFullNameInNavigationBar.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strFullname;
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline == false)
    {
        [btnOnlineInNavigationBar setSelected:false];
        [btnOfflineInNavigationBar setSelected:true];
    }
    else
    {
        [btnOnlineInNavigationBar setSelected:true];
        [btnOfflineInNavigationBar setSelected:false];
        
        txtClub.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineManagerName;
        txtPlaylist.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistName;
        
        NSInteger indexOfManagerId = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagerIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineManagerId];
        
        if(NSNotFound != indexOfManagerId)
        {
            self.objSelectedManager = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagers objectAtIndex:indexOfManagerId];
            [clubPickerView selectRow:indexOfManagerId inComponent:0 animated:NO];
            [clubPickerView reloadAllComponents];
        }
        
        if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineManagerSetWithPresetPlaylist)
        {
            txtPlaylist.userInteractionEnabled = false;
            txtPlaylist.text = self.objSelectedManager.strManagerPlaylistName;
            
            Playlist *objPlaylist = [[Playlist alloc] init];
            objPlaylist.strPlaylistID = [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistId;
            objPlaylist.strPlaylistName = [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistName;
            
            self.objSelectedPlaylist = objPlaylist;
        }
        else
        {
            NSMutableArray *arrayPlaylistIds = [[NSMutableArray alloc] init];
            
            for(int i = 0 ; i < [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists.count; i++)
            {
                Playlist *objPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists objectAtIndex:i];
                
                [arrayPlaylistIds addObject:objPlaylist.strPlaylistID];
            }
            
            NSInteger indexOfPlaylistId = [arrayPlaylistIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistId];
            
            if(NSNotFound != indexOfPlaylistId)
            {
                self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists objectAtIndex:indexOfPlaylistId];
                [playlistPickerView selectRow:indexOfPlaylistId inComponent:0 animated:NO];
                [playlistPickerView reloadAllComponents];
            }
        }
        
        if ([MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineManagerId != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineManagerId.length > 0)
        {
            lblChangeStatusToInNavigationBar.text = @"Manager Status :";
        }
        else if ([MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId.length > 0)
        {
            lblChangeStatusToInNavigationBar.text = @"Event Status :";
        }
    }
    
    txtEmail.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strEmail;
    txtFullName.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strFullname;
    txtGender.text = [[MySingleton sharedManager].dataManager.objLoggedInUser.strGender capitalizedString];
    txtPhoneNumber.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strPhoneNumber;
    txtState.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strStateName;
    txtCity.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strCityName;
    
    if([[txtGender.text lowercaseString] isEqualToString:@"male"])
    {
        [genderPickerView selectRow:0 inComponent:0 animated:NO];
    }
    else
    {
        [genderPickerView selectRow:1 inComponent:0 animated:NO];
    }
    
    NSMutableArray *arrayStateIds = [[NSMutableArray alloc] init];
    
    for(int i = 0; i < [MySingleton sharedManager].dataManager.arrayStates.count; i++)
    {
        State *objState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:i];
        [arrayStateIds addObject:objState.strStateID];
    }
    
    NSInteger indexOfStateId = [arrayStateIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strStateId];
    
    if(NSNotFound != indexOfStateId)
    {
        self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:indexOfStateId];
        [statePickerView selectRow:indexOfStateId inComponent:0 animated:NO];
        [cityPickerView reloadAllComponents];
        
        NSMutableArray *arrayCityIds = [[NSMutableArray alloc] init];
        
        for(int j = 0; j < self.objSelectedState.arrayCity.count; j++)
        {
            City *objCity = [self.objSelectedState.arrayCity objectAtIndex:j];
            [arrayCityIds addObject:objCity.strCityID];
        }
        
        NSInteger indexOfCityId = [arrayCityIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strCityId];
        
        if(NSNotFound != indexOfCityId)
        {
            self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:indexOfCityId];
            [cityPickerView selectRow:indexOfCityId inComponent:0 animated:NO];
        }
    }
    
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagers.count <= 0)
    {
        txtClub.userInteractionEnabled = false;
    }
    else
    {
        txtClub.userInteractionEnabled = true;
    }
    
    
    //    if([MySingleton sharedManager].dataManager.objLoggedInUser.arrayDjPlaylists.count <= 0)
    //    {
    //        txtPlaylist.userInteractionEnabled = false;
    //    }
    //    else
    //    {
    //        txtPlaylist.userInteractionEnabled = true;
    //    }
}

-(void)updatedArtistProfileEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Profile Updated" withDetails:@"Your profile information has been updated successfully."];
    });
}

-(void)uploadedArtistProfilePictureEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Profile Picture Updated" withDetails:@"Your profile picture has been changed successfully."];
    });
}

-(void)changedStatusEvent
{
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline == false)
    {
        lblChangeStatusToInNavigationBar.text = @"Change Status to:";
        
        [btnOnlineInNavigationBar setSelected:false];
        [btnOfflineInNavigationBar setSelected:true];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Status Changed" withDetails:@"Your status has been changed successfully."];
        });
    }
    else
    {
        if ([MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineManagerId != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineManagerId.length > 0)
        {
            lblChangeStatusToInNavigationBar.text = @"Manager Status :";
        }
        else if ([MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId.length > 0)
        {
            lblChangeStatusToInNavigationBar.text = @"Event Status :";
        }
        
        // DJ ONLINE
        
        if([MySingleton sharedManager].dataManager.objLoggedInUser == nil)
        {
            [MySingleton sharedManager].dataManager.objLoggedInUser = [[User alloc] init];
        }
        
        [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline = true;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"artistsOnlineStatusUpdatedEvent" object:nil];
    }
}

-(void)artistsOnlineStatusUpdatedEvent
{
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline == false)
    {
        [btnOnlineInNavigationBar setSelected:false];
        [btnOfflineInNavigationBar setSelected:true];
    }
    else
    {
        [btnOnlineInNavigationBar setSelected:true];
        [btnOfflineInNavigationBar setSelected:false];
    }
}

-(void)updatedArtistSocialMediaProfileUrlEvent
{
    [self.view endEditing:YES];
    
    socialMediaPopupContainerView.hidden = TRUE;
    
    lblSocialMediaPopupSuccessFailureMessage.text = @"";
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Profile Updated" withDetails:@"Your profile information has been updated successfully."];
    });
}

-(void)changedArtistOnlinePlaylistEvent
{
    [self.view endEditing:YES];
    
    NSMutableArray *arrayPlaylistIds = [[NSMutableArray alloc] init];
    
    for(int i = 0 ; i < [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists.count; i++)
    {
        Playlist *objPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists objectAtIndex:i];
        
        [arrayPlaylistIds addObject:objPlaylist.strPlaylistID];
    }
    
    NSInteger indexOfPlaylistId = [arrayPlaylistIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strOnlinePlaylistId];
    
    if(NSNotFound != indexOfPlaylistId)
    {
        self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists objectAtIndex:indexOfPlaylistId];
        [playlistPickerView selectRow:indexOfPlaylistId inComponent:0 animated:NO];
        [playlistPickerView reloadAllComponents];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Playlist Changed" withDetails:@"Your playlist has been changed successfully."];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"My Profile"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    
    imageViewOptions.layer.masksToBounds = YES;
    [btnOptions addTarget:self action:@selector(btnOptionsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *lblUserFullNameFont, *lblChangeStatusFont, *btnOnlineFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblUserFullNameFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblChangeStatusFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnOnlineFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblUserFullNameFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblChangeStatusFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnOnlineFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
    }
    else
    {
        lblUserFullNameFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblChangeStatusFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnOnlineFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
    }
    
    imageViewProfilePicture.layer.borderWidth = 2.0f;
    imageViewProfilePicture.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    imageViewProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
    
    imageViewEditProfilePicture.layer.masksToBounds = YES;
    
    lblUserFullNameInNavigationBar.font = lblUserFullNameFont;
    lblUserFullNameInNavigationBar.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    [btnEditProfilePicture addTarget:self action:@selector(btnEditProfilePictureClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblChangeStatusToInNavigationBar.font = lblChangeStatusFont;
    lblChangeStatusToInNavigationBar.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    [btnOnlineInNavigationBar setTitle:@"ONLINE" forState:UIControlStateNormal];
    btnOnlineInNavigationBar.titleLabel.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
    [btnOnlineInNavigationBar setTitleColor:[MySingleton sharedManager].themeGlobalOfflineGreyColor forState:UIControlStateNormal];
    [btnOnlineInNavigationBar setTitleColor:[MySingleton sharedManager].themeGlobalOnlineGreenColor forState:UIControlStateSelected];
    btnOnlineInNavigationBar.layer.masksToBounds = YES;
    btnOnlineInNavigationBar.adjustsImageWhenHighlighted = NO;
    [btnOnlineInNavigationBar addTarget:self action:@selector(btnOnlineInNavigationBarClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [btnOfflineInNavigationBar setTitle:@"OFFLINE" forState:UIControlStateNormal];
    btnOfflineInNavigationBar.titleLabel.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
    [btnOfflineInNavigationBar setTitleColor:[MySingleton sharedManager].themeGlobalOfflineGreyColor forState:UIControlStateNormal];
    [btnOfflineInNavigationBar setTitleColor:[MySingleton sharedManager].themeGlobalOfflineRedColor forState:UIControlStateSelected];
    btnOfflineInNavigationBar.layer.masksToBounds = YES;
    btnOfflineInNavigationBar.adjustsImageWhenHighlighted = NO;
    [btnOfflineInNavigationBar addTarget:self action:@selector(btnOfflineInNavigationBarClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline)
    {
        [btnOnlineInNavigationBar setSelected:true];
        [btnOfflineInNavigationBar setSelected:false];
    }
    else
    {
        [btnOnlineInNavigationBar setSelected:false];
        [btnOfflineInNavigationBar setSelected:true];
    }
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnOptionsClicked:(id)sender
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Options" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Dismiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Dismiss button tappped.
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Change Password" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        [self redirectToChangeArtistPasswordViewController];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:actionSheet animated:YES completion:nil];
    });
}

-(IBAction)btnEditProfilePictureClicked:(id)sender
{
    [self.view endEditing:YES];
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Take a Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Take a Photo
        [self dismissViewControllerAnimated:YES completion:nil];
        [self takeAPhoto];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Choose from Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Choose from Gallery
        [self dismissViewControllerAnimated:YES completion:nil];
        [self chooseFromGallery];
    }]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:actionSheet animated:YES completion:nil];
    });
}

-(IBAction)btnOnlineInNavigationBarClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if(![MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline)
    {
        self.boolIsStatusPickerOpenedForChangePlaylist = false;
        
        txtClub.textColor = [MySingleton sharedManager].textfieldTextColor;
        
        if([MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagers.count <= 0)
        {
            txtClub.userInteractionEnabled = false;
        }
        else
        {
            txtClub.userInteractionEnabled = true;
        }
        
        statusPickerContainerView.hidden = false;
    }
}

-(IBAction)btnOfflineInNavigationBarClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline)
    {
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"";
        alertViewController.message = @"Are you sure you want to go offline?";
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = YES;
        alertViewController.swipeDismissalGestureEnabled = YES;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
            if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnline)
            {
                NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
                [[MySingleton sharedManager].dataManager changeStatus:@"offline" withArtistId:[prefs objectForKey:@"userid"] withManagerId:@"" withPlaylistId:@""];
            }
        }]];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertViewController animated:YES completion:nil];
        });
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    mainInnerScrollView.delegate = self;
    mainInnerScrollView.contentSize = CGSizeMake(mainInnerScrollView.frame.size.width, mainInnerScrollView.frame.size.height);
    
    self.arrayGender = [[NSMutableArray alloc]initWithObjects:@"Male",@"Female",nil];
    
    UIFont *lblFont, *txtFieldFont, *btnFont, *lblSocialMediaPopupContainerViewTitleFont, *lblSocialMediaPopupSuccessFailureMessageFont, *btnSaveInSocialMediaPopupContainerViewFont;
    CGFloat socialMediaPopupContainerViewCornerRadius, btnSaveInSocialMediaPopupContainerViewCornerRadiusValue;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        btnSaveInSocialMediaPopupContainerViewFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        
        socialMediaPopupContainerViewCornerRadius = 10.0f;
        lblSocialMediaPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblSocialMediaPopupSuccessFailureMessageFont = [MySingleton sharedManager].themeFontTenSizeRegular;
        btnSaveInSocialMediaPopupContainerViewCornerRadiusValue = 5.0f;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        btnSaveInSocialMediaPopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        
        socialMediaPopupContainerViewCornerRadius = 12.0f;
        lblSocialMediaPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontEighteenSizeRegular;
        lblSocialMediaPopupSuccessFailureMessageFont = [MySingleton sharedManager].themeFontElevenSizeRegular;
        btnSaveInSocialMediaPopupContainerViewCornerRadiusValue = 8.0f;
    }
    else
    {
        lblFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        btnSaveInSocialMediaPopupContainerViewFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        
        socialMediaPopupContainerViewCornerRadius = 15.0f;
        lblSocialMediaPopupContainerViewTitleFont = [MySingleton sharedManager].themeFontTwentySizeRegular;
        lblSocialMediaPopupSuccessFailureMessageFont = [MySingleton sharedManager].themeFontElevenSizeRegular;
        btnSaveInSocialMediaPopupContainerViewCornerRadiusValue = 8.0f;
    }
    
    lblEmail.font = lblFont;
    lblEmail.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    txtEmail.font = txtFieldFont;
    txtEmail.delegate = self;
    [txtEmail setValue:[MySingleton sharedManager].textfieldPlaceholderColor
            forKeyPath:@"placeholderLabel.textColor"];
    txtEmail.textColor = [MySingleton sharedManager].textfieldDisabledTextColor;
    txtEmail.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtEmail setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    txtEmail.userInteractionEnabled = false;
    
    txtEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    
    lblFullName.font = lblFont;
    lblFullName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    txtFullName.font = txtFieldFont;
    txtFullName.delegate = self;
    [txtFullName setValue:[MySingleton sharedManager].textfieldPlaceholderColor
               forKeyPath:@"placeholderLabel.textColor"];
    txtFullName.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtFullName.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtFullName setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    txtFullNameBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    
    lblGender.font = lblFont;
    lblGender.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    genderPickerView = [[UIPickerView alloc] init];
    genderPickerView.delegate = self;
    genderPickerView.dataSource = self;
    genderPickerView.showsSelectionIndicator = YES;
    genderPickerView.tag = 1;
    genderPickerView.backgroundColor = [UIColor whiteColor];
    
    txtGender.font = txtFieldFont;
    txtGender.delegate = self;
    [txtGender setValue:[MySingleton sharedManager].textfieldPlaceholderColor
             forKeyPath:@"placeholderLabel.textColor"];
    txtGender.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtGender.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtGender setInputView:genderPickerView];
    [txtGender setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    imageViewGenderDownArrow.layer.masksToBounds = YES;
    imageViewGenderDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewGenderDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewGenderDownArrowTapped:)];
    imageViewGenderDownArrowTapGesture.delegate = self;
    [imageViewGenderDownArrow addGestureRecognizer:imageViewGenderDownArrowTapGesture];
    
    txtGenderBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    imageViewGenderDownArrow.layer.masksToBounds = YES;
    
    
    lblPhoneNumber.font = lblFont;
    lblPhoneNumber.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    txtPhoneNumber.font = txtFieldFont;
    txtPhoneNumber.delegate = self;
    [txtPhoneNumber setValue:[MySingleton sharedManager].textfieldPlaceholderColor
                  forKeyPath:@"placeholderLabel.textColor"];
    txtPhoneNumber.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPhoneNumber.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPhoneNumber setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtPhoneNumber.keyboardType = UIKeyboardTypePhonePad;
    
    txtPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    
    lblState.font = lblFont;
    lblState.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    statePickerView = [[UIPickerView alloc] init];
    statePickerView.delegate = self;
    statePickerView.dataSource = self;
    statePickerView.showsSelectionIndicator = YES;
    statePickerView.tag = 2;
    statePickerView.backgroundColor = [UIColor whiteColor];
    
    txtState.font = txtFieldFont;
    txtState.delegate = self;
    [txtState setValue:[MySingleton sharedManager].textfieldPlaceholderColor
            forKeyPath:@"placeholderLabel.textColor"];
    txtState.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtState.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtState setInputView:statePickerView];
    [txtState setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    imageViewStateDownArrow.layer.masksToBounds = YES;
    imageViewStateDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewStateDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewStateDownArrowTapped:)];
    imageViewStateDownArrowTapGesture.delegate = self;
    [imageViewStateDownArrow addGestureRecognizer:imageViewStateDownArrowTapGesture];
    
    txtStateBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    imageViewStateDownArrow.layer.masksToBounds = YES;
    
    
    lblCity.font = lblFont;
    lblCity.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    cityPickerView = [[UIPickerView alloc] init];
    cityPickerView.delegate = self;
    cityPickerView.dataSource = self;
    cityPickerView.showsSelectionIndicator = YES;
    cityPickerView.tag = 3;
    cityPickerView.backgroundColor = [UIColor whiteColor];
    
    txtCity.font = txtFieldFont;
    txtCity.delegate = self;
    [txtCity setValue:[MySingleton sharedManager].textfieldPlaceholderColor
           forKeyPath:@"placeholderLabel.textColor"];
    txtCity.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtCity.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtCity setInputView:cityPickerView];
    [txtCity setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    imageViewCityDownArrow.layer.masksToBounds = YES;
    imageViewCityDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewCityDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewCityDownArrowTapped:)];
    imageViewCityDownArrowTapGesture.delegate = self;
    [imageViewCityDownArrow addGestureRecognizer:imageViewCityDownArrowTapGesture];
    
    txtCityBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    
    imageViewCityDownArrow.layer.masksToBounds = YES;
    
    lblSocialMedia.font = lblFont;
    lblSocialMedia.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    [btnFacebook addTarget:self action:@selector(btnFacebookClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnTwitter addTarget:self action:@selector(btnTwitterClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnInstagram addTarget:self action:@selector(btnInstagramClicked:) forControlEvents:UIControlEventTouchUpInside];
    [btnGooglePlus addTarget:self action:@selector(btnGooglePlusClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    btnUpdateProfile.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnUpdateProfile.layer.masksToBounds = true;
    btnUpdateProfile.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnUpdateProfile.titleLabel.font = btnFont;
    [btnUpdateProfile setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnUpdateProfile addTarget:self action:@selector(btnUpdateProfileClicked) forControlEvents:UIControlEventTouchUpInside];
    
    socialMediaPopupInnerContainerView.layer.masksToBounds = YES;
    socialMediaPopupInnerContainerView.layer.cornerRadius = socialMediaPopupContainerViewCornerRadius;
    socialMediaPopupInnerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    lblSocialMediaPopupContainerViewTitle.font = lblSocialMediaPopupContainerViewTitleFont;
    lblSocialMediaPopupContainerViewTitle.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    imageViewCloseSocialMediaPopupContainerView.layer.masksToBounds = YES;
    
    [btnCloseSocialMediaPopupContainerView addTarget:self action:@selector(btnCloseSocialMediaPopupContainerViewClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    txtSocialMediaPopup.text = @"Enter url here";
    txtSocialMediaPopup.layer.masksToBounds = YES;
    txtSocialMediaPopup.layer.cornerRadius = 5.0f;
    txtSocialMediaPopup.layer.borderWidth = 1.0f;
    txtSocialMediaPopup.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtSocialMediaPopup.font = txtFieldFont;
    txtSocialMediaPopup.delegate = self;
    txtSocialMediaPopup.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    txtSocialMediaPopup.tintColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    [txtSocialMediaPopup setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            UITextView *tv = txtSocialMediaPopup;
            CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])/2.0;
            topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
            tv.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
        });
    });
    
    lblSocialMediaPopupSuccessFailureMessage.font = lblSocialMediaPopupSuccessFailureMessageFont;
    lblSocialMediaPopupSuccessFailureMessage.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    btnSaveInSocialMediaPopupContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnSaveInSocialMediaPopupContainerView.layer.masksToBounds = true;
    btnSaveInSocialMediaPopupContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSaveInSocialMediaPopupContainerView.titleLabel.font = btnFont;
    [btnSaveInSocialMediaPopupContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSaveInSocialMediaPopupContainerView addTarget:self action:@selector(btnSaveInSocialMediaPopupContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self setUpStatusPicker];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getArtistProfileByArtistId:[prefs objectForKey:@"userid"]];
}

- (void)imageViewGenderDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtGender becomeFirstResponder];
}

- (void)imageViewStateDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtState becomeFirstResponder];
}

- (void)imageViewCityDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtCity becomeFirstResponder];
}

#pragma mark - StatusPicker Setup Method

-(void)setUpStatusPicker
{
    UIFont *lblStatusPickerTitleFont, *lblFont, *txtFieldFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblStatusPickerTitleFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblStatusPickerTitleFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        lblStatusPickerTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        lblFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    statusPickerInnerContainerView.layer.masksToBounds = true;
    statusPickerInnerContainerView.layer.cornerRadius = 10.0f;
    
    lblStatusPickerTitle.font = lblStatusPickerTitleFont;
    lblStatusPickerTitle.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    imageViewCloseStatusPickerContainerView.layer.masksToBounds = YES;
    [btnCloseStatusPickerContainerView addTarget:self action:@selector(btnCloseStatusPickerContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    lblClub.font = lblFont;
    lblClub.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    txtClubContainerView.layer.masksToBounds = YES;
    txtClubContainerView.layer.cornerRadius = 5.0f;
    txtClubContainerView.layer.borderWidth = 1.0f;
    txtClubContainerView.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    
    clubPickerView = [[UIPickerView alloc] init];
    clubPickerView.delegate = self;
    clubPickerView.dataSource = self;
    clubPickerView.showsSelectionIndicator = YES;
    clubPickerView.tag = 4;
    clubPickerView.backgroundColor = [UIColor whiteColor];
    
    txtClub.font = txtFieldFont;
    txtClub.delegate = self;
    [txtClub setValue:[MySingleton sharedManager].textfieldPlaceholderColor
           forKeyPath:@"placeholderLabel.textColor"];
    txtClub.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtClub.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtClub setInputView:clubPickerView];
    [txtClub setAutocorrectionType:UITextAutocorrectionTypeNo];
    if([MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagers.count <= 0)
    {
        txtClub.userInteractionEnabled = false;
    }
    else
    {
        txtClub.userInteractionEnabled = true;
    }
    
    imageViewClubDownArrow.layer.masksToBounds = YES;
    imageViewClubDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewClubDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClubDownArrowTapped:)];
    imageViewClubDownArrowTapGesture.delegate = self;
    [imageViewClubDownArrow addGestureRecognizer:imageViewClubDownArrowTapGesture];
    
    lblPlaylist.font = lblFont;
    lblPlaylist.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    txtPlaylistContainerView.layer.masksToBounds = YES;
    txtPlaylistContainerView.layer.cornerRadius = 5.0f;
    txtPlaylistContainerView.layer.borderWidth = 1.0f;
    txtPlaylistContainerView.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    
    playlistPickerView = [[UIPickerView alloc] init];
    playlistPickerView.delegate = self;
    playlistPickerView.dataSource = self;
    playlistPickerView.showsSelectionIndicator = YES;
    playlistPickerView.tag = 5;
    playlistPickerView.backgroundColor = [UIColor whiteColor];
    
    txtPlaylist.font = txtFieldFont;
    txtPlaylist.delegate = self;
    [txtPlaylist setValue:[MySingleton sharedManager].textfieldPlaceholderColor
               forKeyPath:@"placeholderLabel.textColor"];
    txtPlaylist.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPlaylist.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPlaylist setInputView:playlistPickerView];
    [txtPlaylist setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    imageViewPlaylistDownArrow.layer.masksToBounds = YES;
    imageViewPlaylistDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewPlaylistDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewPlaylistDownArrowTapped:)];
    imageViewPlaylistDownArrowTapGesture.delegate = self;
    [imageViewPlaylistDownArrow addGestureRecognizer:imageViewPlaylistDownArrowTapGesture];
    
    btnChangeInStatusPickerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnChangeInStatusPickerContainerView.layer.masksToBounds = true;
    btnChangeInStatusPickerContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnChangeInStatusPickerContainerView.titleLabel.font = btnFont;
    [btnChangeInStatusPickerContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnChangeInStatusPickerContainerView addTarget:self action:@selector(btnChangeInStatusPickerContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

- (void)imageViewClubDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtClub becomeFirstResponder];
}

- (void)imageViewPlaylistDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtPlaylist becomeFirstResponder];
}

-(void)btnCloseStatusPickerContainerViewClicked
{
    [self.view endEditing:YES];
    
    statusPickerContainerView.hidden = true;
}

-(void)btnChangeInStatusPickerContainerViewClicked
{
    [self.view endEditing:YES];
    
    if(self.boolIsStatusPickerOpenedForChangePlaylist)
    {
        if(self.objSelectedPlaylist != nil)
        {
            statusPickerContainerView.hidden = true;
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [[MySingleton sharedManager].dataManager changeArtistOnlinePlaylist:[prefs objectForKey:@"userid"] withPlaylistId:self.objSelectedPlaylist.strPlaylistID];
        }
        else
        {
            if(self.objSelectedPlaylist == nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You must select a playlist"];
                });
            }
        }
    }
    else
    {
        if(self.objSelectedManager != nil && self.objSelectedPlaylist != nil)
        {
            statusPickerContainerView.hidden = true;
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [[MySingleton sharedManager].dataManager changeStatus:@"online" withArtistId:[prefs objectForKey:@"userid"] withManagerId:self.objSelectedManager.strManagerID withPlaylistId:self.objSelectedPlaylist.strPlaylistID];
        }
        else
        {
            if(self.objSelectedManager == nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You must select a manager"];
                });
                
            }
            else if(self.objSelectedPlaylist == nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You must select a playlist"];
                });
            }
        }
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtEmail)
    {
        txtEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
    else if(textField == txtFullName)
    {
        txtFullNameBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
    else if(textField == txtPhoneNumber)
    {
        txtPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
    else if(textField == txtGender)
    {
        if(txtGender.text.length == 0 )
        {
            txtGender.text = [self.arrayGender objectAtIndex:0];
            [genderPickerView selectRow:0 inComponent:0 animated:YES];
        }
        
        txtGenderBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
    }
    else if(textField == txtState)
    {
        if(txtState.text.length == 0 )
        {
            if([MySingleton sharedManager].dataManager.arrayStates.count > 0)
            {
                self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:0];
                txtState.text = self.objSelectedState.strStateName;
                txtCity.userInteractionEnabled = true;
                [statePickerView selectRow:0 inComponent:0 animated:YES];
                [cityPickerView reloadAllComponents];
                
                txtStateBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No States Added" withDetails:@"Currently we don't have any states added."];
            }
        }
    }
    else if(textField == txtCity)
    {
        if(txtCity.text.length == 0 )
        {
            if(self.objSelectedState.arrayCity.count > 0)
            {
                self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:0];
                txtCity.text = self.objSelectedCity.strCityName;
                [cityPickerView selectRow:0 inComponent:0 animated:YES];
                
                txtCityBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldActiveStateBottomSeparatorColor;
            }
            else
            {
                [self.view endEditing:true];
                [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
            }
        }
    }
    else if(textField == txtClub)
    {
        if(txtClub.text.length == 0 )
        {
            self.objSelectedManager = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagers objectAtIndex:0];
            txtClub.text = self.objSelectedManager.strManagerName;
            [clubPickerView selectRow:0 inComponent:0 animated:YES];
            
            if((self.objSelectedManager.strManagerPlaylistID != nil && self.objSelectedManager.strManagerPlaylistID.length > 0) && (self.objSelectedManager.strManagerPlaylistName != nil && self.objSelectedManager.strManagerPlaylistName.length > 0))
            {
                [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineClubSetWithPresetPlaylist = true;
                
                txtPlaylist.userInteractionEnabled = false;
                txtPlaylist.text = self.objSelectedManager.strManagerPlaylistName;
                
                Playlist *objPlaylist = [[Playlist alloc] init];
                objPlaylist.strPlaylistID = self.objSelectedManager.strManagerPlaylistID;
                objPlaylist.strPlaylistName = self.objSelectedManager.strManagerPlaylistName;
                
                self.objSelectedPlaylist = objPlaylist;
            }
            else
            {
                [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineManagerSetWithPresetPlaylist = false;
                
                if([MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists.count <= 0)
                {
                    txtPlaylist.userInteractionEnabled = false;
                }
                else
                {
                    txtPlaylist.userInteractionEnabled = true;
                }
            }
        }
    }
    else if(textField == txtPlaylist)
    {
        if(txtPlaylist.text.length == 0 )
        {
            self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists objectAtIndex:0];
            txtPlaylist.text = self.objSelectedPlaylist.strPlaylistName;
            [playlistPickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField == txtEmail)
    {
        txtEmailBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
    else if(textField == txtFullName)
    {
        txtFullNameBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
    else if(textField == txtPhoneNumber)
    {
        txtPhoneNumberBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
    else if(textField == txtGender)
    {
        txtGenderBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
    else if(textField == txtState)
    {
        txtStateBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
    else if(textField == txtCity)
    {
        txtCityBottomSeparatorView.backgroundColor = [MySingleton sharedManager].textfieldNormalStateBottomSeparatorColor;
    }
}

#pragma mark - UITextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if(textView == txtSocialMediaPopup)
    {
        if ([textView.text isEqualToString:@"Enter url here"]) {
            textView.text = @"";
            textView.textColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
        }
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView == txtSocialMediaPopup)
    {
        if ([textView.text isEqualToString:@""]) {
            textView.text = @"Enter url here";
            textView.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
        }
    }
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [self.arrayGender count];
    }
    else if(pickerView.tag == 2)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.arrayStates count];
    }
    else if(pickerView.tag == 3)
    {
        rowsInComponent = [self.objSelectedState.arrayCity count];
    }
    else if(pickerView.tag == 4)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagers count];
    }
    else if(pickerView.tag == 5)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == genderPickerView)
    {
        lblMain.text = self.arrayGender[row];
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == statePickerView)
    {
        State *objState = [MySingleton sharedManager].dataManager.arrayStates[row];
        lblMain.text = objState.strStateName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == cityPickerView)
    {
        City *objCity = self.objSelectedState.arrayCity[row];
        lblMain.text = objCity.strCityName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == clubPickerView)
    {
        Manager *objManager = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagers[row];
        lblMain.text = objManager.strManagerName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else if (pickerView == playlistPickerView)
    {
        Playlist *objPlaylist = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists[row];
        lblMain.text = objPlaylist.strPlaylistName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        txtGender.text = [self.arrayGender objectAtIndex:row];
    }
    else if(pickerView.tag == 2)
    {
        self.objSelectedState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:row];
        txtState.text = self.objSelectedState.strStateName;
        [cityPickerView reloadAllComponents];
        
        if(self.objSelectedState.arrayCity.count > 0)
        {
            [cityPickerView selectRow:0 inComponent:0 animated:NO];
            txtCity.text = @"";
            txtCity.userInteractionEnabled = true;
        }
        else
        {
            txtCity.text = @"";
            txtCity.userInteractionEnabled = true;
            
            [self.view endEditing:true];
            [appDelegate showErrorAlertViewWithTitle:@"No Cities Added" withDetails:@"Currently we don't have any cities added to this state."];
        }
    }
    else if(pickerView.tag == 3)
    {
        self.objSelectedCity = [self.objSelectedState.arrayCity objectAtIndex:row];
        txtCity.text = self.objSelectedCity.strCityName;
    }
    else if(pickerView.tag == 4)
    {
        self.objSelectedManager = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagers objectAtIndex:row];
        txtClub.text = self.objSelectedManager.strManagerName;
        
        self.objSelectedPlaylist = nil;
        txtPlaylist.text = @"";
        
        if((self.objSelectedManager.strManagerPlaylistID != nil && self.objSelectedManager.strManagerPlaylistID.length > 0) && (self.objSelectedManager.strManagerPlaylistName != nil && self.objSelectedManager.strManagerPlaylistName.length > 0))
        {
            [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineManagerSetWithPresetPlaylist = true;
            
            txtPlaylist.userInteractionEnabled = false;
            txtPlaylist.text = self.objSelectedManager.strManagerPlaylistName;
            
            Playlist *objPlaylist = [[Playlist alloc] init];
            objPlaylist.strPlaylistID = self.objSelectedManager.strManagerPlaylistID;
            objPlaylist.strPlaylistName = self.objSelectedManager.strManagerPlaylistName;
            
            self.objSelectedPlaylist = objPlaylist;
        }
        else
        {
            [MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineManagerSetWithPresetPlaylist = false;
            
            if([MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists.count <= 0)
            {
                txtPlaylist.userInteractionEnabled = false;
            }
            else
            {
                txtPlaylist.userInteractionEnabled = true;
            }
        }
    }
    else if(pickerView.tag == 5)
    {
        self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists objectAtIndex:row];
        txtPlaylist.text = self.objSelectedPlaylist.strPlaylistName;
    }
}

#pragma mark - UIImagePickerController Delegate Method

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    @try
    {
        UIImage *image = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
        
        UIImage* smaller = [self imageWithImage:image scaledToWidth:320];
        
        self.imageSelectedProfilePicture = smaller;
        imageViewProfilePicture.image = self.imageSelectedProfilePicture;
        imageViewProfilePicture.layer.masksToBounds = YES;
        
        //SEND IMAGE DATA TO SERVER
        self.imageSelectedProfilePictureData = UIImagePNGRepresentation(smaller);
        self.strImageSelectedProfilePictureBase64Data = [self.imageSelectedProfilePictureData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        
        UIGraphicsEndImageContext();
        
        [picker dismissViewControllerAnimated:NO completion:NULL];
        
        [[MySingleton sharedManager].dataManager uploadArtistProfilePictureWith_File:self.imageSelectedProfilePictureData];
    }
    @catch (NSException *exception) {
        
        NSLog(@"Exception in imagePickerController's didFinishPickingMediaWithInfo Method, %@",exception);
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Other Methods

-(void)takeAPhoto
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.allowsEditing = YES;
        
        if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self presentViewController:imagePickerController animated:YES completion:nil];
            }];
        }
        else
        {
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Camera Unavailable" message:@"Unable to find a camera on your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
}

-(void)chooseFromGallery
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePickerController.allowsEditing = YES;
        
        if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                [self presentViewController:imagePickerController animated:YES completion:nil];
            }];
        }
        else
        {
            [self presentViewController:imagePickerController animated:YES completion:nil];
        }
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Photo library Unavailable" message:@"Unable to find photo library on your device." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
}

- (void)btnUpdateProfileClicked
{
    [self.view endEditing:YES];
    
    if(txtFullName.text.length > 0 && txtGender.text.length > 0 && txtPhoneNumber.text.length > 0 && txtState.text.length > 0 && txtCity.text.length > 0)
    {
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
        [dictParameters setObject:txtFullName.text forKey:@"fullname"];
        [dictParameters setObject:txtGender.text forKey:@"gender"];
        [dictParameters setObject:txtPhoneNumber.text forKey:@"phone_number"];
        [dictParameters setObject:self.objSelectedState.strStateID forKey:@"state_id"];
        [dictParameters setObject:txtState.text forKey:@"state_name"];
        [dictParameters setObject:self.objSelectedCity.strCityID forKey:@"city_id"];
        [dictParameters setObject:txtCity.text forKey:@"city_name"];
        [dictParameters setObject:[prefs objectForKey:@"usertype"] forKey:@"user_type"];
        
        [[MySingleton sharedManager].dataManager updateArtistProfile:dictParameters];
    }
    else
    {
        if(txtFullName.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter full name"];
            });
        }
        else if(txtGender.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please select your gender"];
            });
        }
        else if(txtPhoneNumber.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter your phone number"];
            });
        }
        else if(txtState.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please select a state"];
            });
        }
        else if(txtCity.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please select a city"];
            });
        }
    }
}

-(void)redirectToChangeArtistPasswordViewController
{
    [self.view endEditing:YES];
    
    ChangeArtistPasswordViewController *viewController = [[ChangeArtistPasswordViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)displaySocialMediaPopupContainer
{
    [self.view endEditing:YES];
    
    socialMediaPopupContainerView.hidden = FALSE;
}

- (IBAction)btnCloseSocialMediaPopupContainerViewClicked:(id)sender
{
    [self.view endEditing:YES];
    
    socialMediaPopupContainerView.hidden = TRUE;
    
    lblSocialMediaPopupSuccessFailureMessage.text = @"";
}

-(void)btnSaveInSocialMediaPopupContainerViewClicked
{
    [self.view endEditing:YES];
    
    if(txtSocialMediaPopup.text.length > 0 && ![txtSocialMediaPopup.text isEqualToString:@"Enter url here"])
    {
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
        [dictParameters setObject:txtSocialMediaPopup.text forKey:@"social_media_profile_url"];
        [dictParameters setObject:self.strSocialMediaPopupOpenedFor forKey:@"social_media_profile_type"];
        
        [[MySingleton sharedManager].dataManager updateArtistSocialMediaProfileUrl:dictParameters];
    }
    else
    {
        if(txtSocialMediaPopup.text.length <= 0 || [txtSocialMediaPopup.text isEqualToString:@"Enter url here"])
        {
            if([self.strSocialMediaPopupOpenedFor isEqualToString:@"facebook"])
            {
                lblSocialMediaPopupSuccessFailureMessage.text = @"Please enter your facebook profile or page url.";
            }
            else if([self.strSocialMediaPopupOpenedFor isEqualToString:@"twitter"])
            {
                lblSocialMediaPopupSuccessFailureMessage.text = @"Please enter your twitter profile or page url.";
            }
            else if([self.strSocialMediaPopupOpenedFor isEqualToString:@"instagram"])
            {
                lblSocialMediaPopupSuccessFailureMessage.text = @"Please enter your instagram profile or page url.";
            }
            else if([self.strSocialMediaPopupOpenedFor isEqualToString:@"googleplus"])
            {
                lblSocialMediaPopupSuccessFailureMessage.text = @"Please enter your google plus profile or page url.";
            }
        }
    }
}

- (IBAction)btnFacebookClicked:(id)sender
{
    [self.view endEditing:true];
    
    self.strSocialMediaPopupOpenedFor = @"facebook";
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.strFacebookProfileUrl != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strFacebookProfileUrl.length > 0)
    {
        txtSocialMediaPopup.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strFacebookProfileUrl;
        txtSocialMediaPopup.textColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    }
    else
    {
        txtSocialMediaPopup.text = @"Enter url here";
        txtSocialMediaPopup.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    }
    
    lblSocialMediaPopupContainerViewTitle.text = @"FACEBOOK";
    [self displaySocialMediaPopupContainer];
}

- (void)gotFacebookData
{
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             if (!error)
             {
                 NSLog(@"fetched user:%@", result);
                 
                 if (([[result allKeys] containsObject:@"id"]))
                 {
                     NSString *strFacebookProfileUrl = [NSString stringWithFormat:@"http://www.facebook.com/%@", [result objectForKey:@"id"]];
                     NSLog(@"strFacebookProfileUrl : %@", strFacebookProfileUrl);
                 }
                 else
                 {
                     NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
                     alertViewController.title = @"Facebook Error";
                     alertViewController.message = @"Please make profile information public to login with facebook.";
                     
                     alertViewController.view.tintColor = [UIColor whiteColor];
                     alertViewController.backgroundTapDismissalGestureEnabled = YES;
                     alertViewController.swipeDismissalGestureEnabled = YES;
                     alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
                     
                     alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
                     alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
                     alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
                     alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
                     
                     [alertViewController addAction:[NYAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
                         [alertViewController dismissViewControllerAnimated:YES completion:nil];
                     }]];
                     
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [self presentViewController:alertViewController animated:YES completion:nil];
                     });
                 }
             }
         }];
    }
}

- (IBAction)btnTwitterClicked:(id)sender
{
    [self.view endEditing:true];
    
    self.strSocialMediaPopupOpenedFor = @"twitter";
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.strTwitterProfileUrl != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strTwitterProfileUrl.length > 0)
    {
        txtSocialMediaPopup.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strTwitterProfileUrl;
        txtSocialMediaPopup.textColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    }
    else
    {
        txtSocialMediaPopup.text = @"Enter url here";
        txtSocialMediaPopup.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    }
    
    lblSocialMediaPopupContainerViewTitle.text = @"TWITTER";
    [self displaySocialMediaPopupContainer];
}

- (IBAction)btnInstagramClicked:(id)sender
{
    [self.view endEditing:true];
    
    self.strSocialMediaPopupOpenedFor = @"instagram";
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.strInstagramProfileUrl != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strInstagramProfileUrl.length > 0)
    {
        txtSocialMediaPopup.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strInstagramProfileUrl;
        txtSocialMediaPopup.textColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    }
    else
    {
        txtSocialMediaPopup.text = @"Enter url here";
        txtSocialMediaPopup.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    }
    
    lblSocialMediaPopupContainerViewTitle.text = @"INSTAGRAM";
    [self displaySocialMediaPopupContainer];
}

- (IBAction)btnGooglePlusClicked:(id)sender
{
    [self.view endEditing:true];
    
    self.strSocialMediaPopupOpenedFor = @"googleplus";
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.strGooglePlusProfileUrl != nil && [MySingleton sharedManager].dataManager.objLoggedInUser.strGooglePlusProfileUrl.length > 0)
    {
        txtSocialMediaPopup.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strGooglePlusProfileUrl;
        txtSocialMediaPopup.textColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    }
    else
    {
        txtSocialMediaPopup.text = @"Enter url here";
        txtSocialMediaPopup.textColor = [MySingleton sharedManager].textfieldPlaceholderColor;
    }
    
    lblSocialMediaPopupContainerViewTitle.text = @"GOOGLE PLUS";
    [self displaySocialMediaPopupContainer];
}

@end
