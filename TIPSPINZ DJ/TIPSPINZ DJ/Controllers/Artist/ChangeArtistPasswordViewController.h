//
//  ChangeArtistPasswordViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 31/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangeArtistPasswordViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;

@property (nonatomic,retain) IBOutlet UITextField *txtOldPassword;
@property (nonatomic,retain) IBOutlet UITextField *txtNewPassword;
@property (nonatomic,retain) IBOutlet UITextField *txtRepeatPassword;

@property (nonatomic,retain) IBOutlet UIButton *btnChangePassword;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSString *strUserType;

@end
