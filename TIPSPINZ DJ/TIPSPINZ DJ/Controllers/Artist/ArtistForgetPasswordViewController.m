//
//  ArtistForgetPasswordViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 31/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import "ArtistForgetPasswordViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "CommonUtility.h"

@interface ArtistForgetPasswordViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ArtistForgetPasswordViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;

@synthesize imageViewMain;
@synthesize separatorView1;
@synthesize lblInstructions;
@synthesize separatorView2;

@synthesize txtEmail;

@synthesize btnSubmit;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forgotArtistPasswordEvent) name:@"forgotArtistPasswordEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)forgotArtistPasswordEvent
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [appDelegate showErrorAlertViewWithTitle:@"Email Sent" withDetails:@"We have sent you an email containing your new password. Please use that password to login to the application."];
    });
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Forget Password"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.view endEditing:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    mainInnerScrollView.delegate = self;
    mainInnerScrollView.contentSize = CGSizeMake(mainInnerScrollView.frame.size.width, mainInnerScrollView.frame.size.height);
    
    UIFont *lblInstructionsFont, *txtFieldFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontFourteenSizeLight;
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    imageViewBack.layer.masksToBounds = YES;
    
    separatorView1.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
    separatorView2.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
    
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    lblInstructions.font = lblInstructionsFont;
    
    UIView *txtEmailPaddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, txtEmail.frame.size.height)];
    txtEmail.leftView = txtEmailPaddingView;
    txtEmail.leftViewMode = UITextFieldViewModeAlways;
    txtEmail.layer.masksToBounds = YES;
    txtEmail.layer.cornerRadius = 5.0f;
    txtEmail.layer.borderWidth = 1.0f;
    txtEmail.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    txtEmail.font = txtFieldFont;
    txtEmail.delegate = self;
    [txtEmail setValue:[MySingleton sharedManager].textfieldPlaceholderColor
            forKeyPath:@"placeholderLabel.textColor"];
    txtEmail.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtEmail.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtEmail setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtEmail setKeyboardType:UIKeyboardTypeEmailAddress];
    
    btnSubmit.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnSubmit.layer.masksToBounds = true;
    btnSubmit.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnSubmit.titleLabel.font = btnFont;
    [btnSubmit setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnSubmit addTarget:self action:@selector(btnSubmitClicked) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
}

#pragma mark - Other Methods

- (void)btnSubmitClicked
{
    [self.view endEditing:YES];
    
    CommonUtility *objUtility = [[CommonUtility alloc]init];
    
    if(txtEmail.text.length > 0 && [objUtility isValidEmailAddress:txtEmail.text])
    {
        [[MySingleton sharedManager].dataManager forgetArtistPassword:txtEmail.text withUserType:self.strUserType];
    }
    else
    {
        if(txtEmail.text.length <= 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Please enter email"];
            });
        }
        else if(![objUtility isValidEmailAddress:txtEmail.text])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"Invalid email address"];
            });
        }
    }
}

@end
