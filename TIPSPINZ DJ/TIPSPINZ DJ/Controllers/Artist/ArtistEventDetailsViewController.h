//
//  ArtistEventDetailsViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 31/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

#import "Event.h"
#import "Playlist.h"

@interface ArtistEventDetailsViewController : UIViewController<UIScrollViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIGestureRecognizerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;

@property (nonatomic,retain) IBOutlet AsyncImageView *imageViewLive;

@property (nonatomic,retain) IBOutlet UILabel *lblEventDate;
@property (nonatomic,retain) IBOutlet UILabel *lblEventName;
@property (nonatomic,retain) IBOutlet UILabel *lblEventDescription;

@property (nonatomic,retain) IBOutlet UIView *bottomContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblPlaylist;
@property (nonatomic,retain) IBOutlet UIView *txtPlaylistContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtPlaylist;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewPlaylistDownArrow;

@property (nonatomic,retain) IBOutlet UIButton *btnChangeStatus;
@property (nonatomic,retain) IBOutlet UIButton *btnGoOnlineWithoutPlaylist;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) Event *objSelectedEvent;

@property (nonatomic,retain) UIPickerView *playlistPickerView;
@property (nonatomic,retain) Playlist *objSelectedPlaylist;

@end
