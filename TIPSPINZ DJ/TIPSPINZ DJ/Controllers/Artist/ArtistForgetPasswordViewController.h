//
//  ArtistForgetPasswordViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 31/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArtistForgetPasswordViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;

@property (nonatomic,retain) IBOutlet UIImageView *imageViewMain;
@property (nonatomic,retain) IBOutlet UIView *separatorView1;
@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;
@property (nonatomic,retain) IBOutlet UIView *separatorView2;

@property (nonatomic,retain) IBOutlet UITextField *txtEmail;

@property (nonatomic,retain) IBOutlet UIButton *btnSubmit;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSString *strUserType;

@end
