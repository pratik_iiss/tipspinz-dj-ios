//
//  ArtistMyPlaylistViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 01/06/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import "ArtistMyPlaylistViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "Playlist.h"

#import "MyPlaylistTableViewCell.h"

#import "ArtistPlaylistSongsViewController.h"

@interface ArtistMyPlaylistViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ArtistMyPlaylistViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewSearch;
@synthesize btnSearch;

@synthesize searchTextContainerView;
@synthesize txtSearch;
@synthesize imageViewCloseSearchTextContainerView;
@synthesize btnCloseSearchTextContainerView;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;
@synthesize btnEdit;
@synthesize mainTableView;
@synthesize lblNoData;
@synthesize btnDelete;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
    
    if([MySingleton sharedManager].dataManager.boolIsSongsDeletedFromPlaylistSuccessfully)
    {
        [MySingleton sharedManager].dataManager.boolIsSongsDeletedFromPlaylistSuccessfully = false;
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        [[MySingleton sharedManager].dataManager getAllPlaylistsByArtistId:[prefs objectForKey:@"userid"]];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllPlaylistsByArtistIdEvent) name:@"gotAllPlaylistsByArtistIdEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletedPlaylistByArtistIdEvent) name:@"deletedPlaylistByArtistIdEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllPlaylistsByArtistIdEvent
{
    self.dataRows = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists;
    
    if(self.dataRows.count <= 0)
    {
        btnEdit.hidden = true;
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        btnEdit.hidden = false;
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

-(void)deletedPlaylistByArtistIdEvent
{
    self.dataRows = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists;
    
    if(self.dataRows.count <= 0)
    {
        btnEdit.hidden = true;
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        btnEdit.hidden = false;
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"My Playlist"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    
    imageViewSearch.layer.masksToBounds = YES;
    [btnSearch addTarget:self action:@selector(btnSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *txtFieldFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
    }
    
    searchTextContainerView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    txtSearch.font = txtFieldFont;
    txtSearch.delegate = self;
    [txtSearch setValue:[MySingleton sharedManager].themeGlobalWhiteColor
             forKeyPath:@"placeholderLabel.textColor"];
    txtSearch.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.tintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.returnKeyType = UIReturnKeySearch;
    [txtSearch setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    imageViewCloseSearchTextContainerView.layer.masksToBounds = YES;
    [btnCloseSearchTextContainerView addTarget:self action:@selector(btnCloseSearchTextContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnSearchClicked:(id)sender
{
    [self.view endEditing:YES];
    
    searchTextContainerView.hidden = false;
    [txtSearch becomeFirstResponder];
}

-(void)btnCloseSearchTextContainerViewClicked
{
    [self.view endEditing:YES];
    
    txtSearch.text = @"";
    
    searchTextContainerView.hidden = true;
    
    self.dataRows = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists;
    
    if(self.dataRows.count <= 0)
    {
        btnEdit.hidden = true;
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        btnEdit.hidden = false;
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor clearColor];
    mainTableView.allowsMultipleSelectionDuringEditing = NO;
    mainTableView.hidden = true;
    
    UIFont *btnEditFont, *lblNoDataFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        btnEditFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
        lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    NSString *strBtnEditTitle = [NSString stringWithFormat:@"Edit Playlist"];
    NSMutableAttributedString *strAttrBtnEditTitle = [[NSMutableAttributedString alloc] initWithString:strBtnEditTitle attributes: nil];
    NSRange rangeOfEditSongslist = [strBtnEditTitle rangeOfString:@"Edit Playlist"];
    [strAttrBtnEditTitle addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:rangeOfEditSongslist];
    [strAttrBtnEditTitle addAttribute:NSForegroundColorAttributeName value:[MySingleton sharedManager].themeGlobalBlackColor range:NSMakeRange(0, strBtnEditTitle.length)];
    [btnEdit setAttributedTitle:strAttrBtnEditTitle forState:UIControlStateNormal];
    btnEdit.titleLabel.font = btnEditFont;
    [btnEdit addTarget:self action:@selector(btnEditClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNoData.font = lblNoDataFont;
    lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    btnDelete.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnDelete.layer.masksToBounds = true;
    btnDelete.titleLabel.font = btnFont;
    [btnDelete setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnDelete addTarget:self action:@selector(btnDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.boolIsEditingModeOn = false;
    btnDelete.hidden = true;
    self.arraySelectedPlaylistsIds = [[NSMutableArray alloc] init];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getAllPlaylistsByArtistId:[prefs objectForKey:@"userid"]];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 80;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        MyPlaylistTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        Playlist *objPlaylist = [self.dataRows objectAtIndex:indexPath.row];
        
        if(self.boolIsEditingModeOn)
        {
            cell = [[MyPlaylistTableViewCell alloc] initWithEditingStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
            
            if([self.arraySelectedPlaylistsIds containsObject:objPlaylist.strPlaylistID])
            {
                cell.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_checked.png"];
            }
            else
            {
                cell.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
            }
        }
        else
        {
            cell = [[MyPlaylistTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        }
        
        cell.lblPlaylistName.text = objPlaylist.strPlaylistName;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:true];
    
    if(tableView == mainTableView)
    {
        Playlist *objPlaylist = [self.dataRows objectAtIndex:indexPath.row];
        
        if(self.boolIsEditingModeOn)
        {
            if([self.arraySelectedPlaylistsIds containsObject:objPlaylist.strPlaylistID])
            {
                NSInteger indexOfObject = [self.arraySelectedPlaylistsIds indexOfObject:objPlaylist.strPlaylistID];
                
                if(indexOfObject != NSNotFound)
                {
                    [self.arraySelectedPlaylistsIds removeObjectAtIndex:indexOfObject];
                    [mainTableView reloadData];
                }
            }
            else
            {
                [self.arraySelectedPlaylistsIds addObject:objPlaylist.strPlaylistID];
                [mainTableView reloadData];
            }
        }
        else
        {
            ArtistPlaylistSongsViewController *viewController = [[ArtistPlaylistSongsViewController alloc] init];
            viewController.objSelectedPlaylist = objPlaylist;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if(textField == txtSearch)
    {
//        NSLog(@"Search button clicked for txtSearch.");
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == txtSearch)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists;
        
        if(self.dataRows.count <= 0)
        {
            btnEdit.hidden = true;
            mainTableView.hidden = true;
            lblNoData.hidden = false;
        }
        else
        {
            btnEdit.hidden = false;
            mainTableView.hidden = false;
            lblNoData.hidden = true;
            [mainTableView reloadData];
        }
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    [self searchPlaylistsWithSubstring:substring];
    return YES;
}

#pragma mark - Other Methods

-(IBAction)btnEditClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if(self.boolIsEditingModeOn)
    {
        self.arraySelectedPlaylistsIds = [[NSMutableArray alloc] init];
        self.boolIsEditingModeOn = false;
        btnDelete.hidden = true;
        
        CGRect mainTableViewFrame = mainTableView.frame;
        mainTableViewFrame.size.height = mainInnerScrollView.frame.size.height - mainTableView.frame.origin.y;
        mainTableView.frame = mainTableViewFrame;
    }
    else
    {
        self.boolIsEditingModeOn = true;
        btnDelete.hidden = false;
        
        CGRect mainTableViewFrame = mainTableView.frame;
        mainTableViewFrame.size.height = btnDelete.frame.origin.y - mainTableView.frame.origin.y;
        mainTableView.frame = mainTableViewFrame;
    }
    
    [mainTableView reloadData];
}

- (void)searchPlaylistsWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        for(Playlist *objPlaylist in [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists)
        {
            if (([[objPlaylist.strPlaylistName lowercaseString] containsString:[substring lowercaseString]]))
            {
                [self.dataRows addObject:objPlaylist];
            }
        }
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists;
    }
    
    if(self.dataRows.count <= 0)
    {
        btnEdit.hidden = true;
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        btnEdit.hidden = false;
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

- (IBAction)btnDeleteClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if(self.arraySelectedPlaylistsIds.count > 0)
    {
        NYAlertViewController *alertViewController = [[NYAlertViewController alloc] initWithNibName:nil bundle:nil];
        alertViewController.title = @"";
        alertViewController.message = @"Are you sure you want to delete?";
        
        alertViewController.view.tintColor = [UIColor whiteColor];
        alertViewController.backgroundTapDismissalGestureEnabled = YES;
        alertViewController.swipeDismissalGestureEnabled = YES;
        alertViewController.transitionStyle = NYAlertViewControllerTransitionStyleFade;
        
        alertViewController.titleFont = [MySingleton sharedManager].alertViewTitleFont;
        alertViewController.messageFont = [MySingleton sharedManager].alertViewMessageFont;
        alertViewController.buttonTitleFont = [MySingleton sharedManager].alertViewButtonTitleFont;
        alertViewController.cancelButtonTitleFont = [MySingleton sharedManager].alertViewCancelButtonTitleFont;
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
            NSString *strArraySelectedPlaylistsIds = [self.arraySelectedPlaylistsIds componentsJoinedByString:@","];
            NSLog(@"strArraySelectedPlaylistsIds : %@", strArraySelectedPlaylistsIds);
            
            self.arraySelectedPlaylistsIds = [[NSMutableArray alloc] init];
            self.boolIsEditingModeOn = false;
            btnDelete.hidden = true;
            
            CGRect mainTableViewFrame = mainTableView.frame;
            mainTableViewFrame.size.height = mainInnerScrollView.frame.size.height - mainTableView.frame.origin.y;
            mainTableView.frame = mainTableViewFrame;
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            [[MySingleton sharedManager].dataManager deletePlaylistByArtistId:[prefs objectForKey:@"userid"] withPlaylistId:strArraySelectedPlaylistsIds];
            
        }]];
        
        [alertViewController addAction:[NYAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:^(NYAlertAction *action){
            
            [alertViewController dismissViewControllerAnimated:YES completion:nil];
            
        }]];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertViewController animated:YES completion:nil];
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Select Songs" withDetails:@"Please select atleast one song to delete"];
        });
    }
}

@end
