//
//  ArtistHomeViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 31/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import "ArtistHomeViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "Manager.h"

#import "ClubListTableViewCell.h"
#import "StatePickerTableViewCell.h"

@interface ArtistHomeViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ArtistHomeViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewMenu;
@synthesize btnMenu;
@synthesize lblNavigationTitle;
@synthesize imageViewStateFilter;
@synthesize btnStateFilter;
@synthesize imageViewSearch;
@synthesize btnSearch;

@synthesize searchTextContainerView;
@synthesize txtSearch;
@synthesize imageViewCloseSearchTextContainerView;
@synthesize btnCloseSearchTextContainerView;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;
@synthesize mainTableView;
@synthesize lblNoData;
@synthesize btnAddClubs;

@synthesize statePickerContainerView;
@synthesize statePickerBlackTransparentView;
@synthesize statePickerInnerContainerView;
@synthesize lblStatePickerTitle;
@synthesize imageViewCloseStatePickerContainerView;
@synthesize btnCloseStatePickerContainerView;
@synthesize statePickerTableView;
@synthesize btnGoInStatePickerContainerView;
@synthesize btnSelectAllStatesInPickerContainerView;
@synthesize btnDeselectAllStatesInPickerContainerView;

//========== OTHER VARIABLES ==========//

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotAllManagersEvent) name:@"gotAllManagersEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addedManagersEvent) name:@"addedManagersEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotAllManagersEvent
{
    NSLog(@" [MySingleton sharedManager].dataManager.arrayAllManagers.count : %d",  [MySingleton sharedManager].dataManager.arrayAllManagers.count);
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    [MySingleton sharedManager].dataManager.arrayManagersByArtistStateId = [[NSMutableArray alloc] init];
    
    for(Manager *objManager in [MySingleton sharedManager].dataManager.arrayAllManagers)
    {
        if ([objManager.strManagerStateID isEqualToString:[prefs objectForKey:@"stateid"]])
        {
            [[MySingleton sharedManager].dataManager.arrayManagersByArtistStateId addObject:objManager];
        }
    }
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayManagersByArtistStateId;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
    
    self.arraySelectedStatesIds = [[NSMutableArray alloc] init];
    [self.arraySelectedStatesIds addObject:[prefs objectForKey:@"stateid"]];
}

-(void)addedManagersEvent
{
    [appDelegate showErrorAlertViewWithTitle:@"Request Sent" withDetails:@"Your request has been sent to company(s) successfully. We will add selected manager(s) to your list when company(s) will approve your request"];
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewMenu.layer.masksToBounds = YES;
    [btnMenu addTarget:self action:@selector(btnMenuClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Add Managers"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    
    imageViewStateFilter.layer.masksToBounds = YES;
    [btnStateFilter addTarget:self action:@selector(btnStateFilterClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    imageViewSearch.layer.masksToBounds = YES;
    [btnSearch addTarget:self action:@selector(btnSearchClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    UIFont *txtFieldFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSeventeenSizeRegular;
    }
    
    searchTextContainerView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    txtSearch.font = txtFieldFont;
    txtSearch.delegate = self;
    [txtSearch setValue:[MySingleton sharedManager].themeGlobalWhiteColor
             forKeyPath:@"placeholderLabel.textColor"];
    txtSearch.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.tintColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    txtSearch.returnKeyType = UIReturnKeySearch;
    [txtSearch setAutocorrectionType:UITextAutocorrectionTypeNo];
    txtSearch.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    imageViewCloseSearchTextContainerView.layer.masksToBounds = YES;
    [btnCloseSearchTextContainerView addTarget:self action:@selector(btnCloseSearchTextContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)btnMenuClicked:(id)sender
{
    [self.view endEditing:true];
    
    if(self.sideMenuController.isLeftViewVisible)
    {
        [self.sideMenuController hideLeftViewAnimated];
    }
    else
    {
        [self.sideMenuController showLeftViewAnimated:YES completionHandler:nil];
    }
}

-(IBAction)btnStateFilterClicked:(id)sender
{
    [self.view endEditing:YES];
    
    statePickerContainerView.hidden = false;
    
    [MySingleton sharedManager].floatStatePickerTableViewWidth = statePickerTableView.frame.size.width;
    [statePickerTableView reloadData];
}

-(IBAction)btnSearchClicked:(id)sender
{
    [self.view endEditing:YES];
    
    searchTextContainerView.hidden = false;
    [txtSearch becomeFirstResponder];
}

-(void)btnCloseSearchTextContainerViewClicked
{
    [self.view endEditing:YES];
    
    txtSearch.text = @"";
    
    searchTextContainerView.hidden = true;
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayManagersByArtistStateId;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    mainTableView.delegate = self;
    mainTableView.dataSource = self;
    mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    mainTableView.backgroundColor = [UIColor clearColor];
    mainTableView.hidden = true;
    
    UIFont *lblNoDataFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else
    {
        lblNoDataFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    lblNoData.font = lblNoDataFont;
    lblNoData.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    btnAddClubs.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnAddClubs.layer.masksToBounds = true;
    btnAddClubs.titleLabel.font = btnFont;
    [btnAddClubs setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnAddClubs addTarget:self action:@selector(btnAddClubsClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setUpStatePicker];
    
    self.arraySelectedManagerIds = [[NSMutableArray alloc] init];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [[MySingleton sharedManager].dataManager getAllManagers:[prefs objectForKey:@"userid"]];
}

#pragma mark - StatePicker Setup Method

-(void)setUpStatePicker
{
    UIFont *lblStatePickerTitleFont, *btnFont, *btnSelectAllFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblStatePickerTitleFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        btnSelectAllFont = [MySingleton sharedManager].themeFontTwelveSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblStatePickerTitleFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        btnSelectAllFont = [MySingleton sharedManager].themeFontThirteenSizeBold;
    }
    else
    {
        lblStatePickerTitleFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        btnSelectAllFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    
    statePickerInnerContainerView.layer.masksToBounds = true;
    statePickerInnerContainerView.layer.cornerRadius = 10.0f;
    
    lblStatePickerTitle.font = lblStatePickerTitleFont;
    lblStatePickerTitle.textColor = [MySingleton sharedManager].themeGlobalLightGreyColor;
    
    imageViewCloseStatePickerContainerView.hidden = true;
    btnCloseStatePickerContainerView.hidden = true;
    imageViewCloseStatePickerContainerView.layer.masksToBounds = YES;
    [btnCloseStatePickerContainerView addTarget:self action:@selector(btnCloseStatePickerContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    statePickerTableView.delegate = self;
    statePickerTableView.dataSource = self;
    statePickerTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    statePickerTableView.backgroundColor = [UIColor clearColor];
    
    btnGoInStatePickerContainerView.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnGoInStatePickerContainerView.layer.masksToBounds = true;
    btnGoInStatePickerContainerView.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnGoInStatePickerContainerView.titleLabel.font = btnFont;
    [btnGoInStatePickerContainerView setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnGoInStatePickerContainerView addTarget:self action:@selector(btnGoInStatePickerContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [btnSelectAllStatesInPickerContainerView setTitleColor:[MySingleton sharedManager].themeGlobalBlackColor forState:UIControlStateNormal];
    btnSelectAllStatesInPickerContainerView.titleLabel.font = btnSelectAllFont;
    [btnSelectAllStatesInPickerContainerView addTarget:self action:@selector(btnSelectAllStatesInPickerContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [btnDeselectAllStatesInPickerContainerView setTitleColor:[MySingleton sharedManager].themeGlobalBlackColor forState:UIControlStateNormal];
    btnDeselectAllStatesInPickerContainerView.titleLabel.font = btnSelectAllFont;
    [btnDeselectAllStatesInPickerContainerView addTarget:self action:@selector(btnDeselectAllStatesInPickerContainerViewClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(void)btnCloseStatePickerContainerViewClicked
{
    [self.view endEditing:YES];
    
    statePickerContainerView.hidden = true;
}

-(void)btnGoInStatePickerContainerViewClicked
{
    [self.view endEditing:YES];
    
    statePickerContainerView.hidden = true;
    
    //    if(self.arraySelectedStatesIds.count > 0)
    //    {
    [MySingleton sharedManager].dataManager.arrayManagersByArtistStateId = [[NSMutableArray alloc] init];
    
    for(Manager *objManager in [MySingleton sharedManager].dataManager.arrayAllManagers)
    {
        if ([self.arraySelectedStatesIds containsObject:objManager.strManagerStateID])
        {
            [[MySingleton sharedManager].dataManager.arrayManagersByArtistStateId addObject:objManager];
        }
    }
    
    self.dataRows = [MySingleton sharedManager].dataManager.arrayManagersByArtistStateId;
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
    //    }
    //    else
    //    {
    //        [appDelegate showErrorAlertViewWithTitle:@"Select State" withDetails:@"Please select atleast one state."];
    //    }
}

-(void)btnSelectAllStatesInPickerContainerViewClicked
{
    [self.view endEditing:YES];
    
    self.arraySelectedStatesIds = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [MySingleton sharedManager].dataManager.arrayStates.count; i++)
    {
        State *objState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:i];
        
        [self.arraySelectedStatesIds addObject:objState.strStateID];
    }
    
    [statePickerTableView reloadData];
}

-(void)btnDeselectAllStatesInPickerContainerViewClicked
{
    [self.view endEditing:YES];
    
    self.arraySelectedStatesIds = [[NSMutableArray alloc] init];
    [statePickerTableView reloadData];
}

#pragma mark - UITableViewController Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView == mainTableView)
    {
        return 1;
    }
    else if(tableView == statePickerTableView)
    {
        return 1;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return 0;
    }
    else if(tableView == statePickerTableView)
    {
        return 0;
    }
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return nil;
    }
    else if(tableView == statePickerTableView)
    {
        return nil;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == mainTableView)
    {
        return self.dataRows.count;
    }
    else if(tableView == statePickerTableView)
    {
        return [MySingleton sharedManager].dataManager.arrayStates.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        return 150;
    }
    else if(tableView == statePickerTableView)
    {
        return 60;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"Cell";
    
    if(tableView == mainTableView)
    {
        ClubListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[ClubListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        Manager *objManager = [self.dataRows objectAtIndex:indexPath.row];
        
        cell.lblClubName.text = objManager.strManagerName;
        
        cell.lblClubType.text = objManager.strManagerType;
        
        cell.lblClubAddress.text = objManager.strManagerAddress;
        
//        [[AsyncImageLoader sharedLoader].cache removeAllObjects];
        cell.imageViewClub.imageURL = [NSURL URLWithString:objManager.strManagerImageUrl];
        
        if([[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagerIds containsObject:objManager.strManagerID])
        {
            cell.imageViewCheckboxClubAdded.image = [UIImage imageNamed:@"checkbox_checked.png"];
        }
        else if ([self.arraySelectedManagerIds containsObject:objManager.strManagerID])
        {
            cell.imageViewCheckboxClubAdded.image = [UIImage imageNamed:@"checkbox_checked_club_added.png"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else if(tableView == statePickerTableView)
    {
        StatePickerTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        cell = [[StatePickerTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
        
        State *objState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:indexPath.row];
        
        cell.lblStateName.text = objState.strStateName;
        
        if([self.arraySelectedStatesIds containsObject:objState.strStateID])
        {
            cell.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_checked.png"];
        }
        else
        {
            cell.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == mainTableView)
    {
        Manager *objManager = [self.dataRows objectAtIndex:indexPath.row];
        
        if([[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistApprovedManagerIds containsObject:objManager.strManagerID])
        {
            
        }
        else if([self.arraySelectedManagerIds containsObject:objManager.strManagerID])
        {
            NSInteger indexOfObject = [self.arraySelectedManagerIds indexOfObject:objManager.strManagerID];
            
            if(indexOfObject != NSNotFound)
            {
                [self.arraySelectedManagerIds removeObjectAtIndex:indexOfObject];
                [mainTableView reloadData];
            }
        }
        else
        {
            [self.arraySelectedManagerIds addObject:objManager.strManagerID];
            [mainTableView reloadData];
        }
    }
    else if(tableView == statePickerTableView)
    {
        State *objState = [[MySingleton sharedManager].dataManager.arrayStates objectAtIndex:indexPath.row];
        
        if([self.arraySelectedStatesIds containsObject:objState.strStateID])
        {
            NSInteger indexOfObject = [self.arraySelectedStatesIds indexOfObject:objState.strStateID];
            
            if(indexOfObject != NSNotFound)
            {
                [self.arraySelectedStatesIds removeObjectAtIndex:indexOfObject];
                [statePickerTableView reloadData];
            }
        }
        else
        {
            [self.arraySelectedStatesIds addObject:objState.strStateID];
            [statePickerTableView reloadData];
        }
    }
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    if(textField == txtSearch)
    {
//        NSLog(@"Search button clicked for txtSearch.");
    }
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    if(textField == txtSearch)
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayManagersByArtistStateId;
        
        if(self.dataRows.count <= 0)
        {
            mainTableView.hidden = true;
            lblNoData.hidden = false;
        }
        else
        {
            mainTableView.hidden = false;
            lblNoData.hidden = true;
            [mainTableView reloadData];
        }
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring stringByReplacingCharactersInRange:range withString:string];
    [self searchManagersWithSubstring:substring];
    return YES;
}

#pragma mark - Other Methods

- (void)searchManagersWithSubstring:(NSString *)substring
{
    if(substring.length > 0)
    {
        self.dataRows = [[NSMutableArray alloc] init];
        
        for(Manager *objManager in [MySingleton sharedManager].dataManager.arrayManagersByArtistStateId)
        {
            if (([[objManager.strManagerName lowercaseString] containsString:[substring lowercaseString]]) || ([[objManager.strManagerType lowercaseString] containsString:[substring lowercaseString]]) || ([[objManager.strManagerAddress lowercaseString] containsString:[substring lowercaseString]]))
            {
                [self.dataRows addObject:objManager];
            }
        }
    }
    else
    {
        self.dataRows = [MySingleton sharedManager].dataManager.arrayManagersByArtistStateId;
    }
    
    if(self.dataRows.count <= 0)
    {
        mainTableView.hidden = true;
        lblNoData.hidden = false;
    }
    else
    {
        mainTableView.hidden = false;
        lblNoData.hidden = true;
        [mainTableView reloadData];
    }
}

- (IBAction)btnAddClubsClicked:(id)sender
{
    [self.view endEditing:YES];
    
    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [dictParameters setObject:[prefs objectForKey:@"userid"] forKey:@"user_id"];
    
    NSString *strarraySelectedManagerIds = [self.arraySelectedManagerIds componentsJoinedByString:@","];
    [dictParameters setObject:strarraySelectedManagerIds forKey:@"array_manager_ids_for_approval"];
    
    [[MySingleton sharedManager].dataManager addManagers:dictParameters];
}

@end
