//
//  ArtistHomeViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 31/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

@interface ArtistHomeViewController : UIViewController<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewStateFilter;
@property (nonatomic,retain) IBOutlet UIButton *btnStateFilter;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewSearch;
@property (nonatomic,retain) IBOutlet UIButton *btnSearch;

@property (nonatomic,retain) IBOutlet UIView *searchTextContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtSearch;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseSearchTextContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseSearchTextContainerView;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;
@property (nonatomic,retain) IBOutlet UITableView *mainTableView;
@property (nonatomic,retain) IBOutlet UILabel *lblNoData;
@property (nonatomic,retain) IBOutlet UIButton *btnAddClubs;

@property (nonatomic,retain) IBOutlet UIView *statePickerContainerView;
@property (nonatomic,retain) IBOutlet UIView *statePickerBlackTransparentView;
@property (nonatomic,retain) IBOutlet UIView *statePickerInnerContainerView;
@property (nonatomic,retain) IBOutlet UILabel *lblStatePickerTitle;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewCloseStatePickerContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnCloseStatePickerContainerView;
@property (nonatomic,retain) IBOutlet UITableView *statePickerTableView;
@property (nonatomic,retain) IBOutlet UIButton *btnGoInStatePickerContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnSelectAllStatesInPickerContainerView;
@property (nonatomic,retain) IBOutlet UIButton *btnDeselectAllStatesInPickerContainerView;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) NSMutableArray *dataRows;
@property (nonatomic,retain) NSMutableArray *arraySelectedStatesIds;

@property (nonatomic,retain) NSMutableArray *arraySelectedManagerIds;

@end
