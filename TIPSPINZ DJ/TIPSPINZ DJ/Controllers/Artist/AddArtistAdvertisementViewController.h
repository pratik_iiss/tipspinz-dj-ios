//
//  AddArtistAdvertisementViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 31/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface AddArtistAdvertisementViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewBack;
@property (nonatomic,retain) IBOutlet UIButton *btnBack;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;

@property (nonatomic,retain) IBOutlet AsyncImageView *imageViewAdvertisement;
@property (nonatomic,retain) IBOutlet UIButton *btnAddAdvertisementImage;

@property (nonatomic,retain) IBOutlet UILabel *lblRedirectionUrl;
@property (nonatomic,retain) IBOutlet UITextField *txtRedirectionUrl;

@property (nonatomic,retain) IBOutlet UIButton *btnAddAdvertisement;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) UIImage *imageSelectedPicture;
@property (nonatomic,retain) NSData *imageSelectedPictureData;

@end
