//
//  ChangeArtistPlaylistViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 31/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#import "Playlist.h"

#import "Manager.h"

@interface ChangeArtistPlaylistViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;

@property (nonatomic,retain) IBOutlet UILabel *lblPlaylistName;
@property (nonatomic,retain) IBOutlet UITextField *txtPlaylistName;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewPlaylistDownArrow;

@property (nonatomic,retain) IBOutlet UIButton *btnChangePlaylist;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) UIPickerView *playlistPickerView;
@property (nonatomic,retain) Playlist *objSelectedPlaylist;

@property (nonatomic,retain) Manager *objSelectedManager;

@end

