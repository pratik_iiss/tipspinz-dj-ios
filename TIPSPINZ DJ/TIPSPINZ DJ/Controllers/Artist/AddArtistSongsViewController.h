//
//  AddArtistSongsViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 31/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import "SideMenuViewController.h"

#import "Playlist.h"

@interface AddArtistSongsViewController : UIViewController<UIScrollViewDelegate, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIGestureRecognizerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;
@property (nonatomic,retain) IBOutlet UIScrollView *mainInnerScrollView;

@property (nonatomic,retain) IBOutlet UILabel *lblSelectPlaylist;
@property (nonatomic,retain) IBOutlet UIView *txtPlaylistContainerView;
@property (nonatomic,retain) IBOutlet UITextField *txtPlaylist;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewPlaylistDownArrow;

@property (nonatomic,retain) IBOutlet UILabel *lblSongName;
@property (nonatomic,retain) IBOutlet UITextField *txtSongName;

@property (nonatomic,retain) IBOutlet UILabel *lblArtistName;
@property (nonatomic,retain) IBOutlet UITextField *txtArtistName;

@property (nonatomic,retain) IBOutlet UILabel *lblSongType;
@property (nonatomic,retain) IBOutlet UITextField *txtSongType;

@property (nonatomic,retain) IBOutlet UIButton *btnAddSong;

//========== OTHER VARIABLES ==========//

@property (nonatomic,retain) UIPickerView *playlistPickerView;

@property (nonatomic,retain) Playlist *objSelectedPlaylist;

@end
