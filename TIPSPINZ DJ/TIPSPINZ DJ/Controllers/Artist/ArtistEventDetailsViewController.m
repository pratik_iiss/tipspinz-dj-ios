//
//  ArtistEventDetailsViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 31/05/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import "ArtistEventDetailsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

@interface ArtistEventDetailsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation ArtistEventDetailsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize navigationBarView;
@synthesize imageViewBack;
@synthesize btnBack;
@synthesize lblNavigationTitle;

@synthesize mainContainerView;
@synthesize mainInnerScrollView;

@synthesize imageViewLive;

@synthesize lblEventDate;
@synthesize lblEventName;
@synthesize lblEventDescription;

@synthesize bottomContainerView;

@synthesize lblPlaylist;
@synthesize txtPlaylistContainerView;
@synthesize txtPlaylist;
@synthesize imageViewPlaylistDownArrow;

@synthesize btnChangeStatus;
@synthesize btnGoOnlineWithoutPlaylist;

//========== OTHER VARIABLES ==========//

@synthesize playlistPickerView;

#pragma mark - View Controller Delegate Methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setNavigationBar];
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    if (self.objSelectedEvent.boolIsEventLive)
    {
        imageViewLive.image = [UIImage imageNamed:@"event_online.png"];
    }
    else
    {
        imageViewLive.image = [UIImage imageNamed:@"event_offline.png"];
    }
    
    lblEventDate.text = [NSString stringWithFormat:@"%@", self.objSelectedEvent.strEventDate];
    
    lblEventName.text = [NSString stringWithFormat:@"%@", self.objSelectedEvent.strEventName];
    
    lblEventDescription.text = [NSString stringWithFormat:@"%@", self.objSelectedEvent.strEventDescription];
    [lblEventDescription sizeToFit];
    
    CGFloat lblEventDescriptionHeight;
    
    if(self.objSelectedEvent.strEventDescription.length > 0)
    {
        CGRect lblEventDescriptionRect = [lblEventDescription.text boundingRectWithSize:lblEventDescription.frame.size
                                                                                options:NSStringDrawingUsesLineFragmentOrigin
                                                                             attributes:@{NSFontAttributeName:lblEventDescription.font}
                                                                                context:nil];
        
        CGSize lblEventDescriptionSize = lblEventDescriptionRect.size;
        
        lblEventDescriptionHeight = lblEventDescriptionSize.height;
    }
    else
    {
        lblEventDescriptionHeight = 0;
    }
    
    CGRect lblEventDescriptionFrame = lblEventDescription.frame;
    lblEventDescriptionFrame.size.height = lblEventDescriptionHeight;
    lblEventDescription.frame = lblEventDescriptionFrame;
    
    CGRect bottomContainerViewFrame = bottomContainerView.frame;
    bottomContainerViewFrame.origin.y = lblEventDescription.frame.origin.y + lblEventDescriptionHeight + 20;
    bottomContainerView.frame = bottomContainerViewFrame;
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, bottomContainerView.frame.origin.y + bottomContainerView.frame.size.height + 10);
}

#pragma mark - Setup Notification Methods

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotArtistStatusForEventEvent) name:@"gotArtistStatusForEventEvent" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changedArtistEventStatusEvent) name:@"changedArtistEventStatusEvent" object:nil];
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)gotArtistStatusForEventEvent
{
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineAtEvent == true)
    {
        bottomContainerView.hidden = true;
        mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, lblEventDescription.frame.origin.y + lblEventDescription.frame.size.height + 10);
    }
    else
    {
        if ([MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId.length > 0)
        {
            if ([self.objSelectedEvent.strEventID isEqualToString:[MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId])
            {
                bottomContainerView.hidden = false;
            }
            else
            {
                bottomContainerView.hidden = true;
                mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, lblEventDescription.frame.origin.y + lblEventDescription.frame.size.height + 10);
            }
        }
        else
        {
            [btnChangeStatus setTitle:@"GO ONLINE" forState:UIControlStateNormal];
        }
    }
}

-(void)changedArtistEventStatusEvent
{
    [MySingleton sharedManager].dataManager.boolIsEventStatusChangedSuccessfully = true;
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineAtEvent == true)
    {
        imageViewLive.image = [UIImage imageNamed:@"event_online.png"];
        
        [btnChangeStatus setTitle:@"GO OFFLINE" forState:UIControlStateNormal];
        
        txtPlaylist.text = [MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventPlaylistName;
        txtPlaylist.userInteractionEnabled = false;
        
        NSMutableArray *arrayPlaylistIds = [[NSMutableArray alloc] init];
        
        for(int i = 0 ; i < [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists.count; i++)
        {
            Playlist *objPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists objectAtIndex:i];
            
            [arrayPlaylistIds addObject:objPlaylist.strPlaylistID];
        }
        
        NSInteger indexOfPlaylistId = [arrayPlaylistIds indexOfObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventPlaylistId];
        
        if(NSNotFound != indexOfPlaylistId)
        {
            self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists objectAtIndex:indexOfPlaylistId];
            [playlistPickerView selectRow:indexOfPlaylistId inComponent:0 animated:NO];
            [playlistPickerView reloadAllComponents];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Status Changed" withDetails:@"Your status has been changed successfully."];
        });
    }
    else
    {
        imageViewLive.image = [UIImage imageNamed:@"event_offline.png"];
        
        [btnChangeStatus setTitle:@"GO ONLINE" forState:UIControlStateNormal];
        
        txtPlaylist.userInteractionEnabled = true;
        txtPlaylist.text = @"";
        
        self.objSelectedPlaylist = nil;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [appDelegate showErrorAlertViewWithTitle:@"Status Changed" withDetails:@"Your status has been changed successfully."];
        });
    }
}

#pragma mark - Navigation Bar Methods

-(void)setNavigationBar
{
    navigationBarView.backgroundColor = [MySingleton sharedManager].navigationBarBackgroundColor;
    
    imageViewBack.layer.masksToBounds = YES;
    [btnBack addTarget:self action:@selector(btnBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    lblNavigationTitle.text = [NSString stringWithFormat:@"Event Details"];
    lblNavigationTitle.textColor = [MySingleton sharedManager].navigationBarTitleColor;
    lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleFont;
    if(([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568) && lblNavigationTitle.text.length > 18)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if(([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812) && lblNavigationTitle.text.length > 20)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
    else if([MySingleton sharedManager].screenHeight >= 736 && lblNavigationTitle.text.length > 22)
    {
        lblNavigationTitle.font = [MySingleton sharedManager].navigationBarTitleSmallFont;
    }
}

-(IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    UIFont *txtFieldFont, *btnFont, *lblEventDateFont, *lblEventNameFont, *lblEventDescriptionFont, *lblPlaylistTitleFont, *btnGoOnlineWithoutPlaylistFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        lblEventDateFont = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        lblEventNameFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        lblEventDescriptionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblPlaylistTitleFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnGoOnlineWithoutPlaylistFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        txtFieldFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
        lblEventDateFont = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        lblEventNameFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        lblEventDescriptionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblPlaylistTitleFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnGoOnlineWithoutPlaylistFont = [MySingleton sharedManager].themeFontFifteenSizeRegular;
    }
    else
    {
        txtFieldFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
        lblEventDateFont = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        lblEventNameFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
        lblEventDescriptionFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        lblPlaylistTitleFont = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        btnGoOnlineWithoutPlaylistFont = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    imageViewLive.layer.masksToBounds = true;
    
    lblEventDate.font = lblEventDateFont;
    lblEventDate.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    lblEventName.font = lblEventNameFont;
    lblEventName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    lblEventDescription.font = lblEventDescriptionFont;
    lblEventDescription.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
    
    bottomContainerView.layer.masksToBounds = YES;
    bottomContainerView.layer.cornerRadius = 5.0f;
    bottomContainerView.layer.borderWidth = 1.0f;
    bottomContainerView.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
    
    lblPlaylist.font = lblPlaylistTitleFont;
    lblPlaylist.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
    
    txtPlaylistContainerView.layer.masksToBounds = YES;
    txtPlaylistContainerView.layer.cornerRadius = 5.0f;
    txtPlaylistContainerView.layer.borderWidth = 1.0f;
    txtPlaylistContainerView.layer.borderColor = [MySingleton sharedManager].textfieldBorderColor.CGColor;
    
    playlistPickerView = [[UIPickerView alloc] init];
    playlistPickerView.delegate = self;
    playlistPickerView.dataSource = self;
    playlistPickerView.showsSelectionIndicator = YES;
    playlistPickerView.tag = 1;
    playlistPickerView.backgroundColor = [UIColor whiteColor];
    
    txtPlaylist.font = txtFieldFont;
    txtPlaylist.delegate = self;
    [txtPlaylist setValue:[MySingleton sharedManager].textfieldPlaceholderColor
               forKeyPath:@"placeholderLabel.textColor"];
    txtPlaylist.textColor = [MySingleton sharedManager].textfieldTextColor;
    txtPlaylist.tintColor = [MySingleton sharedManager].textfieldTextColor;
    [txtPlaylist setInputView:playlistPickerView];
    [txtPlaylist setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    imageViewPlaylistDownArrow.layer.masksToBounds = YES;
    imageViewPlaylistDownArrow.userInteractionEnabled = true;
    UITapGestureRecognizer *imageViewPlaylistDownArrowTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewPlaylistDownArrowTapped:)];
    imageViewPlaylistDownArrowTapGesture.delegate = self;
    [imageViewPlaylistDownArrow addGestureRecognizer:imageViewPlaylistDownArrowTapGesture];
    
    btnChangeStatus.backgroundColor = [MySingleton sharedManager].themeGlobalLightBlueColor;
    btnChangeStatus.layer.masksToBounds = true;
    btnChangeStatus.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnChangeStatus.titleLabel.font = btnFont;
    [btnChangeStatus setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnChangeStatus addTarget:self action:@selector(btnChangeStatusClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnGoOnlineWithoutPlaylist.layer.masksToBounds = true;
    btnGoOnlineWithoutPlaylist.titleLabel.font = btnGoOnlineWithoutPlaylistFont;
    [btnGoOnlineWithoutPlaylist setTitleColor:[MySingleton sharedManager].themeGlobalDarkBlueColor forState:UIControlStateNormal];
    [btnGoOnlineWithoutPlaylist addTarget:self action:@selector(btnGoOnlineWithoutPlaylistClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUserId = [NSString stringWithFormat:@"%@", [prefs objectForKey:@"userid"]];
    
    [dictParameters setObject:strUserId forKey:@"user_id"];
    [dictParameters setObject:self.objSelectedEvent.strEventID forKey:@"event_id"];
    
    [[MySingleton sharedManager].dataManager getArtistStatusForEvent:dictParameters];
}

- (void)imageViewPlaylistDownArrowTapped: (UITapGestureRecognizer *)recognizer
{
    [txtPlaylist becomeFirstResponder];
}

#pragma mark - UITextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if(textField == txtPlaylist)
    {
        if(txtPlaylist.text.length == 0 )
        {
            self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists objectAtIndex:0];
            txtPlaylist.text = self.objSelectedPlaylist.strPlaylistName;
            [playlistPickerView selectRow:0 inComponent:0 animated:YES];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

#pragma mark - UIPickerView Delegate Methods

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger rowsInComponent;
    
    if(pickerView.tag == 1)
    {
        rowsInComponent = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists count];
    }
    
    return rowsInComponent;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel* lblMain = (UILabel*)view;
    if (!lblMain){
        lblMain = [[UILabel alloc] init];
        // Setup label properties - frame, font, colors etc
    }
    
    if (pickerView == playlistPickerView)
    {
        Playlist *objPlaylist = [MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists[row];
        lblMain.text = objPlaylist.strPlaylistName;
        lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeRegular;
    }
    
    lblMain.textAlignment = NSTextAlignmentCenter;
    return lblMain;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return [MySingleton sharedManager].screenWidth;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if(pickerView.tag == 1)
    {
        self.objSelectedPlaylist = [[MySingleton sharedManager].dataManager.objLoggedInUser.arrayArtistPlaylists objectAtIndex:row];
        txtPlaylist.text = self.objSelectedPlaylist.strPlaylistName;
    }
}

#pragma mark - Other Methods

-(IBAction)btnChangeStatusClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineAtEvent == true)
    {
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserId = [NSString stringWithFormat:@"%@", [prefs objectForKey:@"userid"]];
        
        [dictParameters setObject:strUserId forKey:@"user_id"];
        [dictParameters setObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventId forKey:@"event_id"];
        [dictParameters setObject:[MySingleton sharedManager].dataManager.objLoggedInUser.strOnlineEventPlaylistId forKey:@"playlist_id"];
        [dictParameters setObject:@"0" forKey:@"is_online"];
        
        [[MySingleton sharedManager].dataManager changeArtistEventStatus:dictParameters];
    }
    else
    {
        if(self.objSelectedPlaylist != nil)
        {
            NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
            
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strUserId = [NSString stringWithFormat:@"%@", [prefs objectForKey:@"userid"]];
            
            [dictParameters setObject:strUserId forKey:@"user_id"];
            [dictParameters setObject:self.objSelectedEvent.strEventID forKey:@"event_id"];
            [dictParameters setObject:self.objSelectedPlaylist.strPlaylistID forKey:@"playlist_id"];
            [dictParameters setObject:@"1" forKey:@"is_online"];
            
            [[MySingleton sharedManager].dataManager changeArtistEventStatus:dictParameters];
        }
        else
        {
            if(self.objSelectedPlaylist == nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [appDelegate showErrorAlertViewWithTitle:nil withDetails:@"You must select a playlist"];
                });
            }
        }
    }
}

-(IBAction)btnGoOnlineWithoutPlaylistClicked:(id)sender
{
    [self.view endEditing:YES];
    
    if([MySingleton sharedManager].dataManager.objLoggedInUser.boolIsOnlineAtEvent == true)
    {
        [appDelegate showErrorAlertViewWithTitle:@"" withDetails:@"You are already online at an event. Please go offline with event first."];
    }
    else
    {
        NSMutableDictionary *dictParameters = [[NSMutableDictionary alloc] init];
        
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUserId = [NSString stringWithFormat:@"%@", [prefs objectForKey:@"userid"]];
        
        [dictParameters setObject:strUserId forKey:@"user_id"];
        [dictParameters setObject:self.objSelectedEvent.strEventID forKey:@"event_id"];
        [dictParameters setObject:@"" forKey:@"playlist_id"];
        [dictParameters setObject:@"1" forKey:@"is_online"];
        
        [[MySingleton sharedManager].dataManager changeArtistEventStatus:dictParameters];
    }
}

@end
