//
//  LoginOptionsViewController.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 21/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "LoginOptionsViewController.h"
#import "MySingleton.h"

#import "IQKeyboardManager.h"

#import "LoginViewController.h"
#import "LoginClubOwnerViewController.h"

#import "ArtistLoginViewController.h"
#import "LoginCompanyViewController.h"

@interface LoginOptionsViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL boolIsSetupNotificationEventCalledOnce;
}

@end

@implementation LoginOptionsViewController

//========== IBOUTLETS ==========//

@synthesize mainScrollView;

@synthesize mainContainerView;
@synthesize mainBackgroundImageView;

@synthesize imageViewMainLogo;

@synthesize lblInstructions;

@synthesize btnClubOwner;
@synthesize btnDj;

@synthesize btnCompany;
@synthesize btnArtist;

//========== OTHER VARIABLES ==========//

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupNotificationEvent];
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupNotificationEvent];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self.view endEditing:YES];
    
    [self removeNotificationEventObserver];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:true];
    [[IQKeyboardManager sharedManager] setEnable:true];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupNotificationEvent
{
    if(boolIsSetupNotificationEventCalledOnce == false)
    {
        boolIsSetupNotificationEventCalledOnce = true;
    }
}

-(void)removeNotificationEventObserver
{
    boolIsSetupNotificationEventCalledOnce = false;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Layout Subviews Methods

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
}

#pragma mark - UI Setup Method

- (void)setupInitialView
{
    if (@available(iOS 11.0, *))
    {
        mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else
    {
        self.automaticallyAdjustsScrollViewInsets = false;
    }
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    mainScrollView.delegate = self;
    
    UIFont *lblInstructionsFont, *btnFont;
    
    if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFourteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontFifteenSizeBold;
    }
    else if([MySingleton sharedManager].screenHeight >= 736)
    {
        lblInstructionsFont = [MySingleton sharedManager].themeFontThirteenSizeRegular;
        btnFont = [MySingleton sharedManager].themeFontSixteenSizeBold;
    }
    
    mainBackgroundImageView.layer.masksToBounds = true;
    
    lblInstructions.font = lblInstructionsFont;
    lblInstructions.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
    
    btnClubOwner.layer.masksToBounds = true;
    btnClubOwner.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnClubOwner.layer.borderWidth = 1.0f;
    btnClubOwner.layer.borderColor = [MySingleton sharedManager].themeGlobalWhiteColor.CGColor;
    btnClubOwner.titleLabel.font = btnFont;
    [btnClubOwner setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnClubOwner addTarget:self action:@selector(btnClubOwnerClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnDj.layer.masksToBounds = true;
    btnDj.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnDj.layer.borderWidth = 1.0f;
    btnDj.layer.borderColor = [MySingleton sharedManager].themeGlobalWhiteColor.CGColor;
    btnDj.titleLabel.font = btnFont;
    [btnDj setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnDj addTarget:self action:@selector(btnDjClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnCompany.layer.masksToBounds = true;
    btnCompany.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnCompany.layer.borderWidth = 1.0f;
    btnCompany.layer.borderColor = [MySingleton sharedManager].themeGlobalWhiteColor.CGColor;
    btnCompany.titleLabel.font = btnFont;
    [btnCompany setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnCompany addTarget:self action:@selector(btnCompanyClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    btnArtist.layer.masksToBounds = true;
    btnArtist.layer.cornerRadius = [MySingleton sharedManager].floatButtonCornerRadius;
    btnArtist.layer.borderWidth = 1.0f;
    btnArtist.layer.borderColor = [MySingleton sharedManager].themeGlobalWhiteColor.CGColor;
    btnArtist.titleLabel.font = btnFont;
    [btnArtist setTitleColor:[MySingleton sharedManager].themeGlobalWhiteColor forState:UIControlStateNormal];
    [btnArtist addTarget:self action:@selector(btnArtistClicked:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Other Methods

-(void)doneClicked:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)btnClubOwnerClicked:(id)sender
{
    [self.view endEditing:true];
    
    LoginClubOwnerViewController *viewController = [[LoginClubOwnerViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)btnDjClicked:(id)sender
{
    [self.view endEditing:true];
    
    LoginViewController *viewController = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)btnCompanyClicked:(id)sender
{
    [self.view endEditing:true];
    
    LoginCompanyViewController *viewController = [[LoginCompanyViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (IBAction)btnArtistClicked:(id)sender
{
    [self.view endEditing:true];
    
    ArtistLoginViewController *viewController = [[ArtistLoginViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
