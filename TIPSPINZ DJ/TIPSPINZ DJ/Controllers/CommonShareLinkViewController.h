//
//  CommonShareLinkViewController.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 05/06/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommonShareLinkViewController : UIViewController<UIScrollViewDelegate, UIGestureRecognizerDelegate>

//========== IBOUTLETS ==========//

@property (nonatomic,retain) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic,retain) IBOutlet UIView *navigationBarView;
@property (nonatomic,retain) IBOutlet UIImageView *imageViewMenu;
@property (nonatomic,retain) IBOutlet UIButton *btnMenu;
@property (nonatomic,retain) IBOutlet UILabel *lblNavigationTitle;

@property (nonatomic,retain) IBOutlet UIView *mainContainerView;

@property (nonatomic,retain) IBOutlet UILabel *lblInstructions;

@property (nonatomic,retain) IBOutlet UILabel *lblShareUrl;
@property (nonatomic,retain) IBOutlet UILabel *lblTapToCopy;

@property (nonatomic,retain) IBOutlet UIButton *btnShareLink;

//========== OTHER VARIABLES ==========//

@end
