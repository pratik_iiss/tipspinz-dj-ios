//
//  SideBarTableViewHeader.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 03/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "SideBarTableViewHeader.h"
#import "MySingleton.h"

@implementation SideBarTableViewHeader

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:self.frame];
        self.mainContainer.backgroundColor = [UIColor clearColor];
        
        //======= ADD MAIN IMAGE VIEW INTO BOX =======//
        self.imageViewMain = [[AsyncImageView alloc]initWithFrame:CGRectMake(30, 10, 50, 50)];
        self.imageViewMain.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewMain.layer.masksToBounds = YES;
        self.imageViewMain.layer.cornerRadius = (self.imageViewMain.frame.size.width / 2);
        [self.mainContainer addSubview:self.imageViewMain];
        
        //======== ADD MAIN LABEL INTO BOX ========//
        self.lblMain = [[UILabel alloc]initWithFrame:CGRectMake(30, 70, self.mainContainer.frame.size.width - 30, 20)];
        self.lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeMedium;
        self.lblMain.textAlignment = NSTextAlignmentLeft;
        self.lblMain.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        [self.mainContainer addSubview:self.lblMain];
        
        //======== ADD CHANGE STATUS TO LABEL INTO BOX ========//
        self.lblChangeStatusTo = [[UILabel alloc]initWithFrame:CGRectMake(30, 100, self.mainContainer.frame.size.width - 180, 20)];
        self.lblChangeStatusTo.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        self.lblChangeStatusTo.textAlignment = NSTextAlignmentLeft;
        self.lblChangeStatusTo.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        self.lblChangeStatusTo.text = @"Status :";
        [self.mainContainer addSubview:self.lblChangeStatusTo];
        
        //======== ADD BUTTON ONLINE INTO BOX ========//
        self.btnOnline = [[UIButton alloc]initWithFrame:CGRectMake((self.lblChangeStatusTo.frame.origin.x + self.lblChangeStatusTo.frame.size.width + 10), 100, 55, 20)];
        [self.btnOnline setTitle:@"ONLINE" forState:UIControlStateNormal];
        self.btnOnline.titleLabel.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
        [self.btnOnline setTitleColor:[MySingleton sharedManager].themeGlobalOfflineGreyColor forState:UIControlStateNormal];
        [self.btnOnline setTitleColor:[MySingleton sharedManager].themeGlobalOnlineGreenColor forState:UIControlStateSelected];
        self.btnOnline.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.btnOnline];
        
        //======== ADD BUTTON OFFLINE INTO BOX ========//
        self.btnOffline = [[UIButton alloc]initWithFrame:CGRectMake((self.btnOnline.frame.origin.x + self.btnOnline.frame.size.width + 10), 100, 55, 20)];
        [self.btnOffline setTitle:@"OFFLINE" forState:UIControlStateNormal];
        self.btnOffline.titleLabel.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
        [self.btnOffline setTitleColor:[MySingleton sharedManager].themeGlobalOfflineGreyColor forState:UIControlStateNormal];
        [self.btnOffline setTitleColor:[MySingleton sharedManager].themeGlobalOfflineRedColor forState:UIControlStateSelected];
        self.btnOffline.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.btnOffline];
        
        //======= ADD SEPAROTOR VIEW INTO BOX =======//
        self.separatorView = [[UIView alloc]initWithFrame:CGRectMake(0,self.mainContainer.frame.size.height-1,self.mainContainer.frame.size.width, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        self.separatorView.alpha = 0.5f;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
    }
    
    return self;
}

- (id)initWithClubOwnerFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:self.frame];
        self.mainContainer.backgroundColor = [UIColor clearColor];
        
        //======= ADD MAIN IMAGE VIEW INTO BOX =======//
        self.imageViewMain = [[AsyncImageView alloc]initWithFrame:CGRectMake(30, 10, 50, 50)];
        self.imageViewMain.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewMain.layer.masksToBounds = YES;
        self.imageViewMain.layer.cornerRadius = (self.imageViewMain.frame.size.width / 2);
        [self.mainContainer addSubview:self.imageViewMain];
        
        //======== ADD MAIN LABEL INTO BOX ========//
        self.lblMain = [[UILabel alloc]initWithFrame:CGRectMake(30, 70, self.mainContainer.frame.size.width - 30, 20)];
        self.lblMain.font = [MySingleton sharedManager].themeFontSixteenSizeMedium;
        self.lblMain.textAlignment = NSTextAlignmentLeft;
        self.lblMain.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        [self.mainContainer addSubview:self.lblMain];
        
        //======= ADD SEPAROTOR VIEW INTO BOX =======//
        self.separatorView = [[UIView alloc]initWithFrame:CGRectMake(0,self.mainContainer.frame.size.height-1,self.mainContainer.frame.size.width, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        self.separatorView.alpha = 0.5f;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
    }
    
    return self;
}

@end
