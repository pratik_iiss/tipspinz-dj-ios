//
//  SideBarTableViewHeader.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 03/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface SideBarTableViewHeader : UIView

@property (nonatomic,retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewMain;
@property (nonatomic,retain) UILabel *lblMain;

@property (nonatomic,retain) UILabel *lblChangeStatusTo;
@property (nonatomic,retain) UIButton *btnOnline;
@property (nonatomic,retain) UIButton *btnOffline;

@property (nonatomic,retain) UIView *separatorView;

- (id)initWithClubOwnerFrame:(CGRect)frame;

@end
