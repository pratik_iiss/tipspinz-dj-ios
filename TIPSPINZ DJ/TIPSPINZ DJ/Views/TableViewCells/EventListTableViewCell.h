//
//  EventListTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/12/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface EventListTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIView *mainContainer;

@property (nonatomic, retain) UILabel *lblEventDate;
@property (nonatomic, retain) UILabel *lblEventName;
@property (nonatomic, retain) UILabel *lblEventDescription;

@property (nonatomic, retain) AsyncImageView *imageViewLive;

@property (nonatomic, retain) AsyncImageView *imageViewRightArrow;
@property (nonatomic, retain) UIButton *btnDelete;

@property (nonatomic, retain) UIView *separatorView;

@end
