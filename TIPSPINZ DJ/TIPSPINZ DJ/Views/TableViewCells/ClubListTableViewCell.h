//
//  ClubListTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 21/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface ClubListTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewClub;

@property (nonatomic, retain) UIView *blackTransparentView;

@property (nonatomic, retain) UILabel *lblClubName;
@property (nonatomic, retain) UILabel *lblClubType;

@property (nonatomic, retain) AsyncImageView *imageViewLocationPin;
@property (nonatomic, retain) UILabel *lblClubAddress;

@property (nonatomic, retain) AsyncImageView *imageViewCheckboxClubAdded;

@property (nonatomic, retain) UIView *separatorView;

- (id)initWithClubOwnerStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
