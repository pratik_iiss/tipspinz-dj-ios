//
//  MyEarningsSongRequestTableViewCell.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 26/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface MyEarningsSongRequestTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewSongIcon;

@property (nonatomic, retain) UILabel *lblSongName;
@property (nonatomic, retain) UILabel *lblSongArtistName;
@property (nonatomic, retain) UILabel *lblSongRequestTime;

@property (nonatomic, retain) UILabel *lblSongRequestAmount;

@property (nonatomic, retain) UIView *separatorView;

@end
