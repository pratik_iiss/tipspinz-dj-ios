//
//  DjRequestListTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 17/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "DjRequestListTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 80
#define CellBigHeight 130

@implementation DjRequestListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD DJ PROFILE PICTURE IMAGE VIEW INTO BOX =======//
        self.imageViewDjProfilePicture = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, (self.mainContainer.frame.size.height - 50)/2, 50, 50)];
        self.imageViewDjProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewDjProfilePicture.layer.masksToBounds = YES;
        self.imageViewDjProfilePicture.layer.cornerRadius = (self.imageViewDjProfilePicture.frame.size.height / 2);
        [self.mainContainer addSubview:self.imageViewDjProfilePicture];
        
        //======= ADD LABEL DJ NAME INTO BOX =======//
        self.lblDjName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10), 10, self.mainContainer.frame.size.width - (self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10) - 50, 30)];
        self.lblDjName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
        self.lblDjName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblDjName.textAlignment = NSTextAlignmentLeft;
        self.lblDjName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblDjName];
        
        //======= ADD LABEL CLUB NAME INTO BOX =======//
        self.lblClubName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10), 50, self.mainContainer.frame.size.width - (self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10) - 50, 20)];
        self.lblClubName.font = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        self.lblClubName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblClubName.textAlignment = NSTextAlignmentLeft;
        self.lblClubName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblClubName];
        
        //======== ADD BUTTON ACCEPT INTO BOX ========//
        self.btnAccept = [[UIButton alloc]initWithFrame:CGRectMake((self.lblDjName.frame.origin.x + self.lblDjName.frame.size.width + 10), ((self.mainContainer.frame.size.height - 60)-5)/2, 30, 30)];
        self.btnAccept.layer.masksToBounds = YES;
        [self.btnAccept setImage:[UIImage imageNamed:@"accept.png"] forState:UIControlStateNormal];
        [self.mainContainer addSubview:self.btnAccept];
        
        //======== ADD BUTTON DECLINE INTO BOX ========//
        self.btnDecline = [[UIButton alloc]initWithFrame:CGRectMake((self.lblDjName.frame.origin.x + self.lblDjName.frame.size.width + 10), (self.btnAccept.frame.origin.y + self.btnAccept.frame.size.height + 5), 30, 30)];
        self.btnDecline.layer.masksToBounds = YES;
        [self.btnDecline setImage:[UIImage imageNamed:@"decline.png"] forState:UIControlStateNormal];
        [self.mainContainer addSubview:self.btnDecline];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (id)initWithNewStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellBigHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD DJ PROFILE PICTURE IMAGE VIEW INTO BOX =======//
        self.imageViewDjProfilePicture = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.imageViewDjProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewDjProfilePicture.layer.masksToBounds = YES;
        self.imageViewDjProfilePicture.layer.cornerRadius = (self.imageViewDjProfilePicture.frame.size.height / 2);
        [self.mainContainer addSubview:self.imageViewDjProfilePicture];
        
        //======= ADD LABEL DJ NAME INTO BOX =======//
        self.lblDjName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10), 10, self.mainContainer.frame.size.width - (self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10) - 15, 30)];
        self.lblDjName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
        self.lblDjName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblDjName.textAlignment = NSTextAlignmentLeft;
        self.lblDjName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblDjName];
        
        //======= ADD LABEL CLUB NAME INTO BOX =======//
        self.lblClubName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10), 50, self.mainContainer.frame.size.width - (self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10) - 15, 20)];
        self.lblClubName.font = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        self.lblClubName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblClubName.textAlignment = NSTextAlignmentLeft;
        self.lblClubName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblClubName];
        
        //======= ADD BUTTON ACCEPT CONTAINER VIEW =======//
        self.btnAcceptContainerView = [[UIView alloc]initWithFrame:CGRectMake(15, (self.lblClubName.frame.origin.y + self.lblClubName.frame.size.height + 10), ((self.mainContainer.frame.size.width - 40)/2), 40)];
        self.btnAcceptContainerView.layer.masksToBounds = YES;
        self.btnAcceptContainerView.layer.cornerRadius = 5.0f;
        self.btnAcceptContainerView.layer.borderWidth = 1.0f;
        self.btnAcceptContainerView.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        self.btnAcceptContainerView.backgroundColor =  [UIColor clearColor];
        
        //======= ADD ACCEPT IMAGE VIEW ACCEPT CONTAINER VIEW =======//
        self.imageViewAccept = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 5, 30, 30)];
        self.imageViewAccept.image = [UIImage imageNamed:@"accept_dj_my_request.png"];
        self.imageViewAccept.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewAccept.layer.masksToBounds = YES;
        [self.btnAcceptContainerView addSubview:self.imageViewAccept];
        
        //======= ADD LABEL ACCEPT INTO ACCEPT CONTAINER VIEW =======//
        self.lblAccept = [[UILabel alloc] initWithFrame:CGRectMake(35, 5, (self.btnAcceptContainerView.frame.size.width - 50), 30)];
        self.lblAccept.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        self.lblAccept.textColor = [MySingleton sharedManager].themeGlobalDjRequestAcceptGreenColor;
        self.lblAccept.text = @"ACCEPT";
        self.lblAccept.textAlignment = NSTextAlignmentCenter;
        self.lblAccept.layer.masksToBounds = YES;
        [self.btnAcceptContainerView addSubview:self.lblAccept];
        
        //======== ADD BUTTON ACCEPT INTO BOX ========//
        self.btnAccept = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, (self.btnAcceptContainerView.frame.size.width), (self.btnAcceptContainerView.frame.size.height))];
        [self.btnAcceptContainerView addSubview:self.btnAccept];
        
        [self.mainContainer addSubview:self.btnAcceptContainerView];
        
        
        //======= ADD BUTTON DECLINE CONTAINER VIEW =======//
        self.btnDeclineContainerView = [[UIView alloc]initWithFrame:CGRectMake((self.btnAcceptContainerView.frame.origin.x + self.btnAcceptContainerView.frame.size.width + 10), (self.lblClubName.frame.origin.y + self.lblClubName.frame.size.height + 10), ((self.mainContainer.frame.size.width - 40)/2), 40)];
        self.btnDeclineContainerView.layer.masksToBounds = YES;
        self.btnDeclineContainerView.layer.cornerRadius = 5.0f;
        self.btnDeclineContainerView.layer.borderWidth = 1.0f;
        self.btnDeclineContainerView.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        self.btnDeclineContainerView.backgroundColor =  [UIColor clearColor];
        
        //======= ADD DECLINE IMAGE VIEW DECLINE CONTAINER VIEW =======//
        self.imageViewDecline = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 5, 30, 30)];
        self.imageViewDecline.image = [UIImage imageNamed:@"decline_dj_my_request.png"];
        self.imageViewDecline.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewDecline.layer.masksToBounds = YES;
        [self.btnDeclineContainerView addSubview:self.imageViewDecline];
        
        //======= ADD LABEL DECLINE INTO DECLINE CONTAINER VIEW =======//
        self.lblDecline = [[UILabel alloc] initWithFrame:CGRectMake(35, 5, (self.btnDeclineContainerView.frame.size.width - 50), 30)];
        self.lblDecline.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        self.lblDecline.textColor = [MySingleton sharedManager].themeGlobalDjRequestDeclineRedColor;
        self.lblDecline.text = @"DECLINE";
        self.lblDecline.textAlignment = NSTextAlignmentCenter;
        self.lblDecline.layer.masksToBounds = YES;
        [self.btnDeclineContainerView addSubview:self.lblDecline];
        
        //======== ADD BUTTON DECLINE INTO BOX ========//
        self.btnDecline = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, (self.btnDeclineContainerView.frame.size.width), (self.btnDeclineContainerView.frame.size.height))];
        [self.btnDeclineContainerView addSubview:self.btnDecline];
        
        [self.mainContainer addSubview:self.btnDeclineContainerView];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
