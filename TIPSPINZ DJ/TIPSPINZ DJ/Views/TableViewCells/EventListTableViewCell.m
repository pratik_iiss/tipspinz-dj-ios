//
//  EventListTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/12/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "EventListTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 90

@implementation EventListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD LIVE IMAGE VIEW INTO BOX =======//
        self.imageViewLive = [[AsyncImageView alloc]initWithFrame:CGRectMake(10, 10, 20, 20)];
        self.imageViewLive.image = [UIImage imageNamed:@"event_offline.png"];
        self.imageViewLive.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewLive.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewLive];
        
        //======= ADD LABEL EVENT DATE INTO BOX =======//
        self.lblEventDate = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, self.mainContainer.frame.size.width - 80, 20)];
        self.lblEventDate.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblEventDate.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblEventDate.textAlignment = NSTextAlignmentLeft;
        self.lblEventDate.numberOfLines = 1;
        self.lblEventDate.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblEventDate];
        
        //======= ADD LABEL EVENT NAME INTO BOX =======//
        self.lblEventName = [[UILabel alloc] initWithFrame:CGRectMake(10, (self.lblEventDate.frame.origin.y + self.lblEventDate.frame.size.height + 5), self.mainContainer.frame.size.width - 50, 20)];
        self.lblEventName.font = [MySingleton sharedManager].themeFontFourteenSizeBold;
        self.lblEventName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblEventName.textAlignment = NSTextAlignmentLeft;
        self.lblEventName.numberOfLines = 1;
        self.lblEventName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblEventName];
        
        //======= ADD LABEL EVENT DESCRIPTION INTO BOX =======//
        self.lblEventDescription = [[UILabel alloc] initWithFrame:CGRectMake(10, (self.lblEventName.frame.origin.y + self.lblEventName.frame.size.height + 5), self.mainContainer.frame.size.width - 50, 20)];
        self.lblEventDescription.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        self.lblEventDescription.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblEventDescription.textAlignment = NSTextAlignmentLeft;
        self.lblEventDescription.numberOfLines = 0;
        self.lblEventDescription.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblEventDescription];
        
        //======= ADD RIGHT ARROW IMAGE VIEW INTO BOX =======//
        self.imageViewRightArrow = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.mainContainer.frame.size.width - 30), (self.mainContainer.frame.size.height - 20)/2, 20, 20)];
        self.imageViewRightArrow.image = [UIImage imageNamed:@"delete_dj_event.png"];
        self.imageViewRightArrow.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewRightArrow.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewRightArrow];
        
        //======== ADD BUTTON ACCEPT INTO BOX ========//
        self.btnDelete = [[UIButton alloc]initWithFrame:CGRectMake((self.mainContainer.frame.size.width - 40), 0, (self.mainContainer.frame.size.width - (self.mainContainer.frame.size.width - 40)), self.mainContainer.frame.size.height)];
        self.btnDelete.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.btnDelete];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(0, self.mainContainer.frame.size.height-1, cellWidth, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
