//
//  MyEarningsShoutoutRequestTableViewCell.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 26/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "MyEarningsShoutoutRequestTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 110

@implementation MyEarningsShoutoutRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD SHOUTOUT ICON IMAGE VIEW INTO BOX =======//
        self.imageViewShoutoutIcon = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, (self.mainContainer.frame.size.height - 50)/2, 50, 50)];
        self.imageViewShoutoutIcon.image = [UIImage imageNamed:@"shoutout_request.png"];
        self.imageViewShoutoutIcon.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewShoutoutIcon.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewShoutoutIcon];
        
        //======= ADD LABEL SHOUTOUT OCCASION INTO BOX =======//
        self.lblShoutoutOccasion = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), 15, self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 10, 15)];
        self.lblShoutoutOccasion.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblShoutoutOccasion.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblShoutoutOccasion.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutOccasion.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutOccasion];
        
        //======= ADD LABEL SHOUTOUT FOR WHOM INTO BOX =======//
        self.lblShoutoutForWhom = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), (self.lblShoutoutOccasion.frame.origin.y + self.lblShoutoutOccasion.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 10, 12)];
        self.lblShoutoutForWhom.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblShoutoutForWhom.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutForWhom.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutForWhom.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutForWhom];
        
        //======= ADD LABEL SHOUTOUT NOTE COMMENTS INTO BOX =======//
        self.lblShoutoutNoteComments = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), (self.lblShoutoutForWhom.frame.origin.y + self.lblShoutoutForWhom.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 10, 24)];
        self.lblShoutoutNoteComments.numberOfLines = 0;
        self.lblShoutoutNoteComments.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblShoutoutNoteComments.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutNoteComments.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutNoteComments.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutNoteComments];
        
        //======= ADD LABEL SHOUTOUT REQUEST TIME INTO BOX =======//
        self.lblShoutoutRequestTime = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), (self.lblShoutoutNoteComments.frame.origin.y + self.lblShoutoutNoteComments.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 90, 12)];
        self.lblShoutoutRequestTime.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblShoutoutRequestTime.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutRequestTime.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutRequestTime.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutRequestTime];
        
        //======= ADD LABEL SHOUTOUT REQUEST AMOUNT INTO BOX =======//
        self.lblShoutoutRequestAmount = [[UILabel alloc] initWithFrame:CGRectMake((self.lblShoutoutRequestTime.frame.origin.x + self.lblShoutoutRequestTime.frame.size.width + 10), self.lblShoutoutRequestTime.frame.origin.y, self.mainContainer.frame.size.width - (self.lblShoutoutRequestTime.frame.origin.x + self.lblShoutoutRequestTime.frame.size.width + 10) - 10, 15)];
        self.lblShoutoutRequestAmount.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
        self.lblShoutoutRequestAmount.textColor = [MySingleton sharedManager].themeGlobalRequestAcceptedGreenColor;
        self.lblShoutoutRequestAmount.textAlignment = NSTextAlignmentRight;
        self.lblShoutoutRequestAmount.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutRequestAmount];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
