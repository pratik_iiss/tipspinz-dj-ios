//
//  UnacceptedSongRequestTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface UnacceptedSongRequestTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewSongIcon;

@property (nonatomic, retain) UILabel *lblSongName;
@property (nonatomic, retain) UILabel *lblSongArtistName;
@property (nonatomic, retain) UILabel *lblSongRequestTime;

@property (nonatomic, retain) UILabel *lblSongRequestTotalAmount;
@property (nonatomic, retain) UILabel *lblSongRequestDjAmount;

@property (nonatomic, retain) UIView *btnAcceptContainerView;
@property (nonatomic, retain) AsyncImageView *imageViewAccept;
@property (nonatomic, retain) UILabel *lblAccept;
@property (nonatomic, retain) UIButton *btnAccept;

@property (nonatomic, retain) UIView *btnDeclineContainerView;
@property (nonatomic, retain) AsyncImageView *imageViewDecline;
@property (nonatomic, retain) UILabel *lblDecline;
@property (nonatomic, retain) UIButton *btnDecline;

@property (nonatomic, retain) UIView *separatorView;

@end
