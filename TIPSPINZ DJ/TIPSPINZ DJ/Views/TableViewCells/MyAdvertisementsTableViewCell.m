//
//  MyAdvertisementsTableViewCell.m
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 23/01/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import "MyAdvertisementsTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 80

@implementation MyAdvertisementsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD MAIN IMAGE VIEW INTO BOX =======//
        self.imageViewMain = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.imageViewMain.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewMain.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewMain];
        
        //======= ADD LABEL MAIN INTO BOX =======//
        self.lblMain = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewMain.frame.origin.x + self.imageViewMain.frame.size.width + 10), 15, self.mainContainer.frame.size.width - (self.imageViewMain.frame.origin.x + self.imageViewMain.frame.size.width + 10) - 40, 50)];
        self.lblMain.font = [MySingleton sharedManager].themeFontFourteenSizeMedium;
        self.lblMain.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblMain.textAlignment = NSTextAlignmentLeft;
        self.lblMain.numberOfLines = 2;
        self.lblMain.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblMain];
        
        //======= ADD DELETE IMAGE VIEW INTO BOX =======//
        self.imageViewDelete = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.lblMain.frame.origin.x + self.lblMain.frame.size.width + 5), 30, 20, 20)];
        self.imageViewDelete.image = [UIImage imageNamed:@"delete_my_advertisement.png"];
        self.imageViewDelete.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewDelete.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewDelete];
        
        //======== ADD BUTTON DELETE INTO BOX ========//
        self.btnDelete = [[UIButton alloc]initWithFrame:CGRectMake(self.imageViewDelete.frame.origin.x, self.imageViewDelete.frame.origin.y, self.imageViewDelete.frame.size.width, self.imageViewDelete.frame.size.height)];
        [self.mainContainer addSubview:self.btnDelete];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
