//
//  MyPlaylistTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 10/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface MyPlaylistTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewPlaylistIcon;

@property (nonatomic, retain) UILabel *lblPlaylistName;

@property (nonatomic, retain) AsyncImageView *imageViewRightArrow;

@property (nonatomic, retain) AsyncImageView *imageViewCheckbox;

@property (nonatomic, retain) UIView *separatorView;

- (id)initWithEditingStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
