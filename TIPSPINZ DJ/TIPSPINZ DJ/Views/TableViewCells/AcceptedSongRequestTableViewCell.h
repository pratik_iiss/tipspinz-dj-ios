//
//  AcceptedSongRequestTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface AcceptedSongRequestTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewSongIcon;

@property (nonatomic, retain) UILabel *lblSongName;
@property (nonatomic, retain) UILabel *lblSongArtistName;
@property (nonatomic, retain) UILabel *lblSongRequestTime;

@property (nonatomic, retain) UILabel *lblSongRequestStatus;
@property (nonatomic, retain) UILabel *lblSongRequestAcceptanceType;

@property (nonatomic, retain) UIView *bottomContainer;

@property (nonatomic, retain) UIView *shareOnSocialMediaContainer;
@property (nonatomic, retain) UILabel *lblShareOnSocialMedia;
@property (nonatomic, retain) AsyncImageView *imageViewShareOnSocialMedia;
@property (nonatomic, retain) UIButton *btnShareOnSocialMedia;

@property (nonatomic, retain) UIView *playedContainer;
@property (nonatomic, retain) UILabel *lblPlayed;
@property (nonatomic, retain) AsyncImageView *imageViewPlayed;
@property (nonatomic, retain) UIButton *btnPlayed;

@property (nonatomic, retain) UIView *separatorView;

@end
