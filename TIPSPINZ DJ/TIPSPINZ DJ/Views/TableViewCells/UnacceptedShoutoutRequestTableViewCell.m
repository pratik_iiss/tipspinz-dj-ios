//
//  UnacceptedShoutoutRequestTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "UnacceptedShoutoutRequestTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 180

@implementation UnacceptedShoutoutRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD SHOUTOUT ICON IMAGE VIEW INTO BOX =======//
        self.imageViewShoutoutIcon = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.imageViewShoutoutIcon.image = [UIImage imageNamed:@"shoutout_request.png"];
        self.imageViewShoutoutIcon.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewShoutoutIcon.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewShoutoutIcon];
        
        //======= ADD LABEL SHOUTOUT OCCASION INTO BOX =======//
        self.lblShoutoutOccasion = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), 15, self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 15, 15)];
        self.lblShoutoutOccasion.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblShoutoutOccasion.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblShoutoutOccasion.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutOccasion.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutOccasion];
        
        //======= ADD LABEL SHOUTOUT FOR WHOM INTO BOX =======//
        self.lblShoutoutForWhom = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), (self.lblShoutoutOccasion.frame.origin.y + self.lblShoutoutOccasion.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 15, 12)];
        self.lblShoutoutForWhom.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblShoutoutForWhom.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutForWhom.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutForWhom.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutForWhom];
        
        //======= ADD LABEL SHOUTOUT NOTE COMMENTS INTO BOX =======//
        self.lblShoutoutNoteComments = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), (self.lblShoutoutForWhom.frame.origin.y + self.lblShoutoutForWhom.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 15, 24)];
        self.lblShoutoutNoteComments.numberOfLines = 0;
        self.lblShoutoutNoteComments.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblShoutoutNoteComments.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutNoteComments.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutNoteComments.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutNoteComments];
        
        //======= ADD LABEL SHOUTOUT REQUEST TIME INTO BOX =======//
        self.lblShoutoutRequestTime = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), (self.lblShoutoutNoteComments.frame.origin.y + self.lblShoutoutNoteComments.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 15, 12)];
        self.lblShoutoutRequestTime.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblShoutoutRequestTime.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutRequestTime.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutRequestTime.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutRequestTime];
        
        //======= ADD LABEL SHOUTOUT REQUEST TOTAL AMOUNT INTO BOX =======//
        self.lblShoutoutRequestTotalAmount = [[UILabel alloc] initWithFrame:CGRectMake(15, (self.lblShoutoutRequestTime.frame.origin.y + self.lblShoutoutRequestTime.frame.size.height + 5), (self.mainContainer.frame.size.width - 40)/2, 12)];
        self.lblShoutoutRequestTotalAmount.font = [MySingleton sharedManager].themeFontTenSizeBold;
        self.lblShoutoutRequestTotalAmount.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutRequestTotalAmount.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutRequestTotalAmount.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutRequestTotalAmount];
        
        //======= ADD LABEL SHOUTOUT REQUEST DJ AMOUNT INTO BOX =======//
        self.lblShoutoutRequestDjAmount = [[UILabel alloc] initWithFrame:CGRectMake((self.lblShoutoutRequestTotalAmount.frame.origin.x + self.lblShoutoutRequestTotalAmount.frame.size.width + 10), (self.lblShoutoutRequestTime.frame.origin.y + self.lblShoutoutRequestTime.frame.size.height + 5), (self.mainContainer.frame.size.width - 40)/2, 12)];
        self.lblShoutoutRequestDjAmount.font = [MySingleton sharedManager].themeFontTenSizeBold;
        self.lblShoutoutRequestDjAmount.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutRequestDjAmount.textAlignment = NSTextAlignmentRight;
        self.lblShoutoutRequestDjAmount.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutRequestDjAmount];
        
        //======= ADD BUTTON ACCEPT CONTAINER VIEW =======//
        self.btnAcceptContainerView = [[UIView alloc]initWithFrame:CGRectMake(15, (self.lblShoutoutRequestTime.frame.origin.y + self.lblShoutoutRequestTime.frame.size.height + 10), ((self.mainContainer.frame.size.width - 40)/2), 40)];
        self.btnAcceptContainerView.layer.masksToBounds = YES;
        self.btnAcceptContainerView.layer.cornerRadius = 5.0f;
        self.btnAcceptContainerView.layer.borderWidth = 1.0f;
        self.btnAcceptContainerView.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        self.btnAcceptContainerView.backgroundColor =  [UIColor clearColor];
        
        //======= ADD ACCEPT IMAGE VIEW ACCEPT CONTAINER VIEW =======//
        self.imageViewAccept = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 5, 30, 30)];
        self.imageViewAccept.image = [UIImage imageNamed:@"accept_dj_my_request.png"];
        self.imageViewAccept.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewAccept.layer.masksToBounds = YES;
        [self.btnAcceptContainerView addSubview:self.imageViewAccept];
        
        //======= ADD LABEL ACCEPT INTO ACCEPT CONTAINER VIEW =======//
        self.lblAccept = [[UILabel alloc] initWithFrame:CGRectMake(35, 5, (self.btnAcceptContainerView.frame.size.width - 50), 30)];
        self.lblAccept.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        self.lblAccept.textColor = [MySingleton sharedManager].themeGlobalDjRequestAcceptGreenColor;
        self.lblAccept.text = @"ACCEPT";
        self.lblAccept.textAlignment = NSTextAlignmentCenter;
        self.lblAccept.layer.masksToBounds = YES;
        [self.btnAcceptContainerView addSubview:self.lblAccept];
        
        //======== ADD BUTTON ACCEPT INTO BOX ========//
        self.btnAccept = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, (self.btnAcceptContainerView.frame.size.width), (self.btnAcceptContainerView.frame.size.height))];
        [self.btnAcceptContainerView addSubview:self.btnAccept];
        
        [self.mainContainer addSubview:self.btnAcceptContainerView];
        
        
        //======= ADD BUTTON DECLINE CONTAINER VIEW =======//
        self.btnDeclineContainerView = [[UIView alloc]initWithFrame:CGRectMake((self.btnAcceptContainerView.frame.origin.x + self.btnAcceptContainerView.frame.size.width + 10), (self.lblShoutoutRequestTime.frame.origin.y + self.lblShoutoutRequestTime.frame.size.height + 10), ((self.mainContainer.frame.size.width - 40)/2), 40)];
        self.btnDeclineContainerView.layer.masksToBounds = YES;
        self.btnDeclineContainerView.layer.cornerRadius = 5.0f;
        self.btnDeclineContainerView.layer.borderWidth = 1.0f;
        self.btnDeclineContainerView.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        self.btnDeclineContainerView.backgroundColor =  [UIColor clearColor];
        
        //======= ADD DECLINE IMAGE VIEW DECLINE CONTAINER VIEW =======//
        self.imageViewDecline = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 5, 30, 30)];
        self.imageViewDecline.image = [UIImage imageNamed:@"decline_dj_my_request.png"];
        self.imageViewDecline.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewDecline.layer.masksToBounds = YES;
        [self.btnDeclineContainerView addSubview:self.imageViewDecline];
        
        //======= ADD LABEL DECLINE INTO DECLINE CONTAINER VIEW =======//
        self.lblDecline = [[UILabel alloc] initWithFrame:CGRectMake(35, 5, (self.btnDeclineContainerView.frame.size.width - 50), 30)];
        self.lblDecline.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        self.lblDecline.textColor = [MySingleton sharedManager].themeGlobalDjRequestDeclineRedColor;
        self.lblDecline.text = @"DECLINE";
        self.lblDecline.textAlignment = NSTextAlignmentCenter;
        self.lblDecline.layer.masksToBounds = YES;
        [self.btnDeclineContainerView addSubview:self.lblDecline];
        
        //======== ADD BUTTON DECLINE INTO BOX ========//
        self.btnDecline = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, (self.btnDeclineContainerView.frame.size.width), (self.btnDeclineContainerView.frame.size.height))];
        [self.btnDeclineContainerView addSubview:self.btnDecline];
        
        [self.mainContainer addSubview:self.btnDeclineContainerView];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
