//
//  StatePickerTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 29/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface StatePickerTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewLocationPin;
@property (nonatomic, retain) UILabel *lblStateName;

@property (nonatomic, retain) AsyncImageView *imageViewCheckbox;

@property (nonatomic, retain) UIView *separatorView;

@end
