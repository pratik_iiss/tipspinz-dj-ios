//
//  UnacceptedSongRequestTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "UnacceptedSongRequestTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 130

@implementation UnacceptedSongRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD SONG ICON IMAGE VIEW INTO BOX =======//
        self.imageViewSongIcon = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.imageViewSongIcon.image = [UIImage imageNamed:@"music_icon.png"];
        self.imageViewSongIcon.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewSongIcon.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewSongIcon];
        
        //======= ADD LABEL SONG NAME INTO BOX =======//
        self.lblSongName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10), 15, self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 15, 15)];
        self.lblSongName.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblSongName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblSongName.textAlignment = NSTextAlignmentLeft;
        self.lblSongName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongName];
        
        //======= ADD LABEL SONG ARTIST NAME INTO BOX =======//
        self.lblSongArtistName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10), (self.lblSongName.frame.origin.y + self.lblSongName.frame.size.height + 5), (self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 20)/2, 12)];
        self.lblSongArtistName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblSongArtistName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblSongArtistName.textAlignment = NSTextAlignmentLeft;
        self.lblSongArtistName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongArtistName];
        
        //======= ADD LABEL SONG REQUEST TIME INTO BOX =======//
        self.lblSongRequestTime = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10), (self.lblSongArtistName.frame.origin.y + self.lblSongArtistName.frame.size.height + 5), (self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 20)/2, 12)];
        self.lblSongRequestTime.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblSongRequestTime.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblSongRequestTime.textAlignment = NSTextAlignmentLeft;
        self.lblSongRequestTime.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongRequestTime];
        
        //======= ADD LABEL SONG REQUEST TOTAL AMOUNT INTO BOX =======//
        self.lblSongRequestTotalAmount = [[UILabel alloc] initWithFrame:CGRectMake((self.lblSongArtistName.frame.origin.x + self.lblSongArtistName.frame.size.width + 5), (self.lblSongName.frame.origin.y + self.lblSongName.frame.size.height + 5), (self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 20)/2, 12)];
        self.lblSongRequestTotalAmount.font = [MySingleton sharedManager].themeFontTenSizeBold;
        self.lblSongRequestTotalAmount.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblSongRequestTotalAmount.textAlignment = NSTextAlignmentRight;
        self.lblSongRequestTotalAmount.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongRequestTotalAmount];
        
        //======= ADD LABEL SONG REQUEST DJ AMOUNT INTO BOX =======//
        self.lblSongRequestDjAmount = [[UILabel alloc] initWithFrame:CGRectMake((self.lblSongRequestTime.frame.origin.x + self.lblSongRequestTime.frame.size.width + 5), (self.lblSongArtistName.frame.origin.y + self.lblSongArtistName.frame.size.height + 5), (self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 20)/2, 12)];
        self.lblSongRequestDjAmount.font = [MySingleton sharedManager].themeFontTenSizeBold;
        self.lblSongRequestDjAmount.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblSongRequestDjAmount.textAlignment = NSTextAlignmentRight;
        self.lblSongRequestDjAmount.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongRequestDjAmount];
        
        //======= ADD BUTTON ACCEPT CONTAINER VIEW =======//
        self.btnAcceptContainerView = [[UIView alloc]initWithFrame:CGRectMake(15, (self.lblSongRequestTime.frame.origin.y + self.lblSongRequestTime.frame.size.height + 10), ((self.mainContainer.frame.size.width - 40)/2), 40)];
        self.btnAcceptContainerView.layer.masksToBounds = YES;
        self.btnAcceptContainerView.layer.cornerRadius = 5.0f;
        self.btnAcceptContainerView.layer.borderWidth = 1.0f;
        self.btnAcceptContainerView.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        self.btnAcceptContainerView.backgroundColor =  [UIColor clearColor];
        
        //======= ADD ACCEPT IMAGE VIEW ACCEPT CONTAINER VIEW =======//
        self.imageViewAccept = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 5, 30, 30)];
        self.imageViewAccept.image = [UIImage imageNamed:@"accept_dj_my_request.png"];
        self.imageViewAccept.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewAccept.layer.masksToBounds = YES;
        [self.btnAcceptContainerView addSubview:self.imageViewAccept];
        
        //======= ADD LABEL ACCEPT INTO ACCEPT CONTAINER VIEW =======//
        self.lblAccept = [[UILabel alloc] initWithFrame:CGRectMake(35, 5, (self.btnAcceptContainerView.frame.size.width - 50), 30)];
        self.lblAccept.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        self.lblAccept.textColor = [MySingleton sharedManager].themeGlobalDjRequestAcceptGreenColor;
        self.lblAccept.text = @"ACCEPT";
        self.lblAccept.textAlignment = NSTextAlignmentCenter;
        self.lblAccept.layer.masksToBounds = YES;
        [self.btnAcceptContainerView addSubview:self.lblAccept];
        
        //======== ADD BUTTON ACCEPT INTO BOX ========//
        self.btnAccept = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, (self.btnAcceptContainerView.frame.size.width), (self.btnAcceptContainerView.frame.size.height))];
        [self.btnAccept setUserInteractionEnabled:YES];
        [self.btnAcceptContainerView addSubview:self.btnAccept];
        
        [self.mainContainer addSubview:self.btnAcceptContainerView];
        
        
        //======= ADD BUTTON DECLINE CONTAINER VIEW =======//
        self.btnDeclineContainerView = [[UIView alloc]initWithFrame:CGRectMake((self.btnAcceptContainerView.frame.origin.x + self.btnAcceptContainerView.frame.size.width + 10), (self.lblSongRequestTime.frame.origin.y + self.lblSongRequestTime.frame.size.height + 10), ((self.mainContainer.frame.size.width - 40)/2), 40)];
        self.btnDeclineContainerView.layer.masksToBounds = YES;
        self.btnDeclineContainerView.layer.cornerRadius = 5.0f;
        self.btnDeclineContainerView.layer.borderWidth = 1.0f;
        self.btnDeclineContainerView.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkGreyColor.CGColor;
        self.btnDeclineContainerView.backgroundColor =  [UIColor clearColor];
        
        //======= ADD DECLINE IMAGE VIEW DECLINE CONTAINER VIEW =======//
        self.imageViewDecline = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 5, 30, 30)];
        self.imageViewDecline.image = [UIImage imageNamed:@"decline_dj_my_request.png"];
        self.imageViewDecline.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewDecline.layer.masksToBounds = YES;
        [self.btnDeclineContainerView addSubview:self.imageViewDecline];
        
        //======= ADD LABEL DECLINE INTO DECLINE CONTAINER VIEW =======//
        self.lblDecline = [[UILabel alloc] initWithFrame:CGRectMake(35, 5, (self.btnDeclineContainerView.frame.size.width - 50), 30)];
        self.lblDecline.font = [MySingleton sharedManager].themeFontTwelveSizeRegular;
        self.lblDecline.textColor = [MySingleton sharedManager].themeGlobalDjRequestDeclineRedColor;
        self.lblDecline.text = @"DECLINE";
        self.lblDecline.textAlignment = NSTextAlignmentCenter;
        self.lblDecline.layer.masksToBounds = YES;
        [self.btnDeclineContainerView addSubview:self.lblDecline];
        
        //======== ADD BUTTON DECLINE INTO BOX ========//
        self.btnDecline = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, (self.btnDeclineContainerView.frame.size.width), (self.btnDeclineContainerView.frame.size.height))];
        [self.btnDeclineContainerView addSubview:self.btnDecline];
        
        [self.mainContainer addSubview:self.btnDeclineContainerView];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
