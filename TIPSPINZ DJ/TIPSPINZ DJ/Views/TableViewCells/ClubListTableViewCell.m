//
//  ClubListTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 21/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "ClubListTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 150

@implementation ClubListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD CLUB IMAGE VIEW INTO BOX =======//
        self.imageViewClub = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, self.mainContainer.frame.size.width, self.mainContainer.frame.size.height)];
        self.imageViewClub.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewClub.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewClub];
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.blackTransparentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.mainContainer.frame.size.width, self.mainContainer.frame.size.height)];
        self.blackTransparentView.backgroundColor =  [UIColor blackColor];
        self.blackTransparentView.alpha = 0.5f;
        [self.mainContainer addSubview:self.blackTransparentView];
        
        //======= ADD LABEL CLUB NAME INTO BOX =======//
        self.lblClubName = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.mainContainer.frame.size.width - 20, 30)];
        self.lblClubName.font = [MySingleton sharedManager].themeFontEighteenSizeBold;
        self.lblClubName.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        self.lblClubName.textAlignment = NSTextAlignmentLeft;
        self.lblClubName.numberOfLines = 1;
        self.lblClubName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblClubName];
        
        //======= ADD LABEL CLUB TYPE INTO BOX =======//
        self.lblClubType = [[UILabel alloc] initWithFrame:CGRectMake(10, (self.lblClubName.frame.origin.y + self.lblClubName.frame.size.height + 5), self.mainContainer.frame.size.width - 20, 20)];
        self.lblClubType.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblClubType.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        self.lblClubType.textAlignment = NSTextAlignmentLeft;
        self.lblClubType.numberOfLines = 1;
        self.lblClubType.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblClubType];
        
        //======= ADD CLUB ADDRESS PIN IMAGE VIEW INTO BOX =======//
        self.imageViewLocationPin = [[AsyncImageView alloc]initWithFrame:CGRectMake(10, (self.mainContainer.frame.size.height - 40), 20, 20)];
        self.imageViewLocationPin.image = [UIImage imageNamed:@"location_pin.png"];
        self.imageViewLocationPin.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewLocationPin.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewLocationPin];
        
        //======= ADD LABEL CLUB ADDRESS INTO BOX =======//
        self.lblClubAddress = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewLocationPin.frame.origin.x + self.imageViewLocationPin.frame.size.width + 10), (self.mainContainer.frame.size.height - 50), (self.mainContainer.frame.size.width - (self.imageViewLocationPin.frame.origin.x + self.imageViewLocationPin.frame.size.width + 10))-40, 40)];
        self.lblClubAddress.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblClubAddress.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        self.lblClubAddress.textAlignment = NSTextAlignmentLeft;
        self.lblClubAddress.numberOfLines = 2;
        self.lblClubAddress.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblClubAddress];
        
        //======= ADD CHECKBOX CLUB ADDED IMAGE VIEW INTO BOX =======//
        self.imageViewCheckboxClubAdded = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.mainContainer.frame.size.width - 30), (self.mainContainer.frame.size.height - 20)/2, 20, 20)];
        self.imageViewCheckboxClubAdded.image = [UIImage imageNamed:@"checkbox_unchecked_club_added.png"];
        self.imageViewCheckboxClubAdded.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewCheckboxClubAdded.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewCheckboxClubAdded];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(0, self.mainContainer.frame.size.height-1, cellWidth, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (id)initWithClubOwnerStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD CLUB IMAGE VIEW INTO BOX =======//
        self.imageViewClub = [[AsyncImageView alloc]initWithFrame:CGRectMake(0, 0, self.mainContainer.frame.size.width, self.mainContainer.frame.size.height)];
        self.imageViewClub.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewClub.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewClub];
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.blackTransparentView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.mainContainer.frame.size.width, self.mainContainer.frame.size.height)];
        self.blackTransparentView.backgroundColor =  [UIColor blackColor];
        self.blackTransparentView.alpha = 0.5f;
        [self.mainContainer addSubview:self.blackTransparentView];
        
        //======= ADD LABEL CLUB NAME INTO BOX =======//
        self.lblClubName = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, self.mainContainer.frame.size.width - 20, 30)];
        self.lblClubName.font = [MySingleton sharedManager].themeFontEighteenSizeBold;
        self.lblClubName.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        self.lblClubName.textAlignment = NSTextAlignmentLeft;
        self.lblClubName.numberOfLines = 1;
        self.lblClubName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblClubName];
        
        //======= ADD LABEL CLUB TYPE INTO BOX =======//
        self.lblClubType = [[UILabel alloc] initWithFrame:CGRectMake(10, (self.lblClubName.frame.origin.y + self.lblClubName.frame.size.height + 5), self.mainContainer.frame.size.width - 20, 20)];
        self.lblClubType.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblClubType.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        self.lblClubType.textAlignment = NSTextAlignmentLeft;
        self.lblClubType.numberOfLines = 1;
        self.lblClubType.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblClubType];
        
        //======= ADD CLUB ADDRESS PIN IMAGE VIEW INTO BOX =======//
        self.imageViewLocationPin = [[AsyncImageView alloc]initWithFrame:CGRectMake(10, (self.mainContainer.frame.size.height - 40), 20, 20)];
        self.imageViewLocationPin.image = [UIImage imageNamed:@"location_pin.png"];
        self.imageViewLocationPin.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewLocationPin.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewLocationPin];
        
        //======= ADD LABEL CLUB ADDRESS INTO BOX =======//
        self.lblClubAddress = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewLocationPin.frame.origin.x + self.imageViewLocationPin.frame.size.width + 10), (self.mainContainer.frame.size.height - 50), (self.mainContainer.frame.size.width - (self.imageViewLocationPin.frame.origin.x + self.imageViewLocationPin.frame.size.width + 10))-40, 40)];
        self.lblClubAddress.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblClubAddress.textColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        self.lblClubAddress.textAlignment = NSTextAlignmentLeft;
        self.lblClubAddress.numberOfLines = 2;
        self.lblClubAddress.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblClubAddress];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(0, self.mainContainer.frame.size.height-1, cellWidth, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalWhiteColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
