//
//  AcceptedShoutoutRequestTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface AcceptedShoutoutRequestTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewShoutoutIcon;

@property (nonatomic, retain) UILabel *lblShoutoutOccasion;
@property (nonatomic, retain) UILabel *lblShoutoutForWhom;
@property (nonatomic, retain) UILabel *lblShoutoutNoteComments;
@property (nonatomic, retain) UILabel *lblShoutoutRequestTime;

@property (nonatomic, retain) UILabel *lblShoutoutRequestStatus;

@property (nonatomic, retain) UIView *bottomContainer;

@property (nonatomic, retain) UIView *shareOnSocialMediaContainer;
@property (nonatomic, retain) UILabel *lblShareOnSocialMedia;
@property (nonatomic, retain) AsyncImageView *imageViewShareOnSocialMedia;
@property (nonatomic, retain) UIButton *btnShareOnSocialMedia;

@property (nonatomic, retain) UIView *playedContainer;
@property (nonatomic, retain) UILabel *lblPlayed;
@property (nonatomic, retain) AsyncImageView *imageViewPlayed;
@property (nonatomic, retain) UIButton *btnPlayed;

@property (nonatomic, retain) UIView *separatorView;

@end
