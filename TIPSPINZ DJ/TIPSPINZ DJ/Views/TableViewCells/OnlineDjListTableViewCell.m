//
//  OnlineDjListTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 19/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "OnlineDjListTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 80

@implementation OnlineDjListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD DJ PROFILE PICTURE IMAGE VIEW INTO BOX =======//
        self.imageViewDjProfilePicture = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, (self.mainContainer.frame.size.height - 50)/2, 50, 50)];
        self.imageViewDjProfilePicture.contentMode = UIViewContentModeScaleAspectFill;
        self.imageViewDjProfilePicture.layer.masksToBounds = YES;
        self.imageViewDjProfilePicture.layer.cornerRadius = (self.imageViewDjProfilePicture.frame.size.height / 2);
        [self.mainContainer addSubview:self.imageViewDjProfilePicture];
        
        //======= ADD LABEL DJ NAME INTO BOX =======//
        self.lblDjName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10), 10, self.mainContainer.frame.size.width - (self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10) - 25, 30)];
        self.lblDjName.font = [MySingleton sharedManager].themeFontSixteenSizeBold;
        self.lblDjName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblDjName.textAlignment = NSTextAlignmentLeft;
        self.lblDjName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblDjName];
        
        //======= ADD LABEL CLUB NAME INTO BOX =======//
        self.lblClubName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10), 50, self.mainContainer.frame.size.width - (self.imageViewDjProfilePicture.frame.origin.x + self.imageViewDjProfilePicture.frame.size.width + 10) - 25, 20)];
        self.lblClubName.font = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        self.lblClubName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblClubName.textAlignment = NSTextAlignmentLeft;
        self.lblClubName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblClubName];
        
        //======= ADD ONLINE IMAGE VIEW INTO BOX =======//
        self.imageViewDjOnline = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.lblDjName.frame.origin.x + self.lblDjName.frame.size.width + 5), 20, 10, 10)];
        self.imageViewDjOnline.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewDjOnline.image = [UIImage imageNamed:@"dj_online.png"];
        self.imageViewDjOnline.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewDjOnline];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView = [[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
