//
//  MyEarningsShoutoutRequestTableViewCell.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 26/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface MyEarningsShoutoutRequestTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewShoutoutIcon;

@property (nonatomic, retain) UILabel *lblShoutoutOccasion;
@property (nonatomic, retain) UILabel *lblShoutoutForWhom;
@property (nonatomic, retain) UILabel *lblShoutoutNoteComments;
@property (nonatomic, retain) UILabel *lblShoutoutRequestTime;

@property (nonatomic, retain) UILabel *lblShoutoutRequestAmount;

@property (nonatomic, retain) UIView *separatorView;

@end
