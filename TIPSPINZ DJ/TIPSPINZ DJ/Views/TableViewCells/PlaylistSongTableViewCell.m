//
//  PlaylistSongTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 10/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "PlaylistSongTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 80

@implementation PlaylistSongTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD SONG ICON IMAGE VIEW INTO BOX =======//
        self.imageViewSongIcon = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.imageViewSongIcon.image = [UIImage imageNamed:@"music_icon.png"];
        self.imageViewSongIcon.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewSongIcon.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewSongIcon];
        
        //======= ADD LABEL SONG NAME INTO BOX =======//
        self.lblSongName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10), 24, self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 10, 15)];
        self.lblSongName.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblSongName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblSongName.textAlignment = NSTextAlignmentLeft;
        self.lblSongName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongName];
        
        //======= ADD LABEL SONG ARTIST NAME INTO BOX =======//
        self.lblSongArtistName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10), (self.lblSongName.frame.origin.y + self.lblSongName.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 10, 12)];
        self.lblSongArtistName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblSongArtistName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblSongArtistName.textAlignment = NSTextAlignmentLeft;
        self.lblSongArtistName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongArtistName];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (id)initWithEditingStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD SONG ICON IMAGE VIEW INTO BOX =======//
        self.imageViewSongIcon = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.imageViewSongIcon.image = [UIImage imageNamed:@"music_icon.png"];
        self.imageViewSongIcon.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewSongIcon.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewSongIcon];
        
        //======= ADD LABEL SONG NAME INTO BOX =======//
        self.lblSongName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10), 24, self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 40, 15)];
        self.lblSongName.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblSongName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblSongName.textAlignment = NSTextAlignmentLeft;
        self.lblSongName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongName];
        
        //======= ADD LABEL SONG ARTIST NAME INTO BOX =======//
        self.lblSongArtistName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10), (self.lblSongName.frame.origin.y + self.lblSongName.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 40, 12)];
        self.lblSongArtistName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblSongArtistName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblSongArtistName.textAlignment = NSTextAlignmentLeft;
        self.lblSongArtistName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongArtistName];
        
        //======= ADD CHECKBOX IMAGE VIEW INTO BOX =======//
        self.imageViewCheckbox = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.mainContainer.frame.size.width - 30), (self.mainContainer.frame.size.height - 20)/2, 20, 20)];
        self.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
        self.imageViewCheckbox.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewCheckbox.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewCheckbox];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
