//
//  UnacceptedShoutoutRequestTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface UnacceptedShoutoutRequestTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewShoutoutIcon;

@property (nonatomic, retain) UILabel *lblShoutoutOccasion;
@property (nonatomic, retain) UILabel *lblShoutoutForWhom;
@property (nonatomic, retain) UILabel *lblShoutoutNoteComments;
@property (nonatomic, retain) UILabel *lblShoutoutRequestTime;

@property (nonatomic, retain) UILabel *lblShoutoutRequestTotalAmount;
@property (nonatomic, retain) UILabel *lblShoutoutRequestDjAmount;

@property (nonatomic, retain) UIView *btnAcceptContainerView;
@property (nonatomic, retain) AsyncImageView *imageViewAccept;
@property (nonatomic, retain) UILabel *lblAccept;
@property (nonatomic, retain) UIButton *btnAccept;

@property (nonatomic, retain) UIView *btnDeclineContainerView;
@property (nonatomic, retain) AsyncImageView *imageViewDecline;
@property (nonatomic, retain) UILabel *lblDecline;
@property (nonatomic, retain) UIButton *btnDecline;

@property (nonatomic, retain) UIView *separatorView;

@end
