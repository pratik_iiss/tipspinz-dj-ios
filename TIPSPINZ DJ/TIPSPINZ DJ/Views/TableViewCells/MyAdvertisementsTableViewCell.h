//
//  MyAdvertisementsTableViewCell.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 23/01/18.
//  Copyright © 2018 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface MyAdvertisementsTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewMain;

@property (nonatomic, retain) UILabel *lblMain;

@property (nonatomic, retain) AsyncImageView *imageViewDelete;
@property (nonatomic, retain) UIButton *btnDelete;

@property (nonatomic, retain) UIView *separatorView;

@end
