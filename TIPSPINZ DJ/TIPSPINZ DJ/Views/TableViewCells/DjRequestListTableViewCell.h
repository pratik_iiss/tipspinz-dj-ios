//
//  DjRequestListTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 17/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface DjRequestListTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewDjProfilePicture;
@property (nonatomic, retain) UILabel *lblDjName;
@property (nonatomic, retain) UILabel *lblClubName;

@property (nonatomic, retain) UIView *btnAcceptContainerView;
@property (nonatomic, retain) AsyncImageView *imageViewAccept;
@property (nonatomic, retain) UILabel *lblAccept;
@property (nonatomic, retain) UIButton *btnAccept;

@property (nonatomic, retain) UIView *btnDeclineContainerView;
@property (nonatomic, retain) AsyncImageView *imageViewDecline;
@property (nonatomic, retain) UILabel *lblDecline;
@property (nonatomic, retain) UIButton *btnDecline;

@property (nonatomic, retain) UIView *separatorView;

- (id)initWithNewStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@end
