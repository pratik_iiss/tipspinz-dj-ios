//
//  ClubOwnerDjLogTableViewCell.h
//  TIPSPINZ DJ
//
//  Created by Pratik Gujarati on 03/06/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface ClubOwnerDjLogTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIView *mainContainer;

@property (nonatomic, retain) IBOutlet AsyncImageView *imageViewDjProfilePicture;
@property (nonatomic, retain) IBOutlet UILabel *lblDjName;
@property (nonatomic, retain) IBOutlet UILabel *lblClubName;

@property (nonatomic, retain) IBOutlet UILabel *lblOnlineTitle;
@property (nonatomic, retain) IBOutlet AsyncImageView *imageViewOnlineDate;
@property (nonatomic, retain) IBOutlet UILabel *lblOnlineDate;
@property (nonatomic, retain) IBOutlet AsyncImageView *imageViewOnlineTime;
@property (nonatomic, retain) IBOutlet UILabel *lblOnlineTime;

@property (nonatomic, retain) IBOutlet UILabel *lblOfflineTitle;
@property (nonatomic, retain) IBOutlet AsyncImageView *imageViewOfflineDate;
@property (nonatomic, retain) IBOutlet UILabel *lblOfflineDate;
@property (nonatomic, retain) IBOutlet AsyncImageView *imageViewOfflineTime;
@property (nonatomic, retain) IBOutlet UILabel *lblOfflineTime;

@property (nonatomic, retain) IBOutlet UIView *separatorView;

@end
