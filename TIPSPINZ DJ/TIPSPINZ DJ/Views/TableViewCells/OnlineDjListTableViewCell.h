//
//  OnlineDjListTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 19/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface OnlineDjListTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewDjProfilePicture;
@property (nonatomic, retain) UILabel *lblDjName;
@property (nonatomic, retain) UILabel *lblClubName;

@property (nonatomic, retain) AsyncImageView *imageViewDjOnline;

@property (nonatomic, retain) UIView *separatorView;

@end
