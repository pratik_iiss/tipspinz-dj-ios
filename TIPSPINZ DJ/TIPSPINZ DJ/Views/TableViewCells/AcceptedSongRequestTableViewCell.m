//
//  AcceptedSongRequestTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "AcceptedSongRequestTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 150

@implementation AcceptedSongRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD SONG ICON IMAGE VIEW INTO BOX =======//
        self.imageViewSongIcon = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.imageViewSongIcon.image = [UIImage imageNamed:@"music_icon.png"];
        self.imageViewSongIcon.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewSongIcon.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewSongIcon];
        
        //======= ADD LABEL SONG NAME INTO BOX =======//
        self.lblSongName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10), 15, self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 10, 15)];
        self.lblSongName.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblSongName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblSongName.textAlignment = NSTextAlignmentLeft;
        self.lblSongName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongName];
        
        //======= ADD LABEL SONG ARTIST NAME INTO BOX =======//
        self.lblSongArtistName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10), (self.lblSongName.frame.origin.y + self.lblSongName.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 10, 12)];
        self.lblSongArtistName.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblSongArtistName.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblSongArtistName.textAlignment = NSTextAlignmentLeft;
        self.lblSongArtistName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongArtistName];
        
        //======= ADD LABEL SONG REQUEST TIME INTO BOX =======//
        self.lblSongRequestTime = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10), (self.lblSongArtistName.frame.origin.y + self.lblSongArtistName.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewSongIcon.frame.origin.x + self.imageViewSongIcon.frame.size.width + 10) - 90, 12)];
        self.lblSongRequestTime.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblSongRequestTime.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblSongRequestTime.textAlignment = NSTextAlignmentLeft;
        self.lblSongRequestTime.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongRequestTime];
        
        //======= ADD LABEL SONG REQUEST STATUS INTO BOX =======//
        self.lblSongRequestStatus = [[UILabel alloc] initWithFrame:CGRectMake((self.lblSongRequestTime.frame.origin.x + self.lblSongRequestTime.frame.size.width + 10), self.lblSongRequestTime.frame.origin.y, self.mainContainer.frame.size.width - (self.lblSongRequestTime.frame.origin.x + self.lblSongRequestTime.frame.size.width + 10) - 10, 15)];
        self.lblSongRequestStatus.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
        self.lblSongRequestStatus.textColor = [MySingleton sharedManager].themeGlobalRequestAcceptedOrangeColor;
        self.lblSongRequestStatus.textAlignment = NSTextAlignmentRight;
        self.lblSongRequestStatus.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongRequestStatus];
        
        //======= ADD LABEL SONG REQUEST ACCEPTANCE TYPE INTO BOX =======//
        self.lblSongRequestAcceptanceType = [[UILabel alloc] initWithFrame:CGRectMake(15, self.lblSongRequestStatus.frame.origin.y + self.lblSongRequestStatus.frame.size.height + 5, self.mainContainer.frame.size.width - 30, 20)];
        if([MySingleton sharedManager].screenHeight == 480 || [MySingleton sharedManager].screenHeight == 568)
        {
            self.lblSongRequestAcceptanceType.font = [MySingleton sharedManager].themeFontTenSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight == 667 || [MySingleton sharedManager].screenHeight == 812)
        {
            self.lblSongRequestAcceptanceType.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
        }
        else if([MySingleton sharedManager].screenHeight >= 736)
        {
            self.lblSongRequestAcceptanceType.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
        }
        self.lblSongRequestAcceptanceType.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblSongRequestAcceptanceType.textAlignment = NSTextAlignmentCenter;
        self.lblSongRequestAcceptanceType.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblSongRequestAcceptanceType];
        
        //======= ADD BOTTOM CONTAINER VIEW =======//
        self.bottomContainer = [[UIView alloc]initWithFrame:CGRectMake(10, (self.lblSongRequestAcceptanceType.frame.origin.y + self.lblSongRequestAcceptanceType.frame.size.height + 5), (self.mainContainer.frame.size.width - 20), 30)];
        self.bottomContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD SHARE ON SOCIAL MEDIA CONTAINER VIEW =======//
        self.shareOnSocialMediaContainer = [[UIView alloc]initWithFrame:CGRectMake(5, 0, (self.bottomContainer.frame.size.width - 15)/2, self.bottomContainer.frame.size.height)];
        self.shareOnSocialMediaContainer.backgroundColor =  [UIColor clearColor];
        self.shareOnSocialMediaContainer.layer.masksToBounds = true;
        self.shareOnSocialMediaContainer.layer.cornerRadius = 3.0f;
        self.shareOnSocialMediaContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkBlueColor.CGColor;
        self.shareOnSocialMediaContainer.layer.borderWidth = 1.0f;
        
        //======= ADD LABEL SHARE ON SOCIAL MEDIA CONTAINER BOX =======//
        self.lblShareOnSocialMedia = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, (self.shareOnSocialMediaContainer.frame.size.width - 45), self.shareOnSocialMediaContainer.frame.size.height)];
        self.lblShareOnSocialMedia.font = [MySingleton sharedManager].themeFontSevenSizeBold;
        self.lblShareOnSocialMedia.textColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
        self.lblShareOnSocialMedia.textAlignment = NSTextAlignmentCenter;
        self.lblShareOnSocialMedia.layer.masksToBounds = YES;
        self.lblShareOnSocialMedia.text = @"SHARE ON SOCIAL MEDIA";
        [self.shareOnSocialMediaContainer addSubview:self.lblShareOnSocialMedia];
        
        //======= ADD SHARE ON SOCIAL MEDIA VIEW INTO CONTAINER BOX =======//
        self.imageViewShareOnSocialMedia = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.lblShareOnSocialMedia.frame.origin.x + self.lblShareOnSocialMedia.frame.size.width + 10), 7.5, 15, 15)];
        self.imageViewShareOnSocialMedia.image = [UIImage imageNamed:@"share_on_social_media.png"];
        self.imageViewShareOnSocialMedia.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewShareOnSocialMedia.layer.masksToBounds = YES;
        [self.shareOnSocialMediaContainer addSubview:self.imageViewShareOnSocialMedia];
        
        //======== ADD BUTTON SHARE ON SOCIAL MEDIA INTO CONTAINER BOX ========//
        self.btnShareOnSocialMedia = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.shareOnSocialMediaContainer.frame.size.width, self.shareOnSocialMediaContainer.frame.size.height)];
        [self.shareOnSocialMediaContainer addSubview:self.btnShareOnSocialMedia];
        
        [self.bottomContainer addSubview:self.shareOnSocialMediaContainer];
        
        //======= ADD PLAYED CONTAINER VIEW =======//
        self.playedContainer = [[UIView alloc]initWithFrame:CGRectMake((self.shareOnSocialMediaContainer.frame.origin.x + self.shareOnSocialMediaContainer.frame.size.width + 5), 0, (self.bottomContainer.frame.size.width - 15)/2, self.bottomContainer.frame.size.height)];
        self.playedContainer.backgroundColor =  [UIColor clearColor];
        self.playedContainer.layer.masksToBounds = true;
        self.playedContainer.layer.cornerRadius = 3.0f;
        self.playedContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkBlueColor.CGColor;
        self.playedContainer.layer.borderWidth = 1.0f;
        
        //======= ADD LABEL PLAYED INTO CONTAINER BOX =======//
        self.lblPlayed = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, (self.playedContainer.frame.size.width - 45), self.playedContainer.frame.size.height)];
        self.lblPlayed.font = [MySingleton sharedManager].themeFontSevenSizeBold;
        self.lblPlayed.textColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
        self.lblPlayed.textAlignment = NSTextAlignmentCenter;
        self.lblPlayed.layer.masksToBounds = YES;
        self.lblPlayed.text = @"PLAYED";
        [self.playedContainer addSubview:self.lblPlayed];
        
        //======= ADD PLAYED IMAGEVIEW VIEW INTO CONTAINER BOX =======//
        self.imageViewPlayed = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.lblPlayed.frame.origin.x + self.lblPlayed.frame.size.width + 10), 7.5, 15, 15)];
        self.imageViewPlayed.image = [UIImage imageNamed:@"played.png"];
        self.imageViewPlayed.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewPlayed.layer.masksToBounds = YES;
        [self.playedContainer addSubview:self.imageViewPlayed];
        
        //======== ADD BUTTON PLAYED INTO CONTAINER BOX ========//
        self.btnPlayed = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.playedContainer.frame.size.width, self.playedContainer.frame.size.height)];
        [self.playedContainer addSubview:self.btnPlayed];
        
        [self.bottomContainer addSubview:self.playedContainer];
        
        [self.mainContainer addSubview:self.bottomContainer];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
