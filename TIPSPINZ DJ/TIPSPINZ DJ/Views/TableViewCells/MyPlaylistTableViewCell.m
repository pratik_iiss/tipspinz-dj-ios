//
//  MyPlaylistTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 10/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "MyPlaylistTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 80

@implementation MyPlaylistTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD PLAYLIST ICON IMAGE VIEW INTO BOX =======//
        self.imageViewPlaylistIcon = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.imageViewPlaylistIcon.image = [UIImage imageNamed:@"music_icon.png"];
        self.imageViewPlaylistIcon.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewPlaylistIcon.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewPlaylistIcon];
        
        //======= ADD LABEL PLAYLIST NAME INTO BOX =======//
        self.lblPlaylistName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewPlaylistIcon.frame.origin.x + self.imageViewPlaylistIcon.frame.size.width + 10), 15, self.mainContainer.frame.size.width - (self.imageViewPlaylistIcon.frame.origin.x + self.imageViewPlaylistIcon.frame.size.width + 10) - 40, 50)];
        self.lblPlaylistName.font = [MySingleton sharedManager].themeFontFourteenSizeMedium;
        self.lblPlaylistName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblPlaylistName.textAlignment = NSTextAlignmentLeft;
        self.lblPlaylistName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblPlaylistName];
        
        //======= ADD RIGHT ARROW IMAGE VIEW INTO BOX =======//
        self.imageViewRightArrow = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.lblPlaylistName.frame.origin.x + self.lblPlaylistName.frame.size.width + 5), 30, 20, 20)];
        self.imageViewRightArrow.image = [UIImage imageNamed:@"arrow_right_grey.png"];
        self.imageViewRightArrow.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewRightArrow.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewRightArrow];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (id)initWithEditingStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD PLAYLIST ICON IMAGE VIEW INTO BOX =======//
        self.imageViewPlaylistIcon = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.imageViewPlaylistIcon.image = [UIImage imageNamed:@"music_icon.png"];
        self.imageViewPlaylistIcon.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewPlaylistIcon.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewPlaylistIcon];
        
        //======= ADD LABEL PLAYLIST NAME INTO BOX =======//
        self.lblPlaylistName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewPlaylistIcon.frame.origin.x + self.imageViewPlaylistIcon.frame.size.width + 10), 15, self.mainContainer.frame.size.width - (self.imageViewPlaylistIcon.frame.origin.x + self.imageViewPlaylistIcon.frame.size.width + 10) - 40, 50)];
        self.lblPlaylistName.font = [MySingleton sharedManager].themeFontFourteenSizeMedium;
        self.lblPlaylistName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblPlaylistName.textAlignment = NSTextAlignmentLeft;
        self.lblPlaylistName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblPlaylistName];
        
        //======= ADD CHECKBOX IMAGE VIEW INTO BOX =======//
        self.imageViewCheckbox = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.mainContainer.frame.size.width - 30), (self.mainContainer.frame.size.height - 20)/2, 20, 20)];
        self.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_unchecked.png"];
        self.imageViewCheckbox.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewCheckbox.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewCheckbox];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
