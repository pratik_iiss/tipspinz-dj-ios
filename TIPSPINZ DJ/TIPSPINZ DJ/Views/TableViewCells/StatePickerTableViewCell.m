//
//  StatePickerTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 29/03/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "StatePickerTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 60

@implementation StatePickerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].floatStatePickerTableViewWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD CLUB ADDRESS PIN IMAGE VIEW INTO BOX =======//
        self.imageViewLocationPin = [[AsyncImageView alloc]initWithFrame:CGRectMake(10, 20, 20, 20)];
        self.imageViewLocationPin.image = [UIImage imageNamed:@"location_pin_black.png"];
        self.imageViewLocationPin.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewLocationPin.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewLocationPin];
        
        //======= ADD LABEL STATE NAME INTO BOX =======//
        self.lblStateName = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewLocationPin.frame.origin.x + self.imageViewLocationPin.frame.size.width + 10), 10, (self.mainContainer.frame.size.width - (self.imageViewLocationPin.frame.origin.x + self.imageViewLocationPin.frame.size.width + 10))-10, 40)];
        self.lblStateName.font = [MySingleton sharedManager].themeFontFourteenSizeRegular;
        self.lblStateName.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblStateName.textAlignment = NSTextAlignmentLeft;
        self.lblStateName.numberOfLines = 1;
        self.lblStateName.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblStateName];
        
        //======= ADD CHECKBOX IMAGE VIEW INTO BOX =======//
        self.imageViewCheckbox = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.mainContainer.frame.size.width - 30), (self.mainContainer.frame.size.height - 20)/2, 20, 20)];
        self.imageViewCheckbox.image = [UIImage imageNamed:@"checkbox_checked.png"];
        self.imageViewCheckbox.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewCheckbox.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewCheckbox];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(0, self.mainContainer.frame.size.height-1, cellWidth, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
