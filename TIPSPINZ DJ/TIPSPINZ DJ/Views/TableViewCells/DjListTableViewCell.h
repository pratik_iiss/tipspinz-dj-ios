//
//  DjListTableViewCell.h
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface DjListTableViewCell : UITableViewCell

@property (nonatomic, retain) UIView *mainContainer;

@property (nonatomic, retain) AsyncImageView *imageViewDjProfilePicture;
@property (nonatomic, retain) UILabel *lblDjName;
@property (nonatomic, retain) UILabel *lblClubName;

@property (nonatomic, retain) UIView *separatorView;

@end
