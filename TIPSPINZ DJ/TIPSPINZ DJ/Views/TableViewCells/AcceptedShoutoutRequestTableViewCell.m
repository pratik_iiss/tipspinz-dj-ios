//
//  AcceptedShoutoutRequestTableViewCell.m
//  TIPSPINZ
//
//  Created by Pratik Gujarati on 12/04/17.
//  Copyright © 2017 innovativeiteration. All rights reserved.
//

#import "AcceptedShoutoutRequestTableViewCell.h"
#import "MySingleton.h"

#define CellHeight 150

@implementation AcceptedShoutoutRequestTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:NO animated:animated];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [self setSelectedBackgroundView:bgColorView];
    
    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        float cellHeight = CellHeight;
        float cellWidth = [MySingleton sharedManager].screenWidth;
        
        //======= ADD MAIN CONTAINER VIEW =======//
        self.mainContainer = [[UIView alloc]initWithFrame:CGRectMake(0, 0, cellWidth, cellHeight)];
        self.mainContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD SHOUTOUT ICON IMAGE VIEW INTO BOX =======//
        self.imageViewShoutoutIcon = [[AsyncImageView alloc]initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.imageViewShoutoutIcon.image = [UIImage imageNamed:@"shoutout_request.png"];
        self.imageViewShoutoutIcon.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewShoutoutIcon.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.imageViewShoutoutIcon];
        
        //======= ADD LABEL SHOUTOUT OCCASION INTO BOX =======//
        self.lblShoutoutOccasion = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), 15, self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 10, 15)];
        self.lblShoutoutOccasion.font = [MySingleton sharedManager].themeFontTwelveSizeMedium;
        self.lblShoutoutOccasion.textColor = [MySingleton sharedManager].themeGlobalBlackColor;
        self.lblShoutoutOccasion.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutOccasion.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutOccasion];
        
        //======= ADD LABEL SHOUTOUT FOR WHOM INTO BOX =======//
        self.lblShoutoutForWhom = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), (self.lblShoutoutOccasion.frame.origin.y + self.lblShoutoutOccasion.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 10, 12)];
        self.lblShoutoutForWhom.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblShoutoutForWhom.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutForWhom.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutForWhom.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutForWhom];
        
        //======= ADD LABEL SHOUTOUT NOTE COMMENTS INTO BOX =======//
        self.lblShoutoutNoteComments = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), (self.lblShoutoutForWhom.frame.origin.y + self.lblShoutoutForWhom.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 10, 24)];
        self.lblShoutoutNoteComments.numberOfLines = 0;
        self.lblShoutoutNoteComments.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblShoutoutNoteComments.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutNoteComments.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutNoteComments.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutNoteComments];
        
        //======= ADD LABEL SHOUTOUT REQUEST TIME INTO BOX =======//
        self.lblShoutoutRequestTime = [[UILabel alloc] initWithFrame:CGRectMake((self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10), (self.lblShoutoutNoteComments.frame.origin.y + self.lblShoutoutNoteComments.frame.size.height + 5), self.mainContainer.frame.size.width - (self.imageViewShoutoutIcon.frame.origin.x + self.imageViewShoutoutIcon.frame.size.width + 10) - 90, 12)];
        self.lblShoutoutRequestTime.font = [MySingleton sharedManager].themeFontTenSizeRegular;
        self.lblShoutoutRequestTime.textColor = [MySingleton sharedManager].themeGlobalDarkGreyColor;
        self.lblShoutoutRequestTime.textAlignment = NSTextAlignmentLeft;
        self.lblShoutoutRequestTime.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutRequestTime];
        
        //======= ADD LABEL SHOUTOUT REQUEST STATUS INTO BOX =======//
        self.lblShoutoutRequestStatus = [[UILabel alloc] initWithFrame:CGRectMake((self.lblShoutoutRequestTime.frame.origin.x + self.lblShoutoutRequestTime.frame.size.width + 10), self.lblShoutoutRequestTime.frame.origin.y, self.mainContainer.frame.size.width - (self.lblShoutoutRequestTime.frame.origin.x + self.lblShoutoutRequestTime.frame.size.width + 10) - 10, 15)];
        self.lblShoutoutRequestStatus.font = [MySingleton sharedManager].themeFontTwelveSizeBold;
        self.lblShoutoutRequestStatus.textColor = [MySingleton sharedManager].themeGlobalRequestAcceptedOrangeColor;
        self.lblShoutoutRequestStatus.textAlignment = NSTextAlignmentRight;
        self.lblShoutoutRequestStatus.layer.masksToBounds = YES;
        [self.mainContainer addSubview:self.lblShoutoutRequestStatus];
        
        //======= ADD BOTTOM CONTAINER VIEW =======//
        self.bottomContainer = [[UIView alloc]initWithFrame:CGRectMake(10, (self.lblShoutoutRequestStatus.frame.origin.y + self.lblShoutoutRequestStatus.frame.size.height + 10), (self.mainContainer.frame.size.width - 20), 30)];
        self.bottomContainer.backgroundColor =  [UIColor clearColor];
        
        //======= ADD SHARE ON SOCIAL MEDIA CONTAINER VIEW =======//
        self.shareOnSocialMediaContainer = [[UIView alloc]initWithFrame:CGRectMake(5, 0, (self.bottomContainer.frame.size.width - 15)/2, self.bottomContainer.frame.size.height)];
        self.shareOnSocialMediaContainer.backgroundColor =  [UIColor clearColor];
        self.shareOnSocialMediaContainer.layer.masksToBounds = true;
        self.shareOnSocialMediaContainer.layer.cornerRadius = 3.0f;
        self.shareOnSocialMediaContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkBlueColor.CGColor;
        self.shareOnSocialMediaContainer.layer.borderWidth = 1.0f;
        
        //======= ADD LABEL SHARE ON SOCIAL MEDIA CONTAINER BOX =======//
        self.lblShareOnSocialMedia = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, (self.shareOnSocialMediaContainer.frame.size.width - 45), self.shareOnSocialMediaContainer.frame.size.height)];
        self.lblShareOnSocialMedia.font = [MySingleton sharedManager].themeFontSevenSizeBold;
        self.lblShareOnSocialMedia.textColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
        self.lblShareOnSocialMedia.textAlignment = NSTextAlignmentCenter;
        self.lblShareOnSocialMedia.layer.masksToBounds = YES;
        self.lblShareOnSocialMedia.text = @"SHARE ON SOCIAL MEDIA";
        [self.shareOnSocialMediaContainer addSubview:self.lblShareOnSocialMedia];
        
        //======= ADD SHARE ON SOCIAL MEDIA VIEW INTO CONTAINER BOX =======//
        self.imageViewShareOnSocialMedia = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.lblShareOnSocialMedia.frame.origin.x + self.lblShareOnSocialMedia.frame.size.width + 10), 7.5, 15, 15)];
        self.imageViewShareOnSocialMedia.image = [UIImage imageNamed:@"share_on_social_media.png"];
        self.imageViewShareOnSocialMedia.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewShareOnSocialMedia.layer.masksToBounds = YES;
        [self.shareOnSocialMediaContainer addSubview:self.imageViewShareOnSocialMedia];
        
        //======== ADD BUTTON SHARE ON SOCIAL MEDIA INTO CONTAINER BOX ========//
        self.btnShareOnSocialMedia = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.shareOnSocialMediaContainer.frame.size.width, self.shareOnSocialMediaContainer.frame.size.height)];
        [self.shareOnSocialMediaContainer addSubview:self.btnShareOnSocialMedia];
        
        [self.bottomContainer addSubview:self.shareOnSocialMediaContainer];
        
        //======= ADD PLAYED CONTAINER VIEW =======//
        self.playedContainer = [[UIView alloc]initWithFrame:CGRectMake((self.shareOnSocialMediaContainer.frame.origin.x + self.shareOnSocialMediaContainer.frame.size.width + 5), 0, (self.bottomContainer.frame.size.width - 15)/2, self.bottomContainer.frame.size.height)];
        self.playedContainer.backgroundColor =  [UIColor clearColor];
        self.playedContainer.layer.masksToBounds = true;
        self.playedContainer.layer.cornerRadius = 3.0f;
        self.playedContainer.layer.borderColor = [MySingleton sharedManager].themeGlobalDarkBlueColor.CGColor;
        self.playedContainer.layer.borderWidth = 1.0f;
        
        //======= ADD LABEL PLAYED INTO CONTAINER BOX =======//
        self.lblPlayed = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, (self.playedContainer.frame.size.width - 45), self.playedContainer.frame.size.height)];
        self.lblPlayed.font = [MySingleton sharedManager].themeFontSevenSizeBold;
        self.lblPlayed.textColor = [MySingleton sharedManager].themeGlobalDarkBlueColor;
        self.lblPlayed.textAlignment = NSTextAlignmentCenter;
        self.lblPlayed.layer.masksToBounds = YES;
        self.lblPlayed.text = @"PLAYED";
        [self.playedContainer addSubview:self.lblPlayed];
        
        //======= ADD PLAYED IMAGEVIEW VIEW INTO CONTAINER BOX =======//
        self.imageViewPlayed = [[AsyncImageView alloc]initWithFrame:CGRectMake((self.lblPlayed.frame.origin.x + self.lblPlayed.frame.size.width + 10), 7.5, 15, 15)];
        self.imageViewPlayed.image = [UIImage imageNamed:@"played.png"];
        self.imageViewPlayed.contentMode = UIViewContentModeScaleAspectFit;
        self.imageViewPlayed.layer.masksToBounds = YES;
        [self.playedContainer addSubview:self.imageViewPlayed];
        
        //======== ADD BUTTON PLAYED INTO CONTAINER BOX ========//
        self.btnPlayed = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.playedContainer.frame.size.width, self.playedContainer.frame.size.height)];
        [self.playedContainer addSubview:self.btnPlayed];
        
        [self.bottomContainer addSubview:self.playedContainer];
        
        [self.mainContainer addSubview:self.bottomContainer];
        
        //========ADD SEPERATOR INTO BOX ========//
        self.separatorView =[[UIView alloc]initWithFrame:CGRectMake(20, self.mainContainer.frame.size.height-1, cellWidth - 40, 0.5)];
        self.separatorView.backgroundColor = [MySingleton sharedManager].themeGlobalSeperatorGreyColor;
        [self.mainContainer addSubview:self.separatorView];
        
        [self addSubview:self.mainContainer];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

@end
